if [ "stage" = "$1" ]
  then POSTFIX="-alpha"
  else POSTFIX="$"
fi

git log -n 1  --pretty="%s" | grep -Poq "(?<=chore\(release\): )\d+.\d+.\d+$POSTFIX" && echo '--skip.bump --skip.commit --skip.changelog'

