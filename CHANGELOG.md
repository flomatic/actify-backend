# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.68-alpha.29](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.68-alpha.26...@actify/socket-gateway@0.0.68-alpha.29) (2021-01-20)


### Features

* 🎸 add an endpoint to return user segment count ([5e07d15](https://git.alibaba.ir///commit/5e07d15d1748b5d4026f475b2468e339bb2221a1))
* 🎸 add journey exit block ([aa40363](https://git.alibaba.ir///commit/aa403630f5a4b20593cde13a4d9f89ed2d8f0407))
* 🎸 add segment count calculation time ([e732316](https://git.alibaba.ir///commit/e73231622470d16b0fe4b259ce241c6b1bc6be6b))
* 🎸 endpoint to return users in a segmetn ([0eeaa26](https://git.alibaba.ir///commit/0eeaa2654de3b16bdfac29e7390670796764d061))
* 🎸 merge the two segment modules in api and processor ([a8a9145](https://git.alibaba.ir///commit/a8a914590f6eb4bae458624443dc49c7d9beff26))


### Bug Fixes

* 🐛 fix OffilineCheckProcess incorrect import ([76a8a1d](https://git.alibaba.ir///commit/76a8a1d20bc239e2c9f861a25fdec5f72b7ef066))
* 🐛 fix rate limit ([a216b4a](https://git.alibaba.ir///commit/a216b4aa54c1269ced9c1780b9082ccaf1b59f4a))

### [0.0.68-alpha.28](https://git.alibaba.ir///compare/@actify/api@0.0.68-alpha.26...@actify/api@0.0.68-alpha.28) (2021-01-20)


### Features

* 🎸 add an endpoint to return user segment count ([5e07d15](https://git.alibaba.ir///commit/5e07d15d1748b5d4026f475b2468e339bb2221a1))
* 🎸 add journey exit block ([aa40363](https://git.alibaba.ir///commit/aa403630f5a4b20593cde13a4d9f89ed2d8f0407))
* 🎸 add segment count calculation time ([e732316](https://git.alibaba.ir///commit/e73231622470d16b0fe4b259ce241c6b1bc6be6b))
* 🎸 endpoint to return users in a segmetn ([0eeaa26](https://git.alibaba.ir///commit/0eeaa2654de3b16bdfac29e7390670796764d061))
* 🎸 merge the two segment modules in api and processor ([a8a9145](https://git.alibaba.ir///commit/a8a914590f6eb4bae458624443dc49c7d9beff26))


### Bug Fixes

* 🐛 fix OffilineCheckProcess incorrect import ([76a8a1d](https://git.alibaba.ir///commit/76a8a1d20bc239e2c9f861a25fdec5f72b7ef066))
* 🐛 fix rate limit ([a216b4a](https://git.alibaba.ir///commit/a216b4aa54c1269ced9c1780b9082ccaf1b59f4a))

### [0.0.68-alpha.27](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.26...@actify/main-processor@0.0.68-alpha.27) (2021-01-20)


### Features

* 🎸 add an endpoint to return user segment count ([5e07d15](https://git.alibaba.ir///commit/5e07d15d1748b5d4026f475b2468e339bb2221a1))
* 🎸 add journey exit block ([aa40363](https://git.alibaba.ir///commit/aa403630f5a4b20593cde13a4d9f89ed2d8f0407))
* 🎸 add segment count calculation time ([e732316](https://git.alibaba.ir///commit/e73231622470d16b0fe4b259ce241c6b1bc6be6b))
* 🎸 endpoint to return users in a segmetn ([0eeaa26](https://git.alibaba.ir///commit/0eeaa2654de3b16bdfac29e7390670796764d061))
* 🎸 merge the two segment modules in api and processor ([a8a9145](https://git.alibaba.ir///commit/a8a914590f6eb4bae458624443dc49c7d9beff26))


### Bug Fixes

* 🐛 fix OffilineCheckProcess incorrect import ([76a8a1d](https://git.alibaba.ir///commit/76a8a1d20bc239e2c9f861a25fdec5f72b7ef066))
* 🐛 fix rate limit ([a216b4a](https://git.alibaba.ir///commit/a216b4aa54c1269ced9c1780b9082ccaf1b59f4a))

### [0.0.68-alpha.26](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.25...@actify/main-processor@0.0.68-alpha.26) (2021-01-16)


### Bug Fixes

* 🐛 bug fix ([dec2a71](https://git.alibaba.ir///commit/dec2a71094c6b34ae39cee7a061bde081055c35c))

### [0.0.68-alpha.25](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.24...@actify/main-processor@0.0.68-alpha.25) (2021-01-16)


### Bug Fixes

* 🐛 bug fix ([d218b36](https://git.alibaba.ir///commit/d218b366ab8a19799cbc586f782fe789cf512c4f))

### [0.0.68-alpha.24](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.23...@actify/main-processor@0.0.68-alpha.24) (2021-01-16)


### Features

* 🎸 add from, text and reply_to to emails ([acecfca](https://git.alibaba.ir///commit/acecfca362c0de6954580a37a4fdde21b38e9f45))


### Bug Fixes

* 🐛 journey bug fix (event) ([728870a](https://git.alibaba.ir///commit/728870a8c967ef0cc04e843377829a36e9f73176))

### [0.0.68-alpha.23](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.22...@actify/main-processor@0.0.68-alpha.23) (2021-01-13)


### Bug Fixes

* 🐛 journey wait for time fix ([8e4a169](https://git.alibaba.ir///commit/8e4a169ad3681774c93e528e4cd65db9874b5216))

### [0.0.68-alpha.22](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.21...@actify/main-processor@0.0.68-alpha.22) (2021-01-13)


### Bug Fixes

* 🐛 journey user interaction bug fix ([3aa3b4a](https://git.alibaba.ir///commit/3aa3b4a8b973f5a1281d06ef792377ff81926457))

### [0.0.68-alpha.21](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.18...@actify/main-processor@0.0.68-alpha.21) (2021-01-12)


### Bug Fixes

* 🐛 bug fix ([9203f2c](https://git.alibaba.ir///commit/9203f2c1b18d6d657f96ab632176c074c55e27a1))
* 🐛 emaile template variable replace ([2e83699](https://git.alibaba.ir///commit/2e83699d229c0f26b660a27ae30544e8330f5243))

### [0.0.68-alpha.20](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.68-alpha.17...@actify/socket-gateway@0.0.68-alpha.20) (2021-01-12)


### Bug Fixes

* 🐛 minor modification to journey validation ([20a1a18](https://git.alibaba.ir///commit/20a1a18a689b09dbd598135b5e1e7daaf60cda20))

### [0.0.68-alpha.19](https://git.alibaba.ir///compare/@actify/api@0.0.68-alpha.16...@actify/api@0.0.68-alpha.19) (2021-01-12)


### Bug Fixes

* 🐛 minor modification to journey validation ([20a1a18](https://git.alibaba.ir///commit/20a1a18a689b09dbd598135b5e1e7daaf60cda20))

### [0.0.68-alpha.18](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.15...@actify/main-processor@0.0.68-alpha.18) (2021-01-12)


### Bug Fixes

* 🐛 minor modification to journey validation ([20a1a18](https://git.alibaba.ir///commit/20a1a18a689b09dbd598135b5e1e7daaf60cda20))

### [0.0.68-alpha.17](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.68-alpha.14...@actify/socket-gateway@0.0.68-alpha.17) (2021-01-12)

### [0.0.68-alpha.16](https://git.alibaba.ir///compare/@actify/api@0.0.68-alpha.14...@actify/api@0.0.68-alpha.16) (2021-01-12)

### [0.0.68-alpha.15](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.14...@actify/main-processor@0.0.68-alpha.15) (2021-01-12)

### [0.0.68-alpha.14](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.13...@actify/main-processor@0.0.68-alpha.14) (2021-01-11)


### Bug Fixes

* 🐛 changed ping time ([d9a518a](https://git.alibaba.ir///commit/d9a518add0d48550bb365115a4e218080d56a837))
* 🐛 journey bug fix ([6e0606b](https://git.alibaba.ir///commit/6e0606b536bd8abca691169358548fcc4403ed1d))

### [0.0.68-alpha.13](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.12...@actify/main-processor@0.0.68-alpha.13) (2021-01-11)


### Bug Fixes

* 🐛 remove console.log ([62b2062](https://git.alibaba.ir///commit/62b2062737c56ab5a6a3c9fe87b990403160f6e6))

### [0.0.68-alpha.12](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.11...@actify/main-processor@0.0.68-alpha.12) (2021-01-11)


### Bug Fixes

* 🐛 bug fix ([20b9876](https://git.alibaba.ir///commit/20b987637110cf0e5e5106f0685ef373b6416055))
* 🐛 journey bug fix ([3a78523](https://git.alibaba.ir///commit/3a785233b8d494a14ca6792233d4c9aec57fd5c6))

### [0.0.68-alpha.11](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.8...@actify/main-processor@0.0.68-alpha.11) (2021-01-10)


### Bug Fixes

* 🐛 bug fix ([d345462](https://git.alibaba.ir///commit/d34546253ba9a27891d215ec5ea83c7e2dbf8631))

### [0.0.68-alpha.10](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.68-alpha.7...@actify/socket-gateway@0.0.68-alpha.10) (2021-01-10)

### [0.0.68-alpha.9](https://git.alibaba.ir///compare/@actify/api@0.0.68-alpha.7...@actify/api@0.0.68-alpha.9) (2021-01-10)

### [0.0.68-alpha.8](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.7...@actify/main-processor@0.0.68-alpha.8) (2021-01-10)

### [0.0.68-alpha.7](https://git.alibaba.ir///compare/@actify/main-processor@0.0.68-alpha.0...@actify/main-processor@0.0.68-alpha.7) (2021-01-10)


### Bug Fixes

* 🐛 fix get journeys validation ([68f38ba](https://git.alibaba.ir///commit/68f38bafcef03a7c9598e00782b2a299acfb0d71))
* 🐛 journey fix ([4951a50](https://git.alibaba.ir///commit/4951a50b1019808d84065a7d0ef6685089d89577))
* 🐛 support empty journey names ([c6e2671](https://git.alibaba.ir///commit/c6e2671767a2fbc7f5fb40609658c69ea4aaa627))

### [0.0.68-alpha.6](https://git.alibaba.ir///compare/@actify/api@0.0.68-alpha.5...@actify/api@0.0.68-alpha.6) (2021-01-06)


### Bug Fixes

* 🐛 support empty journey names ([c6e2671](https://git.alibaba.ir///commit/c6e2671767a2fbc7f5fb40609658c69ea4aaa627))

### [0.0.68-alpha.5](https://git.alibaba.ir///compare/@actify/api@0.0.68-alpha.4...@actify/api@0.0.68-alpha.5) (2021-01-06)

### [0.0.68-alpha.4](https://git.alibaba.ir///compare/@actify/api@0.0.68-alpha.3...@actify/api@0.0.68-alpha.4) (2021-01-06)

### [0.0.68-alpha.3](https://git.alibaba.ir///compare/@actify/api@0.0.68-alpha.2...@actify/api@0.0.68-alpha.3) (2021-01-06)


### Bug Fixes

* 🐛 fix get journeys validation ([68f38ba](https://git.alibaba.ir///commit/68f38bafcef03a7c9598e00782b2a299acfb0d71))

### [0.0.68-alpha.2](https://git.alibaba.ir///compare/@actify/api@0.0.68-alpha.1...@actify/api@0.0.68-alpha.2) (2021-01-06)

### [0.0.68-alpha.1](https://git.alibaba.ir///compare/@actify/api@0.0.68-alpha.0...@actify/api@0.0.68-alpha.1) (2021-01-06)

### [0.0.68-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.67...@actify/main-processor@0.0.68-alpha.0) (2021-01-05)


### Bug Fixes

* 🐛 bug fix ([fce2b92](https://git.alibaba.ir///commit/fce2b924ad629141a5e6fa83768988001b8669b2))

### [0.0.67](https://git.alibaba.ir///compare/@actify/main-processor@0.0.67-alpha.1...@actify/main-processor@0.0.67) (2021-01-05)


### Features

* 🎸 add logger ([a4d43ad](https://git.alibaba.ir///commit/a4d43ada73899c23b0d80500b45e10a13d007e86))


### Bug Fixes

* 🐛 bug fix ([eff38fe](https://git.alibaba.ir///commit/eff38fe2c81e769c2f392e5abb3ccf7cf0b598ea))

### [0.0.67-alpha.3](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.67-alpha.0...@actify/socket-gateway@0.0.67-alpha.3) (2021-01-05)


### Features

* 🎸 style swagger ([3f2c8fa](https://git.alibaba.ir///commit/3f2c8faf38c636af06089f811a1ca1552d75cb58))

### [0.0.67-alpha.2](https://git.alibaba.ir///compare/@actify/api@0.0.67-alpha.0...@actify/api@0.0.67-alpha.2) (2021-01-05)


### Features

* 🎸 style swagger ([3f2c8fa](https://git.alibaba.ir///commit/3f2c8faf38c636af06089f811a1ca1552d75cb58))

### [0.0.67-alpha.1](https://git.alibaba.ir///compare/@actify/main-processor@0.0.67-alpha.0...@actify/main-processor@0.0.67-alpha.1) (2021-01-05)


### Features

* 🎸 style swagger ([3f2c8fa](https://git.alibaba.ir///commit/3f2c8faf38c636af06089f811a1ca1552d75cb58))

### [0.0.67-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.66...@actify/main-processor@0.0.67-alpha.0) (2021-01-05)

### [0.0.66](https://git.alibaba.ir///compare/@actify/main-processor@0.0.65...@actify/main-processor@0.0.66) (2021-01-05)


### Bug Fixes

* 🐛 add silence of the lambs to email and push notificaitons ([efcd591](https://git.alibaba.ir///commit/efcd591e8d02797c2ffddd0ee3be54de7e0289c8))

### [0.0.65](https://git.alibaba.ir///compare/@actify/main-processor@0.0.64...@actify/main-processor@0.0.65) (2021-01-05)


### Bug Fixes

* 🐛 bug fix ([504bd5b](https://git.alibaba.ir///commit/504bd5bd6c89d0a34eb16bf8de69b2cc00a61e9b))

### [0.0.64](https://git.alibaba.ir///compare/@actify/main-processor@0.0.63...@actify/main-processor@0.0.64) (2021-01-05)

### [0.0.61-alpha.14](https://git.alibaba.ir///compare/@actify/main-processor@0.0.61-alpha.10...@actify/main-processor@0.0.61-alpha.14) (2021-01-04)


### Features

* 🎸 limit journys to only one trigger ([aaff16f](https://git.alibaba.ir///commit/aaff16fa1db3bc2fc0093edef8fac2d429dd9145))

### [0.0.61-alpha.10](https://git.alibaba.ir///compare/@actify/main-processor@0.0.61-alpha.7...@actify/main-processor@0.0.61-alpha.10) (2021-01-03)

### [0.0.63](https://git.alibaba.ir///compare/@actify/main-processor@0.0.62...@actify/main-processor@0.0.63) (2021-01-05)


### Bug Fixes

* 🐛 bug fix ([83c12fc](https://git.alibaba.ir///commit/83c12fc628d4f73bf5cac220c803c1913ed6de29))

### [0.0.61](https://git.alibaba.ir///compare/@actify/main-processor@0.0.61-alpha.7...@actify/main-processor@0.0.61) (2021-01-05)
=======
### [0.0.61-alpha.16](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.61-alpha.12...@actify/socket-gateway@0.0.61-alpha.16) (2021-01-04)


### Features

* 🎸 limit journys to only one trigger ([aaff16f](https://git.alibaba.ir///commit/aaff16fa1db3bc2fc0093edef8fac2d429dd9145))

### [0.0.61-alpha.15](https://git.alibaba.ir///compare/@actify/api@0.0.61-alpha.13...@actify/api@0.0.61-alpha.15) (2021-01-04)

### [0.0.61-alpha.14](https://git.alibaba.ir///compare/@actify/main-processor@0.0.61-alpha.10...@actify/main-processor@0.0.61-alpha.14) (2021-01-04)


### Features

* 🎸 limit journys to only one trigger ([aaff16f](https://git.alibaba.ir///commit/aaff16fa1db3bc2fc0093edef8fac2d429dd9145))

### [0.0.61-alpha.13](https://git.alibaba.ir///compare/@actify/api@0.0.61-alpha.11...@actify/api@0.0.61-alpha.13) (2021-01-04)


### Features

* 🎸 limit journys to only one trigger ([aaff16f](https://git.alibaba.ir///commit/aaff16fa1db3bc2fc0093edef8fac2d429dd9145))

### [0.0.61-alpha.12](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.61-alpha.9...@actify/socket-gateway@0.0.61-alpha.12) (2021-01-03)
>>>>>>> 35e35a96779bcb98fa370e8d526928129eaaeead


### Features

* 🎸 journey basic implemention ([3d5acdf](https://git.alibaba.ir///commit/3d5acdf7985ab74588ae4f3e07dbc996d78c6254))
* 🎸 journey complete ([fb20a7c](https://git.alibaba.ir///commit/fb20a7cfd41a3b911b09b30cb0d3ebe97c413c46))
* 🎸 replace api call variables ([5972ce6](https://git.alibaba.ir///commit/5972ce6575c8929952605b2a722f51eefdbe9225))

<<<<<<< HEAD

### Bug Fixes

* 🐛 bug fix on production ([f21b088](https://git.alibaba.ir///commit/f21b0887ad4e14e61a6eaa4ec881fab902001ea6))
=======
### [0.0.61-alpha.11](https://git.alibaba.ir///compare/@actify/api@0.0.61-alpha.8...@actify/api@0.0.61-alpha.11) (2021-01-03)


### Features

* 🎸 journey basic implemention ([3d5acdf](https://git.alibaba.ir///commit/3d5acdf7985ab74588ae4f3e07dbc996d78c6254))
* 🎸 journey complete ([fb20a7c](https://git.alibaba.ir///commit/fb20a7cfd41a3b911b09b30cb0d3ebe97c413c46))
* 🎸 replace api call variables ([5972ce6](https://git.alibaba.ir///commit/5972ce6575c8929952605b2a722f51eefdbe9225))

### [0.0.61-alpha.10](https://git.alibaba.ir///compare/@actify/main-processor@0.0.61-alpha.7...@actify/main-processor@0.0.61-alpha.10) (2021-01-03)


### Features

* 🎸 journey basic implemention ([3d5acdf](https://git.alibaba.ir///commit/3d5acdf7985ab74588ae4f3e07dbc996d78c6254))
* 🎸 journey complete ([fb20a7c](https://git.alibaba.ir///commit/fb20a7cfd41a3b911b09b30cb0d3ebe97c413c46))
* 🎸 replace api call variables ([5972ce6](https://git.alibaba.ir///commit/5972ce6575c8929952605b2a722f51eefdbe9225))
>>>>>>> 35e35a96779bcb98fa370e8d526928129eaaeead

### [0.0.61-alpha.9](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.61-alpha.5...@actify/socket-gateway@0.0.61-alpha.9) (2020-12-30)


### Features

* 🎸 support endpoint versions ([9d5eadd](https://git.alibaba.ir///commit/9d5eadd898fd700e901d4efe3f71b7f49afe6a33))


### Bug Fixes

* 🐛 remove journey template module from processor ([41a19a2](https://git.alibaba.ir///commit/41a19a202cecd488a52389e57a4ce87218a7f5e9))

### [0.0.61-alpha.8](https://git.alibaba.ir///compare/@actify/api@0.0.61-alpha.4...@actify/api@0.0.61-alpha.8) (2020-12-30)


### Features

* 🎸 support endpoint versions ([9d5eadd](https://git.alibaba.ir///commit/9d5eadd898fd700e901d4efe3f71b7f49afe6a33))


### Bug Fixes

* 🐛 remove journey template module from processor ([41a19a2](https://git.alibaba.ir///commit/41a19a202cecd488a52389e57a4ce87218a7f5e9))

### [0.0.61-alpha.7](https://git.alibaba.ir///compare/@actify/main-processor@0.0.61-alpha.6...@actify/main-processor@0.0.61-alpha.7) (2020-12-30)


### Features

* 🎸 support endpoint versions ([9d5eadd](https://git.alibaba.ir///commit/9d5eadd898fd700e901d4efe3f71b7f49afe6a33))

### [0.0.61-alpha.6](https://git.alibaba.ir///compare/@actify/main-processor@0.0.61-alpha.3...@actify/main-processor@0.0.61-alpha.6) (2020-12-29)


### Bug Fixes

* 🐛 remove journey template module from processor ([41a19a2](https://git.alibaba.ir///commit/41a19a202cecd488a52389e57a4ce87218a7f5e9))

### [0.0.61-alpha.5](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.61-alpha.2...@actify/socket-gateway@0.0.61-alpha.5) (2020-12-29)

### [0.0.61-alpha.4](https://git.alibaba.ir///compare/@actify/api@0.0.61-alpha.1...@actify/api@0.0.61-alpha.4) (2020-12-29)

### [0.0.61-alpha.3](https://git.alibaba.ir///compare/@actify/main-processor@0.0.61-alpha.0...@actify/main-processor@0.0.61-alpha.3) (2020-12-29)

### [0.0.61-alpha.2](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.60...@actify/socket-gateway@0.0.61-alpha.2) (2020-12-29)


### Features

* 🎸 add tag crud funcs ([a97668f](https://git.alibaba.ir///commit/a97668f789a97339768531c36bdac4426d63818a))


### Bug Fixes

* 🐛 bug fix ([d82abd5](https://git.alibaba.ir///commit/d82abd52891eb543fbb4f787b303c575afdd5e9c))
* 🐛 refactory journey and remove unused blocks ([806433f](https://git.alibaba.ir///commit/806433fb757f86f331fa03dcf0bc8e3d3c36e3d9))

### [0.0.61-alpha.1](https://git.alibaba.ir///compare/@actify/api@0.0.59...@actify/api@0.0.61-alpha.1) (2020-12-29)


### Features

* 🎸 add tag crud funcs ([a97668f](https://git.alibaba.ir///commit/a97668f789a97339768531c36bdac4426d63818a))


### Bug Fixes

* 🐛 bug fix ([d82abd5](https://git.alibaba.ir///commit/d82abd52891eb543fbb4f787b303c575afdd5e9c))
* 🐛 refactory journey and remove unused blocks ([806433f](https://git.alibaba.ir///commit/806433fb757f86f331fa03dcf0bc8e3d3c36e3d9))

### [0.0.61-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.58...@actify/main-processor@0.0.61-alpha.0) (2020-12-29)


### Features

* 🎸 add tag crud funcs ([a97668f](https://git.alibaba.ir///commit/a97668f789a97339768531c36bdac4426d63818a))


### Bug Fixes

* 🐛 bug fix ([d82abd5](https://git.alibaba.ir///commit/d82abd52891eb543fbb4f787b303c575afdd5e9c))
* 🐛 refactory journey and remove unused blocks ([806433f](https://git.alibaba.ir///commit/806433fb757f86f331fa03dcf0bc8e3d3c36e3d9))

### [0.0.60](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.58-alpha.3...@actify/socket-gateway@0.0.60) (2020-12-27)

### [0.0.59](https://git.alibaba.ir///compare/@actify/api@0.0.58-alpha.2...@actify/api@0.0.59) (2020-12-27)

### [0.0.58](https://git.alibaba.ir///compare/@actify/main-processor@0.0.58-alpha.1...@actify/main-processor@0.0.58) (2020-12-27)

### [0.0.58-alpha.3](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.58-alpha.0...@actify/socket-gateway@0.0.58-alpha.3) (2020-12-27)


### Bug Fixes

* 🐛 fix a socketgateway build bug ([e6d7dac](https://git.alibaba.ir///commit/e6d7dac45ce1327f9f0790e1414fd0ec8e85662e))

### [0.0.58-alpha.2](https://git.alibaba.ir///compare/@actify/api@0.0.58-alpha.0...@actify/api@0.0.58-alpha.2) (2020-12-27)


### Bug Fixes

* 🐛 fix a socketgateway build bug ([e6d7dac](https://git.alibaba.ir///commit/e6d7dac45ce1327f9f0790e1414fd0ec8e85662e))

### [0.0.58-alpha.1](https://git.alibaba.ir///compare/@actify/main-processor@0.0.58-alpha.0...@actify/main-processor@0.0.58-alpha.1) (2020-12-27)


### Bug Fixes

* 🐛 fix a socketgateway build bug ([e6d7dac](https://git.alibaba.ir///commit/e6d7dac45ce1327f9f0790e1414fd0ec8e85662e))

### [0.0.58-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.57...@actify/main-processor@0.0.58-alpha.0) (2020-12-27)


### Bug Fixes

* 🐛 push notification bug fix ([c27242c](https://git.alibaba.ir///commit/c27242c95caabd0a40aee9a65f248a3227050231))

### [0.0.57](https://git.alibaba.ir///compare/@actify/main-processor@0.0.57-alpha.14...@actify/main-processor@0.0.57) (2020-12-26)

### [0.0.57-alpha.14](https://git.alibaba.ir///compare/@actify/main-processor@0.0.57-alpha.13...@actify/main-processor@0.0.57-alpha.14) (2020-12-26)


### Bug Fixes

* 🐛 bug fix ([fd3fc62](https://git.alibaba.ir///commit/fd3fc627415897cb484993cb5187a79e6e496526))

### [0.0.57-alpha.13](https://git.alibaba.ir///compare/@actify/main-processor@0.0.57-alpha.12...@actify/main-processor@0.0.57-alpha.13) (2020-12-26)


### Bug Fixes

* 🐛 bug fix ([5717f35](https://git.alibaba.ir///commit/5717f35232e88ec1c763c815a12d50c50fbde97a))

### [0.0.57-alpha.12](https://git.alibaba.ir///compare/@actify/main-processor@0.0.57-alpha.11...@actify/main-processor@0.0.57-alpha.12) (2020-12-26)


### Bug Fixes

* 🐛 bug fix ([c9f36cc](https://git.alibaba.ir///commit/c9f36ccb5e0be8464b16c0d9cdde747906e01d75))

### [0.0.57-alpha.11](https://git.alibaba.ir///compare/@actify/main-processor@0.0.55-alpha.1...@actify/main-processor@0.0.57-alpha.11) (2020-12-26)


### Bug Fixes

* 🐛 bug fix ([b4efe8c](https://git.alibaba.ir///commit/b4efe8cff3743c048c041cb085f5a00048884d30))

### [0.0.57-alpha.7](https://git.alibaba.ir///compare/@actify/main-processor@0.0.57-alpha.4...@actify/main-processor@0.0.57-alpha.7) (2020-12-26)

### [0.0.57-alpha.4](https://git.alibaba.ir///compare/@actify/main-processor@0.0.57-alpha.1...@actify/main-processor@0.0.57-alpha.4) (2020-12-26)

### [0.0.57-alpha.1](https://git.alibaba.ir///compare/@actify/main-processor@0.0.55-alpha.0...@actify/main-processor@0.0.57-alpha.1) (2020-12-23)


### Bug Fixes

* **api:** dockerfile ([2d25cc1](https://git.alibaba.ir///commit/2d25cc11621fbf06768ccccd8129121c05238f96))
* **socket:** missed a thing oops! ([64ce076](https://git.alibaba.ir///commit/64ce07671c941b33c9438396d453a3d508c16457))
* cli socket port conflict ([8cde0a8](https://git.alibaba.ir///commit/8cde0a87d8e6ee286c2b166b64e004c2808beab5))

### [0.0.57-alpha.10](https://git.alibaba.ir///compare/@actify/api@0.0.57-alpha.8...@actify/api@0.0.57-alpha.10) (2020-12-26)

### [0.0.57-alpha.9](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.57-alpha.6...@actify/socket-gateway@0.0.57-alpha.9) (2020-12-26)

### [0.0.57-alpha.8](https://git.alibaba.ir///compare/@actify/api@0.0.57-alpha.5...@actify/api@0.0.57-alpha.8) (2020-12-26)

### [0.0.57-alpha.7](https://git.alibaba.ir///compare/@actify/main-processor@0.0.57-alpha.4...@actify/main-processor@0.0.57-alpha.7) (2020-12-26)

### [0.0.57-alpha.6](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.57-alpha.3...@actify/socket-gateway@0.0.57-alpha.6) (2020-12-26)

### [0.0.57-alpha.5](https://git.alibaba.ir///compare/@actify/api@0.0.57-alpha.2...@actify/api@0.0.57-alpha.5) (2020-12-26)

### [0.0.57-alpha.4](https://git.alibaba.ir///compare/@actify/main-processor@0.0.57-alpha.1...@actify/main-processor@0.0.57-alpha.4) (2020-12-26)

### [0.0.57-alpha.3](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.55-alpha.0...@actify/socket-gateway@0.0.57-alpha.3) (2020-12-23)

### Bug Fixes

-   **api:** dockerfile ([2d25cc1](https://git.alibaba.ir///commit/2d25cc11621fbf06768ccccd8129121c05238f96))
-   **socket:** missed a thing oops! ([64ce076](https://git.alibaba.ir///commit/64ce07671c941b33c9438396d453a3d508c16457))
-   cli socket port conflict ([8cde0a8](https://git.alibaba.ir///commit/8cde0a87d8e6ee286c2b166b64e004c2808beab5))

### [0.0.57-alpha.2](https://git.alibaba.ir///compare/@actify/api@0.0.57-alpha.0...@actify/api@0.0.57-alpha.2) (2020-12-23)

### [0.0.57-alpha.1](https://git.alibaba.ir///compare/@actify/main-processor@0.0.55-alpha.0...@actify/main-processor@0.0.57-alpha.1) (2020-12-23)

### Bug Fixes

-   **api:** dockerfile ([2d25cc1](https://git.alibaba.ir///commit/2d25cc11621fbf06768ccccd8129121c05238f96))
-   **socket:** missed a thing oops! ([64ce076](https://git.alibaba.ir///commit/64ce07671c941b33c9438396d453a3d508c16457))
-   cli socket port conflict ([8cde0a8](https://git.alibaba.ir///commit/8cde0a87d8e6ee286c2b166b64e004c2808beab5))

### [0.0.57-alpha.0](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/api@0.0.56...@actify/api@0.0.57-alpha.0) (2020-12-23)

### [0.0.56](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/api@0.0.55...@actify/api@0.0.56) (2020-12-23)

### Bug Fixes

-   **socket:** missed a thing oops! ([64ce076](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/64ce07671c941b33c9438396d453a3d508c16457))

### [0.0.55](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/api@0.0.55-alpha.0...@actify/api@0.0.55) (2020-12-23)

### Bug Fixes

-   **api:** dockerfile ([2d25cc1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/2d25cc11621fbf06768ccccd8129121c05238f96))
-   cli socket port conflict ([8cde0a8](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/8cde0a87d8e6ee286c2b166b64e004c2808beab5))

### [0.0.55-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.54...@actify/main-processor@0.0.55-alpha.0) (2020-12-23)

### [0.0.54](https://git.alibaba.ir///compare/@actify/main-processor@0.0.54-alpha.0...@actify/main-processor@0.0.54) (2020-12-23)

### Bug Fixes

-   🐛 bug fix ([64b456f](https://git.alibaba.ir///commit/64b456fa40814a8bdb3ba588f8ead0ef91c9c00b))

### [0.0.54-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.53...@actify/main-processor@0.0.54-alpha.0) (2020-12-23)

### [0.0.53](https://git.alibaba.ir///compare/@actify/main-processor@0.0.53-alpha.4...@actify/main-processor@0.0.53) (2020-12-23)

### Bug Fixes

-   🐛 redis cache ([4aaa1a8](https://git.alibaba.ir///commit/4aaa1a8fd26b924c88983948cd50b8f82812ea62))

### [0.0.53-alpha.6](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.53-alpha.3...@actify/socket-gateway@0.0.53-alpha.6) (2020-12-21)

### [0.0.53-alpha.5](https://git.alibaba.ir///compare/@actify/api@0.0.53-alpha.2...@actify/api@0.0.53-alpha.5) (2020-12-21)

### [0.0.53-alpha.4](https://git.alibaba.ir///compare/@actify/main-processor@0.0.53-alpha.1...@actify/main-processor@0.0.53-alpha.4) (2020-12-21)

### [0.0.53-alpha.3](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.52...@actify/socket-gateway@0.0.53-alpha.3) (2020-12-21)

### Features

-   🎸 push_notification ([a491b80](https://git.alibaba.ir///commit/a491b80fdf01ccc174199c6450992b55b05e9850))

### Bug Fixes

-   🐛 bug fix ([3fabfb1](https://git.alibaba.ir///commit/3fabfb140b584648edd57759532182ad37ec7d2e))
-   🐛 changes on email service ([da6dd20](https://git.alibaba.ir///commit/da6dd209fa196f19480d9563a0bfd0c3a0d25cf7))
-   🐛 shadow_socks_job ([d83b8b4](https://git.alibaba.ir///commit/d83b8b4856c9b2ad4bae92d66c5bb104f3f9f74f))

### [0.0.53-alpha.2](https://git.alibaba.ir///compare/@actify/api@0.0.53-alpha.0...@actify/api@0.0.53-alpha.2) (2020-12-21)

### Bug Fixes

-   🐛 shadow_socks_job ([d83b8b4](https://git.alibaba.ir///commit/d83b8b4856c9b2ad4bae92d66c5bb104f3f9f74f))

### [0.0.53-alpha.1](https://git.alibaba.ir///compare/@actify/main-processor@0.0.52...@actify/main-processor@0.0.53-alpha.1) (2020-12-21)

### Features

-   🎸 push_notification ([a491b80](https://git.alibaba.ir///commit/a491b80fdf01ccc174199c6450992b55b05e9850))

### Bug Fixes

-   🐛 bug fix ([3fabfb1](https://git.alibaba.ir///commit/3fabfb140b584648edd57759532182ad37ec7d2e))
-   🐛 changes on email service ([da6dd20](https://git.alibaba.ir///commit/da6dd209fa196f19480d9563a0bfd0c3a0d25cf7))
-   🐛 shadow_socks_job ([d83b8b4](https://git.alibaba.ir///commit/d83b8b4856c9b2ad4bae92d66c5bb104f3f9f74f))

### [0.0.53-alpha.0](https://git.alibaba.ir///compare/@actify/api@0.0.52...@actify/api@0.0.53-alpha.0) (2020-12-20)

### Features

-   🎸 push_notification ([a491b80](https://git.alibaba.ir///commit/a491b80fdf01ccc174199c6450992b55b05e9850))

### Bug Fixes

-   🐛 bug fix ([3fabfb1](https://git.alibaba.ir///commit/3fabfb140b584648edd57759532182ad37ec7d2e))
-   🐛 changes on email service ([da6dd20](https://git.alibaba.ir///commit/da6dd209fa196f19480d9563a0bfd0c3a0d25cf7))

### [0.0.52](https://git.alibaba.ir///compare/@actify/main-processor@0.0.52-alpha.0...@actify/main-processor@0.0.52) (2020-12-19)

### [0.0.52-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.51...@actify/main-processor@0.0.52-alpha.0) (2020-12-19)

### Features

-   🎸 bug fix ([3bc1f3b](https://git.alibaba.ir///commit/3bc1f3b321ffd68b6ecbc973a41b511bf0c69973))

### [0.0.51](https://git.alibaba.ir///compare/@actify/main-processor@0.0.51-alpha.0...@actify/main-processor@0.0.51) (2020-12-19)

### [0.0.51-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.50...@actify/main-processor@0.0.51-alpha.0) (2020-12-19)

### Bug Fixes

-   🐛 bug fix ([136216f](https://git.alibaba.ir///commit/136216faced27b717a4c17e4cf651ac248f9c159))

### [0.0.43](https://git.alibaba.ir///compare/@actify/main-processor@0.0.43-alpha.2...@actify/main-processor@0.0.43) (2020-12-19)

### [0.0.43-alpha.2](https://git.alibaba.ir///compare/@actify/main-processor@0.0.43...@actify/main-processor@0.0.43-alpha.2) (2020-12-19)

### Bug Fixes

-   🐛 merge ([b4cf00d](https://git.alibaba.ir///commit/b4cf00ddfbc4be333c8b9884575837de59654fea))
-   🐛 replace jwt secrets ([54d788c](https://git.alibaba.ir///commit/54d788c7fe6952544ece0bad0e969c81ca550c28))

### [0.0.43-alpha.1](https://git.alibaba.ir///compare/@actify/api@0.0.43-alpha.0...@actify/api@0.0.43-alpha.1) (2020-12-16)

### Bug Fixes

-   🐛 replace jwt secrets ([54d788c](https://git.alibaba.ir///commit/54d788c7fe6952544ece0bad0e969c81ca550c28))

### [0.0.43-alpha.0](https://git.alibaba.ir///compare/@actify/api@0.0.42...@actify/api@0.0.43-alpha.0) (2020-12-15)

### [0.0.42](https://git.alibaba.ir///compare/@actify/main-processor@0.0.42-alpha.0...@actify/main-processor@0.0.42) (2020-12-15)

### [0.0.42-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.41...@actify/main-processor@0.0.42-alpha.0) (2020-12-15)

### Bug Fixes

-   🐛 bug fix ([24e1435](https://git.alibaba.ir///commit/24e14357e36be33a9c07a4f2101ff98a05e63ad4))

### [0.0.41](https://git.alibaba.ir///compare/@actify/main-processor@0.0.40...@actify/main-processor@0.0.41) (2020-12-15)

### Bug Fixes

-   🐛 bug fix ([a863dc0](https://git.alibaba.ir///commit/a863dc09f89396c1f6965d7acbb5397269dc1d80))

### [0.0.40](https://git.alibaba.ir///compare/@actify/main-processor@0.0.39...@actify/main-processor@0.0.40) (2020-12-15)

### Bug Fixes

-   🐛 bug fix ([d77df42](https://git.alibaba.ir///commit/d77df422a4d9dce47e46d636cfb2e5f341b7ef2a))
-   🐛 bug fix ([9f3fd54](https://git.alibaba.ir///commit/9f3fd54338c04cf63e3c117c2620fe5db082571b))

### [0.0.39](https://git.alibaba.ir///compare/@actify/main-processor@0.0.39-alpha.0...@actify/main-processor@0.0.39) (2020-12-15)

### [0.0.39-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.38...@actify/main-processor@0.0.39-alpha.0) (2020-12-15)

### Bug Fixes

-   🐛 bug fix ([adc7928](https://git.alibaba.ir///commit/adc7928608801bb7a9f9c8423b25bebff40a5325))

### [0.0.38](https://git.alibaba.ir///compare/@actify/main-processor@0.0.38-alpha.0...@actify/main-processor@0.0.38) (2020-12-14)

### [0.0.38-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.37...@actify/main-processor@0.0.38-alpha.0) (2020-12-14)

### Bug Fixes

-   🐛 bug fix ([fd66af5](https://git.alibaba.ir///commit/fd66af509354d02dd20232205d5ef83846fa2c11))

### [0.0.37](https://git.alibaba.ir///compare/@actify/main-processor@0.0.37-alpha.0...@actify/main-processor@0.0.37) (2020-12-14)

### [0.0.37-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.36...@actify/main-processor@0.0.37-alpha.0) (2020-12-14)

### Bug Fixes

-   🐛 bug fix ([506a59c](https://git.alibaba.ir///commit/506a59c2b3dc438007665b17888efca73aae7a3a))

### [0.0.36](https://git.alibaba.ir///compare/@actify/main-processor@0.0.36-alpha.0...@actify/main-processor@0.0.36) (2020-12-14)

### [0.0.36-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.35...@actify/main-processor@0.0.36-alpha.0) (2020-12-14)

### Bug Fixes

-   🐛 bug fix ([3d3c4b8](https://git.alibaba.ir///commit/3d3c4b8e7d13908ffc05f5d3acb1c28a23416106))

### [0.0.35](https://git.alibaba.ir///compare/@actify/main-processor@0.0.35-alpha.0...@actify/main-processor@0.0.35) (2020-12-14)

### [0.0.35-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.34...@actify/main-processor@0.0.35-alpha.0) (2020-12-14)

### Features

-   🎸 bug fix ([e810e34](https://git.alibaba.ir///commit/e810e346918c8111505e26b63d05f34aaddb4fe0))

### [0.0.34](https://git.alibaba.ir///compare/@actify/main-processor@0.0.33...@actify/main-processor@0.0.34) (2020-12-14)

### Bug Fixes

-   🐛 bug fix ([7616e60](https://git.alibaba.ir///commit/7616e602831e079ffaf7962d980bae4a9343cfa8))

### [0.0.33](https://git.alibaba.ir///compare/@actify/main-processor@0.0.33-alpha.1...@actify/main-processor@0.0.33) (2020-12-14)

### [0.0.33-alpha.1](https://git.alibaba.ir///compare/@actify/main-processor@0.0.33-alpha.0...@actify/main-processor@0.0.33-alpha.1) (2020-12-14)

### Bug Fixes

-   🐛 bug fix ([7cf6608](https://git.alibaba.ir///commit/7cf6608b2cc6f31ad8e3f5857c1a26f0de239313))

### [0.0.33-alpha.0](https://git.alibaba.ir///compare/@actify/api@0.0.32...@actify/api@0.0.33-alpha.0) (2020-12-14)

### [0.0.32](https://git.alibaba.ir///compare/@actify/main-processor@0.0.31...@actify/main-processor@0.0.32) (2020-12-14)

### Bug Fixes

-   🐛 remove log ([bbbd8f6](https://git.alibaba.ir///commit/bbbd8f62d91060ae4a2ccc0e543fd0f4239e50a9))

### [0.0.31](https://git.alibaba.ir///compare/@actify/main-processor@0.0.31-alpha.1...@actify/main-processor@0.0.31) (2020-12-14)

### [0.0.31-alpha.1](https://git.alibaba.ir///compare/@actify/main-processor@0.0.30...@actify/main-processor@0.0.31-alpha.1) (2020-12-14)

### [0.0.30](https://git.alibaba.ir///compare/@actify/main-processor@0.0.30-alpha.0...@actify/main-processor@0.0.30) (2020-12-14)

### [0.0.30-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.29...@actify/main-processor@0.0.30-alpha.0) (2020-12-14)

### Bug Fixes

-   🐛 bug fix ([5180ba5](https://git.alibaba.ir///commit/5180ba50db5d0d315c941248ecf052059d89b70a))

### [0.0.29](https://git.alibaba.ir///compare/@actify/main-processor@0.0.29-alpha.5...@actify/main-processor@0.0.29) (2020-12-14)

### [0.0.29-alpha.5](https://git.alibaba.ir///compare/@actify/main-processor@0.0.29-alpha.4...@actify/main-processor@0.0.29-alpha.5) (2020-12-14)

### Bug Fixes

-   🐛 bug fix ([3f92f5a](https://git.alibaba.ir///commit/3f92f5aca65200c23686899a686534f468779fa1))

### [0.0.29-alpha.4](https://git.alibaba.ir///compare/@actify/main-processor@0.0.29-alpha.3...@actify/main-processor@0.0.29-alpha.4) (2020-12-14)

### Bug Fixes

-   # 🐛 bug fix ([70c4999](https://git.alibaba.ir///commit/70c49997ce42e11d0f4a8b6bf5a92c23d90a33c4))

### [0.0.29-alpha.4](https://git.alibaba.ir///compare/@actify/api@0.0.29-alpha.3...@actify/api@0.0.29-alpha.4) (2020-12-14)

> > > > > > > 6f1cebad224227b4a6730bcf9578fbc888786ca6

### [0.0.29-alpha.3](https://git.alibaba.ir///compare/@actify/main-processor@0.0.29-alpha.2...@actify/main-processor@0.0.29-alpha.3) (2020-12-13)

### Bug Fixes

-   🐛 bug fix ([e946cee](https://git.alibaba.ir///commit/e946ceeaf1def55bd355a4eae415409afaed6b33))

### [0.0.29-alpha.2](https://git.alibaba.ir///compare/@actify/main-processor@0.0.29-alpha.1...@actify/main-processor@0.0.29-alpha.2) (2020-12-13)

### Bug Fixes

-   🐛 timeout of shortlink ([c5e6ba5](https://git.alibaba.ir///commit/c5e6ba5cf1137660d58f72a774925f8956711839))

### [0.0.29-alpha.1](https://git.alibaba.ir///compare/@actify/main-processor@0.0.29-alpha.0...@actify/main-processor@0.0.29-alpha.1) (2020-12-13)

### Bug Fixes

-   🐛 bug fix ([ca4454e](https://git.alibaba.ir///commit/ca4454eda5a6cd6fdfda3e53fd43cc0506f8b619))

### [0.0.29-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.28...@actify/main-processor@0.0.29-alpha.0) (2020-12-13)

### [0.0.28](https://git.alibaba.ir///compare/@actify/main-processor@0.0.27...@actify/main-processor@0.0.28) (2020-12-13)

### Bug Fixes

-   🐛 bug fix ([6ed89ca](https://git.alibaba.ir///commit/6ed89cae78ea33dc05b6877fdd331e82bee5d7c4))

### [0.0.27](https://git.alibaba.ir///compare/@actify/main-processor@0.0.27-alpha.0...@actify/main-processor@0.0.27) (2020-12-12)

### [0.0.27-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.26...@actify/main-processor@0.0.27-alpha.0) (2020-12-12)

### Bug Fixes

-   🐛 bug fix ([0073fe2](https://git.alibaba.ir///commit/0073fe249ed2807d4b8d6b2c1a6cb6f6a32f28e2))

### [0.0.26](https://git.alibaba.ir///compare/@actify/main-processor@0.0.26-alpha.1...@actify/main-processor@0.0.26) (2020-12-11)

### [0.0.26-alpha.1](https://git.alibaba.ir///compare/@actify/main-processor@0.0.26-alpha.0...@actify/main-processor@0.0.26-alpha.1) (2020-12-11)

### Bug Fixes

-   🐛 bug fix ([a5276f4](https://git.alibaba.ir///commit/a5276f42748a56f494aa55c9fabffb977d413373))

### [0.0.26-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.25...@actify/main-processor@0.0.26-alpha.0) (2020-12-11)

### [0.0.25](https://git.alibaba.ir///compare/@actify/main-processor@0.0.24...@actify/main-processor@0.0.25) (2020-12-11)

### Bug Fixes

-   🐛 bug fix ([769d0c4](https://git.alibaba.ir///commit/769d0c4c6da529c41617f12fb5998141c34dfd83))

### [0.0.24](https://git.alibaba.ir///compare/@actify/main-processor@0.0.24-alpha.0...@actify/main-processor@0.0.24) (2020-12-11)

### [0.0.24-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.23...@actify/main-processor@0.0.24-alpha.0) (2020-12-11)

### [0.0.23](https://git.alibaba.ir///compare/@actify/main-processor@0.0.22...@actify/main-processor@0.0.23) (2020-12-11)

### Bug Fixes

-   🐛 bug fix ([ad76369](https://git.alibaba.ir///commit/ad76369de49d6928a12d2c596774abd4688df14e))

### [0.0.22](https://git.alibaba.ir///compare/@actify/main-processor@0.0.21...@actify/main-processor@0.0.22) (2020-12-11)

### Bug Fixes

-   🐛 bug fix] ([bb65fcd](https://git.alibaba.ir///commit/bb65fcdc808aac4053899a83bcffdf215bb15f81))

### [0.0.21](https://git.alibaba.ir///compare/@actify/main-processor@0.0.20...@actify/main-processor@0.0.21) (2020-12-11)

### Features

-   🎸 bug fix ([e76276b](https://git.alibaba.ir///commit/e76276b9be2719e0e918aa73ed26917d27c6f90d))

### [0.0.20](https://git.alibaba.ir///compare/@actify/main-processor@0.0.19...@actify/main-processor@0.0.20) (2020-12-11)

### Bug Fixes

-   🐛 db bug fix ([f580432](https://git.alibaba.ir///commit/f580432bcab6514abc190241f16aa931459e50f9))

### [0.0.19](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18...@actify/main-processor@0.0.19) (2020-12-11)

### Bug Fixes

-   🐛 bug fix ([066cc4e](https://git.alibaba.ir///commit/066cc4e8801de8465a7a9f79632d2f0c7701cb29))

### [0.0.18](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.45...@actify/main-processor@0.0.18) (2020-12-11)

### [0.0.18-alpha.45](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.44...@actify/main-processor@0.0.18-alpha.45) (2020-12-11)

### Bug Fixes

-   🐛 bug fix ([26c0257](https://git.alibaba.ir///commit/26c0257c1ced095c648e71ccd458eae5307f585c))

### [0.0.18-alpha.44](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.43...@actify/main-processor@0.0.18-alpha.44) (2020-12-10)

### Bug Fixes

-   🐛 bug fix ([0cd5c0c](https://git.alibaba.ir///commit/0cd5c0c66423e0aea79d2cb390ca24cf622de382))

### [0.0.18-alpha.43](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.42...@actify/main-processor@0.0.18-alpha.43) (2020-12-09)

### [0.0.18-alpha.42](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.41...@actify/api@0.0.18-alpha.42) (2020-12-09)

### [0.0.18-alpha.41](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.40...@actify/main-processor@0.0.18-alpha.41) (2020-12-08)

### Bug Fixes

-   🐛 bug fix ([3d0e74c](https://git.alibaba.ir///commit/3d0e74c55bcc055a506e8a63a2a696ff90a9a7a2))
-   🐛 bug fix ([55aa363](https://git.alibaba.ir///commit/55aa3637abac4c4f1d1fd41bfd1a38a64b384f29))

### [0.0.18-alpha.40](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.39...@actify/main-processor@0.0.18-alpha.40) (2020-12-08)

### Bug Fixes

-   🐛 bug fix ([00e668b](https://git.alibaba.ir///commit/00e668b14613bfb7b112387bfdd0afeaca8ff7dc))

### [0.0.18-alpha.39](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.38...@actify/main-processor@0.0.18-alpha.39) (2020-12-08)

### Features

-   🎸 add user info ([e1b59f7](https://git.alibaba.ir///commit/e1b59f7728e83b0a6f9423ed1bec2a02fcced94f))

### [0.0.18-alpha.38](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.37...@actify/main-processor@0.0.18-alpha.38) (2020-12-08)

### Bug Fixes

-   🐛 bug fix ([37f7f98](https://git.alibaba.ir///commit/37f7f9866a09c3ccb92cb5827b4417afbda88482))

### [0.0.18-alpha.37](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.36...@actify/main-processor@0.0.18-alpha.37) (2020-12-08)

### Bug Fixes

-   🐛 campaign scheduling fix ([880ce37](https://git.alibaba.ir///commit/880ce3711fb54995977a5173d9b2c45103703756))

### [0.0.18-alpha.36](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.28...@actify/main-processor@0.0.18-alpha.36) (2020-12-06)

### Bug Fixes

-   🐛 campaign status fix ([c6ae227](https://git.alibaba.ir///commit/c6ae227ebbd51219398e89b88de65980f028076d))
-   🐛 change dto and validation values for the category ([56c743c](https://git.alibaba.ir///commit/56c743c57ad967dc0c22fd82766f9596a48d128f))
-   🐛 fix a boolean for variables ([d6402cd](https://git.alibaba.ir///commit/d6402cda3bb31353f983546ba7f55d6594f375ac))
-   🐛 remove required segment description ([585fc1e](https://git.alibaba.ir///commit/585fc1e996d69bcef85f42cc00de9ca349078a91))
-   🐛 solve a bug where finished was true everywhere ([b6af153](https://git.alibaba.ir///commit/b6af153f1b26b6112a53aefc82ca2774ba3c1600))

### [0.0.18-alpha.35](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.34...@actify/api@0.0.18-alpha.35) (2020-12-05)

### Bug Fixes

-   🐛 solve a bug where finished was true everywhere ([b6af153](https://git.alibaba.ir///commit/b6af153f1b26b6112a53aefc82ca2774ba3c1600))

### [0.0.18-alpha.34](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.33...@actify/api@0.0.18-alpha.34) (2020-12-05)

### Bug Fixes

-   🐛 fix a boolean for variables ([d6402cd](https://git.alibaba.ir///commit/d6402cda3bb31353f983546ba7f55d6594f375ac))

### [0.0.18-alpha.33](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.32...@actify/api@0.0.18-alpha.33) (2020-12-03)

### [0.0.18-alpha.32](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.31...@actify/api@0.0.18-alpha.32) (2020-12-03)

### Bug Fixes

-   🐛 change dto and validation values for the category ([56c743c](https://git.alibaba.ir///commit/56c743c57ad967dc0c22fd82766f9596a48d128f))

### [0.0.18-alpha.31](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.30...@actify/api@0.0.18-alpha.31) (2020-12-02)

### Bug Fixes

-   🐛 remove required segment description ([585fc1e](https://git.alibaba.ir///commit/585fc1e996d69bcef85f42cc00de9ca349078a91))

### [0.0.18-alpha.30](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.29...@actify/api@0.0.18-alpha.30) (2020-12-02)

### [0.0.18-alpha.29](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.28...@actify/api@0.0.18-alpha.29) (2020-12-02)

### [0.0.18-alpha.28](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.27...@actify/main-processor@0.0.18-alpha.28) (2020-12-02)

### Bug Fixes

-   🐛 add logger for shortlink generator ([614ff1b](https://git.alibaba.ir///commit/614ff1bfd47defd896ee0c01c23608a4ab2dde69))

### [0.0.18-alpha.27](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.26...@actify/main-processor@0.0.18-alpha.27) (2020-11-30)

### Bug Fixes

-   🐛 bug fix ([9077b4e](https://git.alibaba.ir///commit/9077b4eae95d06ef2126a5dc473e8ad8da4e59ce))

### [0.0.18-alpha.26](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.25...@actify/main-processor@0.0.18-alpha.26) (2020-11-30)

### Bug Fixes

-   🐛 bug fic ([ea1e166](https://git.alibaba.ir///commit/ea1e16669eea7fc36ba3e49247202bcd9bde09b3))

### [0.0.18-alpha.25](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.24...@actify/main-processor@0.0.18-alpha.25) (2020-11-30)

### Bug Fixes

-   🐛 bug fix ([6c9578d](https://git.alibaba.ir///commit/6c9578dbea6c11df9779cfe02f721a29c55e572d))

### [0.0.18-alpha.24](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.23...@actify/main-processor@0.0.18-alpha.24) (2020-11-30)

### Bug Fixes

-   🐛 bug fix ([cd75d5f](https://git.alibaba.ir///commit/cd75d5f73402613b34c2e62497dac98934ad667e))

### [0.0.18-alpha.23](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.22...@actify/main-processor@0.0.18-alpha.23) (2020-11-30)

### Bug Fixes

-   🐛 bug fix ([3939ac4](https://git.alibaba.ir///commit/3939ac4c98843dfa1b2be63e76167181b329751d))

### [0.0.18-alpha.22](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.21...@actify/main-processor@0.0.18-alpha.22) (2020-11-30)

### Bug Fixes

-   # 🐛 bug fix ([9da6b16](https://git.alibaba.ir///commit/9da6b1698448ba94d23a13732fe2f50e57cdacd1))

### [0.0.18-alpha.22](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.21...@actify/api@0.0.18-alpha.22) (2020-11-30)

> > > > > > > 5e911d8055174fcbabe8f19d0f4c89cbc2f7bfdf

### [0.0.18-alpha.21](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.20...@actify/main-processor@0.0.18-alpha.21) (2020-11-30)

### Bug Fixes

-   🐛 bug fix ([fc32051](https://git.alibaba.ir///commit/fc3205198f131adc013dd53c2d3c29a88a092cc4))

### [0.0.18-alpha.20](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.19...@actify/main-processor@0.0.18-alpha.20) (2020-11-30)

### Bug Fixes

-   🐛 bug fix ([87b8a49](https://git.alibaba.ir///commit/87b8a49986d75c9bd90b97c463de574bcef1e507))

### [0.0.18-alpha.19](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.18...@actify/main-processor@0.0.18-alpha.19) (2020-11-30)

### Bug Fixes

-   🐛 bug fix ([a02245d](https://git.alibaba.ir///commit/a02245dfb4605464001e29327ff545a5e1c2de46))

### [0.0.18-alpha.18](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.17...@actify/main-processor@0.0.18-alpha.18) (2020-11-30)

### Bug Fixes

-   🐛 bug fix ([03db34b](https://git.alibaba.ir///commit/03db34be24c8155c9a5f3e06a37d797abb822c1b))

### [0.0.18-alpha.17](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.16...@actify/main-processor@0.0.18-alpha.17) (2020-11-29)

### Bug Fixes

-   🐛 sms-campaign ([698f517](https://git.alibaba.ir///commit/698f517f158793039d390aa9c61131a806a6e6c3))

### [0.0.18-alpha.16](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.10...@actify/main-processor@0.0.18-alpha.16) (2020-11-29)

### Features

-   🎸 add config endpoint ([bb416af](https://git.alibaba.ir///commit/bb416af54454c5dd5172519d7f0f2c78527d2dd3))
-   🎸 add endpoints to retrive variables ([e2cca91](https://git.alibaba.ir///commit/e2cca914898c56181a86bf301d5bb86d46c3e515))

### Bug Fixes

-   🐛 fix ([3d9a2a0](https://git.alibaba.ir///commit/3d9a2a05cc4e0a46b84d831529113291dd56ea98))
-   🐛 fix configurtation file names ([b1217c8](https://git.alibaba.ir///commit/b1217c8958baf29acd149cd77d89d39b6d375825))

### [0.0.18-alpha.15](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.14...@actify/api@0.0.18-alpha.15) (2020-11-28)

### [0.0.18-alpha.14](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.13...@actify/api@0.0.18-alpha.14) (2020-11-28)

### Features

-   🎸 add endpoints to retrive variables ([e2cca91](https://git.alibaba.ir///commit/e2cca914898c56181a86bf301d5bb86d46c3e515))

### [0.0.18-alpha.13](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.11...@actify/api@0.0.18-alpha.13) (2020-11-28)

### Bug Fixes

-   🐛 fix configurtation file names ([b1217c8](https://git.alibaba.ir///commit/b1217c8958baf29acd149cd77d89d39b6d375825))

### [0.0.18-alpha.11](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.10...@actify/api@0.0.18-alpha.11) (2020-11-28)

### Features

-   🎸 add config endpoint ([bb416af](https://git.alibaba.ir///commit/bb416af54454c5dd5172519d7f0f2c78527d2dd3))

### [0.0.18-alpha.10](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.9...@actify/main-processor@0.0.18-alpha.10) (2020-11-25)

### Features

-   🎸 add rate limit to campaign ([997ff1f](https://git.alibaba.ir///commit/997ff1f1e64a09d1d6f58303fcab4a636021343a))

### [0.0.18-alpha.9](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.8...@actify/main-processor@0.0.18-alpha.9) (2020-11-24)

### Bug Fixes

-   🐛 segmentation bug fix ([2829632](https://git.alibaba.ir///commit/2829632ede4b9b69fb5c1a46051fa918fd89bd8d))

### [0.0.18-alpha.8](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.7...@actify/main-processor@0.0.18-alpha.8) (2020-11-24)

### Bug Fixes

-   🐛 bug fix ([76f3e27](https://git.alibaba.ir///commit/76f3e27e0d731f7773ab6da34173376e95a5fc25))
-   🐛 email fix ([23f6cb3](https://git.alibaba.ir///commit/23f6cb36aa8a99811bed3eb252b1c75c7c66cf2b))

### [0.0.18-alpha.7](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.1...@actify/main-processor@0.0.18-alpha.7) (2020-11-23)

### Features

-   🎸 basic source ([276059e](https://git.alibaba.ir///commit/276059ebb54303e5f8db22d86a8bf2f3b4d8e89d))
-   add sdk ([cce1f3e](https://git.alibaba.ir///commit/cce1f3eb62c1aeaa0f07eb44e0e53a6ddef25390))
-   add session recorder to sdk ([b60adef](https://git.alibaba.ir///commit/b60adef32a3182f5ee3244c13b767ad2d9e48bed))
-   sdk ([f6051b1](https://git.alibaba.ir///commit/f6051b1752250decec97c65329aa76b509ed213e))
-   sdk ([4716280](https://git.alibaba.ir///commit/471628090ddb7edd129c97d7f35b31039246e3f3))

### Bug Fixes

-   🐛 ready for merge with develop ([af5610c](https://git.alibaba.ir///commit/af5610cb8d3d402f61433fe089446f4979187d8f))
-   **sdk:** publish new version ([0ea7438](https://git.alibaba.ir///commit/0ea74380980d48ba7db72d5c55895c46e5e5a341))
-   dockerfile ([850703d](https://git.alibaba.ir///commit/850703ddb6ab5776131e4ebac41df29b377e08f6))
-   sdk ([38712f3](https://git.alibaba.ir///commit/38712f3c0c219f9ebe2b631e25a8b4c63e0d5b07))
-   sdk ([9fa52db](https://git.alibaba.ir///commit/9fa52dbed9e93fe2225ab94baadaf2053fdf1fa3))
-   sdk lazy loading ([0ca9e80](https://git.alibaba.ir///commit/0ca9e802f8a0e60a16f8a344c171305632cf0747))
-   socket build sdk ([1eed570](https://git.alibaba.ir///commit/1eed57056a2ad8a36804652e13939259b0c869d8))
-   socket build sdk ([ec90768](https://git.alibaba.ir///commit/ec9076824fd28144ea6f824c97fd03db6e7ba0d3))

### [0.0.18-alpha.6](https://git.alibaba.ir///compare/@actify/api@0.0.18-alpha.2...@actify/api@0.0.18-alpha.6) (2020-11-22)

### Features

-   add sdk ([cce1f3e](https://git.alibaba.ir///commit/cce1f3eb62c1aeaa0f07eb44e0e53a6ddef25390))
-   add session recorder to sdk ([b60adef](https://git.alibaba.ir///commit/b60adef32a3182f5ee3244c13b767ad2d9e48bed))
-   sdk ([f6051b1](https://git.alibaba.ir///commit/f6051b1752250decec97c65329aa76b509ed213e))
-   sdk ([4716280](https://git.alibaba.ir///commit/471628090ddb7edd129c97d7f35b31039246e3f3))

### Bug Fixes

-   dockerfile ([850703d](https://git.alibaba.ir///commit/850703ddb6ab5776131e4ebac41df29b377e08f6))
-   sdk ([38712f3](https://git.alibaba.ir///commit/38712f3c0c219f9ebe2b631e25a8b4c63e0d5b07))
-   sdk ([9fa52db](https://git.alibaba.ir///commit/9fa52dbed9e93fe2225ab94baadaf2053fdf1fa3))
-   socket build sdk ([1eed570](https://git.alibaba.ir///commit/1eed57056a2ad8a36804652e13939259b0c869d8))
-   socket build sdk ([ec90768](https://git.alibaba.ir///commit/ec9076824fd28144ea6f824c97fd03db6e7ba0d3))

### [0.0.18-alpha.5](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/socket-gateway@0.0.18-alpha.4...@actify/socket-gateway@0.0.18-alpha.5) (2020-11-21)

### Bug Fixes

-   dockerfile ([850703d](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/850703ddb6ab5776131e4ebac41df29b377e08f6))

### [0.0.18-alpha.4](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/socket-gateway@0.0.18-alpha.3...@actify/socket-gateway@0.0.18-alpha.4) (2020-11-21)

### Features

-   🎸 add referrer funcs ([2027b04](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/2027b04fe5e0aae364ad97efea635dbda2d050b5))

### [0.0.18-alpha.3](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.17...@actify/socket-gateway@0.0.18-alpha.3) (2020-11-21)

### Features

-   🎸 add referrer funcs ([2027b04](https://git.alibaba.ir///commit/2027b04fe5e0aae364ad97efea635dbda2d050b5))
-   add event type ([5acaaad](https://git.alibaba.ir///commit/5acaaadcc72ec5f4f623fbfacc116e23177a11ca))

### [0.0.18-alpha.2](https://git.alibaba.ir///compare/@actify/api@0.0.17...@actify/api@0.0.18-alpha.2) (2020-11-21)

### Features

-   🎸 add referrer funcs ([2027b04](https://git.alibaba.ir///commit/2027b04fe5e0aae364ad97efea635dbda2d050b5))
-   add event type ([5acaaad](https://git.alibaba.ir///commit/5acaaadcc72ec5f4f623fbfacc116e23177a11ca))

### [0.0.18-alpha.1](https://git.alibaba.ir///compare/@actify/main-processor@0.0.18-alpha.0...@actify/main-processor@0.0.18-alpha.1) (2020-11-21)

### Features

-   🎸 add referrer funcs ([2027b04](https://git.alibaba.ir///commit/2027b04fe5e0aae364ad97efea635dbda2d050b5))

### [0.0.18-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.17...@actify/main-processor@0.0.18-alpha.0) (2020-11-11)

### Features

-   add event type ([5acaaad](https://git.alibaba.ir///commit/5acaaadcc72ec5f4f623fbfacc116e23177a11ca))

### [0.0.17](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/socket-gateway@0.0.17-alpha.2...@actify/socket-gateway@0.0.17) (2020-11-11)

### Bug Fixes

-   date log ([33b0cf1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/33b0cf1dc76670e543cd26cc342ac34f88916833))

### [0.0.17](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/api@0.0.17-alpha.0...@actify/api@0.0.17) (2020-11-11)

### Bug Fixes

-   date log ([33b0cf1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/33b0cf1dc76670e543cd26cc342ac34f88916833))
-   event ([ccde59b](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/ccde59be69f0565726979933aa257d65cb40492e))
-   socket service attach date to events ([b3c7a7d](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/b3c7a7d0e953461862668b81870353aa62145d4a))

### [0.0.17](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.17-alpha.3...@actify/main-processor@0.0.17) (2020-11-11)

### [0.0.17-alpha.3](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.17-alpha.1...@actify/main-processor@0.0.17-alpha.3) (2020-11-10)

### Bug Fixes

-   date log ([33b0cf1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/33b0cf1dc76670e543cd26cc342ac34f88916833))
-   socket service attach date to events ([b3c7a7d](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/b3c7a7d0e953461862668b81870353aa62145d4a))

### [0.0.17-alpha.2](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/socket-gateway@0.0.17-alpha.0...@actify/socket-gateway@0.0.17-alpha.2) (2020-11-10)

### Bug Fixes

-   event ([ccde59b](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/ccde59be69f0565726979933aa257d65cb40492e))
-   socket service attach date to events ([b3c7a7d](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/b3c7a7d0e953461862668b81870353aa62145d4a))

### [0.0.17-alpha.1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.17-alpha.0...@actify/main-processor@0.0.17-alpha.1) (2020-11-10)

### Bug Fixes

-   event ([ccde59b](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/ccde59be69f0565726979933aa257d65cb40492e))

### [0.0.17-alpha.0](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/socket-gateway@0.0.14...@actify/socket-gateway@0.0.17-alpha.0) (2020-11-10)

### Features

-   add sentry to capture exception logger ([fb8cd88](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/fb8cd88221adf8bd3c4733bdcfcd170551aa1daf))

### Bug Fixes

-   error ([7fda0f0](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/7fda0f0a46d7ed7f20c92bbf12e35d3e8d7fdb7d))
-   logger ([93af1fb](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/93af1fba44d7e17fd3911fe4cbfe2ea750e2e3a7))
-   **ci:** main-processor deploy fail status ([9c7b357](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/9c7b357f1e2770951dae27a3a639f9d3704af9cf))
-   version ([e84878d](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/e84878d004946fbbaec52364c2626e52fbd13a4d))
-   version ([485c578](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/485c5785db2519ea6c0ab78704ecdea0ef686854))

### [0.0.17-alpha.0](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/api@0.0.14...@actify/api@0.0.17-alpha.0) (2020-11-10)

### Features

-   add sentry to capture exception logger ([fb8cd88](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/fb8cd88221adf8bd3c4733bdcfcd170551aa1daf))

### Bug Fixes

-   error ([7fda0f0](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/7fda0f0a46d7ed7f20c92bbf12e35d3e8d7fdb7d))
-   logger ([93af1fb](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/93af1fba44d7e17fd3911fe4cbfe2ea750e2e3a7))
-   **ci:** main-processor deploy fail status ([9c7b357](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/9c7b357f1e2770951dae27a3a639f9d3704af9cf))
-   version ([e84878d](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/e84878d004946fbbaec52364c2626e52fbd13a4d))
-   version ([485c578](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/485c5785db2519ea6c0ab78704ecdea0ef686854))

### [0.0.17-alpha.0](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.16...@actify/main-processor@0.0.17-alpha.0) (2020-11-10)

### Features

-   add sentry to capture exception logger ([fb8cd88](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/fb8cd88221adf8bd3c4733bdcfcd170551aa1daf))

### Bug Fixes

-   error ([7fda0f0](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/7fda0f0a46d7ed7f20c92bbf12e35d3e8d7fdb7d))

### [0.0.16](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.14...@actify/main-processor@0.0.16) (2020-11-09)

### Bug Fixes

-   logger ([93af1fb](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/93af1fba44d7e17fd3911fe4cbfe2ea750e2e3a7))
-   **ci:** main-processor deploy fail status ([9c7b357](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/9c7b357f1e2770951dae27a3a639f9d3704af9cf))
-   version ([e84878d](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/e84878d004946fbbaec52364c2626e52fbd13a4d))
-   version ([485c578](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/485c5785db2519ea6c0ab78704ecdea0ef686854))

### [0.0.14](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/socket-gateway@0.0.14-alpha.41...@actify/socket-gateway@0.0.14) (2020-11-09)

### Bug Fixes

-   release ([3e2c55a](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/3e2c55a40fed4008b169381c502d5925880c8049))

### [0.0.14](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/api@0.0.14-alpha.40...@actify/api@0.0.14) (2020-11-09)

### Bug Fixes

-   release ([3e2c55a](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/3e2c55a40fed4008b169381c502d5925880c8049))

### [0.0.14](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.14-alpha.39...@actify/main-processor@0.0.14) (2020-11-09)

### Bug Fixes

-   release ([3e2c55a](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/3e2c55a40fed4008b169381c502d5925880c8049))

### [0.0.14-alpha.41](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/socket-gateway@0.0.14-alpha.35...@actify/socket-gateway@0.0.14-alpha.41) (2020-11-09)

### Features

-   add event process logging ([5450b6c](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/5450b6ca0634f7e5d1f431223f65f6e680773c23))

### [0.0.14-alpha.40](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/api@0.0.14-alpha.37...@actify/api@0.0.14-alpha.40) (2020-11-09)

### Features

-   add event process logging ([5450b6c](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/5450b6ca0634f7e5d1f431223f65f6e680773c23))

### [0.0.14-alpha.39](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.14-alpha.36...@actify/main-processor@0.0.14-alpha.39) (2020-11-09)

### Features

-   add event process logging ([5450b6c](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/5450b6ca0634f7e5d1f431223f65f6e680773c23))

### [0.0.14-alpha.35](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.32...@actify/socket-gateway@0.0.14-alpha.35) (2020-11-09)

### [0.0.14-alpha.34](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.31...@actify/api@0.0.14-alpha.34) (2020-11-09)

### [0.0.14-alpha.33](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.30...@actify/main-processor@0.0.14-alpha.33) (2020-11-09)

### [0.0.14-alpha.32](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.29...@actify/socket-gateway@0.0.14-alpha.32) (2020-11-09)

### Bug Fixes

-   🐛 bug fix ([35430dd](https://git.alibaba.ir///commit/35430dd9beca44e44d23d6e1e09e16b14f257409))

### [0.0.14-alpha.31](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.28...@actify/api@0.0.14-alpha.31) (2020-11-09)

### Bug Fixes

-   🐛 bug fix ([35430dd](https://git.alibaba.ir///commit/35430dd9beca44e44d23d6e1e09e16b14f257409))

### [0.0.14-alpha.30](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.27...@actify/main-processor@0.0.14-alpha.30) (2020-11-09)

### Bug Fixes

-   🐛 bug fix ([35430dd](https://git.alibaba.ir///commit/35430dd9beca44e44d23d6e1e09e16b14f257409))

### [0.0.14-alpha.29](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.26...@actify/socket-gateway@0.0.14-alpha.29) (2020-11-09)

### Features

-   🎸 add firebase_token to pipeline ([497915d](https://git.alibaba.ir///commit/497915de4ad1c4232b9ceda13656fccdd1d5b400))

### [0.0.14-alpha.28](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.25...@actify/api@0.0.14-alpha.28) (2020-11-09)

### Features

-   🎸 add firebase_token to pipeline ([497915d](https://git.alibaba.ir///commit/497915de4ad1c4232b9ceda13656fccdd1d5b400))

### [0.0.14-alpha.27](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.24...@actify/main-processor@0.0.14-alpha.27) (2020-11-09)

### Features

-   🎸 add firebase_token to pipeline ([497915d](https://git.alibaba.ir///commit/497915de4ad1c4232b9ceda13656fccdd1d5b400))

### [0.0.14-alpha.26](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.23...@actify/socket-gateway@0.0.14-alpha.26) (2020-11-08)

### Bug Fixes

-   🐛 bug fix ([b5a9f3a](https://git.alibaba.ir///commit/b5a9f3a118737cb1132100e5686304cc23eda18e))

### [0.0.14-alpha.25](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.22...@actify/api@0.0.14-alpha.25) (2020-11-08)

### Bug Fixes

-   🐛 bug fix ([b5a9f3a](https://git.alibaba.ir///commit/b5a9f3a118737cb1132100e5686304cc23eda18e))

### [0.0.14-alpha.24](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.21...@actify/main-processor@0.0.14-alpha.24) (2020-11-08)

### Bug Fixes

-   🐛 bug fix ([b5a9f3a](https://git.alibaba.ir///commit/b5a9f3a118737cb1132100e5686304cc23eda18e))

### [0.0.14-alpha.23](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.20...@actify/socket-gateway@0.0.14-alpha.23) (2020-11-08)

### Bug Fixes

-   🐛 bug check ([75e65c6](https://git.alibaba.ir///commit/75e65c69b273e408faf158e72d0a67daf1691d0b))

### [0.0.14-alpha.22](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.19...@actify/api@0.0.14-alpha.22) (2020-11-08)

### Bug Fixes

-   🐛 bug check ([75e65c6](https://git.alibaba.ir///commit/75e65c69b273e408faf158e72d0a67daf1691d0b))

### [0.0.14-alpha.21](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.18...@actify/main-processor@0.0.14-alpha.21) (2020-11-08)

### Bug Fixes

-   🐛 bug check ([75e65c6](https://git.alibaba.ir///commit/75e65c69b273e408faf158e72d0a67daf1691d0b))

### [0.0.14-alpha.20](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.17...@actify/socket-gateway@0.0.14-alpha.20) (2020-11-08)

### Bug Fixes

-   🐛 engagement schema problem ([dae6bf9](https://git.alibaba.ir///commit/dae6bf917392ebdb4271a304de68fb1d4c953699))

### [0.0.14-alpha.19](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.16...@actify/api@0.0.14-alpha.19) (2020-11-08)

### Bug Fixes

-   🐛 engagement schema problem ([dae6bf9](https://git.alibaba.ir///commit/dae6bf917392ebdb4271a304de68fb1d4c953699))

### [0.0.14-alpha.18](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.15...@actify/main-processor@0.0.14-alpha.18) (2020-11-08)

### Bug Fixes

-   🐛 engagement schema problem ([dae6bf9](https://git.alibaba.ir///commit/dae6bf917392ebdb4271a304de68fb1d4c953699))

### [0.0.14-alpha.17](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.14...@actify/socket-gateway@0.0.14-alpha.17) (2020-11-07)

### Features

-   🎸 add schema for user devices ([08125d8](https://git.alibaba.ir///commit/08125d822493227e755a13bc7bd18ee8e1edbd14))
-   🎸 segmentation query ([1784608](https://git.alibaba.ir///commit/1784608ac21afb0de87a85aeeb63dec542472e98))
-   add health check to main processor and elasticsearch ([1bf6766](https://git.alibaba.ir///commit/1bf676602f489657893855709e582c4f0d8c687c))

### Bug Fixes

-   🐛 bug fix ([7eb9c37](https://git.alibaba.ir///commit/7eb9c3769db8818626b40c304aa9a6e5ef5c2e27))
-   error counter on socket parser ([8edf6d5](https://git.alibaba.ir///commit/8edf6d5da5597e6b5bea525362f2efcdb7dde8d5))
-   minor ([c863383](https://git.alibaba.ir///commit/c8633835fdbbb1e16a4548f0745bbb416a08d247))

### [0.0.14-alpha.16](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.13...@actify/api@0.0.14-alpha.16) (2020-11-07)

### Features

-   🎸 add schema for user devices ([08125d8](https://git.alibaba.ir///commit/08125d822493227e755a13bc7bd18ee8e1edbd14))
-   🎸 segmentation query ([1784608](https://git.alibaba.ir///commit/1784608ac21afb0de87a85aeeb63dec542472e98))
-   add health check to main processor and elasticsearch ([1bf6766](https://git.alibaba.ir///commit/1bf676602f489657893855709e582c4f0d8c687c))

### Bug Fixes

-   🐛 bug fix ([7eb9c37](https://git.alibaba.ir///commit/7eb9c3769db8818626b40c304aa9a6e5ef5c2e27))
-   error counter on socket parser ([8edf6d5](https://git.alibaba.ir///commit/8edf6d5da5597e6b5bea525362f2efcdb7dde8d5))
-   minor ([c863383](https://git.alibaba.ir///commit/c8633835fdbbb1e16a4548f0745bbb416a08d247))

### [0.0.14-alpha.15](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.12...@actify/main-processor@0.0.14-alpha.15) (2020-11-07)

### Features

-   🎸 add schema for user devices ([08125d8](https://git.alibaba.ir///commit/08125d822493227e755a13bc7bd18ee8e1edbd14))
-   🎸 segmentation query ([1784608](https://git.alibaba.ir///commit/1784608ac21afb0de87a85aeeb63dec542472e98))
-   add health check to main processor and elasticsearch ([1bf6766](https://git.alibaba.ir///commit/1bf676602f489657893855709e582c4f0d8c687c))

### Bug Fixes

-   🐛 bug fix ([7eb9c37](https://git.alibaba.ir///commit/7eb9c3769db8818626b40c304aa9a6e5ef5c2e27))
-   error counter on socket parser ([8edf6d5](https://git.alibaba.ir///commit/8edf6d5da5597e6b5bea525362f2efcdb7dde8d5))
-   minor ([c863383](https://git.alibaba.ir///commit/c8633835fdbbb1e16a4548f0745bbb416a08d247))

### [0.0.14-alpha.14](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.11...@actify/socket-gateway@0.0.14-alpha.14) (2020-11-05)

### Bug Fixes

-   🐛 bug fix ([f6098f1](https://git.alibaba.ir///commit/f6098f1b1b2be66a4f3077f87e97208b903d223f))

### [0.0.14-alpha.13](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.10...@actify/api@0.0.14-alpha.13) (2020-11-05)

### Bug Fixes

-   🐛 bug fix ([f6098f1](https://git.alibaba.ir///commit/f6098f1b1b2be66a4f3077f87e97208b903d223f))

### [0.0.14-alpha.12](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.9...@actify/main-processor@0.0.14-alpha.12) (2020-11-05)

### Bug Fixes

-   🐛 bug fix ([f6098f1](https://git.alibaba.ir///commit/f6098f1b1b2be66a4f3077f87e97208b903d223f))

### [0.0.14-alpha.11](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.8...@actify/socket-gateway@0.0.14-alpha.11) (2020-11-01)

### Features

-   🎸 engagement bug fix ([cbe701a](https://git.alibaba.ir///commit/cbe701a81e7ac46bf6a06d7a2a58a4bd1747bcb7))

### [0.0.14-alpha.10](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.7...@actify/api@0.0.14-alpha.10) (2020-11-01)

### Features

-   🎸 engagement bug fix ([cbe701a](https://git.alibaba.ir///commit/cbe701a81e7ac46bf6a06d7a2a58a4bd1747bcb7))

### [0.0.14-alpha.9](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.6...@actify/main-processor@0.0.14-alpha.9) (2020-11-01)

### Features

-   🎸 engagement bug fix ([cbe701a](https://git.alibaba.ir///commit/cbe701a81e7ac46bf6a06d7a2a58a4bd1747bcb7))

### [0.0.14-alpha.8](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.5...@actify/socket-gateway@0.0.14-alpha.8) (2020-11-01)

### [0.0.14-alpha.7](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.4...@actify/api@0.0.14-alpha.7) (2020-11-01)

### [0.0.14-alpha.6](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.3...@actify/main-processor@0.0.14-alpha.6) (2020-11-01)

### [0.0.14-alpha.5](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.14-alpha.2...@actify/socket-gateway@0.0.14-alpha.5) (2020-11-01)

### Bug Fixes

-   🐛 merge bug fix ([39c8fd2](https://git.alibaba.ir///commit/39c8fd2c188f697561dfde4c855f45c3ac3d6959))

### [0.0.14-alpha.4](https://git.alibaba.ir///compare/@actify/api@0.0.14-alpha.1...@actify/api@0.0.14-alpha.4) (2020-11-01)

### Bug Fixes

-   🐛 merge bug fix ([39c8fd2](https://git.alibaba.ir///commit/39c8fd2c188f697561dfde4c855f45c3ac3d6959))

### [0.0.14-alpha.3](https://git.alibaba.ir///compare/@actify/main-processor@0.0.14-alpha.0...@actify/main-processor@0.0.14-alpha.3) (2020-11-01)

### Bug Fixes

-   🐛 merge bug fix ([39c8fd2](https://git.alibaba.ir///commit/39c8fd2c188f697561dfde4c855f45c3ac3d6959))

### [0.0.14-alpha.2](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.13...@actify/socket-gateway@0.0.14-alpha.2) (2020-10-31)

### Features

-   🎸 add engagement ([8e72e11](https://git.alibaba.ir///commit/8e72e113cf3ec66036feb33a7974e709ce2f6be1))
-   🎸 added source to user ([9081ba5](https://git.alibaba.ir///commit/9081ba526fa1b0d61622edde9970c2e810dca483))
-   🎸 merge with feat/source ([528d675](https://git.alibaba.ir///commit/528d675863e8a79234b3c8318f8558883c7a5e80))

### Bug Fixes

-   🐛 bug fix ([66317f2](https://git.alibaba.ir///commit/66317f2d8d9ab4fee2fff3a9442976175ecb66d3))

### [0.0.14-alpha.1](https://git.alibaba.ir///compare/@actify/api@0.0.12...@actify/api@0.0.14-alpha.1) (2020-10-31)

### Features

-   🎸 add engagement ([8e72e11](https://git.alibaba.ir///commit/8e72e113cf3ec66036feb33a7974e709ce2f6be1))
-   🎸 added source to user ([9081ba5](https://git.alibaba.ir///commit/9081ba526fa1b0d61622edde9970c2e810dca483))
-   🎸 merge with feat/source ([528d675](https://git.alibaba.ir///commit/528d675863e8a79234b3c8318f8558883c7a5e80))

### Bug Fixes

-   🐛 bug fix ([66317f2](https://git.alibaba.ir///commit/66317f2d8d9ab4fee2fff3a9442976175ecb66d3))

### [0.0.14-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.11...@actify/main-processor@0.0.14-alpha.0) (2020-10-31)

### Features

-   🎸 add engagement ([8e72e11](https://git.alibaba.ir///commit/8e72e113cf3ec66036feb33a7974e709ce2f6be1))
-   🎸 added source to user ([9081ba5](https://git.alibaba.ir///commit/9081ba526fa1b0d61622edde9970c2e810dca483))
-   🎸 merge with feat/source ([528d675](https://git.alibaba.ir///commit/528d675863e8a79234b3c8318f8558883c7a5e80))

### Bug Fixes

-   🐛 bug fix ([66317f2](https://git.alibaba.ir///commit/66317f2d8d9ab4fee2fff3a9442976175ecb66d3))

### [0.0.13](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.11-alpha.2...@actify/socket-gateway@0.0.13) (2020-10-31)

### [0.0.12](https://git.alibaba.ir///compare/@actify/api@0.0.11-alpha.1...@actify/api@0.0.12) (2020-10-31)

### [0.0.11](https://git.alibaba.ir///compare/@actify/main-processor@0.0.11-alpha.0...@actify/main-processor@0.0.11) (2020-10-31)

### [0.0.11-alpha.2](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.10...@actify/socket-gateway@0.0.11-alpha.2) (2020-10-31)

### Bug Fixes

-   🐛 bug fix ([406edf8](https://git.alibaba.ir///commit/406edf8742391fb4aa93c509bcc819ef9c0b6c83))

### [0.0.11-alpha.1](https://git.alibaba.ir///compare/@actify/api@0.0.9...@actify/api@0.0.11-alpha.1) (2020-10-31)

### Bug Fixes

-   🐛 bug fix ([406edf8](https://git.alibaba.ir///commit/406edf8742391fb4aa93c509bcc819ef9c0b6c83))

### [0.0.11-alpha.0](https://git.alibaba.ir///compare/@actify/main-processor@0.0.8...@actify/main-processor@0.0.11-alpha.0) (2020-10-31)

### Bug Fixes

-   🐛 bug fix ([406edf8](https://git.alibaba.ir///commit/406edf8742391fb4aa93c509bcc819ef9c0b6c83))

### [0.0.10](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.7...@actify/socket-gateway@0.0.10) (2020-10-31)

### [0.0.9](https://git.alibaba.ir///compare/@actify/api@0.0.6...@actify/api@0.0.9) (2020-10-31)

### [0.0.8](https://git.alibaba.ir///compare/@actify/main-processor@0.0.5...@actify/main-processor@0.0.8) (2020-10-31)

### [0.0.7](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/socket-gateway@0.0.4...@actify/socket-gateway@0.0.7) (2020-10-28)

### Features

-   🎸 added user attributes ([1885461](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/188546117553be14d968336e7fcf033b67cfc73a))

### Bug Fixes

-   yml file ([d66d33b](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d66d33bfa21d0bc320324174ad93f007a3e52424))

### [0.0.6](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/api@0.0.3...@actify/api@0.0.6) (2020-10-28)

### Features

-   🎸 added user attributes ([1885461](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/188546117553be14d968336e7fcf033b67cfc73a))

### Bug Fixes

-   yml file ([d66d33b](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d66d33bfa21d0bc320324174ad93f007a3e52424))

### [0.0.5](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2...@actify/main-processor@0.0.5) (2020-10-28)

### Features

-   🎸 added user attributes ([1885461](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/188546117553be14d968336e7fcf033b67cfc73a))

### Bug Fixes

-   yml file ([d66d33b](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d66d33bfa21d0bc320324174ad93f007a3e52424))

### [0.0.4](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.2-alpha.36...@actify/socket-gateway@0.0.4) (2020-10-27)

### [0.0.3](https://git.alibaba.ir///compare/@actify/api@0.0.2-alpha.35...@actify/api@0.0.3) (2020-10-27)

### Bug Fixes

-   🐛 session not valid problem ([32a5bd5](https://git.alibaba.ir///commit/32a5bd5d7870d8f786d2e96675f0702f5b277c88))

### [0.0.2](https://git.alibaba.ir///compare/@actify/main-processor@0.0.2-alpha.33...@actify/main-processor@0.0.2) (2020-10-27)

### Bug Fixes

-   🐛 increase ping time ([4fa0f34](https://git.alibaba.ir///commit/4fa0f34d782f02e9830b6842fbff32d2a5c50117))
-   🐛 server-events sorting issue ([016f9a9](https://git.alibaba.ir///commit/016f9a99d05fd03c3cee64ddb6fc2faad3902f0f))
-   🐛 session not valid problem ([32a5bd5](https://git.alibaba.ir///commit/32a5bd5d7870d8f786d2e96675f0702f5b277c88))

### [0.0.2-alpha.36](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.2-alpha.34...@actify/socket-gateway@0.0.2-alpha.36) (2020-10-27)

### Bug Fixes

-   🐛 server-events sorting issue ([016f9a9](https://git.alibaba.ir///commit/016f9a99d05fd03c3cee64ddb6fc2faad3902f0f))
-   🐛 session not valid problem ([32a5bd5](https://git.alibaba.ir///commit/32a5bd5d7870d8f786d2e96675f0702f5b277c88))

### [0.0.2-alpha.35](https://git.alibaba.ir///compare/@actify/api@0.0.2-alpha.27...@actify/api@0.0.2-alpha.35) (2020-10-24)

### Bug Fixes

-   🐛 hotfix for socket_gateway ([634c72e](https://git.alibaba.ir///commit/634c72efcc8cecc291ba45d5fd51101e50730331))
-   🐛 increase ping time ([4fa0f34](https://git.alibaba.ir///commit/4fa0f34d782f02e9830b6842fbff32d2a5c50117))
-   🐛 online_users ([5376fd3](https://git.alibaba.ir///commit/5376fd3af216752b028a9c45b66b2eebaf807feb))
-   🐛 resolve event not collecting issue ([26b3161](https://git.alibaba.ir///commit/26b3161d91a691a2e3700838c22f7b02920688a7))
-   🐛 server-events sorting issue ([016f9a9](https://git.alibaba.ir///commit/016f9a99d05fd03c3cee64ddb6fc2faad3902f0f))

### [0.0.2-alpha.34](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.2-alpha.32...@actify/socket-gateway@0.0.2-alpha.34) (2020-10-22)

### Bug Fixes

-   🐛 increase ping time ([4fa0f34](https://git.alibaba.ir///commit/4fa0f34d782f02e9830b6842fbff32d2a5c50117))

### [0.0.2-alpha.33](https://git.alibaba.ir///compare/@actify/main-processor@0.0.2-alpha.30...@actify/main-processor@0.0.2-alpha.33) (2020-10-22)

### Bug Fixes

-   🐛 online_users ([5376fd3](https://git.alibaba.ir///commit/5376fd3af216752b028a9c45b66b2eebaf807feb))
-   🐛 resolve event not collecting issue ([26b3161](https://git.alibaba.ir///commit/26b3161d91a691a2e3700838c22f7b02920688a7))

### [0.0.2-alpha.32](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.2-alpha.31...@actify/socket-gateway@0.0.2-alpha.32) (2020-10-22)

### Bug Fixes

-   🐛 online_users ([5376fd3](https://git.alibaba.ir///commit/5376fd3af216752b028a9c45b66b2eebaf807feb))

### [0.0.2-alpha.31](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.2-alpha.29...@actify/socket-gateway@0.0.2-alpha.31) (2020-10-19)

### Bug Fixes

-   🐛 resolve event not collecting issue ([26b3161](https://git.alibaba.ir///commit/26b3161d91a691a2e3700838c22f7b02920688a7))

### [0.0.2-alpha.30](https://git.alibaba.ir///compare/@actify/main-processor@0.0.2-alpha.26...@actify/main-processor@0.0.2-alpha.30) (2020-10-12)

### Bug Fixes

-   🐛 hotfix for socket_gateway ([634c72e](https://git.alibaba.ir///commit/634c72efcc8cecc291ba45d5fd51101e50730331))

### [0.0.2-alpha.29](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.2-alpha.28...@actify/socket-gateway@0.0.2-alpha.29) (2020-10-12)

### Bug Fixes

-   🐛 hotfix for socket_gateway ([634c72e](https://git.alibaba.ir///commit/634c72efcc8cecc291ba45d5fd51101e50730331))

### [0.0.2-alpha.28](https://git.alibaba.ir///compare/@actify/socket-gateway@0.0.2-alpha.21...@actify/socket-gateway@0.0.2-alpha.28) (2020-10-12)

### Features

-   🎸 add push-notification ([26f7e1c](https://git.alibaba.ir///commit/26f7e1c76482b3c5c177675c5937ffdd3ba01435))
-   🎸 add REST client for server events functionality ([e960ac7](https://git.alibaba.ir///commit/e960ac7179916d8a841486fc2fa8df79034e6604))
-   🎸 feat/mobile-integration ([6c17e55](https://git.alibaba.ir///commit/6c17e55676ea7f5b73da8d8ed60dcf3058ac4a46))
-   🎸 integrate with mobile ([d3e963d](https://git.alibaba.ir///commit/d3e963d5a6797dba19855eb47f79deaea310c50f))
-   add nestjs console ([1364ba5](https://git.alibaba.ir///commit/1364ba5a28b05b9be47241c89d189a42c0b0f0ec))
-   console ([306b23d](https://git.alibaba.ir///commit/306b23d58f2356bd41874eef470a499e4f4acb58))
-   refactor and implement connection store ([317dff1](https://git.alibaba.ir///commit/317dff1ea602f7b537c9f9c160f83a2e089702ab))

### Bug Fixes

-   🐛 add eslint rules ([930c3ef](https://git.alibaba.ir///commit/930c3effbfb1d1705ba3bd2547cfc897030ecd35))
-   🐛 bug fix ([6652d8d](https://git.alibaba.ir///commit/6652d8d290ca6529a8f9e01c9a886824634ccea7))
-   🐛 merge pub sub ([63ebefd](https://git.alibaba.ir///commit/63ebefd0d3f0487a922184cdbdbbb0e4f29e7603))
-   ci ([6a40d57](https://git.alibaba.ir///commit/6a40d5795239ffbb3735f3b8d3e8239144db7a29))
-   ctl ([8d30772](https://git.alibaba.ir///commit/8d3077263dd38c79ba39430dfcba4fce3fb4915c))
-   eslint ([ad0ec33](https://git.alibaba.ir///commit/ad0ec335d2a2f183a9dd41adc51cda88bcb7dff7))
-   eslint ([3101a10](https://git.alibaba.ir///commit/3101a10e42d1026a91971626a38c68503471b16f))
-   gitlab ci kube rollout status ([3a9d0cc](https://git.alibaba.ir///commit/3a9d0ccad9278ddb52294a3d978a32e55add70e5))

### [0.0.2-alpha.27](https://git.alibaba.ir///compare/@actify/api@0.0.2-alpha.22...@actify/api@0.0.2-alpha.27) (2020-10-12)

### Features

-   🎸 add REST client for server events functionality ([e960ac7](https://git.alibaba.ir///commit/e960ac7179916d8a841486fc2fa8df79034e6604))
-   🎸 feat/mobile-integration ([6c17e55](https://git.alibaba.ir///commit/6c17e55676ea7f5b73da8d8ed60dcf3058ac4a46))
-   🎸 integrate with mobile ([d3e963d](https://git.alibaba.ir///commit/d3e963d5a6797dba19855eb47f79deaea310c50f))
-   refactor and implement connection store ([317dff1](https://git.alibaba.ir///commit/317dff1ea602f7b537c9f9c160f83a2e089702ab))

### Bug Fixes

-   🐛 merge pub sub ([63ebefd](https://git.alibaba.ir///commit/63ebefd0d3f0487a922184cdbdbbb0e4f29e7603))
-   ci ([6a40d57](https://git.alibaba.ir///commit/6a40d5795239ffbb3735f3b8d3e8239144db7a29))
-   eslint ([ad0ec33](https://git.alibaba.ir///commit/ad0ec335d2a2f183a9dd41adc51cda88bcb7dff7))
-   gitlab ci kube rollout status ([3a9d0cc](https://git.alibaba.ir///commit/3a9d0ccad9278ddb52294a3d978a32e55add70e5))

### [0.0.2-alpha.26](https://git.alibaba.ir///compare/@actify/main-processor@0.0.2-alpha.25...@actify/main-processor@0.0.2-alpha.26) (2020-10-12)

### Features

-   🎸 feat/mobile-integration ([6c17e55](https://git.alibaba.ir///commit/6c17e55676ea7f5b73da8d8ed60dcf3058ac4a46))
-   🎸 integrate with mobile ([d3e963d](https://git.alibaba.ir///commit/d3e963d5a6797dba19855eb47f79deaea310c50f))
-   refactor and implement connection store ([317dff1](https://git.alibaba.ir///commit/317dff1ea602f7b537c9f9c160f83a2e089702ab))

### Bug Fixes

-   🐛 merge pub sub ([63ebefd](https://git.alibaba.ir///commit/63ebefd0d3f0487a922184cdbdbbb0e4f29e7603))
-   eslint ([ad0ec33](https://git.alibaba.ir///commit/ad0ec335d2a2f183a9dd41adc51cda88bcb7dff7))

### [0.0.2-alpha.25](https://git.alibaba.ir///compare/@actify/main-processor@0.0.2-alpha.24...@actify/main-processor@0.0.2-alpha.25) (2020-10-10)

### [0.0.2-alpha.24](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.23...@actify/main-processor@0.0.2-alpha.24) (2020-10-07)

### Bug Fixes

-   ci ([6a40d57](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/6a40d5795239ffbb3735f3b8d3e8239144db7a29))
-   gitlab ci kube rollout status ([3a9d0cc](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/3a9d0ccad9278ddb52294a3d978a32e55add70e5))

### [0.0.2-alpha.23](https://git.alibaba.ir///compare/@actify/main-processor@0.0.2-alpha.20...@actify/main-processor@0.0.2-alpha.23) (2020-10-06)

### Features

-   🎸 add push-notification ([26f7e1c](https://git.alibaba.ir///commit/26f7e1c76482b3c5c177675c5937ffdd3ba01435))
-   🎸 add REST client for server events functionality ([e960ac7](https://git.alibaba.ir///commit/e960ac7179916d8a841486fc2fa8df79034e6604))
-   add nestjs console ([1364ba5](https://git.alibaba.ir///commit/1364ba5a28b05b9be47241c89d189a42c0b0f0ec))
-   console ([306b23d](https://git.alibaba.ir///commit/306b23d58f2356bd41874eef470a499e4f4acb58))

### Bug Fixes

-   🐛 add eslint rules ([930c3ef](https://git.alibaba.ir///commit/930c3effbfb1d1705ba3bd2547cfc897030ecd35))
-   🐛 bug fix ([6652d8d](https://git.alibaba.ir///commit/6652d8d290ca6529a8f9e01c9a886824634ccea7))
-   ctl ([8d30772](https://git.alibaba.ir///commit/8d3077263dd38c79ba39430dfcba4fce3fb4915c))
-   eslint ([3101a10](https://git.alibaba.ir///commit/3101a10e42d1026a91971626a38c68503471b16f))

### [0.0.2-alpha.22](https://git.alibaba.ir///compare/@actify/api@0.0.2-alpha.20...@actify/api@0.0.2-alpha.22) (2020-10-04)

### Features

-   🎸 add push-notification ([26f7e1c](https://git.alibaba.ir///commit/26f7e1c76482b3c5c177675c5937ffdd3ba01435))
-   add nestjs console ([1364ba5](https://git.alibaba.ir///commit/1364ba5a28b05b9be47241c89d189a42c0b0f0ec))
-   console ([306b23d](https://git.alibaba.ir///commit/306b23d58f2356bd41874eef470a499e4f4acb58))

### Bug Fixes

-   🐛 add eslint rules ([930c3ef](https://git.alibaba.ir///commit/930c3effbfb1d1705ba3bd2547cfc897030ecd35))
-   🐛 bug fix ([6652d8d](https://git.alibaba.ir///commit/6652d8d290ca6529a8f9e01c9a886824634ccea7))
-   ctl ([8d30772](https://git.alibaba.ir///commit/8d3077263dd38c79ba39430dfcba4fce3fb4915c))
-   eslint ([3101a10](https://git.alibaba.ir///commit/3101a10e42d1026a91971626a38c68503471b16f))

### [0.0.2-alpha.21](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/socket-gateway@0.0.2-alpha.20...@actify/socket-gateway@0.0.2-alpha.21) (2020-09-30)

### [0.0.2-alpha.20](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/socket-gateway@0.0.2-alpha.20...@actify/socket-gateway@0.0.2-alpha.20) (2020-09-30)

### [0.0.2-alpha.19](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.14...@actify/main-processor@0.0.2-alpha.19) (2020-09-30)

### 0.0.2-alpha.18 (2020-09-30)

### Features

-   ci ([d6db0a3](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d6db0a3a270f71d7f096e464ac0043c0ae8a815b))
-   standard-version ([6948574](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/6948574b30d85ac828e9ee831b6261a575fd22e2))

### Bug Fixes

-   eslint ([c501cca](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/c501ccae6bb98917bbbfb545e11e6431c36ff4c9))
-   gitlab ci ([d047db2](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d047db2bf1f68c329f80593fd792bd2545ae0325))
-   m ([4910eca](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/4910eca910e5a21bc5e2007f6de227fd8e17c2ea))
-   m ([a6db696](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/a6db69654c63015b388569a37a622b69ef13fc06))
-   s ([a6ffcc1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/a6ffcc1ea8b1ad431ba9e274efe78cff263bf306))

### [0.0.2-alpha.17](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/api@0.0.2-alpha.16...@actify/api@0.0.2-alpha.17) (2020-09-30)

### [0.0.2-alpha.16](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/api@0.0.2-alpha.15...@actify/api@0.0.2-alpha.16) (2020-09-30)

### 0.0.2-alpha.15 (2020-09-30)

### Features

-   ci ([d6db0a3](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d6db0a3a270f71d7f096e464ac0043c0ae8a815b))
-   standard-version ([6948574](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/6948574b30d85ac828e9ee831b6261a575fd22e2))

### Bug Fixes

-   eslint ([c501cca](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/c501ccae6bb98917bbbfb545e11e6431c36ff4c9))
-   gitlab ci ([d047db2](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d047db2bf1f68c329f80593fd792bd2545ae0325))
-   m ([4910eca](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/4910eca910e5a21bc5e2007f6de227fd8e17c2ea))
-   m ([a6db696](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/a6db69654c63015b388569a37a622b69ef13fc06))
-   s ([a6ffcc1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/a6ffcc1ea8b1ad431ba9e274efe78cff263bf306))

### 0.0.2-alpha.14 (2020-09-30)

### Features

-   ci ([d6db0a3](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d6db0a3a270f71d7f096e464ac0043c0ae8a815b))
-   standard-version ([6948574](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/6948574b30d85ac828e9ee831b6261a575fd22e2))

### Bug Fixes

-   eslint ([c501cca](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/c501ccae6bb98917bbbfb545e11e6431c36ff4c9))
-   gitlab ci ([d047db2](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d047db2bf1f68c329f80593fd792bd2545ae0325))
-   m ([4910eca](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/4910eca910e5a21bc5e2007f6de227fd8e17c2ea))
-   m ([a6db696](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/a6db69654c63015b388569a37a622b69ef13fc06))
-   s ([a6ffcc1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/a6ffcc1ea8b1ad431ba9e274efe78cff263bf306))

### [0.0.2-alpha.13](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.12...@actify/main-processor@0.0.2-alpha.13) (2020-09-30)

### [0.0.2-alpha.12](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.11...@actify/main-processor@0.0.2-alpha.12) (2020-09-30)

### [0.0.2-alpha.11](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.1...@actify/main-processor@0.0.2-alpha.11) (2020-09-30)

### [0.0.2-alpha.1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.0...@actify/main-processor@0.0.2-alpha.1) (2020-09-30)

### Bug Fixes

-   gitlab ci ([d047db2](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d047db2bf1f68c329f80593fd792bd2545ae0325))

### 0.0.2-alpha.0 (2020-09-30)

### Features

-   ci ([d6db0a3](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/d6db0a3a270f71d7f096e464ac0043c0ae8a815b))
-   standard-version ([6948574](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/6948574b30d85ac828e9ee831b6261a575fd22e2))

### Bug Fixes

-   eslint ([c501cca](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/c501ccae6bb98917bbbfb545e11e6431c36ff4c9))
-   m ([4910eca](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/4910eca910e5a21bc5e2007f6de227fd8e17c2ea))
-   m ([a6db696](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/a6db69654c63015b388569a37a622b69ef13fc06))
-   s ([a6ffcc1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/a6ffcc1ea8b1ad431ba9e274efe78cff263bf306))

### [0.0.2-alpha.13](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.12...@actify/main-processor@0.0.2-alpha.13) (2020-09-30)

### [0.0.2-alpha.12](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.11...@actify/main-processor@0.0.2-alpha.12) (2020-09-30)

### [0.0.2-alpha.11](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.10...@actify/main-processor@0.0.2-alpha.11) (2020-09-30)

### [0.0.2-alpha.10](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.9...@actify/main-processor@0.0.2-alpha.10) (2020-09-30)

### Bug Fixes

-   m ([c6afade](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/c6afade691c8a8d2a04953f77de4bef3b8543e50))

### [0.0.2-alpha.9](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.8...@actify/main-processor@0.0.2-alpha.9) (2020-09-30)

### Bug Fixes

-   m ([a904adb](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/a904adba84a08c511b154855eb6067be3195a5ff))

### [0.0.2-alpha.8](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.7...@actify/main-processor@0.0.2-alpha.8) (2020-09-30)

### [0.0.2-alpha.7](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.6...@actify/main-processor@0.0.2-alpha.7) (2020-09-30)

### Bug Fixes

-   m ([7ea51a6](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/7ea51a64a618041e9f2354981af5c5901757db82))

### [0.0.2-alpha.6](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.5...@actify/main-processor@0.0.2-alpha.6) (2020-09-30)

### Bug Fixes

-   m ([4cc573f](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/4cc573f5fd90d2aab1b9c4ed8fc2d3cf52966555))

### [0.0.2-alpha.5](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.4...@actify/main-processor@0.0.2-alpha.5) (2020-09-30)

### Bug Fixes

-   m ([f857d81](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/f857d817972c453797dcbe5af15b9937bc2da9c1))

### [0.0.2-alpha.4](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.3...@actify/main-processor@0.0.2-alpha.4) (2020-09-30)

### [0.0.2-alpha.3](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.2...@actify/main-processor@0.0.2-alpha.3) (2020-09-30)

### [0.0.2-alpha.2](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.0...@actify/main-processor@0.0.2-alpha.2) (2020-09-30)

### [0.0.2-alpha.1](https://git.alibaba.ir/frontend-ecosystem/actify-mono/compare/@actify/main-processor@0.0.2-alpha.0...@actify/main-processor@0.0.2-alpha.1) (2020-09-30)

### 0.0.2-alpha.0 (2020-09-30)

### Features

-   standard-version ([6948574](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/6948574b30d85ac828e9ee831b6261a575fd22e2))

### Bug Fixes

-   eslint ([c501cca](https://git.alibaba.ir/frontend-ecosystem/actify-mono/commit/c501ccae6bb98917bbbfb545e11e6431c36ff4c9))
