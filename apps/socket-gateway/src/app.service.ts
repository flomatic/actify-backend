import { Injectable } from '@nestjs/common';
import { RedisService } from '@shared/redis/redis.service';

@Injectable()
export class AppService {
    constructor(private readonly redisService: RedisService) {}
    getHealth(): any {
        return this.redisService.checkHealth();
    }
}
