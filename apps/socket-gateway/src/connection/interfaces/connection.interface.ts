export default interface Connection {
    sessionId: string;
    userAttribute: any;
    userAgent?: any;
    channel: string;
    firebaseToken?: string;
    deviceInfo?: any;
    device_id: string;
    numberOfConnections: number;
    ipAddress?: any;
    connectionDate: Date;
    tracking_id?: string;
    referrer?: string;
    hostname: string;
}
