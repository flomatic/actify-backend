import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RedisService } from '@shared/redis/redis.service';
import Connection from './interfaces/connection.interface';

@Injectable()
export class ConnectionService {
    private keysTimeOut: number;
    constructor(
        readonly redisService: RedisService,
        readonly configService: ConfigService
    ) {
        this.keysTimeOut = Number(configService.get('KEYS_TIMEOUT')) || 3600;
    }

    async addConnection(
        connection: Connection,
        timeInSeconds?: number
    ): Promise<void> {
        const data = await this.redisService.setWithTTL(
            `connection:${connection.sessionId}`,
            connection,
            this.keysTimeOut
        );
        this.addShadowConnection(connection);
        return data;
    }
    /*  the reason of adding this shadow connection is because of that redis just return 
     the key of a connection when it's going to be expired, so we create a 
     shadow connection and when a key expired we get the connection from shadow connection 
    */
    private async addShadowConnection(connection: Connection): Promise<void> {
        return this.redisService.set(
            `shadow_connection:${connection.sessionId}`,
            connection
        );
    }

    async getShadowConnectionBySessionId(
        connectionId: string
    ): Promise<Connection> {
        const connection: Connection = await this.redisService.get(
            `shadow_connection:${connectionId}`
        );
        return connection;
    }

    async removeConnectionBySessionId(connectionId: string): Promise<boolean> {
        return this.redisService.clearByKey(`connection:${connectionId}`);
    }

    async removeShadowConnectionBySessionId(
        connectionId: string
    ): Promise<boolean> {
        return this.redisService.clearByKey(
            `shadow_connection:${connectionId}`
        );
    }

    async getConnectionBySessionId(connectionId: string): Promise<Connection> {
        const connection: Connection = await this.redisService.get(
            `connection:${connectionId}`
        );
        return connection;
    }

    async updateExpiry(connection: Connection, timeInSeconds?: number) {
        await this.redisService
            .getReadClient()
            .expire(
                `connection:${connection.sessionId}`,
                timeInSeconds || this.keysTimeOut
            );
    }

    async addNumberOfConnections(
        connection: Connection,
        timeInSeconds?: number
    ) {
        connection.numberOfConnections++;
        await this.redisService.setWithTTL(
            `connection:${connection.sessionId}`,
            connection,
            timeInSeconds || this.keysTimeOut
        );
    }

    async negativeNumberConnections(
        connection: Connection,
        timeInSeconds?: number
    ) {
        connection.numberOfConnections--;
        await this.redisService.setWithTTL(
            `connection:${connection.sessionId}`,
            connection,
            timeInSeconds || this.keysTimeOut
        );
    }

    async listenExpiry(
        listener: (pattern, channelPattern, emittedKey) => void
    ) {
        const redisClient = this.redisService.getNewInstance();
        redisClient.on('message', listener);
        redisClient.subscribe('__keyevent@0__:expired');
    }
}
