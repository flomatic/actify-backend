import { Module } from '@nestjs/common';
import { RedisModule } from '@shared/redis/redis.module';
import { ConnectionService } from './connection.service';

@Module({
    imports: [RedisModule],
    providers: [ConnectionService],
    exports: [ConnectionService]
})
export class ConnectionModule {}
