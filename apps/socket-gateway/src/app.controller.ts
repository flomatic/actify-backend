import { Controller, DefaultValuePipe, Get, Query, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { createReadStream } from 'fs';
import { ConfigService } from '@nestjs/config';

@Controller()
export class AppController {
    constructor(
        private readonly appService: AppService,
        private readonly configService: ConfigService
    ) {}

    @Get('health')
    health(): string {
        return this.appService.getHealth();
    }

    @Get('actify-sdk.js')
    sdk(
        @Query('format', new DefaultValuePipe('umd')) format: string,
        @Query('url') url: string,
        @Res() res
    ) {
        const websocketURL =
            url || this.configService.get('WEBSOCKET_PUBLIC_URL');
        res.status(200);
        res.header('Content-Type', 'application/javascript');
        res.write(`var ACTIFY_WEBSOCKET_URL="${websocketURL}";\n\n`);
        return createReadStream(
            `./apps/sdk-js/dist/actify-sdk.${format}.js`
        ).pipe(res);
    }
}
