import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestjsLogger } from '@shared/logger/logger.service';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    app.useLogger(app.get(NestjsLogger));

    const configService: ConfigService = app.get(ConfigService);
    await app.listen(configService.get('SOCKET_API_PORT') || 3000);
}
bootstrap();
