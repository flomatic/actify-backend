import { EventModule } from './../event/event.module';
import { ConnectionModule } from './../connection/connection.module';
import { SocketService } from './socket.service';
import { Module } from '@nestjs/common';
import { RedisModule } from '@shared/redis/redis.module';

@Module(
    process.env.ENV !== 'console'
        ? {
            imports: [ConnectionModule, EventModule, RedisModule],
            providers: [SocketService]
        }
        : {}
)
export class SocketModule {}
