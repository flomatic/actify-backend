import * as Joi from 'joi';

export const UserAttrsValidation = Joi.array()
    .items({
        variable: Joi.string().required(),
        value: Joi.string().required
    })
    .max(100);
