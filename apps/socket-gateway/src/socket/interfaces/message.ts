export interface Message {
    type: MessageType;
    event_name?: any;
    session_id?: string;
    body: any;
    date: Date;
}

export enum MessageType {
    INIT = 'init',
    CLOSE = 'close',
    EVENT = 'event',
    PING = 'ping',
    ERROR = 'error',
    PONG = 'pong',
    USER_ATTRS = 'user_attrs'
}
