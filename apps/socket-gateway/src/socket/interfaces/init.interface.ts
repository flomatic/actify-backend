export interface InitBody {
    user_attr: {
        key: string;
        value: string;
    };
    ip_address: string;
    user_agent: string;
    firebase_token: string;
    device_id: string;
    channel: string;
    device_info: string;
    old_session_id?: string;
    tracking_id?: string;
    referrer?: string;
}
