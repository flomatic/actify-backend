export const InitBodySchema = {
    old_session_id: { type: 'string', optional: true },
    user_attr: {
        type: 'object',
        props: {
            key: { type: 'string' },
            value: { type: 'string' }
        },
        optional: true
    },
    device_id: { type: 'string' },
    ip_address: {
        type: 'string',
        pattern: /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/,
        optional: true
    },
    firebase_token: { type: 'string', optional: true },
    channel: {
        type: 'enum',
        values: ['android', 'ios', 'browser'],
        optional: true
    }
};
