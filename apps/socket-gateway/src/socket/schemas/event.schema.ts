export const EventBodySchema = {
    name: { type: 'string', optional: false },
    payload: { type: 'object', optional: true }
};
