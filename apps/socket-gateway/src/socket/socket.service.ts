import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { RedisService } from '@shared/redis/redis.service';
import { UserAttrValidation } from 'apps/api/src/public/validation/user-attr.validation';
import Connection from 'apps/socket-gateway/src/connection/interfaces/connection.interface';
import { IncomingMessage } from 'http';
import { Queue } from '../utils/queue';
import { ConnectionService } from './../connection/connection.service';
import { EventService } from './../event/event.service';
import { whiteList } from './../white-list';
import { InitBody } from './interfaces/init.interface';
import { Message, MessageType } from './interfaces/message';
import { InitBodySchema } from './schemas/init.schema';
/* eslint @typescript-eslint/no-var-requires: "off" */
const Validator = require('fastest-validator');
import WebSocket = require('ws');
import uuid = require('uuid');

const PING_TIME = 1000;
const CONNECTION_EXPAND_TTL = 3;
interface WebsocketInterface extends WebSocket {
    sendMessage(type: MessageType, body: unknown);
    ip_address: string;
    sessionId: string;
}

@Injectable()
export class SocketService implements OnModuleInit {
    private wss: WebSocket.Server;
    private validation: any = new Validator();
    private queue: Queue;
    private hostname: string;
    private connections: Record<string, WebsocketInterface> = {};

    constructor(
        private readonly configService: ConfigService,
        private readonly eventService: EventService,
        private readonly connectionService: ConnectionService,
        private readonly loggerService: BLogger,
        private readonly redisService: RedisService
    ) {
        this.hostname = configService.get('HOSTNAME');
        this.queue = new Queue(1);
        this.wss = new WebSocket.Server({
            port: +this.configService.get('SOCKET_PORT') || 8888
        });
    }

    async onModuleInit(): Promise<void> {
        this.wss.on('connection', this.handleConnection);
        await this.connectionService.listenExpiry(async (_, data) => {
            /* we don't have the value of a expired connection
            so we use shadow connection */
            const sessionId = data.split('connection:')?.[1];
            if (sessionId) {
                /* eslint-disable */
                const shadowConnection = await this.connectionService.getShadowConnectionBySessionId(
                    sessionId
                );
                if (shadowConnection) {
                    await this.eventService.userIsOffline(shadowConnection);
                }
                /* we don't need shadow_connection now because the original
                connection is expired now  */
                await this.connectionService.removeShadowConnectionBySessionId(
                    sessionId
                );
            }
        });
        this.redisService.getNewInstance().subscribe('socket:' + this.hostname);
        this.redisService.readClient.on('message', (channel, str) => {
            const msg: Message = JSON.parse(str) as Message;
            const ws = this.connections[msg.session_id];
            if (!ws) return;
            ws.sendMessage(msg.type, msg.body);
        });
    }
    handleConnection = async (
        ws: WebsocketInterface,
        request: IncomingMessage
    ) => {
        ws.sendMessage = (type, body) =>
            ws.send(
                JSON.stringify({
                    type,
                    body: body,
                    date: new Date()
                })
            );
        ws.ip_address = this.resolveIpAddress(request);
        try {
            await this.checkValidClient(ws, request);
        } catch (err) {
            ws.sendMessage(MessageType.ERROR, {
                message: 'origin_is_not_valid',
                data: {}
            });
            ws.close();
            return;
        }

        ws.on('message', async (data: WebSocket.Data) => {
            const message: Message = this.parseMessage(data);
            if (message) {
                this.queue.add(async () => {
                    await this.handleMessage(message, ws);
                });
            }
        });

        ws.on('error', (error) => {
            this.loggerService.error(ACTIONS.WEB_SOCKET, error);
            /*
            Github Issue with ws
            https://github.com/taigaio/taiga-events/issues/42
            */
        });
        ws.on('close', () => {
            // if (!ws?.sessionId) return;
            // this.queue.add(async () => {
            //     const sessionId = ws.sessionId;
            //     const connection = await this.connectionService.getConnectionBySessionId(
            //         sessionId
            //     );
            //     if (connection) {
            //         if (connection.numberOfConnections === 1) {
            //             await this.connectionService.updateExpiry(
            //                 connection,
            //                 60
            //             );
            //         } else {
            //             await this.connectionService.negativeNumberConnections(
            //                 connection
            //             );
            //         }
            //     }
            // });
        });
    };

    private ping(ws) {
        const message: Message = {
            type: MessageType.PING,
            body: {},
            date: new Date()
        };
        ws.send(JSON.stringify(message));
        ws.tm = setTimeout(() => {
            ws.close();
        }, PING_TIME);
    }

    private parseMessage(data: any): Message | null {
        try {
            const jsonData = JSON.parse(data.toString());
            const message: Message = jsonData;
            message.date = new Date();
            return message;
        } catch (e) {
            return null;
        }
    }

    private async handleMessage(message: Message, ws: WebsocketInterface) {
        if (!message.date) {
            message.date = new Date();
        }

        /*eslint-disable */
        switch (message.type) {
            case MessageType.INIT:
                await this.handleInitMessage(message, ws);
                break;
            case MessageType.EVENT:
                await this.handleEvent(message, ws);
                break;
            case MessageType.PONG:
                await this.handlePingEvent(ws);
                break;
            case MessageType.USER_ATTRS:
                await this.handleUserAttrs(message, ws);
                break;
            default:
                break;
        }
        /*eslint-enable */
    }

    private async handleInitMessage(message: Message, ws: WebsocketInterface) {
        const initBody: InitBody = message.body;
        const isValid = this.validation.validate(initBody, InitBodySchema);
        if (typeof isValid !== 'boolean') {
            ws.sendMessage(MessageType.ERROR, {
                message: 'init_is_not_valid',
                data: isValid
            });
            return;
        }
        let sessionId: string = uuid();
        let connection: Connection = {
            sessionId,
            ipAddress: ws.ip_address,
            numberOfConnections: 1,
            channel: initBody.channel?.toLowerCase() || 'browser',
            firebaseToken: initBody.firebase_token,
            device_id: initBody.device_id,
            deviceInfo: initBody.device_info,
            userAttribute: initBody?.user_attr,
            userAgent: initBody?.user_agent,
            referrer: initBody?.referrer,
            tracking_id: initBody?.tracking_id,
            connectionDate: new Date(),
            hostname: this.hostname
        };
        if (initBody?.old_session_id) {
            const oldSessionId = initBody.old_session_id;
            const oldConnection = await this.connectionService.getConnectionBySessionId(
                oldSessionId
            );
            if (oldConnection) {
                if (!oldConnection.userAttribute && connection.userAttribute) {
                    await this.connectionService.addConnection(connection);
                    await this.eventService.userIsOffline(oldConnection);
                    await this.connectionService.removeConnectionBySessionId(
                        oldConnection.sessionId
                    );
                } else {
                    sessionId = oldConnection.sessionId;
                    connection = oldConnection;
                    this.connectionService.addNumberOfConnections(connection);
                }
            } else {
                this.connectionService.addConnection(connection);
            }
        } else {
            this.connectionService.addConnection(connection);
        }
        ws.sessionId = sessionId;
        if (connection.numberOfConnections === 1) {
            this.eventService.userIsOnline(connection);
        }
        ws.sendMessage(MessageType.INIT, {
            session_id: sessionId
        });
        this.ping(ws);
        this.connections[sessionId] = ws;
    }

    private async handlePingEvent(ws) {
        const connection: Connection = await this.connectionService.getConnectionBySessionId(
            ws.sessionId
        );
        if (connection) {
            this.connectionService.updateExpiry(
                connection,
                CONNECTION_EXPAND_TTL
            );
        }
        clearTimeout(ws.tm);
        setTimeout(() => this.ping(ws), PING_TIME);
    }

    async handleUserAttrs(message: Message, ws: WebSocket) {
        const body: InitBody = message.body;
        const { error } = UserAttrValidation.validate(body);
        if (error) {
            const resMessage: Message = {
                type: MessageType.ERROR,
                session_id: message.session_id,
                body: {
                    message: 'user_attrs_not_valid'
                },
                date: new Date()
            };
            ws.send(JSON.stringify(resMessage));
            return;
        }
        const connection = await this.connectionService.getConnectionBySessionId(
            message.session_id
        );
        if (connection) {
            this.eventService.addEvent(connection, 'user_attrs', message);
        }
    }
    private async handleEvent(message: Message, ws: WebSocket) {
        const sessionId = message.session_id;
        const eventName = message.event_name;
        if (!eventName) {
            const resMessage: Message = {
                type: MessageType.ERROR,
                session_id: sessionId,
                body: {
                    message: 'event_name_not_provided'
                },
                date: new Date()
            };
            ws.send(JSON.stringify(resMessage));
        }
        if (!sessionId) {
            const resMessage: Message = {
                type: MessageType.ERROR,
                session_id: sessionId,
                body: {
                    message: 'please_set_init_first'
                },
                date: new Date()
            };
            ws.send(JSON.stringify(resMessage));
        } else {
            const connection = await this.connectionService.getConnectionBySessionId(
                sessionId
            );
            if (connection) {
                this.eventService.addEvent(connection, eventName, message);
            } else {
                const resMessage: Message = {
                    type: MessageType.ERROR,
                    session_id: sessionId,
                    body: {
                        message: 'session_id_is_not_valid'
                    },
                    date: new Date()
                };
                ws.send(JSON.stringify(resMessage));
            }
        }
    }

    private resolveIpAddress(request: IncomingMessage) {
        let ipAddress: string =
            (request.headers['x-forwarded-for'] as string) ||
            request.connection.remoteAddress;
        if (ipAddress.substr(0, 7) === '::ffff:') {
            ipAddress = ipAddress.substr(7);
        }
        return ipAddress;
    }

    private async checkValidClient(ws: WebsocketInterface, request) {
        const channel =
            request.headers?.channel?.toString().toLowerCase() || 'browser';
        if (
            channel === 'browser' &&
            this.configService.get<string>('ENV') !== 'development'
        ) {
            const origin = request.headers.origin;
            const isInWhiteList =
                origin && whiteList.some((address) => origin.includes(address));
            if (!isInWhiteList) {
                throw new Error('origin_is_not_valid');
            }
        }
    }
}
