import { Module } from '@nestjs/common';
import { RedisModule } from '@shared/redis/redis.module';
import { EventService } from './event.service';

@Module({
    imports: [RedisModule],
    providers: [EventService],
    exports: [EventService]
})
export class EventModule {}
