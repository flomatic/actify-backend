import { Injectable } from '@nestjs/common';
import { RedisService } from '@shared/redis/redis.service';
import Connection from 'apps/socket-gateway/src/connection/interfaces/connection.interface';
import { Message, MessageType } from './../socket/interfaces/message';

@Injectable()
export class EventService {
    constructor(readonly redisService: RedisService) {}

    async addEvent(
        connection: Connection,
        name: string,
        message: Message
    ): Promise<void> {
        const messageString: string = JSON.stringify(message);
        const connectionString: string = JSON.stringify(connection);
        await this.redisService.readWriteClient.xadd(
            'event',
            '*',
            'message',
            messageString,
            'connection',
            connectionString
        );
    }

    async userIsOnline(connection: Connection): Promise<void> {
        const connectionString: string = JSON.stringify(connection);
        await this.redisService.readWriteClient.xadd(
            'user_availability',
            '*',
            'connection',
            connectionString,
            'is_online',
            'true'
        );
    }

    async userIsOffline(connection: Connection): Promise<void> {
        const connectionString: string = JSON.stringify(connection);
        await this.redisService.readWriteClient.xadd(
            'user_availability',
            '*',
            'connection',
            connectionString,
            'is_online',
            'false'
        );
        await this.addEvent(connection, 'user_is_offline', {
            type: MessageType.EVENT,
            event_name: 'user_is_offline',
            session_id: connection.sessionId,
            body: {}
        } as Message);
    }
}
