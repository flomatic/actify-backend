import { Module } from '@nestjs/common';
import { ConfigModule } from '@shared/config.module';
import { RedisModule } from '@shared/redis/redis.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SocketModule } from './socket/socket.module';
import { SentryModule } from '@shared/sentry/sentry.module';

@Module({
    imports: [ConfigModule, SocketModule, RedisModule, SentryModule],
    providers: [AppController, AppService],
    controllers: [AppController]
})
export class AppModule {}
