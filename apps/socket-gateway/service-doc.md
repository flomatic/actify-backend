MinCPU: 1
MaxCPU: 2
MinMemory: 2048
MaxMemory:  2048
MaxRequestPerSec: 1000
MinCountInstances:  2
MaxCountInstances: 4
Edge: false
Dependencies: mongo, redis ver 5
HealthCheckAddr: /health
HealthCheckAddrVerbose: /health