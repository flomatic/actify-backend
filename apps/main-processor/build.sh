#!/bin/bash

DEPLOY_ENV=$1
TEAM_NAME=$2

if [ $DEPLOY_ENV == "stg" ]; then

    DOCKER_FILE=${CI_PROJECT_DIR}/Dockerfile.Staging
    IMAGE_NAME=$IMAGE_ADDR_STG_CURRENT

elif [ $DEPLOY_ENV == "prod" ]; then

    DOCKER_FILE=${CI_PROJECT_DIR}/Dockerfile
    IMAGE_NAME=$IMAGE_ADDR_PRD_CURRENT

fi

docker build -t $IMAGE_NAME -f $DOCKER_FILE .
