import { INestApplication } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { RedisService } from '@shared/redis/redis.service';
import { CampaignInterface } from 'apps/main-processor/src/campaign/interface/_campaign.interface';
import { Model } from 'mongoose';
import { AppModule } from './../src/app.module';

describe('CampaignProcessor (e2e)', () => {
    let apps: INestApplication[] = [];

    beforeEach(async () => {
        apps = [];

        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule]
        }).compile();

        const app = moduleFixture.createNestApplication();
        const redisService = app.get<RedisService>('RedisService');

        await redisService.set('CAMPAIGN_HANDLER', null);
    });

    it('Should handle job queues in only one instance', async (done) => {
        for (let i = 0; i <= 3; i++) {
            const moduleFixture: TestingModule = await Test.createTestingModule(
                {
                    imports: [AppModule]
                }
            ).compile();

            const app = moduleFixture.createNestApplication();
            const configService = app.get<any>(ConfigService);

            configService.mockedConfig.CLIENT_NAME = `WORKER_${i}`;

            await app.init();

            apps.push(app);
        }

        const redisService = apps[0].get<RedisService>('RedisService');

        expect(await redisService.get('CAMPAIGN_HANDLER')).toBe('WORKER_0');

        await Promise.all(apps.map((p) => p.close()));

        done();
    });

    it('Should send sms', async (done) => {
        for (let i = 0; i <= 3; i++) {
            const moduleFixture: TestingModule = await Test.createTestingModule(
                {
                    imports: [AppModule]
                }
            ).compile();
            const app = moduleFixture.createNestApplication();
            const configService = app.get<any>(ConfigService);
            configService.mockedConfig.CLIENT_NAME = `WORKER_${i}`;
            await app.init();

            if (i === 0) {
                const campaignModel: Model<CampaignInterface> = app.get(
                    'CampaignModel'
                );
                /* eslint-disable-next-line */
                // const segmentModel: Model<UserSegmentationInterface> = app.get('UserSegmentationModel');
                // await new segmentModel({
                //   name: 'test',
                //   members: {type: 'ALL'},
                //   ciriteria:
                // })

                await new campaignModel({}).save();
            }

            apps.push(app);
        }

        const redisService = apps[0].get<RedisService>('RedisService');
        expect(await redisService.get('CAMPAIGN_HANDLER')).toBe('WORKER_0');
        await apps[0].close();

        setTimeout(async () => {
            expect(await redisService.get('CAMPAIGN_HANDLER')).not.toBe(
                'WORKER_0'
            );
            apps.unshift();
            await Promise.all(apps.map((p) => p.close()));
            done();
        }, 3000);
    }, 10000);

    it('Should switch to second worker', async (done) => {
        for (let i = 0; i <= 3; i++) {
            const moduleFixture: TestingModule = await Test.createTestingModule(
                {
                    imports: [AppModule]
                }
            ).compile();
            const app = moduleFixture.createNestApplication();
            const configService = app.get<any>(ConfigService);
            configService.mockedConfig.CLIENT_NAME = `WORKER_${i}`;
            await app.init();
            apps.push(app);
        }

        const redisService = apps[0].get<RedisService>('RedisService');
        expect(await redisService.get('CAMPAIGN_HANDLER')).toBe('WORKER_0');
        await apps[0].close();
        setTimeout(async () => {
            expect(await redisService.get('CAMPAIGN_HANDLER')).not.toBe(
                'WORKER_0'
            );
            apps.unshift();
            await Promise.all(apps.map((p) => p.close()));
            done();
        }, 3000);
    }, 10000);
});
