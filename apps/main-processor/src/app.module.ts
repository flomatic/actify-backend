import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigModule } from '@shared/config.module';
import { EmailModule } from '@shared/email/email.module';
import { RedisModule } from '@shared/redis/redis.module';
import { SentryModule } from '@shared/sentry/sentry.module';
import { MongoMemoryServer } from 'mongodb-memory-server-core';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BackendJobModule } from 'apps/main-processor/src/backend-job/backend-job.module';
import { SocketParserModule } from './socket-parser/socket-parser.module';

@Module({
    imports: [
        ConfigModule,
        RedisModule,
        EmailModule,
        BackendJobModule,
        ScheduleModule.forRoot(),
        SentryModule,
        MongooseModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => {
                let uri = configService.get('MONGO_URI');
                if (process.env.NODE_ENV === 'test') {
                    const memoryServer = new MongoMemoryServer();
                    uri = await memoryServer.getUri();
                }

                return {
                    uri,
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                    useCreateIndex: true
                };
            },
            inject: [ConfigService]
        }),
        SocketParserModule
    ],
    controllers: [AppController],
    providers: [AppService]
})
export class AppModule {}
