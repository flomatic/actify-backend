import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SessionInterface } from '@shared/models/session.model';
import { getFromToDateInDay } from 'apps/main-processor/src/analytics/helper/date-utils';
import { Model } from 'mongoose';

@Injectable()
export class SessionService {
    constructor(
        @InjectModel('Session')
        private readonly sessionModel: Model<SessionInterface>
    ) {}

    async createNewSession(session) {
        const query = {
            session_id: session.session_id
        };
        const update = {
            $set: session
        };
        const options = { upsert: true };
        return this.sessionModel.collection.updateOne(query, update, options);
    }

    async closeSession(sessionId: string) {
        const query = {
            session_id: sessionId
        };
        const update = {
            $set: { is_online: false, end_date: new Date() }
        };
        const options = { upsert: false };
        return await this.sessionModel.collection.updateOne(
            query,
            update,
            options
        );
    }

    async findSessionBySessionId(sessionId: string) {
        return await this.sessionModel.findOne({ session_id: sessionId });
    }

    async findOnlineCounts() {
        const date = getFromToDateInDay();
        const startHourOfDay = date.from_date;
        return await this.sessionModel
            .find({
                $and: [
                    { is_online: true },
                    { start_date: { $gte: startHourOfDay } }
                ]
            })
            .count();
    }

    async findLastSession({ user_unique_value, user_unique_key, date }) {
        const lastSession = await this.sessionModel
            .findOne({
                user_unique_value,
                user_unique_key,
                start_date: { $lte: new Date(date) }
            })
            .sort({ _id: -1 });

        return lastSession;
    }
}
