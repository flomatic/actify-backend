import { Injectable } from '@nestjs/common';
import { RedisService } from '@shared/redis/redis.service';

@Injectable()
export class AppService {
    constructor(private readonly redisService: RedisService) {}

    checkHealthy() {
        return this.redisService.checkHealth();
    }
}
