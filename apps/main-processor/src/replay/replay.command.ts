import { RedisService } from '@shared/redis/redis.service';
import { Command, Console } from 'nestjs-console';
import redis = require('redis');

@Console()
export class ReplayCommand {
    private client: redis.RedisClient;

    constructor(private readonly redisService: RedisService) {
        this.client = redisService.getReadWriteClient();
    }

    @Command({
        command: 'replay:add_full_snapshot',
        description: 'insert a sample fullsnapshot'
    })
    async create() {
        //
    }

    async addEvent(connection, name: string, message: any): Promise<void> {
        // await this.redisService.client.publish(
        //   'event',
        //   JSON.stringify({ name, ...connection, ...message }),
        // );
        const messageString: string = JSON.stringify(message);
        const connectionString: string = JSON.stringify(connection);
        await this.client.xadd(
            'event',
            '*',
            'message',
            messageString,
            'connection',
            connectionString
        );
    }
}
