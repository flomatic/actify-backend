import type {
    domContentLoadedEvent,
    loadedEvent,
    fullSnapshotEvent,
    incrementalSnapshotEvent,
    metaEvent,
    customEvent,
    styleSheetEvent
} from 'rrweb/typings/types';

export type ReplayEvent = (
    | domContentLoadedEvent
    | loadedEvent
    | fullSnapshotEvent
    | incrementalSnapshotEvent
    | metaEvent
    | customEvent
    | styleSheetEvent
) & { type: EventType };

export enum EventType {
    DomContentLoaded = 0,
    Load = 1,
    FullSnapshot = 2,
    IncrementalSnapshot = 3,
    Meta = 4,
    Custom = 5
}

export type ReplayEventType = {
    tab_session_id: string;
    user_agent: string;
    events: any[];
    url: string;
    context: { [key: string]: any };
};

export {
    domContentLoadedEvent,
    loadedEvent,
    fullSnapshotEvent,
    incrementalSnapshotEvent,
    metaEvent,
    customEvent,
    styleSheetEvent
};
