import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import { ReplayEvent } from '../replay.types';

export interface ReplayInterface extends Document {
    _id: string;
    session_id: string;
    url: string;
    events: ReplayEvent[];
    context: { [key: string]: any };
}

export const ReplaySchema = new mongoose.Schema({
    _id: { type: String },
    session_id: { type: String, index: true },
    user_agent: { type: String },
    url: { type: String, index: true },
    events: {
        type: Array,
        select: false
    },
    context: {
        type: mongoose.Schema.Types.Mixed,
        default: () => ({})
    }
});
