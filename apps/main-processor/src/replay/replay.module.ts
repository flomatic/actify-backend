import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { RedisModule } from '@shared/redis/redis.module';
import { Client as MinioClient } from 'minio';
import { FullSnapshotProcessor } from './processors/fullsnapshot.processor';
import { ReplayCommand } from './replay.command';
import { ReplayService } from './replay.service';
import { ReplaySchema } from './schema/replay.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Replay', schema: ReplaySchema }]),
        RedisModule
    ],
    providers: [
        {
            provide: MinioClient,
            useFactory(configService: ConfigService) {
                const config = {
                    endPoint: configService.get('MINIO_ENDPOINT'),
                    // port: +configService.get('MINIO_PORT') || undefined,
                    useSSL: !!+configService.get('MINIO_SSL'),
                    accessKey: configService.get('MINIO_ACCESS_KEY'),
                    secretKey: configService.get('MINIO_SECRET_KEY')
                };
                return new MinioClient(config);
            },
            inject: [ConfigService]
        },
        ReplayService,
        FullSnapshotProcessor,
        ReplayCommand
    ],
    exports: [ReplayService]
})
export class ReplayModule {}
