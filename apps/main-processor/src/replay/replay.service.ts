import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SessionInterface } from '@shared/models/session.model';
import { Model } from 'mongoose';
import { Event } from '../socket-parser/interfaces/event';
import { FullSnapshotProcessor } from './processors/fullsnapshot.processor';
import { ProcessorInterface } from './processors/processor.interface';
import { EventType, ReplayEvent, ReplayEventType } from './replay.types';
import { ReplayInterface } from './schema/replay.schema';

@Injectable()
export class ReplayService {
    private eventTypeProcessors = new Map<
        EventType,
        ProcessorInterface<ReplayEvent>
    >();

    constructor(
        @InjectModel('Replay')
        private readonly replayModel: Model<ReplayInterface>,
        private readonly fullSnapshotProcessor: FullSnapshotProcessor
    ) {
        this.addEventTypeProcessor(
            EventType.FullSnapshot,
            fullSnapshotProcessor
        );
    }

    async recordEvent(session: SessionInterface, event: Event) {
        const replayEvent = event.body as ReplayEventType;
        const replay = await this.findOrCreateSession(replayEvent, session);

        for (const i in replayEvent.events) {
            replayEvent.events[i] = await this.processEvent(
                replayEvent.events[i]
            );
        }

        if (replay.isNew) {
            replay.events = replayEvent.events;
            await replay.save();
        } else {
            await replay.update({
                $push: { events: { $each: replayEvent.events } }
            });
        }
        return replay;
    }

    async findOrCreateSession(
        replayEvent: ReplayEventType,
        session: SessionInterface
    ) {
        let replay = await this.replayModel.findById(
            replayEvent.tab_session_id
        );
        if (!replay) {
            replay = new this.replayModel({
                _id: replayEvent.tab_session_id,
                session_id: session.id,
                url: replayEvent.url,
                user_agent: replayEvent.user_agent,
                events: []
            });
        }
        return replay;
    }

    private addEventTypeProcessor(
        event: EventType,
        processor: ProcessorInterface<ReplayEvent>
    ) {
        this.eventTypeProcessors.set(event, processor);
    }

    private processEvent(event: ReplayEvent) {
        const processor = this.eventTypeProcessors.get(event.type);
        if (processor) {
            return processor.process(event);
        }
        return Promise.resolve(event);
    }
}
