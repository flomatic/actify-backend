import { ReplayEvent } from '../replay.types';

export interface ProcessorInterface<T extends ReplayEvent> {
    process(event: T): Promise<T>;
}
