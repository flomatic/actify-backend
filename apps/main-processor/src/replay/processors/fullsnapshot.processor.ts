import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import Axios from 'axios';
import { Client as MinioClient } from 'minio';
import { elementNode, NodeType, serializedNodeWithId } from 'rrweb-snapshot';
import { Readable } from 'stream';
import { fullSnapshotEvent } from '../replay.types';
import { ProcessorInterface } from './processor.interface';
import URL = require('url');

const BUCKET_NAME = 'frontend-cdn';

@Injectable()
export class FullSnapshotProcessor
implements ProcessorInterface<fullSnapshotEvent>, OnModuleInit {
    private logger: Logger = new Logger('FullSnapshotProcessor');

    constructor(private readonly minioClient: MinioClient) {}

    async onModuleInit() {
        if ((process as any).console) return;
        await Promise.race([
            (async () => {
                if (!(await this.minioClient.bucketExists(BUCKET_NAME))) {
                    await this.minioClient.makeBucket(BUCKET_NAME, 'ir-asia');
                }
            })(),
            new Promise((resolve) => setTimeout(resolve, 3000))
        ]);
    }

    async process(event: fullSnapshotEvent): Promise<fullSnapshotEvent> {
        this.logger.verbose('Processing fullsnapshot');
        if (event.data.node.type === NodeType.Document) {
            await this.storeAssets(
                event.data.node.childNodes.find(
                    (node) => node.type === NodeType.Element
                )
            );
        }
        return event;
    }

    private async storeAssets(node: serializedNodeWithId) {
        const TAGS = {
            img: ['src'],
            link: ['href'],
            video: ['src'],
            audio: ['src']
            // Scripts should not be stored
            // script: ['src'],
        };

        const promises = [];

        this.walk(node, (node: elementNode) => {
            if (TAGS[node.tagName]) {
                for (const attrKey of TAGS[node.tagName]) {
                    const url = node.attributes[attrKey];
                    if (!url) continue;

                    promises.push(
                        this.downloadFileAndStore(url)
                            .then((cdnURL) => {
                                node.attributes[attrKey] = cdnURL;
                            })
                            .catch((err) => {
                                console.error(err.message);
                            })
                    );
                }
            }
        });
        await Promise.all(promises);
    }

    private walk(node: serializedNodeWithId, callback) {
        if (node.type === NodeType.Element) {
            callback(node);
            for (const childNode of node.childNodes) {
                this.walk(childNode, callback);
            }
        }
    }

    async downloadFileAndStore(url): Promise<string> {
        const parsedURL = URL.parse(url);
        const filePath = `replay-session/assets/${parsedURL.host.replace(
            ':',
            '_'
        )}${parsedURL.pathname}`;

        const stats = await this.minioClient
            .statObject(BUCKET_NAME, filePath)
            .catch(() => null);
        if (
            !stats ||
            Date.now() - stats.lastModified.getTime() > 60 * 60 * 1000
        ) {
            this.logger.verbose('Downloading: ' + url);
            const { stream } = await this.downloadFile(url);

            await this.minioClient.putObject(BUCKET_NAME, filePath, stream);
        }

        return `https://cdn.alibaba.ir/ostorage/frontend-cdn/${filePath}`;
    }

    downloadFile(url): Promise<{ stream: Readable; headers: any }> {
        return Axios.get(url, { timeout: 5000, responseType: 'stream' }).then(
            (res) => ({
                stream: res.data,
                headers: res.headers
            })
        );
    }
}
