import { MongooseModule } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { promises as fs } from 'fs';
import { Client } from 'minio';
import { MongoMemoryServer } from 'mongodb-memory-server-core';
import { resolve } from 'path';
import { fullSnapshotEvent } from 'rrweb/typings/types';
import { FullSnapshotProcessor } from './processors/fullsnapshot.processor';
import { ReplayModule } from './replay.module';

describe('FullsnapshotProcessor', () => {
    let processor: FullSnapshotProcessor;
    let minio: Client;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                // ReplayModule,
                MongooseModule.forRootAsync({
                    useFactory: async () => {
                        const memoryServer = new MongoMemoryServer();
                        const uri = await memoryServer.getUri();
                        return {
                            uri,
                            useNewUrlParser: true,
                            useUnifiedTopology: true,
                            useCreateIndex: true
                        };
                    }
                })
            ]
        }).compile();
        processor = await moduleRef.get<FullSnapshotProcessor>(
            FullSnapshotProcessor
        );
        minio = await moduleRef.get<Client>(Client);
    });

    it('should store assets', async function () {
        const data = (
            await fs.readFile(
                resolve(__dirname, './sample/fullsnapshot-sample.json')
            )
        ).toString();
        const event = JSON.parse(data) as fullSnapshotEvent;
        await processor.process(event);
    });
});
