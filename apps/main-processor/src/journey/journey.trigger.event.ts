import { Channel } from '@entries/channel/enum/channel';
import { User } from '@shared/models/user.model';

export class JourneyTriggerEvent {
    constructor(
        readonly body?: {
            sessionId: string;
            user: User;
            channel: Channel;
            user_id: string;
            body?: any;
            name?: string;
            time: Date;
            type: 'event' | 'connection';
            is_online?: boolean;
            access_from?: string;
        }
    ) {}
}
