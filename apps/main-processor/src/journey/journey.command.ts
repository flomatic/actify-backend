import { JourneyField } from '@entries/journey/model/journey-field';
import { JourneyJob } from './delayed-jobs/journey-job';
import { JourneyTriggerEvent } from './journey.trigger.event';

export class JourneyCommand {
    constructor(
        public destFields: Array<JourneyField>,
        public job: JourneyJob,
        public event?: JourneyTriggerEvent
    ) {}
}
