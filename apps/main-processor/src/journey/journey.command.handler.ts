import { JourneyFieldAction } from '@entries/journey/enum/journey-field-action';
import { JourneyField } from '@entries/journey/model/journey-field';
import { OperationCallback } from '@entries/journey/model/operation-callback';
import { CommandBus, CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { JourneyAnalyticsService } from '../journey-analytics/journey-analytics.service';
import { CallApiHelper } from './helpers/call-api';
import { CheckGlobalVarsHelper } from './helpers/check-global-vars';
import { EventHappenedHelper } from './helpers/event-happened';
import { UserInteractionHelper } from './helpers/user-interaction';
import { TriggedByUserSegmentationHelper } from './helpers/trigged-by-user-segmentation';
import { WaitForEventHelper } from './helpers/wait-for-event';
import { WaitForTimeHelper } from './helpers/wait-for-time';
import { JourneyCommand } from './journey.command';
import mongoose = require('mongoose');

@CommandHandler(JourneyCommand)
export class JourneyCommandHandler implements ICommandHandler<JourneyCommand> {
    constructor(
        private readonly waitForTimeHelper: WaitForTimeHelper,
        private readonly eventHappenedHelper: EventHappenedHelper,
        private readonly callApiHelper: CallApiHelper,
        private readonly checkGlobalVarsHelper: CheckGlobalVarsHelper,
        private readonly userInteraction: UserInteractionHelper,
        private readonly triggedByUserSegmentationHelper: TriggedByUserSegmentationHelper,
        private readonly journeyAnalyticsService: JourneyAnalyticsService,
        private readonly waitForEventHelper: WaitForEventHelper,
        private readonly commandBus: CommandBus
    ) {}

    async execute(command: JourneyCommand): Promise<any> {
        const results: OperationCallback[] = await Promise.all(
            command.destFields.map(async (journeyField) => {
                if (journeyField.name === JourneyFieldAction.WAIT_FOR_EVENT) {
                    return this.waitForEventHelper.waitForEvent(
                        command,
                        journeyField as JourneyField
                    );
                } else if (
                    journeyField.name === JourneyFieldAction.WAIT_FOR_TIME
                ) {
                    return this.waitForTimeHelper.waitForTime(
                        command,
                        journeyField as JourneyField
                    );
                } else if (
                    journeyField.name === JourneyFieldAction.FILTER_WITH_EVENT
                ) {
                    return this.eventHappenedHelper.eventHappened(
                        command,
                        journeyField as JourneyField
                    );
                } else if (journeyField.name === JourneyFieldAction.CALL_API) {
                    return this.callApiHelper.callApi(
                        command,
                        journeyField as JourneyField
                    );
                } else if (
                    journeyField.name ===
                    JourneyFieldAction.FILTER_WITH_API_VARIABLES
                ) {
                    return this.checkGlobalVarsHelper.checkVars(
                        command,
                        journeyField as JourneyField
                    );
                } else if (
                    journeyField.name === JourneyFieldAction.USER_INTERACTION
                ) {
                    return this.userInteraction.interact(
                        command,
                        journeyField as JourneyField
                    );
                } else if (
                    journeyField.name ===
                    JourneyFieldAction.FILTER_WITH_USER_SEGMENTATION
                ) {
                    return this.triggedByUserSegmentationHelper.checkFilter(
                        command,
                        journeyField as JourneyField
                    );
                }
            })
        );

        let hasNextLevel = false;
        let shouldPause = false;
        const destFields: JourneyField[] = [];
        results.forEach((callback) => {
            if (callback.has_next_level) {
                hasNextLevel = true;
                callback?.destFields?.forEach((d) => {
                    if (d) {
                        destFields.push(d);
                    }
                });
            }
            if (callback.state === 'paused') {
                shouldPause = true;
            }
            if (callback.has_next_level) {
                this.journeyAnalyticsService.add(
                    mongoose.Types.ObjectId(
                        command.job.journeyJobModel.journey_id
                    ),
                    {
                        status: 'middle',
                        id: callback.current_id
                    }
                );
            }
        });

        if (destFields.length > 0) {
            await this.commandBus.execute(
                new JourneyCommand(destFields, command.job, command.event)
            );
        }
        if (shouldPause && destFields.length === 0) {
            let timeOut = null;
            results.forEach((result) => {
                if (result.time_out) {
                    if (!timeOut) {
                        timeOut = result.time_out;
                    }
                    result.time_out < timeOut
                        ? (timeOut = result.time_out)
                        : null;
                }
            });
            const newDestFields = command.destFields;
            await command.job.updateCurrentFields(newDestFields, timeOut);
        }

        if (!hasNextLevel) {
            const ids = results.map((callback) => callback.current_id);
            this.journeyAnalyticsService.finish(
                mongoose.Types.ObjectId(command.job.journeyJobModel.journey_id),
                ids
            );
        }

        if (!hasNextLevel && !shouldPause) {
            command.job.finish();
        }
    }
}
