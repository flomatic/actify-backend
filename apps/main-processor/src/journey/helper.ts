import { Channel } from '@entries/channel/enum/channel';
import { Filter } from '@entries/filter/model/filter';
import { KeyVariable } from '@entries/keyvariable/model/keyvariable';
import { SegmentOperationType } from '@entries/segment/enum/segment-operation-type';
import { SegmentCriteria } from '@entries/segment/interface/segment-criteria.interface';
import {
    ActivityLevel,
    DeviceInfoInterface,
    User,
    UserInterface
} from '@shared/models/user.model';

export function checkFilter(filters: Filter[], body: any): boolean {
    if (filters.length === 0) {
        return true;
    }
    const sortedFilters = filters?.sort(
        (filter1, filter2) => filter1.key - filter2.key
    );
    const result: any[] = [];
    sortedFilters?.forEach((filter, index) => {
        const operation = filter.operation as any;
        if (operation.variable) {
            const variable = getValueOfEvent(operation.variable, body);
            if (variable) {
                if (typeof variable === 'string') {
                    if (operation.condition === 'equal') {
                        result[index] = variable === operation.value;
                    } else {
                        result[index] = variable.includes(operation.value);
                    }
                } else {
                    if (operation.condition === 'equal') {
                        result[index] = variable === operation.value;
                    } else if (operation.condition === 'lower_or_equal') {
                        result[index] = variable <= operation.value;
                    } else if (operation.condition === 'greater_or_equal') {
                        result[index] = variable >= operation.value;
                    }
                }
            }
        } else if (operation.operator) {
            result[index] = operation.operator;
        }
    });
    let finalResult = true;
    if (result.length === 1) {
        finalResult = result[0];
    }
    if (result.length === 0) {
        finalResult = false;
    }
    for (let i = 1; i < result.length; i = i + 2) {
        let sumResult: any;
        if (i === 1) {
            sumResult = result[i - 1];
        } else {
            sumResult = finalResult;
        }
        if (result[i] && typeof result[i] === 'string') {
            if (result[i] === 'and') {
                finalResult = sumResult && result[i + 1];
            } else {
                finalResult = sumResult || result[i + 1];
            }
        }
    }
    return finalResult;
}

export function filterMatch(
    filters: Filter[],
    variable: number | string
): boolean {
    if (filters.length === 0) {
        return true;
    }
    const sortedFilters = filters?.sort(
        (filter1, filter2) => filter1.key - filter2.key
    );
    const result: any[] = [];
    sortedFilters?.forEach((filter, index) => {
        const operation = filter.operation as any;
        if (operation.variable) {
            // const variable = getValueOfEvent(operation.variable, body);
            // if (variable) {
            if (typeof variable === 'string') {
                if (operation.condition === 'equal') {
                    result[index] = variable === operation.value;
                } else {
                    result[index] = variable.includes(operation.value);
                }
            } else {
                if (operation.condition === 'equal') {
                    result[index] = variable === operation.value;
                } else if (operation.condition === 'lower_or_equal') {
                    result[index] = variable <= operation.value;
                } else if (operation.condition === 'greater_or_equal') {
                    result[index] = variable >= operation.value;
                }
            }
            // }
        } else if (operation.operator) {
            result[index] = operation.operator;
        }
    });
    let finalResult = true;
    if (result.length === 1) {
        finalResult = result[0];
    }
    if (result.length === 0) {
        finalResult = false;
    }
    for (let i = 1; i < result.length; i = i + 2) {
        let sumResult: any;
        if (i === 1) {
            sumResult = result[i - 1];
        } else {
            sumResult = finalResult;
        }
        if (result[i] && typeof result[i] === 'string') {
            if (result[i] === 'and') {
                finalResult = sumResult && result[i + 1];
            } else {
                finalResult = sumResult || result[i + 1];
            }
        }
    }
    return finalResult;
}

export function checkSegmentationCriteria(
    criteria: SegmentCriteria[],
    user: User,
    channel: Channel
): boolean {
    if (criteria.length === 0) {
        return true;
    }
    const sortedCriteria = criteria?.sort(
        (filter1, filter2) => filter1.key - filter2.key
    );
    const result: any[] = [];
    sortedCriteria?.forEach((c, index) => {
        const operation = c.operation as any;
        if (operation.type) {
            const type: SegmentOperationType = operation.type;
            const value: ActivityLevel | Date | Channel | Location =
                operation.value;
            if (type === SegmentOperationType.ACTIVITY_LEVEL) {
                const activityLevel = value;
                result[index] = user.activity_level === activityLevel;
            } else if (type === SegmentOperationType.CHANNEL) {
                result[index] = (value as Channel) === channel;
            } else if (type === SegmentOperationType.JOINED_DATE) {
                result[index] = (value as Date) === user.create_date;
            } else if (type === SegmentOperationType.LAST_VISITED) {
                result[index] = (value as Date) === user.last_activity;
            } else if (type === SegmentOperationType.LOCATION) {
                const userLocation = value as any;
                result[index] =
                    userLocation.city === user.location.city &&
                    userLocation.country === user.location.country;
            }
        } else if (operation.operator) {
            result[index] = operation.operator;
        }
    });
    let finalResult = true;
    if (result.length === 1) {
        finalResult = result[0];
    }
    if (result.length === 0) {
        finalResult = false;
    }
    for (let i = 1; i < result.length; i = i + 2) {
        let sumResult: any;
        if (i === 1) {
            sumResult = result[i - 1];
        } else {
            sumResult = finalResult;
        }
        if (result[i] && typeof result[i] === 'string') {
            if (result[i] === 'and') {
                finalResult = sumResult && result[i + 1];
            } else {
                finalResult = sumResult || result[i + 1];
            }
        }
    }
    return finalResult;
}

export function checkSegmentationCriteriaFromDevices(
    criteria: SegmentCriteria[],
    user: UserInterface,
    devices: DeviceInfoInterface[]
): boolean {
    const finalResults: boolean[] = [];

    if (criteria.length === 0) {
        return true;
    }

    devices.forEach((device) => {
        // tslint:disable-next-line: no-shadowed-variable
        const result: any[] = [];
        const sortedCriteria = criteria?.sort(
            (filter1, filter2) => filter1.key - filter2.key
        );
        sortedCriteria?.forEach((c, index) => {
            const operation = c.operation as any;
            if (operation.type) {
                const type: SegmentOperationType = operation.type;
                const value: ActivityLevel | Date | Channel | Location =
                    operation.value;
                if (type === SegmentOperationType.ACTIVITY_LEVEL) {
                    const activityLevel = value;
                    result[index] = user.activity_level === activityLevel;
                } else if (type === SegmentOperationType.CHANNEL) {
                    result[index] = (value as Channel) === device.type;
                } else if (type === SegmentOperationType.JOINED_DATE) {
                    result[index] = (value as Date) === user.create_date;
                } else if (type === SegmentOperationType.LAST_VISITED) {
                    result[index] = (value as Date) === user.last_activity;
                } else if (type === SegmentOperationType.LOCATION) {
                    const userLocation = value as any;
                    result[index] =
                        userLocation.city === user.location.city &&
                        userLocation.country === user.location.country;
                }
            } else if (operation.operator) {
                result[index] = operation.operator;
            }
            let finalResult = true;
            if (result.length === 1) {
                finalResult = result[0];
            }
            if (result.length === 0) {
                finalResult = false;
            }
            for (let i = 1; i < result.length; i = i + 2) {
                let sumResult: any;
                if (i === 1) {
                    sumResult = result[i - 1];
                } else {
                    sumResult = finalResult;
                }
                if (result[i] && typeof result[i] === 'string') {
                    if (result[i] === 'and') {
                        finalResult = sumResult && result[i + 1];
                    } else {
                        finalResult = sumResult || result[i + 1];
                    }
                }
            }
            finalResults.push(finalResult);
        });
    });

    return finalResults.some((r) => r);
}

export function getKeyValuesFromApiCall(
    variables: Array<KeyVariable>,
    response: any
): Array<{ variable: string; value: string }> {
    const result: Array<{ variable: string; value: string }> = [];

    variables?.forEach((v) => {
        const tree = v.key.split('.');
        if (tree?.length === 0) {
            const numberInArray = +v.key.match(/\[(.*)]/)?.[1];
            if (numberInArray && !isNaN(numberInArray)) {
                const key = v.key.replace(`[${numberInArray}]`, '');
                if (key) {
                    result.push({
                        variable: v.variable,
                        value: response[key][numberInArray - 1]
                    });
                } else {
                    result.push({
                        variable: v.variable,
                        value: response[numberInArray - 1]
                    });
                }
            } else {
                result.push({
                    variable: v.variable,
                    value: response
                });
            }
        } else {
            function circular(t: any[], b) {
                tree.forEach((node) => {
                    const numberInArray = +node.match(/\[(.*)]/)?.[1];
                    if (numberInArray && !isNaN(numberInArray)) {
                        const key = node.replace(`[${numberInArray}]`, '');
                        if (key) {
                            if (
                                b[key][numberInArray] &&
                                typeof b[key][numberInArray] === 'object'
                            ) {
                                t.shift();
                                circular(t, b[key][numberInArray]);
                            } else {
                                result.push({
                                    variable: v.variable,
                                    value: b[key][numberInArray]
                                });
                            }
                        } else {
                            if (
                                b[numberInArray] &&
                                typeof b[numberInArray] === 'object'
                            ) {
                                t.shift();
                                circular(t, b[numberInArray]);
                            } else {
                                result.push({
                                    variable: v.variable,
                                    value: b[numberInArray]
                                });
                            }
                        }
                    } else {
                        if (b[node] && typeof b[node] === 'object') {
                            t.shift();
                            circular(t, b[node]);
                        } else {
                            result.push({
                                variable: v.variable,
                                value: b[node]
                            });
                        }
                    }
                });
            }
            circular(tree, response);
        }
    });
    return result;
}

export function getValueOfEvent(value: string, body: any) {
    const tree = value.split('.');
    if (tree.length === 0) {
        return body;
    }
    if (tree.length > 1) {
        tree.shift();
    }
    let response = null;
    function circular(t: any[], b) {
        t.forEach((node) => {
            if (b[node] && typeof b[node] === 'object') {
                t.shift();
                circular(t, b[node]);
            } else {
                response = b[node];
            }
        });
    }
    circular(tree, body);
    return response;
}

export async function filterAsync<T>(
    array: T[],
    callbackfn: (value: T, index: number, array: T[]) => Promise<boolean>
): Promise<T[]> {
    const filterMap = await mapAsync(array, callbackfn);
    return array?.filter((value, index) => filterMap[index]);
}

export async function mapAsync<T, U>(
    array: T[],
    callbackfn: (value: T, index: number, array: T[]) => Promise<U>
): Promise<U[]> {
    return Promise.all(array.map(callbackfn));
}

export async function forEachAsync<T>(
    array: T[],
    callback: (value: T, index?: number, array?: T[]) => Promise<void>
): Promise<void> {
    for (let index = 0; index < array?.length; index++) {
        await callback(array[index], index, array);
    }
}
