import { forwardRef, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { JourneyModule } from 'apps/main-processor/src/journey/journey.module';
import { JourneyJobService } from '../journey-job.service';

@Module({
    imports: [CqrsModule, forwardRef(() => JourneyModule)],
    providers: [JourneyJobService],
    exports: [JourneyJobService]
})
export class DelayedJobsModule {}
