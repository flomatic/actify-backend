export enum JobStatus {
    WAIT = 'wait',
    RUNNING = 'running',
    FINISHED = 'finished'
}
