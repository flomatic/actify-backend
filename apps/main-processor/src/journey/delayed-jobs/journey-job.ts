import { JourneyFieldAction } from '@entries/journey/enum/journey-field-action';
import { JourneyField } from '@entries/journey/model/journey-field';
import { JourneyJobModel } from '@entries/journey/model/journey-job';
import Agenda from 'agenda';
import { time } from 'console';
import { timeout } from 'rxjs/operators';
import { getJourneyJobName } from '../helpers/namebuilder';
import { JourneyJobService } from '../journey-job.service';
import { JourneyService } from '../journey.service';

export class JourneyJob {
    journeyJobModel: JourneyJobModel;
    journeyJobService: JourneyJobService;
    job: Agenda.Job<JourneyJobModel>;

    constructor(
        journeyJobModel: JourneyJobModel,
        journeyJobService: JourneyJobService
    ) {
        this.journeyJobModel = journeyJobModel;
        this.journeyJobService = journeyJobService;
        this.addOrFindJob(journeyJobModel).then(() => {
            this.job.attrs.data = this.journeyJobModel;
            this.save();
        });
    }

    async addOrFindJob(journeyJobModel: JourneyJobModel) {
        const job: Agenda.Job<JourneyJobModel> = await this.journeyJobService.findJobByName(
            getJourneyJobName(
                journeyJobModel.journey_id,
                journeyJobModel.user_id
            )
        );
        if (job) {
            this.job = job;
        } else {
            this.job = await this.journeyJobService.addJob(journeyJobModel);
        }
    }

    getCurrentFields(): JourneyField[] {
        return (
            this.journeyJobModel?.current_field_ids?.map((id) => {
                return this.journeyJobModel?.journey?.fields?.find(
                    (f) => f.id === id
                );
            }) || this.getInitialFields()
        );
    }

    getVariablesForFieldId(id: string) {
        const variables = [];
        this.journeyJobModel.variables?.forEach((v) => {
            if (v.key.includes(id)) {
                variables.push(v);
            }
        });
        return variables.map((v) => {
            return {
                key: v.key.replace(`field_id::${id}::`, ''),
                value: v.value
            };
        });
    }

    private getInitialFields() {
        const fields = [];
        this.journeyJobModel.journey.fields?.forEach((f) => {
            if (
                f.name === JourneyFieldAction.GETTING_ONLINE ||
                f.name === JourneyFieldAction.GETTING_OFFLINE
            ) {
                fields.push(f);
            }
        });
        return fields;
    }

    getFieldWithNode(node: string, field: JourneyField) {
        const nextFields = [];
        field.path?.forEach((p) => {
            if (p.from_node.toLowerCase() === node.toLowerCase()) {
                nextFields.push(this.getFieldById(p.to_id));
            }
        });
        return nextFields;
    }

    getNextFieldWithNode(node: string) {
        const fields = this.getNextFields();
        const nextFields = [];
        fields.forEach((f) => {
            if (f.on.toLowerCase() === node.toLowerCase()) {
                nextFields.push(f);
            }
        });
        return nextFields.map((n) => n.field);
    }

    async updateCurrentFields(fields: JourneyField[], timeOut?: Date) {
        this.journeyJobModel.current_field_ids = fields.map((f) => f.id);
        await this.save(timeOut);
    }

    async schedule(date: Date) {
        await this.save(date);
    }

    async finish() {
        const jobName = getJourneyJobName(
            this.journeyJobModel.journey_id,
            this.journeyJobModel.user_id
        );
        await this.journeyJobService.finishJobByName(jobName);
    }

    getNextFields(): Array<{
        on: string;
        field: JourneyField;
    }> {
        const fields: Array<{
            on: string;
            field: JourneyField;
        }> = [];
        const currentFields = this.getCurrentFields();
        currentFields.forEach((cf) => {
            cf.path?.forEach((p) => {
                fields.push({
                    on: p.from_node,
                    field: this.getFieldById(p.to_id)
                });
            });
        });
        const nextFields = [];
        currentFields.forEach((f) => {
            f.path?.forEach((p) =>
                nextFields.push({
                    on: p.from_node,
                    field: this.getFieldById(p.to_id)
                })
            );
        });
        return nextFields;
    }
    getFieldById(id: string): JourneyField {
        return this.journeyJobModel.journey.fields.find((f) => f.id === id);
    }

    isNew() {
        let isNew = true;
        this.getInitialFields()?.forEach((initialField) => {
            const fined = this.getCurrentFields().find(
                (f) => f.id === initialField.id
            );
            if (!fined) {
                isNew = false;
            }
        });
        return isNew;
    }

    getVariable(variable: string, defaultValue: any) {
        return (
            this.journeyJobModel.variables?.find((v) => v.key === variable)
                ?.value || defaultValue
        );
    }

    async setVariable(variable: string, value: any, fieldId: string) {
        // if we use field id we can have duplicate variables.
        let isSet = false;
        variable = `${variable}`;
        this.journeyJobModel.variables?.forEach((v, i) => {
            if (v.key === variable) {
                this.journeyJobModel.variables[i].value = value;
                isSet = true;
            }
        });
        if (!isSet) {
            if (!this.journeyJobModel.variables) {
                this.journeyJobModel.variables = [];
            }
            this.journeyJobModel.variables.push({ key: variable, value });
        }
        await this.save();
    }

    async save(date?: Date) {
        if (this.job) {
            this.job.attrs.data = this.journeyJobModel;
            await this.journeyJobService.updateJob(this.job, date);
        }
    }
}
