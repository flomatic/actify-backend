import { JourneyJobModel } from '@entries/journey/model/journey-job';
import { Injectable } from '@nestjs/common';
import { JourneyJobService } from '../journey-job.service';
import { JourneyJob } from './journey-job';

@Injectable()
export class JourneyJobBuilder {
    constructor(private readonly journeyJobService: JourneyJobService) {}

    build(journeyJobModel: JourneyJobModel) {
        return new JourneyJob(journeyJobModel, this.journeyJobService);
    }
}
