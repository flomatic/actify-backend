import { JourneyFieldAction } from '@entries/journey/enum/journey-field-action';
import { JourneyStatus } from '@entries/journey/enum/journey-status';
import { Journey, JourneyDocument } from '@entries/journey/model/journey';
import { JourneyField } from '@entries/journey/model/journey-field';
import { JourneyJobModel } from '@entries/journey/model/journey-job';
import { UserInteraction } from '@entries/journey/model/user-interaction';
import { CommandBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { JourneyJob } from './delayed-jobs/journey-job';
import { JourneyJobBuilder } from './delayed-jobs/journey-job-builder';
import { forEachAsync } from './helper';
import { GettingOfflineHelper } from './helpers/getting-offline';
import { GettingOnlineHelper } from './helpers/getting-online';
import { HappeningEventHelper } from './helpers/happening-event';
import { getJourneyJobName } from './helpers/namebuilder';
import { TriggedByUserSegmentationHelper } from './helpers/trigged-by-user-segmentation';
import { WaitForEventHelper } from './helpers/wait-for-event';
import { JourneyJobService } from './journey-job.service';
import { JourneyCommand } from './journey.command';
import { JourneyService } from './journey.service';
import { JourneyTriggerEvent } from './journey.trigger.event';

@EventsHandler(JourneyTriggerEvent)
export class JourneyTriggerHandler
implements IEventHandler<JourneyTriggerEvent> {
    constructor(
        private readonly journeyService: JourneyService,
        private readonly triggedByUserSegmentationHelper: TriggedByUserSegmentationHelper,
        private readonly happeningEventHelper: HappeningEventHelper,
        private readonly waitForEventHelper: WaitForEventHelper,
        private readonly gettingOnlineHelper: GettingOnlineHelper,
        private readonly journeyJobService: JourneyJobService,
        private readonly journeyJobBuilder: JourneyJobBuilder,
        private readonly gettingOfflineHelper: GettingOfflineHelper,
        private readonly commandBus: CommandBus
    ) {}

    async eventMatchTrigger(
        event: JourneyTriggerEvent,
        triggerField: JourneyField,
        journey: JourneyDocument,
        job: JourneyJob
    ) {
        if (
            triggerField.name === JourneyFieldAction.WAIT_FOR_EVENT &&
            event.body.type === 'event'
        ) {
            return await this.waitForEventHelper.isMatch(
                triggerField,
                event,
                journey,
                job
            );
        }

        if (triggerField.name === JourneyFieldAction.GETTING_ONLINE) {
            return this.gettingOnlineHelper.isMatch(job, event);
        }

        if (triggerField.name === JourneyFieldAction.GETTING_OFFLINE) {
            return this.gettingOfflineHelper.isMatch(job, event);
        }

        return false;
    }

    async getActiveJourneys() {
        let journeys = await this.journeyService.getAllJourneysSync();
        journeys = journeys.filter(
            (journey) =>
                journey.status === JourneyStatus.RUNNING && !journey.isLocked
        );
        return journeys;
    }

    getTriggerFields(journey: Journey): JourneyField[] {
        return journey.fields.filter(
            (field) =>
                field.name === JourneyFieldAction.GETTING_ONLINE ||
                field.name === JourneyFieldAction.GETTING_OFFLINE ||
                field.name === JourneyFieldAction.WAIT_FOR_EVENT
        );
    }

    getTrackingVariables(journey: Journey): Array<string> {
        const trackingVariables = [];
        journey.fields?.forEach((field: JourneyField) => {
            if (field.name === JourneyFieldAction.USER_INTERACTION) {
                const input = field?.inputs as UserInteraction;
                input?.variables?.forEach((v) => {
                    trackingVariables.push(v.variable);
                });
            }
        });
        return trackingVariables;
    }

    async handle(event: JourneyTriggerEvent) {
        const activeJourneys = await this.getActiveJourneys();
        await forEachAsync(activeJourneys, async (journey) => {
            const jobName = getJourneyJobName(journey._id, event.body.user_id);
            let job = await this.journeyJobService.findJourneyJobByName(
                jobName
            );
            if (!job) {
                const journeyJobModel = new JourneyJobModel();
                journeyJobModel.user_id = event.body.user_id;
                journeyJobModel.user = event.body.user;
                journeyJobModel.journey = journey as Journey;
                journeyJobModel.journey_id = journey._id;
                journeyJobModel.event_body = event.body.body;
                journeyJobModel.track_variables = this.getTrackingVariables(
                    journey
                );
                job = this.journeyJobBuilder.build(journeyJobModel);
            }
            const triggers = this.getTriggerFields(journey);
            let isMatch = false;
            await forEachAsync(triggers, async (t) => {
                const matched = await this.eventMatchTrigger(
                    event,
                    t,
                    journey,
                    job
                );
                if (matched) {
                    isMatch = true;
                }
            });
            if (isMatch) {
                const currentFields = job.getCurrentFields();
                const nextFields = job.getNextFields();
                const nextPossibleFields = this.getPossibleNextFields(
                    currentFields,
                    nextFields
                );
                const fileds = [];
                await Promise.all(
                    nextPossibleFields.map(async (n) => {
                        fileds.push(n.field);
                    })
                );
                // await job.updateCurrentFields(fileds);
                await this.commandBus.execute(
                    new JourneyCommand(fileds, job, event)
                );
            }
        });
    }
    getPossibleNextFields(
        currentFields: JourneyField[],
        nextFields: { on: string; field: JourneyField }[]
    ) {
        const excludeFields = [];
        currentFields.forEach((cField) => {
            if (cField.name === JourneyFieldAction.WAIT_FOR_EVENT) {
                let exclude = false;
                cField.path.forEach((path) => {
                    if (path.from_node === 'no_happened') {
                        exclude = true;
                    }
                });
                if (exclude) {
                    excludeFields.push(cField);
                }
            }
        });
        let possibleFields = nextFields;
        excludeFields.forEach((field) => {
            possibleFields = possibleFields.filter(
                (f) => f.field.id !== field.id
            );
        });
        return possibleFields.map((possibleField) => possibleField);
    }
}
