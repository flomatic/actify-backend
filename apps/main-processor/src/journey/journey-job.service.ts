import { JourneyFieldAction } from '@entries/journey/enum/journey-field-action';
import { JourneyJobModel } from '@entries/journey/model/journey-job';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { CommandBus } from '@nestjs/cqrs';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { JourneyService } from 'apps/main-processor/src/journey/journey.service';
import { JourneyJob } from './delayed-jobs/journey-job';
import { JourneyJobBuilder } from './delayed-jobs/journey-job-builder';
import { getJourneyJobName } from './helpers/namebuilder';
import { JourneyCommand } from './journey.command';
import Agenda = require('agenda');
import { timeout } from 'rxjs/operators';

@Injectable()
export class JourneyJobService {
    agenda: Agenda;
    worker: string;
    handler: (
        job: Agenda.Job<JourneyJobModel>,
        done: (error?: Error) => void
    ) => void;

    constructor(
        private readonly journeyService: JourneyService,
        private readonly configService: ConfigService,
        private readonly logger: BLogger,
        private readonly commandBus: CommandBus
    ) {
        this.agenda = new Agenda({
            db: {
                address: this.configService.get('MONGO_AGENDA_URL'),
                collection: 'JourneyJobs'
            }
        });
        this.agenda.start();
        this.worker = this.configService.get('HOSTNAME') || '';
    }

    async findJobs(params): Promise<Agenda.Job<any>[]> {
        return this.agenda.jobs(params);
    }

    async findJobByName(name: string): Promise<Agenda.Job<any>> {
        return (await this.findJobs({ name }))?.[0];
    }

    async findJourneyJobByName(name: string): Promise<any> {
        const job = await this.findJobByName(name);
        if (job) {
            return new JourneyJobBuilder(this).build(job.attrs.data);
        }
        return null;
    }

    async finishJobByName(jobName: string) {
        const job: Agenda.Job = (await this.findJobs({ name: jobName }))?.[0];
        if (job) {
            await job.remove();
        }
    }

    async updateJob(job: Agenda.Job<JourneyJobModel>, timeOut?: Date) {
        if (timeOut) {
            job.schedule(timeOut);
        }
        await job.save();
        return job;
    }

    async addJob(
        jobModel: JourneyJobModel
    ): Promise<Agenda.Job<JourneyJobModel>> {
        const jobName = getJourneyJobName(
            jobModel.journey_id,
            jobModel.user_id
        );
        this.defineJob(jobName);
        const job: Agenda.Job<JourneyJobModel> = this.agenda.create(
            jobName,
            jobModel
        );
        job.schedule(jobModel.timeout);
        await job.save();
        return job;
    }

    async defineJob(jobName: string) {
        this.agenda.define(
            jobName,
            async (
                job: Agenda.Job<JourneyJobModel>,
                done: (error?: Error) => void
            ) => {
                try {
                    const journeyJobModel: JourneyJobModel = job.attrs.data;
                    const journeyId = journeyJobModel.journey_id;
                    const journey = (
                        await this.journeyService.getAllJourneysSync()
                    ).find((x) => x._id.toString() === journeyId.toString());
                    if (journey) {
                        const journeyJob = new JourneyJobBuilder(this).build(
                            journeyJobModel
                        );
                        const currentFields = journeyJob.getCurrentFields();
                        const fields = [];
                        currentFields.forEach((currentField) => {
                            /* eslint-disable */
                            switch (currentField.name) {
                                case JourneyFieldAction.WAIT_FOR_TIME:
                                    fields.push(
                                        ...journeyJob.getFieldWithNode(
                                            'default',
                                            currentField
                                        )
                                    );
                                case JourneyFieldAction.USER_INTERACTION:
                                    fields.push(
                                        ...journeyJob.getFieldWithNode(
                                            'failed',
                                            currentField
                                        )
                                    );
                                case JourneyFieldAction.WAIT_FOR_EVENT:
                                    fields.push(
                                        ...journeyJob.getFieldWithNode(
                                            'not_happened',
                                            currentField
                                        )
                                    );
                            }
                            /* eslint-enable */
                        });
                        if (fields.length > 0) {
                            await this.commandBus.execute(
                                new JourneyCommand(fields, journeyJob)
                            );
                        }
                        done();
                    }
                } catch (e) {
                    this.logger.error(ACTIONS.DELAYED_JOB_SERVICE, e);
                }
            }
        );
    }

    async addServerEventJob(params): Promise<Agenda.Job<JourneyJobModel>> {
        // TODO: Coming soon
        // if (!params.timeout_date) {
        //     params.timeout_date = new Date(new Date().getTime() + 12 * 30 * 24 * 60 * 60 * 1000);
        // }
        // const jobName =
        //     this.worker +
        //     '::' +
        //     'delayed_job' +
        //     '::' +
        //     params.journey_id +
        //     '_' +
        //     params.uuk +
        //     '_' +
        //     params.uuv;
        // this.defineJob(jobName);
        // const job: Agenda.Job = this.agenda.create(jobName, {
        //     states: params.states,
        //     variables: params.variables,
        //     user_attr: { uuk: params.uuk, uuv: params.uuv },
        //     journey_id: params.journey_id
        // });
        // job.schedule(params.timeout_date);
        // await job.save();
        // return job as Agenda.Job<DelayedJobsInterface>;
        return null;
    }

    async cancelJobsForAJourney(journeyId: string): Promise<number> {
        const response: number = await this.agenda.cancel({
            name: { $regex: journeyId }
        });
        return response;
    }
}
