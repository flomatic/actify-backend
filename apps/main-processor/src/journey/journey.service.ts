import { Channel } from '@entries/channel/enum/channel';
import { JourneyFieldAction } from '@entries/journey/enum/journey-field-action';
import { JourneyDocument } from '@entries/journey/model/journey';
import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { EventBus } from '@nestjs/cqrs';
import { InjectModel } from '@nestjs/mongoose';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { SessionInterface } from '@shared/models/session.model';
import { User } from '@shared/models/user.model';
import { UserService } from '@shared/user/user.service';
import { Model, Types } from 'mongoose';
import Connection from '../socket-parser/interfaces/connection';
import { Event } from '../socket-parser/interfaces/event';
import { JourneyTriggerEvent } from './journey.trigger.event';

@Injectable()
export class JourneyService implements OnModuleInit, OnModuleDestroy {
    journeys: JourneyDocument[];
    timer: any;
    constructor(
        @InjectModel('Journey')
        private readonly journeyModel: Model<JourneyDocument>,
        private readonly eventBus: EventBus,
        private readonly userService: UserService,
        private readonly logger: BLogger
    ) {}
    onModuleDestroy() {
        clearTimeout(this.timer);
    }

    async onModuleInit() {
        this.journeys = await this.getAllJourneys();
        this.timer = setTimeout(async () => {
            await this.onModuleInit();
        }, 3000);
    }

    async createJourney(journeys: any[]) {
        await Promise.all(
            journeys.map(async (journey) => {
                await this.journeyModel.updateOne(
                    {
                        name: journey.name
                    },
                    {
                        $set: journey
                    },
                    {
                        upsert: true
                    }
                );
            })
        );
    }

    async getAllJourneys(): Promise<JourneyDocument[]> {
        try {
            return await this.journeyModel.find({}, { __v: 0 });
        } catch (err) {
            this.logger.error(ACTIONS.JOURNEY_SERVICE, err);
        }
    }

    async getAllJourneysSync(): Promise<JourneyDocument[]> {
        if (!this.journeys) {
            this.journeys = await this.getAllJourneys();
        }
        return this.journeys;
    }

    async getAJourney(id: number): Promise<JourneyDocument> {
        try {
            const result = await this.journeyModel.findById(id, { __v: 0 });
            return result;
        } catch (err) {
            this.logger.error(ACTIONS.JOURNEY_SERVICE, err);
        }
    }

    async handleJourneys(event: Event, session: SessionInterface) {
        const user = await this.userService.getUserByKeyValue({
            uuv: session.user_unique_value,
            uuk: session.user_unique_key
        });
        return this.eventBus.publish(
            new JourneyTriggerEvent({
                sessionId: session?.session_id,
                user: user as User,
                channel: session.channel as Channel,
                user_id: user._id,
                body: event.body,
                name: event.event_name,
                time: new Date(),
                type: 'event'
            })
        );
    }

    async handleJourneysForConnection(connection: Connection) {
        const user = await this.userService.getUserByKeyValue({
            uuv: connection.userAttribute.value,
            uuk: connection.userAttribute.key
        });
        return this.eventBus.publish(
            new JourneyTriggerEvent({
                sessionId: connection.sessionId,
                channel: connection.channel as Channel,
                user: user as User,
                user_id: user._id,
                body: null,
                name: connection.isOnline
                    ? JourneyFieldAction.GETTING_ONLINE
                    : JourneyFieldAction.GETTING_OFFLINE,
                type: 'connection',
                time: new Date(),
                access_from: connection.channel
            })
        );
    }

    async findJourneyById(id: Types.ObjectId): Promise<JourneyDocument> {
        return await this.journeyModel.findById(id);
    }
}
