import { JourneyModelModule } from '@entries/journey/model/journey';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { UserModule } from '@shared/user/user.module';
import { VariableReplaceModule } from '@shared/variable-replace/variable-replace.module';
import { MessageTemplateModule } from 'apps/api/src/message-template/message-template.module';
import { JourneyAnalyticsModule } from '../journey-analytics/journey-analytics.module';
import { NotificationModule } from '../notification/notification.module';
import { EventModule } from './../event/event.module';
import { SessionModule } from './../session/session.module';
import { JourneyJobBuilder } from './delayed-jobs/journey-job-builder';
import { DelayedJobsModule } from './delayed-jobs/journey-job.module';
import { CallApiHelper } from './helpers/call-api';
import { CheckGlobalVarsHelper } from './helpers/check-global-vars';
import { EventHappenedHelper } from './helpers/event-happened';
import { GettingOfflineHelper } from './helpers/getting-offline';
import { GettingOnlineHelper } from './helpers/getting-online';
import { HappeningEventHelper } from './helpers/happening-event';
import { UserInteractionHelper } from './helpers/user-interaction';
import { TriggedByUserSegmentationHelper } from './helpers/trigged-by-user-segmentation';
import { WaitForEventHelper } from './helpers/wait-for-event';
import { WaitForTimeHelper } from './helpers/wait-for-time';
import { JourneyJobService } from './journey-job.service';
import { JourneyCommand } from './journey.command';
import { JourneyCommandHandler } from './journey.command.handler';
import { JourneyService } from './journey.service';
import { JourneyTriggerHandler } from './journey.trigger.handler';
import { SegmentModule } from '@shared/segment/segment.module';

@Module({
    imports: [
        CqrsModule,
        UserModule,
        JourneyModelModule,
        EventModule,
        SegmentModule,
        SessionModule,
        NotificationModule,
        VariableReplaceModule,
        MessageTemplateModule,
        JourneyAnalyticsModule,
        DelayedJobsModule
    ],
    providers: [
        JourneyCommand,
        JourneyService,
        JourneyTriggerHandler,
        JourneyCommandHandler,
        HappeningEventHelper,
        CheckGlobalVarsHelper,
        JourneyJobService,
        EventHappenedHelper,
        GettingOfflineHelper,
        GettingOnlineHelper,
        JourneyJobBuilder,
        CallApiHelper,
        TriggedByUserSegmentationHelper,
        UserInteractionHelper,
        WaitForTimeHelper,
        WaitForEventHelper,
        WaitForEventHelper
    ],
    exports: [
        JourneyCommand,
        JourneyService,
        JourneyJobService,
        TriggedByUserSegmentationHelper
    ]
})
export class JourneyModule {}
