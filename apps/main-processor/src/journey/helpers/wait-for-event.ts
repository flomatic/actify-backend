import { JourneyDocument } from '@entries/journey/model/journey';
import { JourneyField } from '@entries/journey/model/journey-field';
import { OperationCallback } from '@entries/journey/model/operation-callback';
import { WaitForEvent } from '@entries/journey/model/wait-for-event';
import { Injectable } from '@nestjs/common';
import { getDateFromTimeOut } from '@shared/utils/date-utils';
import { timeout } from 'rxjs/operators';
import { JourneyJob } from '../delayed-jobs/journey-job';
import { checkFilter } from '../helper';
import { JourneyJobService } from '../journey-job.service';
import { JourneyCommand } from './../journey.command';
import { JourneyTriggerEvent } from './../journey.trigger.event';
import { getNumberOfHappening } from './namebuilder';

@Injectable()
export class WaitForEventHelper {
    constructor(private readonly delayedJobService: JourneyJobService) {}

    async isMatch(
        journeyField: JourneyField,
        event: JourneyTriggerEvent,
        journey: JourneyDocument,
        job: JourneyJob
    ) {
        const waitForEvent = journeyField.inputs as WaitForEvent;
        const numberOfRepeatEvent = waitForEvent.number_of_happening || 1;
        if (waitForEvent.event_name !== event.body.name) {
            return false;
        }
        if (!checkFilter(waitForEvent.filters, event.body.body)) {
            return false;
        }

        if (numberOfRepeatEvent === 1) {
            return true;
        }
        const numberOfHappeningKey = getNumberOfHappening(journeyField.id);
        const totalHappenedSofar: number = job.getVariable(
            numberOfHappeningKey,
            0
        );
        if (totalHappenedSofar > numberOfRepeatEvent) {
            return true;
        }
        job.setVariable(
            numberOfHappeningKey,
            totalHappenedSofar + 1,
            journeyField.id
        );
        job.setVariable(
            waitForEvent.event_name,
            event.body.body,
            journeyField.id
        );
        return false;
    }

    public async waitForEvent(
        command: JourneyCommand,
        journeyField: JourneyField
    ): Promise<OperationCallback> {
        const timeOut = (journeyField.inputs as WaitForEvent)?.time_out;
        const numberOfHappeningKey = getNumberOfHappening(journeyField.id);
        const job = command.job;
        job.setVariable(numberOfHappeningKey, 0, journeyField.id);
        return {
            state: 'paused',
            time_out:
                getDateFromTimeOut(timeOut) ||
                new Date(new Date().getTime() + 30 * 24 * 60 * 60 * 1000),
            has_next_level: journeyField.path?.length > 0 ? true : false,
            current_id: journeyField.id,
            destFields: []
        } as OperationCallback;
    }
}
