import { JourneyDocument } from '@entries/journey/model/journey';
import { JourneyField } from '@entries/journey/model/journey-field';
import { Injectable } from '@nestjs/common';
import { JourneyJob } from '../delayed-jobs/journey-job';
import { JourneyTriggerEvent } from '../journey.trigger.event';
import { JourneyAnalyticsService } from './../../journey-analytics/journey-analytics.service';

@Injectable()
export class GettingOfflineHelper {
    constructor(
        private readonly journeyAnalyticsService: JourneyAnalyticsService
    ) {}
    public async isMatch(job: JourneyJob, event: JourneyTriggerEvent) {
        if (event.body.name === 'getting_offline') {
            return job.isNew() ? true : false;
        }
        return false;
    }
}
