import { FilterWithEvent } from '@entries/journey/model/filter-with-event';
import { JourneyField } from '@entries/journey/model/journey-field';
import { OperationCallback } from '@entries/journey/model/operation-callback';
import { Injectable } from '@nestjs/common';
import { UserService } from '@shared/user/user.service';
import { Types } from 'mongoose';
import { checkFilter } from '../helper';
import { JourneyCommand } from '../journey.command';
import { EventService } from './../../event/event.service';
import { JourneyService } from './../journey.service';

@Injectable()
export class EventHappenedHelper {
    constructor(
        private readonly eventService: EventService,
        private readonly journeyService: JourneyService,
        private readonly userService: UserService
    ) {}
    async eventHappened(
        command: JourneyCommand,
        journeyField: JourneyField
    ): Promise<OperationCallback> {
        const eventInput = journeyField.inputs as FilterWithEvent;
        const date = (journeyField.inputs as FilterWithEvent).date;
        const user = await this.userService.findUserById(
            Types.ObjectId(command.job.journeyJobModel.user_id)
        );
        const events = await this.eventService.findEvents(
            {
                uuk: user.user_unique_key,
                uuv: user.user_unique_value,
                eventName: eventInput.event_name
                // time: {$gte: date., $lte: }
            },
            10
        );
        let numberOfHappening = 0;
        events.forEach((event) => {
            const isMatch = checkFilter(eventInput.filters, event.body);
            if (isMatch) {
                numberOfHappening++;
                command.job.setVariable(
                    eventInput.event_name,
                    event.body.body,
                    journeyField.id
                );
            }
        });
        if (numberOfHappening >= (eventInput.number_of_happening || 1)) {
            const destFields = command.job.getFieldWithNode(
                'happened',
                journeyField
            );
            return {
                state: 'passed',
                status: 'happened',
                current_id: journeyField.id,
                has_next_level: journeyField.path.length > 0 ? true : false,
                destFields
            } as OperationCallback;
        } else {
            const destFields = command.job.getFieldWithNode(
                'not_happened',
                journeyField
            );
            return {
                state: 'passed',
                status: 'not_happened',
                current_id: journeyField.id,
                time_out: null,
                has_next_level: journeyField.path.length > 0 ? true : false,
                destFields
            } as OperationCallback;
        }
    }
}
