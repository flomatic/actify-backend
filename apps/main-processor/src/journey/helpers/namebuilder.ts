export const getJourneyJobName = (journeyId: string, userId: string) => {
    return `journey::${journeyId}::user_id::${userId}`;
};

export const getNumberOfHappening = (fieldId: string) => {
    return `field::${fieldId}::number_of_happening`;
};
