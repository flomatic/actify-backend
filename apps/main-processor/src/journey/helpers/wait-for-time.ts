import { JourneyField } from '@entries/journey/model/journey-field';
import { OperationCallback } from '@entries/journey/model/operation-callback';
import { WaitForTime } from '@entries/journey/model/wait-for-time';
import { Injectable } from '@nestjs/common';
import { getDateFromTimeOut } from '@shared/utils/date-utils';
import { JourneyJobService } from '../journey-job.service';
import { JourneyCommand } from '../journey.command';

@Injectable()
export class WaitForTimeHelper {
    constructor(private readonly jobService: JourneyJobService) {}

    async waitForTime(
        command: JourneyCommand,
        journeyField: JourneyField
    ): Promise<OperationCallback> {
        const timeOut = (journeyField.inputs as WaitForTime).time_out;
        command.job.schedule(getDateFromTimeOut(timeOut));
        // if (command.job) {
        //     command.job.attrs.data.states.push({
        //         current_id: journeyField.id,
        //         variables: []
        //     });
        //     await command.job.save();
        // } else {
        //     command.job = await this.delayedJobService.addJob({
        //         journey_id: command.journeyId,
        //         timeout_date: getDateFromTimeOut(timeOut),
        //         uuk: command.event.body.uuk,
        //         uuv: command.event.body.uuv,
        //         states: [
        //             {
        //                 current_id: journeyField.id,
        //                 variables: []
        //             }
        //         ],
        //         variables: []
        //     });
        // }
        return {
            state: 'paused',
            has_next_level: journeyField.path?.length > 0 ? true : false,
            time_out: getDateFromTimeOut(timeOut),
            current_id: journeyField.id,
            destFields: []
        } as OperationCallback;
    }
}
