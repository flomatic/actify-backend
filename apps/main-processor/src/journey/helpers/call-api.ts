import { HttpMethod } from '@entries/httpmethod/enum/httpmethod';
import { CallApi } from '@entries/journey/model/call-api';
import { JourneyField } from '@entries/journey/model/journey-field';
import { OperationCallback } from '@entries/journey/model/operation-callback';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';
import { getKeyValuesFromApiCall } from '../helper';
import { JourneyCommand } from '../journey.command';
import { JourneyService } from './../journey.service';
import util = require('util');
import xmlToJson = require('xml-to-json-stream');
import { VariableReplaceHelper } from '@shared/variable-replace/variable-replace.helper';
import { User } from '@shared/models/user.model';

@Injectable()
export class CallApiHelper {
    constructor(
        private readonly journeyService: JourneyService,
        private readonly configService: ConfigService,
        private readonly variableHelper: VariableReplaceHelper
    ) {
        axios.defaults.timeout = 10000;
    }
    async callApi(
        command: JourneyCommand,
        journeyField: JourneyField
    ): Promise<OperationCallback> {
        const params = journeyField.inputs as CallApi;
        const replacedParams = this.variableHelper.replaceCallApi(
            params,
            command.job.journeyJobModel.user as User,
            command.job
        );
        const data = {};
        const headers = {};
        const query = {};
        replacedParams.headers?.forEach(
            (header) => (headers[header.key] = header.value)
        );
        replacedParams.data?.forEach(
            (param) => (data[param.key] = param.value)
        );
        replacedParams.query?.forEach(
            (param) => (data[param.key] = param.value)
        );
        const options: any = {
            method: replacedParams.method.toLocaleLowerCase(),
            url: replacedParams.url,
            headers,
            timeout: 3000
        };
        if (replacedParams.method === HttpMethod.POST) {
            options.data = data;
        }
        options.query = query;
        try {
            const response = await axios(options);
            let responseData = response.data;
            const contentType = response.headers['content-type'];
            if (contentType) {
                if (contentType.includes('text/xml')) {
                    const parser = xmlToJson({
                        attributeMode: false
                    });
                    const cPromise = util.promisify(parser.xmlToJson);
                    responseData = await cPromise(responseData);
                }
                if (contentType.includes('text/plain')) {
                    responseData = null;
                }
            }
            if (responseData) {
                const variableValues: Array<{
                    variable: string;
                    value: string;
                }> = getKeyValuesFromApiCall(
                    replacedParams.variables,
                    responseData
                );
                variableValues.forEach((v) => {
                    command.job.setVariable(
                        v.variable,
                        v.value,
                        journeyField.id
                    );
                });
                const destFields = command.job.getFieldWithNode(
                    'success',
                    journeyField
                );
                return {
                    state: 'passed',
                    status: 'success',
                    current_id: journeyField.id,
                    has_next_level: journeyField.path.length > 0 ? true : false,
                    destFields
                } as OperationCallback;
            } else {
                const destFields = command.job.getFieldWithNode(
                    'failed',
                    journeyField
                );
                return {
                    state: 'passed',
                    status: 'failed',
                    current_id: journeyField.id,
                    has_next_level: journeyField.path.length > 0 ? true : false,
                    destFields
                } as OperationCallback;
            }
        } catch (e) {
            const destFields = command.job.getFieldWithNode(
                'failed',
                journeyField
            );
            return {
                state: 'passed',
                status: 'failed',
                current_id: journeyField.id,
                has_next_level: journeyField.path.length > 0 ? true : false,
                destFields
            } as OperationCallback;
        }
    }
}
