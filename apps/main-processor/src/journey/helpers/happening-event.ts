import { JourneyDocument } from '@entries/journey/model/journey';
import { JourneyField } from '@entries/journey/model/journey-field';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JourneyAnalyticsService } from '../../journey-analytics/journey-analytics.service';
import { JourneyTriggerEvent } from './../journey.trigger.event';

import Agenda = require('agenda');

@Injectable()
export class HappeningEventHelper {
    constructor(
        private readonly journeyAnalyticsService: JourneyAnalyticsService,
        private readonly configService: ConfigService
    ) {}
    async isMatch(
        journeyField: JourneyField,
        event: JourneyTriggerEvent,
        journey: JourneyDocument
    ) {
        // const happeningEvent = journeyField.inputs as FilterWithEvent;
        // if (happeningEvent.event_name !== event.body.name) {
        //     return false;
        // }
        // const numberOfRepeatEvent = happeningEvent.number_of_happening;
        // if (checkFilter(happeningEvent.filters, event.body.body)) {
        //     if (job) {
        //         const currentState = job?.attrs?.data?.states.find(
        //             (s) => s.current_id === journeyField.id
        //         );
        //         if (currentState) {
        //             const numberOfHappeningEvent = currentState.variables?.find(
        //                 (variable) => variable.key === 'number_of_happening'
        //             )?.value;
        //             if (
        //                 ((parseInt(numberOfHappeningEvent, 10) + 1) as number) ===
        //                 numberOfRepeatEvent
        //             ) {
        //                 job.attrs.data.states = job.attrs.data.states.filter(
        //                     (s) => s.current_id !== journeyField.id
        //                 );
        //                 await job.save();
        //                 await this.journeyAnalyticsService.add(journey._id, {
        //                     status: 'started',
        //                     id: journeyField.id
        //                 });
        //                 return true;
        //             } else {
        //                 job.attrs.data.states
        //                     .find((s) => s.current_id === journeyField.id)
        //                     .variables.find((variable) => variable.key === 'number_of_happening')
        //                     .value++;
        //                 await job.save();
        //                 return false;
        //             }
        //         } else {
        //             return false;
        //         }
        //     } else {
        //         if (numberOfRepeatEvent > 1) {
        //             await this.delayedJobService.addJob({
        //                 journey_id: journey._id.toString(),
        //                 timeout_date: null,
        //                 uuk: event.body.uuk,
        //                 uuv: event.body.uuv,
        //                 states: [
        //                     {
        //                         current_id: journeyField.id,
        //                         variables: [
        //                             {
        //                                 key: 'number_of_happening',
        //                                 value: 1
        //                             }
        //                         ]
        //                     }
        //                 ],
        //                 variables: []
        //             });
        //             return false;
        //         } else {
        //             await this.journeyAnalyticsService.add(journey._id, {
        //                 status: 'started',
        //                 id: journeyField.id
        //             });
        //             return true;
        //         }
        //     }
        // }
    }
}
