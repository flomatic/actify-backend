import { JourneyField } from '@entries/journey/model/journey-field';
import { OperationCallback } from '@entries/journey/model/operation-callback';
import { UserInteraction } from '@entries/journey/model/user-interaction';
import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { MessageTemplateType } from '@shared/models/message-template.model';
import { UserInterface } from '@shared/models/user.model';
import { UserService } from '@shared/user/user.service';
import { VariableReplaceHelper } from '@shared/variable-replace/variable-replace.helper';
import { MessageTemplateService } from 'apps/api/src/message-template/message-template.service';
import { stat } from 'fs';
import { Types } from 'mongoose';
import { NotificationStatus } from '../../notification/interface/notification-status';
import { NotificationService } from '../../notification/notification.service';
import { JourneyJobService } from '../journey-job.service';
import { JourneyCommand } from '../journey.command';
import { getJourneyJobName } from './namebuilder';

@Injectable()
export class UserInteractionHelper {
    constructor(
        private readonly userService: UserService,
        private readonly variableHelper: VariableReplaceHelper,
        private readonly journeyJobService: JourneyJobService,
        private readonly notificationService: NotificationService,
        private readonly templateService: MessageTemplateService,
        private readonly commandBus: CommandBus
    ) {
        this.notificationService.notificationStatusChanged(
            this.onStatusChanged
        );
    }

    async interact(
        command: JourneyCommand,
        journeyField: JourneyField
    ): Promise<OperationCallback> {
        const user: UserInterface = await this.userService.findUserById(
            Types.ObjectId(command.job.journeyJobModel.user_id)
        );
        const userInteraction = journeyField.inputs as UserInteraction;
        const templateId = userInteraction.template_id;
        const template = await this.templateService.findById(
            Types.ObjectId(templateId)
        );
        if (!template) {
            throw new Error('template was not found');
        }

        let body =
            template.type === MessageTemplateType.EMAIL
                ? (template.data as any).html
                : (template.data as any).body;

        body = await this.variableHelper.getVariablesForTemplate(
            template._id,
            user,
            null,
            command.job,
            userInteraction
        );
        // tslint:disable-next-line: no-console
        try {
            const token = `journey::${command.job.journeyJobModel.journey_id}
            ::user_id::${command.job.journeyJobModel.user_id}`;
            const group = `journey::${command.job.journeyJobModel.journey_id}`;
            const time_out = new Date(
                new Date().getTime() + 7 * 24 * 60 * 60 * 1000
            );
            const retry = {
                in: 5,
                next: 'hour'
            };
            const userId = Types.ObjectId(command.job.journeyJobModel.user_id);
            const notification = {
                body,
                payload: template.data
            };
            const type = template.type;
            const ignoreDnd = command.job.journeyJobModel.journey.ignore_dnd;
            const rateLimit = command.job.journeyJobModel.journey.rate_limit;
            this.notificationService.sendNotification({
                group_name: group,
                token,
                time_out,
                notification,
                retry: retry as any,
                user_id: userId,
                type,
                ignore_sleep_time: ignoreDnd,
                rate_limit: rateLimit
            });
            return {
                state: 'paused',
                has_next_level: journeyField.path?.length > 0 ? true : false,
                current_id: journeyField.id,
                time_out: new Date(
                    new Date().getTime() + 7 * 24 * 60 * 60 * 1000
                ),
                destFields: []
            } as OperationCallback;
        } catch (e) {
            const destFields = command.job.getFieldWithNode(
                'failed',
                journeyField
            );
            return {
                state: 'passed',
                status: 'failed',
                current_id: journeyField.id,
                has_next_level: journeyField.path?.length > 0 ? true : false,
                destFields
            } as OperationCallback;
        }
    }

    onStatusChanged = async (token: string, status: NotificationStatus) => {
        const splitToken = token.split('::');
        const journeyId = splitToken[1];
        const userId = splitToken[3];
        let journeyJobName = getJourneyJobName(journeyId, userId);
        journeyJobName = journeyJobName.replace(/\s/g, '');
        const job = await this.journeyJobService.findJourneyJobByName(
            journeyJobName
        );
        if (job) {
            const destFields = job.getNextFieldWithNode(
                status === NotificationStatus.SUCCESS
                    ? 'sent'
                    : status === NotificationStatus.INTERACTED
                        ? 'interacted'
                        : 'failed'
            );
            if (destFields.length > 0) {
                await this.commandBus.execute(
                    new JourneyCommand(destFields, job)
                );
            } else {
                await job.finish();
            }
        }
    };
}
