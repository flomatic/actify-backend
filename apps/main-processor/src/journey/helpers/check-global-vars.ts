import { FilterWithApiVariables } from '@entries/journey/model/filter-with-api-vars';
import { JourneyField } from '@entries/journey/model/journey-field';
import { OperationCallback } from '@entries/journey/model/operation-callback';
import { Injectable } from '@nestjs/common';
import { filterMatch } from '../helper';
import { JourneyCommand } from '../journey.command';
import { JourneyService } from './../journey.service';

@Injectable()
export class CheckGlobalVarsHelper {
    constructor(private readonly journeyService: JourneyService) {}

    async checkVars(
        command: JourneyCommand,
        journeyField: JourneyField
    ): Promise<OperationCallback> {
        const refApiVariables = journeyField.inputs as FilterWithApiVariables;
        const variables = command.job.getVariablesForFieldId(journeyField.id);
        let checked = true;
        variables?.forEach((v) => {
            checked = filterMatch(refApiVariables.filters, v.value);
        });
        const destFields = command.job.getFieldWithNode(
            checked ? 'true' : 'false',journeyField
        );
        return {
            state: 'passed',
            status: 'true',
            current_id: journeyField.id,
            has_next_level: journeyField.path.length > 0 ? true : false,
            destFields
        } as OperationCallback;
    }
}
