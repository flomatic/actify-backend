import { Channel } from '@entries/channel/enum/channel';
import { EventFilter } from '@entries/event-filter/model/event-filter';
import { FilterWithSegment } from '@entries/journey/model/filter-with-segment';
import { JourneyField } from '@entries/journey/model/journey-field';
import { OperationCallback } from '@entries/journey/model/operation-callback';
import { SegmentDocument } from '@entries/segment/model/segment.model';
import { Injectable } from '@nestjs/common';
import { SessionInterface } from '@shared/models/session.model';
import { User, UserInterface } from '@shared/models/user.model';
import { SegmentQueryHelper } from '@shared/segment/segment-query-helper';
import { SegmentService } from '@shared/segment/segment.service';
import { UserService } from '@shared/user/user.service';
/* eslint-disable-next-line */
import { EventService } from '../../event/event.service';
import { SessionService } from '../../session/session.service';
import { checkFilter, checkSegmentationCriteria } from '../helper';
import { JourneyCommand } from '../journey.command';
import { JourneyService } from '../journey.service';
import { JourneyTriggerEvent } from '../journey.trigger.event';

@Injectable()
export class TriggedByUserSegmentationHelper {
    constructor(
        private readonly userService: UserService,
        private readonly journeyService: JourneyService,
        private readonly userSegmentationService: SegmentService,
        private readonly sessionService: SessionService,
        private readonly eventService: EventService
    ) {}
    public async isMatch(
        segmentationId: string,
        event: JourneyTriggerEvent,
        channel: Channel,
        user: User,
        userId: string
    ): Promise<boolean> {
        /* eslint-disable-next-line */
        const segmentation = await this.userSegmentationService.findSegmentationById(
            segmentationId
        );
        // const user = command.job.journeyJobModel.user;
        // const userId = command.job.journeyJobModel.user_id;
        let isMatchMembers = true;
        let isMatchCriteria = true;
        const isMatchEventFilters = true;
        if (segmentation) {
            if (segmentation.members) {
                if (segmentation.members.type === 'all') {
                    isMatchMembers = true;
                } else {
                    isMatchMembers = segmentation.members.users?.some(
                        (inputUser) => {
                            if (inputUser.key === 'email') {
                                return user.email === inputUser.value;
                            } else if (inputUser.key === 'phone_number') {
                                return user.phone_number === inputUser.value;
                            } else if (user.user_unique_key === inputUser.key) {
                                return (
                                    user.user_unique_value === inputUser.value
                                );
                            }
                        }
                    );
                }
            }
            if (segmentation.criteria) {
                // const session: SessionInterface =
                // await this.sessionService.findSessionBySessionId(
                //     event.body.sessionId
                // );
                const criteria = segmentation.criteria;
                isMatchCriteria = checkSegmentationCriteria(
                    criteria,
                    user,
                    channel
                );
            } else if (segmentation.events) {
                // const segmentationEvent: EventInterface = segmentation.event;
                // const events = await this.eventService.findEvent({
                //     uuk: event.body.uuk,
                //     uuv: event.body.uuv,
                //     eventName: segmentationEvent.event_name
                // });
                // let numberOfHappening = 0;
                // events.forEach((e) => {
                //     // tslint:disable-next-line: no-shadowed-variable
                //     const isMatch = checkFilter(
                //         segmentationEvent.filters,
                //         e.body
                //     );
                //     if (isMatch) {
                //         numberOfHappening++;
                //     }
                // });
                // if (
                //     numberOfHappening >= segmentationEvent.number_of_happening
                // ) {
                //     isMatchEventFilters = true;
                // } else {
                //     isMatchEventFilters = false;
                // }
            }
            return isMatchMembers && isMatchCriteria && isMatchEventFilters;
        }
        return false;
    }

    public async isMatchForAnalytics(params: {
        segmentation_id: string;
        uuk: string;
        uuv: string;
        session_id: string;
    }) {
        /* eslint-disable-next-line */
        const segmentation: SegmentDocument = await this.userSegmentationService.findSegmentationById(
            params.segmentation_id
        );
        let isMatchMembers = true;
        let isMatchCriteria = true;
        const isMatchEventFilters = true;
        if (segmentation) {
            const user: UserInterface = await this.userService.getUserByKeyValue(
                {
                    uuk: params.uuk,
                    uuv: params.uuv
                }
            );
            if (segmentation.members) {
                if (segmentation.members.type === 'all') {
                    isMatchMembers = true;
                } else {
                    isMatchMembers = segmentation.members.users?.some(
                        (inputUser) => {
                            if (inputUser.key === 'email') {
                                return user.email === inputUser.value;
                            } else if (inputUser.key === 'phone_number') {
                                return user.phone_number === inputUser.value;
                            } else if (user.user_unique_key === inputUser.key) {
                                return (
                                    user.user_unique_value === inputUser.value
                                );
                            }
                        }
                    );
                }
            }
            if (segmentation.criteria) {
                const session: SessionInterface = await this.sessionService.findSessionBySessionId(
                    params.session_id
                );
                const criteria = segmentation.criteria;
                isMatchCriteria = checkSegmentationCriteria(
                    criteria,
                    user,
                    session.channel as Channel
                );
            } else if (segmentation.events && segmentation.events.length > 0) {
                const query = SegmentQueryHelper.getEventQuery(segmentation);
                const events = await this.eventService.eventModel
                    .find({
                        uuk: params.uuk,
                        uuv: params.uuv,
                        query
                    })
                    .limit(100);

                const segmentationEvent: EventFilter[] = segmentation.events;
                let isMatchEventFilters = true;
                segmentationEvent?.forEach((s) => {
                    let numberOfHappening = 0;
                    events?.forEach((e) => {
                        const isMatch = checkFilter(s.filters, e.body);
                        if (isMatch) {
                            numberOfHappening++;
                        }
                    });
                    if (numberOfHappening === 0) {
                        isMatchEventFilters = false;
                    }
                });
            }
            return isMatchMembers && isMatchCriteria && isMatchEventFilters;
        }
        return false;
    }

    async checkFilter(
        command: JourneyCommand,
        journeyField: JourneyField
    ): Promise<OperationCallback> {
        const segmentationId = (journeyField.inputs as FilterWithSegment).id;
        const filterPassed = await this.isMatch(
            segmentationId,
            command.event,
            command.event.body.channel,
            command.job.journeyJobModel.user,
            command.job.journeyJobModel.user_id
        );
        const destFields = command.job.getFieldWithNode(
            filterPassed ? 'true' : 'false',
            journeyField
        );
        return {
            state: 'passed',
            status: filterPassed ? 'true' : 'false',
            current_id: journeyField.id,
            has_next_level: journeyField.path.length > 0 ? true : false,
            destFields
        } as OperationCallback;
    }
}
