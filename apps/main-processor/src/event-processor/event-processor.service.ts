import { VariableValue } from '@entries/variablevalue/model/variablevalue';
import { Injectable } from '@nestjs/common';
import { EnumService } from '@shared/enum/enum.service';
import { BLogger } from '@shared/logger/logger.service';
import { SessionInterface } from '@shared/models/session.model';
import { SchemaService } from '@shared/schema/schema.service';
import { UserService } from '@shared/user/user.service';
import Connection from 'apps/main-processor/src/socket-parser/interfaces/connection';
import lodash from 'lodash';
import { ConnectionProcessor } from '../connection-processor/connection-processor.service';
import { NotificationService } from '../notification/notification.service';
import { ReplayService } from '../replay/replay.service';
import { Event } from '../socket-parser/interfaces/event';
import { AnalyticsProcessor } from './../analytics/analytics.processor';
import { EventService } from './../event/event.service';
import { JourneyService } from './../journey/journey.service';
import { SessionService } from './../session/session.service';
import { UserEventService } from './../user-event/user-event.service';
@Injectable()
export class EventProcessor {
    constructor(
        private readonly userEventService: UserEventService,
        private readonly sessionService: SessionService,
        private readonly enumService: EnumService,
        private readonly analyticsProcessor: AnalyticsProcessor,
        private readonly connectioProcessor: ConnectionProcessor,
        private readonly journeyService: JourneyService,
        private readonly notificationService: NotificationService,
        private readonly eventService: EventService,
        private readonly userService: UserService,
        private readonly schemaService: SchemaService,
        // private readonly replayService: ReplayService,
        private readonly logger: BLogger
    ) {}

    async handleEvent(event: Event, connection: Connection) {
        const sessionId = event.session_id;
        // get session from database,
        let session: SessionInterface = await this.sessionService.findSessionBySessionId(
            sessionId
        );
        if (!session) {
            await new Promise((r) => setTimeout(r, 5000));
            session = await this.sessionService.findSessionBySessionId(
                sessionId
            );
            if (!session) {
                return;
            }
        }
        if (event.event_name === 'user_is_offline') {
            await this.analyticsProcessor.handleFlowOrFunnel(event.session_id);
        } else if (event.event_name === 'replay_event') {
            await this.createReplayEvent(session, event, connection);
        } else if (event.event_name === 'user_attrs') {
            await this.handleUserAttrs(event, connection);
        } else if (event.event_name === 'firebase_token') {
            await this.userService.updateFireBaseToken(
                event?.body?.token,
                event?.body?.unique_device_id
            );
        } else if (event.event_name === 'utm_source') {
            await this.notificationService.callback({
                token: event?.body?.token,
                channel: connection.channel,
                uuk: connection.userAttribute.key,
                uuv: connection.userAttribute.value
            });
        } else {
            // analyze schema
            await this.analyzeSchema(event);
            // create events
            await this.createEvent(session, event, connection);
            // deal with journeys
            await this.handleJourneys(event, session);
            // analytics
            await this.handleAnalytics(event, session);
        }
        event.date &&
            this.logger.info('time to finish event processing', {
                time: Date.now() - new Date(event.date).getTime(),
                eventName: event.event_name,
                unit: 'ms'
            });
    }
    async createUserIfNotExist(
        params: VariableValue[],
        connection: Connection
    ) {
        const isUser = lodash.isObject(connection.userAttribute);
        if (isUser) {
            const uuk = connection.userAttribute.key;
            const uuv = connection.userAttribute.value;
            const user = await this.userService.getUserByKeyValue({ uuk, uuv });
            if (!user) {
                await this.connectioProcessor.createUserFromAttrs(
                    params,
                    connection
                );
            }
        }
    }

    async handleUserAttrs(event: Event, connection: Connection) {
        const keys = Object.keys(event.body);
        const values: string[] = Object.values(event.body);

        const params = keys.map((key, index) => ({
            variable: key,
            value: values[index]
        }));
        this.schemaService.analyzeUserAttrs(params);
        this.enumService.addUserAttrsEnums(params);
        if (connection) {
            await this.userService.updateAttr({
                user_unique_key: connection.userAttribute.key,
                user_unique_value: connection.userAttribute.value,
                attrs: params
            });
        }
        await this.createUserIfNotExist(params, connection);
    }

    private async analyzeSchema(event: Event) {
        this.schemaService.analyze(event);
        this.enumService.addEnums(event);
    }

    private async createEvent(
        session: SessionInterface,
        event: Event,
        connection: Connection
    ) {
        const sessionId = event.session_id;
        const isUser = session.user_unique_value && session.user_unique_key;
        let userId = null;
        if (isUser) {
            // insert event in user event
            const userEventData = {
                name: event.event_name,
                body: event.body,
                time: event.date
            };
            await this.userEventService.addEvent(
                userEventData,
                connection,
                sessionId
            );
            const user = await this.userService.getUserByKeyValue({
                uuk: session.user_unique_key,
                uuv: session.user_unique_value
            });
            if (user) {
                userId = user._id;
            }
        }
        const eventData: any = {
            session_id: sessionId,
            user_unique_key: session.user_unique_key,
            user_unique_value: session.user_unique_value,
            user_id: userId,
            body: event.body,
            name: event.event_name,
            time: event.date
        };
        await this.eventService.addEvent(eventData);
        // insert event into events
    }

    async handleJourneys(event: Event, session: SessionInterface) {
        if (session) {
            await this.journeyService.handleJourneys(event, session);
        }
    }

    async handleAnalytics(event: Event, session: SessionInterface) {
        if (session) {
            await this.analyticsProcessor.handleEvent(event);
        }
    }

    private async createReplayEvent(
        session: SessionInterface,
        event: Event,
        connection: Connection
    ) {
        if (session) {
            // await this.replayService.recordEvent(session, event);
        }
    }
}
