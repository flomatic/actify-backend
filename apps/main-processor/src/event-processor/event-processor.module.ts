import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { EnumModule } from '@shared/enum/enum.module';
import { SchemaModule } from '@shared/schema/schema.module';
import { EventModule } from '../event/event.module';
import { JourneyModule } from '../journey/journey.module';
import { NotificationModule } from '../notification/notification.module';
import { ReplayModule } from '../replay/replay.module';
import { UserModule } from '@shared/user/user.module';
import { AnalyticsModule } from './../analytics/analytics.module';
import { SessionModule } from './../session/session.module';
import { UserEventModule } from './../user-event/user-event.module';
import { EventProcessor } from './event-processor.service';
import { ConnectionModule } from '../connection-processor/connection-processor.module';

@Module({
    imports: [
        SessionModule,
        UserEventModule,
        JourneyModule,
        UserModule,
        ConnectionModule,
        NotificationModule,
        CqrsModule,
        AnalyticsModule,
        EventModule,
        SchemaModule,
        EnumModule
        // ReplayModule
    ],
    providers: [EventProcessor],
    exports: [
        EventProcessor
        // ReplayModule
    ]
})
export class EventProcessorModule {}
