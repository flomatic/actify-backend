import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {}

    @Get('/health')
    checkHealthy() {
        return this.appService.checkHealthy();
    }

    @Get('/error')
    error() {
        throw new Error('TEST');
    }
}
