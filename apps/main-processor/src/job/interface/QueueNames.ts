export enum QueueName {
    USER_PROFILE = 'user_profile',
    RETENTION_STATS = 'retention_stats',
    USER_SEGMENTATION = 'user_segmentation',
    OFFLINE_CHECK = 'offline_check'
}
