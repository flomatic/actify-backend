import { Module } from '@nestjs/common';
import { BullModule } from '@shared/bull/bull.module';
import { EnumModule } from '@shared/enum/enum.module';
import { GeoModule } from '@shared/geo-ip/geo-module';
import { LocationModule } from '@shared/location/location.module';
import { RedisModule } from '@shared/redis/redis.module';
import { SchemaModule } from '@shared/schema/schema.module';
import { SettingModule } from '@shared/setting/setting.module';
import { SessionModule } from 'apps/main-processor/src/session/session.module';
import { CampaignModule } from '../campaign/campaign.module';
import { EventModule } from '../event/event.module';
import { UserNotificationModule } from '../user-notification/user-notification.module';
import { RetentionStatsModule } from './../retention-stats/retention-stats.module';
import { UserEventModule } from './../user-event/user-event.module';
import { UserModule } from '@shared/user/user.module';
import { JobService } from './job.service';
import { RetentionStatsProcess } from './process/retentions-stats.process';
import { UserInformationProcess } from './process/user.information.process';
import { SegmentModule } from '@shared/segment/segment.module';
import { OfflineCheckProcess } from './process/offline-check.process';

@Module({
    imports: [
        SettingModule,
        EventModule,
        RedisModule,
        UserModule,
        SessionModule,
        UserNotificationModule,
        LocationModule,
        SchemaModule,
        EnumModule,
        SegmentModule,
        BullModule,
        RetentionStatsModule,
        UserEventModule,
        GeoModule,
        CampaignModule
    ],
    providers: [
        JobService,
        RetentionStatsProcess,
        UserInformationProcess,
        OfflineCheckProcess
    ],
    exports: [JobService]
})
export class JobModule {
    constructor(
        private readonly retentionStatsProcess: RetentionStatsProcess,
        private readonly offlineCheckProcess: OfflineCheckProcess,
        private readonly userInformationProcess: UserInformationProcess
    ) {}
}
