import { Injectable } from '@nestjs/common';
import { BullService } from '@shared/bull/bull.service';
import { QueueName } from './interface/QueueNames';

@Injectable()
export class JobService {
    constructor(private readonly bullService: BullService) {}

    addJob(
        queueName: QueueName,
        data: any,
        config?: {
            timeout?: number;
            exec_date?: Date;
        }
    ) {
        this.bullService.addJob(data, queueName.toString(), config);
    }
}
