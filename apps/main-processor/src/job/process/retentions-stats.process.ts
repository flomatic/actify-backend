import { Injectable } from '@nestjs/common';
import { BullService } from '@shared/bull/bull.service';
import { UserEventInterface } from '@shared/models/user-event.model';
import {
    ActivityLevel,
    UserAvailabilityInterface
} from '@shared/models/user.model';
import { getFromToDateInMonth } from '../../analytics/helper/date-utils';
import { QueueName } from '../../job/interface/QueueNames';
import { RetentionStatsService } from '../../retention-stats/retention-stats.service';
import { UserService } from '@shared/user/user.service';
import { UserEventService } from './../../user-event/user-event.service';
import Bull = require('bull');

@Injectable()
export class RetentionStatsProcess {
    constructor(
        private readonly retentionStatsService: RetentionStatsService,
        private readonly userEventService: UserEventService,
        private readonly userService: UserService,
        private readonly bullService: BullService
    ) {
        this.bullService.defineProcess(
            QueueName.RETENTION_STATS,
            this.defineProcess
        );
    }

    defineProcess = async (job: Bull.Job) => {
        const userAvailability: UserAvailabilityInterface = job.data;
        const userEvents: UserEventInterface[] = await this.userEventService.getLastTwoEvents(
            {
                user_unique_key: userAvailability.user.uuk,
                user_unique_value: userAvailability.user.uuv
            }
        );
        let timeSpentOnSession = 0;
        let timeBetweenTwoSessions = 0;
        if (userEvents?.length === 2) {
            const currentUserEvents: UserEventInterface = userEvents[0];
            if (!currentUserEvents.end_date) {
                currentUserEvents.end_date = userAvailability.connectionDate;
            }
            timeSpentOnSession =
                new Date(currentUserEvents.end_date).getTime() -
                new Date(currentUserEvents.start_date).getTime();
            const lastUserEvents: UserEventInterface = userEvents[1];
            timeBetweenTwoSessions =
                new Date(currentUserEvents.start_date).getTime() -
                new Date(lastUserEvents.start_date).getTime();
            this.retentionStatsService.updateRetention({
                time_spend_on_session: timeSpentOnSession,
                time_between_two_session: timeBetweenTwoSessions
            });

            const user = await this.userService.getUserByKeyValue({
                uuk: userAvailability.user.uuk,
                uuv: userAvailability.user.uuv
            });
            user.average_session_time.count++;
            const totalAverageSessionTime =
                user.average_session_time.value *
                    (user.average_session_time.count - 1) +
                timeSpentOnSession;
            user.average_session_time.value = Math.round(
                totalAverageSessionTime / user.average_session_time.count
            );
            user.average_revisit_time.count++;
            const totalRevisitTime =
                user.average_revisit_time.value *
                    (user.average_revisit_time.count - 1) +
                timeBetweenTwoSessions;
            user.average_revisit_time.value = Math.round(
                totalRevisitTime / user.average_revisit_time.count
            );
            const monthlyRetentionStats = await this.retentionStatsService.getRetention(
                getFromToDateInMonth()
            );
            const monthlyAverageScore = monthlyRetentionStats?.average_score;

            const averageScore = Number(
                (
                    user.average_session_time.value /
                    user.average_revisit_time.value
                ).toFixed(3)
            );
            if (
                averageScore <
                monthlyAverageScore - (1 / 6) * monthlyAverageScore
            ) {
                user.activity_level = ActivityLevel.LOW;
            } else if (
                averageScore >
                monthlyAverageScore + (1 / 6) * monthlyAverageScore
            ) {
                user.activity_level = ActivityLevel.HIGH;
            } else {
                user.activity_level = ActivityLevel.MEDIUM;
            }
            await user.save();
        }
    };
}
