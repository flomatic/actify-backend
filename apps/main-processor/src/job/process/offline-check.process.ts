import { Injectable } from '@nestjs/common';
import { BullService } from '@shared/bull/bull.service';
import { UserAvailabilityInterface } from '@shared/models/user.model';
import { QueueName } from '../../job/interface/QueueNames';
import { SessionService } from '../../session/session.service';
import { UserService } from '@shared/user/user.service';
import Bull = require('bull');

@Injectable()
export class OfflineCheckProcess {
    constructor(
        private readonly userService: UserService,
        private readonly sessionService: SessionService,
        private readonly bullService: BullService
    ) {
        this.bullService.defineProcess(
            QueueName.OFFLINE_CHECK,
            this.defineProcess
        );
    }

    defineProcess = async (job: Bull.Job) => {
        const userAvailability: UserAvailabilityInterface = job.data;
        const sessionId = userAvailability.session_id;
        const session = await this.sessionService.findSessionBySessionId(
            sessionId
        );
        if (session && session.is_online) {
            if (
                new Date().getTime() - session.start_date.getTime() >
                5 * 60 * 60 * 1000
            ) {
                session.is_online = false;
                session.end_date = new Date();
                await session.save();
                const user = await this.userService.getUserByKeyValue({
                    uuk: userAvailability.user.uuk,
                    uuv: userAvailability.user.uuv
                });
                if (user) {
                    user.last_activity = new Date();
                    await user.save();
                }
            }
        }
    };
}
