import { Injectable } from '@nestjs/common';
import { BullService } from '@shared/bull/bull.service';
import { EnumService } from '@shared/enum/enum.service';
import { GeoService } from '@shared/geo-ip/geo-service';
import { LocationService } from '@shared/location/location.service';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import {
    UserAvailabilityInterface,
    UserInterface
} from '@shared/models/user.model';
import { SchemaService } from '@shared/schema/schema.service';
import { UAParser } from 'ua-parser-js';
import { LocationUserService } from '@shared/user/location.user.service';
import { UserService } from '@shared/user/user.service';
import { QueueName } from '../interface/QueueNames';
import Bull = require('bull');

@Injectable()
export class UserInformationProcess {
    constructor(
        private readonly userService: UserService,
        private readonly geoService: GeoService,
        private readonly loggerService: BLogger,
        private readonly schemaService: SchemaService,
        private readonly enumService: EnumService,
        private readonly locationUserService: LocationUserService,
        private readonly locationService: LocationService,
        private readonly bullService: BullService
    ) {
        this.bullService.defineProcess(
            QueueName.USER_PROFILE,
            this.defineProcess
        );
    }
    defineProcess = async (job: Bull.Job) => {
        const userAvailability: UserAvailabilityInterface = job.data;
        const user: UserInterface = await this.userService.getUserByKeyValue({
            uuk: userAvailability.user.uuk,
            uuv: userAvailability.user.uuv
        });
        if (user.email) {
            const source = user.sources.find((s) => s === 'email');
            if (!source) {
                user.sources.push('email');
            }
        }
        if (user.phone_number) {
            const source = user.sources.find((s) => s === 'sms');
            if (!source) {
                user.sources.push('sms');
            }
        }
        const deviceInfo = this.getDeviceInfo(userAvailability);
        if (deviceInfo.firebase_token) {
            const source = user.sources.find((s) => s === 'push_notification');
            if (!source) {
                user.sources.push('push_notification');
            }
        }
        await this.analyzeSchema(deviceInfo);
        await this.analyzeEnum(deviceInfo);
        if (deviceInfo.type === userAvailability.channel)
            if (!user.devices) {
                user.devices = [];
            }
        let index = -1;
        user.devices.forEach((d, dIndex) => {
            if (d.device_id === deviceInfo.device_id) {
                index = dIndex;
            }
        });
        if (index === -1) {
            user.devices.push(deviceInfo);
        } else {
            user.devices[index].last_used_date = deviceInfo.last_used_date;
            user.devices[index].firebase_token = deviceInfo.firebase_token;
        }
        const geoIp = await this.geoService.getIpDetail(
            userAvailability.ip_address
        );
        if (geoIp) {
            const location = {
                country: geoIp.country_long,
                city: geoIp.city,
                time_zone: geoIp.timezone
            };
            await this.locationService.addIfNeeds(location);

            await this.locationUserService.addLocationUser({
                location,
                user_id: user._id
            });
            const locationUser: {
                country: string;
                city: string;
                time_zone: string;
            } = await this.locationUserService.getUserLocation(user._id);
            user.location = locationUser;
        }
        await user.save();
    };

    async analyzeEnum(deviceInfo: {
        type: string;
        browser: any;
        browser_version: any;
        os: any;
        os_version: any;
        last_used_date: Date;
        firebase_token: string;
        device_id: string;
        brand: any;
        model: any;
        manufacturer: any;
        app_version: string;
    }) {
        await this.enumService.addUserDeviceEnums(deviceInfo);
    }
    async analyzeSchema(deviceInfo: {
        type: string;
        browser: any;
        browser_version: any;
        os: any;
        os_version: any;
        last_used_date: Date;
        firebase_token: string;
        device_id: string;
        brand: any;
        model: any;
        manufacturer: any;
        app_version: string;
    }) {
        await this.schemaService.analyzeUserDevice(deviceInfo);
    }

    getDeviceInfo = (userAvailability: UserAvailabilityInterface) => {
        try {
            const parser =
                new UAParser(userAvailability.user_agent) || new UAParser();
            const {
                browser = null,
                device = null,
                os = null
            } = parser.getResult();

            const deviceInfo: {
                device_id: string;
                brand: string;
                model: string;
                manufacturer: string;
                os_version: any;
                app_version: string;
            } = userAvailability.device_info as any;
            const result = {
                type: userAvailability.channel,
                browser: browser.name || null,
                browser_version: browser?.version || null,
                os: os?.name || userAvailability?.channel || null,
                os_version: os?.version || deviceInfo?.os_version || null,
                last_used_date: new Date(),
                firebase_token: userAvailability?.firebase_token || null,
                device_id: userAvailability?.device_id || '1234',
                brand: device?.vendor || deviceInfo?.brand || null,
                model: device?.model || deviceInfo?.model || null,
                manufacturer:
                    device?.vendor || deviceInfo?.manufacturer || null,
                app_version: deviceInfo?.app_version || null
            };
            return result;
        } catch (e) {
            this.loggerService.error(ACTIONS.DEVICE_INFO, e);
        }
    };
}
