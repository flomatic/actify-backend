import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CampaignDocument } from '@shared/models/campaign.model';
import { Model } from 'mongoose';

Injectable();
export class CampaignService {
    constructor(
        @InjectModel('Campaign')
        private readonly campaignModel: Model<CampaignDocument>
    ) {}

    async findAll(): Promise<CampaignDocument[]> {
        return this.campaignModel.find({});
    }

    async findById(id: any): Promise<CampaignDocument> {
        return this.campaignModel.findById(id);
    }

    async find(query) {
        return this.campaignModel.find(query);
    }
}
