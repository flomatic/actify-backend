import { Module } from '@nestjs/common';
import { EmailModule } from '@shared/email/email.module';
import { CampaignModelModule } from '@shared/models/campaign.model';
import { SegmentModule } from '@shared/segment/segment.module';
import { SmsModule } from '@shared/sms/sms.module';
import { UserModule } from '@shared/user/user.module';
import { VariableReplaceModule } from '@shared/variable-replace/variable-replace.module';
import { MessageTemplateModule } from 'apps/api/src/message-template/message-template.module';
import { VariableModule } from 'apps/api/src/variable/variable.module';
import { NotificationModule } from '../notification/notification.module';
import { CampaignProcess } from './campaign.process';
import { CampaignService } from './campaign.service';

@Module({
    imports: [
        CampaignModelModule,
        UserModule,
        SmsModule,
        VariableModule,
        NotificationModule,
        VariableReplaceModule,
        MessageTemplateModule,
        EmailModule,
        SegmentModule
    ],
    providers: [CampaignService, CampaignProcess],
    exports: [CampaignService]
})
export class CampaignModule {
    constructor(private readonly campaignProcess: CampaignProcess) {}
}
