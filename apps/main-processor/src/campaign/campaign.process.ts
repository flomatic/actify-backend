import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import {
    CampaignDocument,
    CampaignMode,
    CampaignStatus,
    CampaignTimeScheduleType,
    CampaignType
} from '@shared/models/campaign.model';
import { MessageTemplateType } from '@shared/models/message-template.model';
import { UserService } from '@shared/user/user.service';
import { VariableReplaceHelper } from '@shared/variable-replace/variable-replace.helper';
import { MessageTemplateService } from 'apps/api/src/message-template/message-template.service';
/* eslint-disable */
import { forEachAsync } from '../journey/helper';
import { NotificationService } from '../notification/notification.service';
import { TokenService } from '../notification/token.service';
import { CampaignService } from './campaign.service';
import Agenda = require('agenda');
import { SegmentDocument } from '@entries/segment/model/segment.model';
import { SegmentService } from '@shared/segment/segment.service';
import { SegmentQueryHelper } from '@shared/segment/segment-query-helper';

@Injectable()
export class CampaignProcess implements OnModuleInit, OnModuleDestroy {
    agenda: Agenda;
    worker: string;
    campaignProcessInterval: number;
    handlerTimeout;
    clientName: string;
    currentLock: string;
    constructor(
        private readonly campaignService: CampaignService,
        private readonly segmentationService: SegmentService,
        private readonly configService: ConfigService,
        private readonly variableHelper: VariableReplaceHelper,
        private readonly userService: UserService,
        private readonly tokenService: TokenService,
        private readonly logger: BLogger,
        private readonly notificationService: NotificationService,
        private readonly messageTemplateService: MessageTemplateService,
        private readonly segmentationCalculator: SegmentQueryHelper
    ) {
        this.campaignProcessInterval =
            Number(this.configService.get('CAMPAIGN_PROCESS_INTERVAL')) || 10;
        this.agenda = new Agenda({
            db: {
                address: this.configService.get('MONGO_AGENDA_URL'),
                collection: 'CampaignJob'
            }
        });
        this.worker = this.configService.get('HOSTNAME') || '';
        this.clientName = this.configService.get('HOSTNAME');
        this.currentLock = this.configService.get(`CAMPAIGN_HANDLER`);
    }

    getQueueName(campaignName: string) {
        return `${this.worker}::` + 'campaign.' + campaignName;
    }

    async onModuleInit() {
        const shouldContinue = await this.isItMyTask();
        if (shouldContinue) {
            await this.agenda.start();
            await this.handleCampaigns();
            this.handlerTimeout = setTimeout(async () => {
                this.onModuleInit();
            }, this.campaignProcessInterval * 1000);
        }
    }

    async onModuleDestroy() {
        clearTimeout(this.handlerTimeout);
    }

    private async isItMyTask() {
        return this.currentLock && this.currentLock == this.clientName;
    }

    async syncJobsWithCampaigns(
        jobs: Agenda.Job<CampaignDocument>[],
        campaigns: CampaignDocument[]
    ) {
        await Promise.all(
            jobs.map(async (job) => {
                const campaign = campaigns?.find(
                    (c) => c._id === job.attrs.data._id
                );
                if (!campaign) {
                    await job.remove();
                } else {
                    if (campaign.status === CampaignStatus.SCHEDULED) {
                        const _q = {
                            name: campaign.name,
                            campaign
                        };
                        await this.setQueueSetting(_q);
                    } else {
                        if (campaign.status === CampaignStatus.STOPPED) {
                            await job.remove();
                        }
                    }
                }
            })
        );
        await Promise.all(
            campaigns.map(async (campaign) => {
                if (campaign.status === CampaignStatus.SCHEDULED) {
                    let job: any = jobs?.find(
                        (j) => j.attrs.data._id === campaign._id
                    );
                    if (!job) {
                        await this.setQueueSetting({
                            name: this.getQueueName(campaign.name),
                            campaign
                        });
                        job = await this.agenda.now(
                            this.getQueueName(campaign.name),
                            campaign
                        );
                    }
                }
            })
        );
    }

    async handleCampaigns() {
        let campaigns = await this.campaignService.find({});
        const jobs = await this.agenda.jobs<CampaignDocument>({});
        await this.syncJobsWithCampaigns(jobs, campaigns);
    }

    async setQueueSetting(job: {
        name: string;
        // queue: Agenda.Job<CampaignDocument>;
        campaign: CampaignDocument;
    }) {
        this.agenda.define<CampaignDocument>(job.name, async (j, done) => {
            let campaign: CampaignDocument = j.attrs.data;
            campaign = await this.campaignService.findById(campaign._id);
            let execDate = this.getNextCampaignExecDate(campaign);
            try {
                if (execDate) {
                    const diffDateInMs = Math.max(
                        execDate.getTime() - new Date().getTime(),
                        0
                    );
                    if (diffDateInMs < 60 * 1000) {
                        try {
                            campaign = await this.defineJob(campaign);
                            execDate = this.getNextCampaignExecDate(campaign);
                            if (execDate) {
                                j.schedule(execDate);
                                campaign.next_running_date = execDate;
                                campaign.status = CampaignStatus.SCHEDULED;
                            } else {
                                campaign.status = CampaignStatus.STOPPED;

                                j.remove();
                            }
                            await j.save();
                            done();
                        } catch (e) {
                            campaign.next_running_date = null;
                            campaign.status = CampaignStatus.STOPPED;
                            this.logger.error(ACTIONS.CAMPAIGN_ERROR, e);
                            done(e);
                        }
                    } else {
                        j.schedule(execDate);
                        await j.save();
                        done();
                    }
                } else {
                    campaign.next_running_date = null;
                    campaign.status = CampaignStatus.STOPPED;
                    j.remove();
                    await j.save();
                    done();
                }
            } catch (e) {
                campaign.next_running_date = null;
                campaign.status = CampaignStatus.STOPPED;
                this.logger.error(ACTIONS.CAMPAIGN_ERROR, e);
                done(e);
            }
            await campaign.save();
        });
    }

    getNextCampaignExecDate(campaign: CampaignDocument) {
        if (campaign.time_schedule.type === CampaignTimeScheduleType.ONE_TIME) {
            if (campaign.last_running_date) {
                return null;
            }
        }

        if (
            campaign.time_schedule.type ===
                CampaignTimeScheduleType.RECURRING &&
            campaign.time_schedule.every &&
            campaign.total_running_count >= campaign.time_schedule.every.number
        ) {
            return null;
        }

        if (campaign.next_running_date) {
            return new Date(campaign.next_running_date);
        }

        if (campaign.time_schedule.type === CampaignTimeScheduleType.ONE_TIME) {
            if (!campaign.time_schedule.date) {
                return new Date();
            } else return new Date(campaign.time_schedule.date);
        } else {
            const period = campaign.time_schedule.every.type;
            const oneDayInMs = 24 * 60 * 60 * 1000;
            /* eslint-disable */
            const delayInMs =
                period === 'day'
                    ? oneDayInMs
                    : period === 'week'
                    ? 7 * oneDayInMs
                    : period === 'month'
                    ? 30 * 7 * oneDayInMs
                    : period === 'year'
                    ? 365 * 30 * 7 * oneDayInMs
                    : null;
            /* eslint-enable */
            if (!campaign.last_running_date) {
                if (campaign.time_schedule?.every?.from) {
                    return new Date(campaign.time_schedule.every.from);
                } else {
                    return new Date();
                }
            } else {
                if (
                    campaign.total_running_count <
                    campaign.time_schedule.every.number
                ) {
                    const date = new Date();
                    date.setTime(
                        new Date(campaign.last_running_date).getTime() +
                            delayInMs
                    );
                    return date;
                } else {
                    return null;
                }
            }
        }
    }

    async defineJob(campaign: CampaignDocument) {
        campaign = await this.campaignService.findById(campaign._id);
        const segmentationId = campaign.segmentation_id;
        let segmentation = await this.segmentationService.findSegmentationById(
            segmentationId
        );
        if (segmentation) {
            campaign.status = CampaignStatus.RUNNING;
            await campaign.save();
            if (campaign.mode === CampaignMode.TEST_MODE) {
                segmentation = await this.segmentationService.findSegmentationById(
                    campaign.test_segmentation_id
                );
                await this.compute(campaign, segmentation);
            } else {
                await this.compute(campaign, segmentation);
            }
        }
        if (campaign.next_running_date) {
            campaign.status = CampaignStatus.SCHEDULED;
        } else {
            campaign.status = CampaignStatus.STOPPED;
        }
        campaign.last_running_date = new Date();
        campaign.total_running_count = campaign.total_running_count + 1;
        return campaign;
    }

    async compute(campaign: CampaignDocument, segmentation: SegmentDocument) {
        let startPoint = 0;
        let count = 0;
        const fetchCount = 100;
        do {
            const users = await this.segmentationCalculator.getUserForSegmentation(
                segmentation,
                fetchCount,
                startPoint
            );
            const totalGet = users.length;
            count = totalGet;
            startPoint = startPoint + Math.min(fetchCount, totalGet);
            if (totalGet === 0) {
                break;
            }
            const queueName = `${this.worker}::` + 'campaign.' + campaign.name;
            const template = await this.messageTemplateService.findById(
                campaign.template_id
            );

            await forEachAsync(users, async (user) => {
                const _user = await this.userService.findUserById(user._id);
                const body = await this.variableHelper.getVariablesForTemplate(
                    campaign.template_id,
                    _user
                );
                if (!(template.data as any).payload) {
                    (template.data as any).payload = [];
                }
                if (template.type === MessageTemplateType.PUSH_NOTIFICATION) {
                    const token = this.tokenService.generateToken(
                        queueName + '::' + user._id.toString()
                    );
                    if (!template.data) {
                        (template as any).data = [];
                    }
                    (template.data as any)?.payload?.push({
                        tracking_id: token
                    });
                }
                const notificationType =
                    campaign.type === CampaignType.SMS_TYPE
                        ? 'sms'
                        : campaign.type === CampaignType.EMAIL_TYPE
                            ? 'email'
                            : 'push_notification';

                this.notificationService.sendNotification({
                    token: queueName,
                    user_id: _user._id,
                    time_out: campaign.time_out,
                    retry: campaign.retry,
                    notification: {
                        body: body,
                        payload: template.data
                    },
                    type: notificationType,
                    ignore_sleep_time: campaign.ignore_sleep_time,
                    ignore_frequency_cap: campaign.ignore_rate_limit,
                    rate_limit: campaign.rate_limit,
                    group_name: queueName
                });
            });
        } while (count != 0);
    }
}
