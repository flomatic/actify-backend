import { Module } from '@nestjs/common';
import { UserStatsModelModule } from '@shared/models/user-stats.model';
import { RedisModule } from '@shared/redis/redis.module';
import { SessionModule } from './../session/session.module';
import { UserEventModule } from './../user-event/user-event.module';
import { UserStatsProcessor } from './user-stats.processor';
import { UserStatsService } from './user-stats.service';

@Module({
    imports: [
        RedisModule,
        UserStatsModelModule,
        UserEventModule,
        SessionModule
    ],
    providers: [UserStatsProcessor, UserStatsService],
    exports: [UserStatsProcessor, UserStatsService]
})
export class UserStatsModule {}
