import { Injectable } from '@nestjs/common';
import {
    TimePeriod,
    UserStatsInterface
} from '@shared/models/user-stats.model';
import { UserAvailabilityInterface } from '@shared/models/user.model';
import { RedisService } from '@shared/redis/redis.service';
import {
    getFromToDateInHour,
    getLastFromToDateInHour
} from '../analytics/helper/date-utils';
import { SessionService } from './../session/session.service';
import { UserStatsService } from './user-stats.service';

@Injectable()
export class UserStatsProcessor {
    constructor(
        private readonly redisService: RedisService,
        private readonly sessionService: SessionService,
        private readonly userStatsService: UserStatsService
    ) {}

    tick: {
        is_new_user?: boolean;
        is_new_session?: boolean;
        source?: 'browser' | 'android' | 'ios';
        unique_visit?: boolean;
        is_online?: boolean;
    } = {};

    async handleUserStats(params: UserAvailabilityInterface) {
        this.tick.source = 'browser';
        this.tick.is_online = params.is_online;
        this.tick.is_new_user = params.is_new_user;
        if (params.is_online) {
            const userData = await this.redisService.get(
                'ip_' + params.ip_address
            );
            if (!userData) {
                const date = new Date();
                date.setDate(date.getDate() + 1);
                date.setHours(0, 0, 0, 0);
                const diff = date.getTime() - new Date().getTime();
                await this.redisService.setWithTTL(
                    params.ip_address,
                    params,
                    Math.floor(diff / 1000)
                );
                this.tick.unique_visit = true;
                this.tick.is_new_session = true;
            } else {
                const parsedString: {
                    is_online?: boolean;
                    is_user?: boolean;
                    is_new_user?: boolean;
                    user_data?: any;
                    session_id?: string;
                    user?: { uuk: string; uuv: string };
                    ip_address?: string;
                    user_attr?: any;
                } = userData;
                this.tick.unique_visit = false;
                this.tick.is_new_session =
                    parsedString.session_id === params.session_id;
            }
        } else {
            this.tick.unique_visit = false;
            this.tick.is_new_session = false;
        }
        await this.updateUserStatsHourly();
    }

    async updateUserStatsHourly() {
        const fromToDate = getFromToDateInHour();
        const userStats: UserStatsInterface = await this.userStatsService.findOneByQuery(
            {
                from_date: fromToDate.from_date,
                to_date: fromToDate.to_date,
                time_period: TimePeriod.HOURLY
            }
        );
        if (userStats) {
            if (this.tick.is_online) {
                if (this.tick.is_new_session) {
                    userStats.total_session.find(
                        (s) => s.channel === this.tick.source
                    ).value++;
                }
                if (this.tick.is_new_user) {
                    userStats.new_users.find(
                        (s) => s.channel === this.tick.source
                    ).value++;
                }
                if (this.tick.unique_visit) {
                    userStats.total_unique_visit.find(
                        (s) => s.channel === this.tick.source
                    ).value++;
                }
                userStats.minimum_online_users.find(
                    (s) => s.channel === this.tick.source
                ).value++;
                const actualValue = userStats.minimum_online_users.find(
                    (s) => s.channel === this.tick.source
                ).value;
                const maxValue = userStats.maximum_online_users.find(
                    (s) => s.channel === this.tick.source
                ).value;
                if (actualValue >= maxValue) {
                    userStats.maximum_online_users.find(
                        (s) => s.channel === this.tick.source
                    ).value = actualValue;
                }
            } else {
                if (
                    userStats.minimum_online_users.find(
                        (s) => s.channel === this.tick.source
                    ).value > 0
                ) {
                    userStats.minimum_online_users.find(
                        (s) => s.channel === this.tick.source
                    ).value--;
                }
            }
            await userStats.save();
        } else {
            const lastFromToDate = getLastFromToDateInHour();
            const lastUserStats: UserStatsInterface = await this.userStatsService.findOneByQuery(
                {
                    from_date: lastFromToDate.from_date,
                    to_date: lastFromToDate.to_date,
                    time_period: TimePeriod.HOURLY
                }
            );
            if (lastUserStats) {
                const dataToBeInserted = {
                    from_date: fromToDate.from_date,
                    to_date: fromToDate.to_date,
                    time_period: TimePeriod.HOURLY,
                    total_session: [
                        {
                            channel: 'browser',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'browser' &&
                                this.tick.is_new_session
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'android',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'browser' &&
                                this.tick.is_new_session
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'ios',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'ios' &&
                                this.tick.is_new_session
                                    ? 1
                                    : 0
                        }
                    ],
                    total_unique_visit: [
                        {
                            channel: 'browser',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'browser' &&
                                this.tick.unique_visit
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'android',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'android' &&
                                this.tick.unique_visit
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'ios',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'ios' &&
                                this.tick.unique_visit
                                    ? 1
                                    : 0
                        }
                    ],
                    minimum_online_users: [
                        {
                            channel: 'browser',
                            value:
                                lastUserStats.minimum_online_users.find(
                                    (s) => s.channel === 'browser'
                                ).value +
                                (this.tick.is_online &&
                                this.tick.source === 'browser'
                                    ? 1
                                    : 0)
                        },
                        {
                            channel: 'android',
                            value:
                                lastUserStats.minimum_online_users.find(
                                    (s) => s.channel === 'android'
                                ).value +
                                (this.tick.is_online &&
                                this.tick.source === 'android'
                                    ? 1
                                    : 0)
                        },
                        {
                            channel: 'ios',
                            value:
                                lastUserStats.minimum_online_users.find(
                                    (s) => s.channel === 'ios'
                                ).value +
                                (this.tick.is_online &&
                                this.tick.source === 'ios'
                                    ? 1
                                    : 0)
                        }
                    ],
                    maximum_online_users: [
                        {
                            channel: 'browser',
                            value:
                                lastUserStats.minimum_online_users.find(
                                    (s) => s.channel === 'browser'
                                ).value +
                                (this.tick.is_online &&
                                this.tick.source === 'browser'
                                    ? 1
                                    : 0)
                        },
                        {
                            channel: 'android',
                            value:
                                lastUserStats.minimum_online_users.find(
                                    (s) => s.channel === 'android'
                                ).value +
                                (this.tick.is_online &&
                                this.tick.source === 'android'
                                    ? 1
                                    : 0)
                        },
                        {
                            channel: 'ios',
                            value:
                                lastUserStats.minimum_online_users.find(
                                    (s) => s.channel === 'ios'
                                ).value +
                                (this.tick.is_online &&
                                this.tick.source === 'ios'
                                    ? 1
                                    : 0)
                        }
                    ],
                    new_users: [
                        {
                            channel: 'browser',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'browser' &&
                                this.tick.is_new_user
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'android',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'android' &&
                                this.tick.is_new_user
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'ios',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'ios' &&
                                this.tick.is_new_user
                                    ? 1
                                    : 0
                        }
                    ]
                };
                await this.userStatsService.create(dataToBeInserted);
            } else {
                const currentOnlineUsers: number = await this.sessionService.findOnlineCounts();
                const dataToBeInserted = {
                    from_date: fromToDate.from_date,
                    to_date: fromToDate.to_date,
                    time_period: TimePeriod.HOURLY,
                    total_session: [
                        {
                            channel: 'browser',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'browser' &&
                                this.tick.is_new_session
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'android',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'android' &&
                                this.tick.is_new_session
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'ios',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'ios' &&
                                this.tick.is_new_session
                                    ? 1
                                    : 0
                        }
                    ],
                    total_unique_visit: [
                        {
                            channel: 'browser',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'browser' &&
                                this.tick.unique_visit
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'android',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'android' &&
                                this.tick.unique_visit
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'ios',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'ios' &&
                                this.tick.unique_visit
                                    ? 1
                                    : 0
                        }
                    ],
                    minimum_online_users: [
                        {
                            channel: 'browser',
                            value: currentOnlineUsers + 1
                        },
                        {
                            channel: 'android',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'android'
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'ios',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'ios'
                                    ? 1
                                    : 0
                        }
                    ],
                    maximum_online_users: [
                        {
                            channel: 'browser',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'browser'
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'android',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'android'
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'ios',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'ios'
                                    ? 1
                                    : 0
                        }
                    ],
                    new_users: [
                        {
                            channel: 'browser',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'browser' &&
                                this.tick.is_new_user
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'android',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'android' &&
                                this.tick.is_new_user
                                    ? 1
                                    : 0
                        },
                        {
                            channel: 'ios',
                            value:
                                this.tick.is_online &&
                                this.tick.source === 'ios' &&
                                this.tick.is_new_user
                                    ? 1
                                    : 0
                        }
                    ]
                };
                await this.userStatsService.create(dataToBeInserted);
            }
        }
    }
}
