import { NestFactory } from '@nestjs/core';
import { NestjsLogger } from '@shared/logger/logger.service';
import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    const logger = app.get(NestjsLogger);
    app.useLogger(logger);

    await app.listen(3005);
}
bootstrap();
