import Joi = require('joi');

export const UserEventValidator = Joi.object({
    phone_number: Joi.string().regex(/^(\+\d{1,3}[- ]?)?\d{10}$/),
    email: Joi.string().email(),
    page_number: Joi.number().max(100)
}).xor('phone_number', 'email');
