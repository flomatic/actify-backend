import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SessionInterface } from '@shared/models/session.model';
import { UserEventInterface } from '@shared/models/user-event.model';
import Connection from 'apps/main-processor/src/socket-parser/interfaces/connection';
import * as _ from 'lodash';
import { Model } from 'mongoose';

@Injectable()
export class UserEventService {
    constructor(
        @InjectModel('UserEvent')
        private readonly userEventModel: Model<UserEventInterface>
    ) {}

    async createNewSession(userEvent): Promise<any> {
        const query = {
            session_id: userEvent.session_id
        };
        const update = {
            $set: { ..._.omit(userEvent, 'events') },
            $setOnInsert: { events: [] }
        };
        const options = { upsert: true };
        await this.userEventModel.collection.updateOne(query, update, options);
    }

    async closeSession(sessionId: string): Promise<any> {
        const query = {
            session_id: sessionId
        };
        const update = {
            $set: { end_date: new Date() }
        };
        const options = { upsert: false };
        await this.userEventModel.collection.updateOne(query, update, options);
    }

    async addEvents(params: {
        user_unique_value: string;
        user_unique_key: string;
        session_id: string;
        duration: number;
        events: Array<{
            name: string;
            body: any;
            time: Date;
        }>;
        start_date: Date;
        end_date: Date;
    }) {
        await this.createNewSession(params);
    }

    async addEvent(
        event: { name: string; body: any; time: Date },
        connection: Connection,
        sessionId
    ) {
        const userEvent = await this.userEventModel.findOne({
            session_id: sessionId
        });
        if (!userEvent) {
            this.createNewSession({
                user_unique_value: connection.userAttribute.value,
                user_unique_key: connection.userAttribute.key,
                session_id: connection.sessionId,
                events: [],
                start_date: connection.connectionDate,
                end_date: null
            });
        } else {
            userEvent.events.push(event);
            await userEvent.save();
        }
    }

    async addServerEvent(
        event: { name: string; body: any; time: Date },
        session: SessionInterface
    ) {
        const sessionId = session.session_id;
        // TODO: There is a redundant query happening here. We can just pass the document down
        const userEvent = await this.userEventModel.findOne({
            session_id: sessionId
        });
        userEvent.events.push(event);
        await userEvent.save();
    }

    async getLastTwoEvents(params: {
        user_unique_key: string;
        user_unique_value: string;
    }): Promise<UserEventInterface[]> {
        return await this.userEventModel
            .find({
                user_unique_key: params.user_unique_key,
                user_unique_value: params.user_unique_value
            })
            ?.limit(2)
            ?.sort({ start_date: -1 })
            ?.lean();
    }

    async getUserEvents(params: {
        user_unique_key: string;
        user_unique_value: string;
        page_number?: number;
    }) {
        const pageNumber = params.page_number || 1;
        const skipValue = (pageNumber - 1) * 10;
        return await this.userEventModel.aggregate([
            {
                $match: {
                    $and: [
                        { user_unique_key: params.user_unique_key },
                        { user_unique_value: params.user_unique_value }
                    ]
                }
            },
            { $sort: { start_date: -1 } },
            { $group: { _id: 'results', result: { $push: '$$CURRENT' } } },
            {
                $project: {
                    _id: 0,
                    result: { $slice: ['$result', skipValue, skipValue + 10] },
                    pages: { $ceil: { $divide: [{ $size: '$result' }, 10] } }
                }
            }
        ]);
    }
    async getUserEventsBySessionId(
        sessionId: string
    ): Promise<UserEventInterface> {
        return await this.userEventModel.findOne({ session_id: sessionId });
    }
}
