import { BadRequestException, Controller, Get, Query } from '@nestjs/common';
import { UserInterface } from '@shared/models/user.model';
import { UserService } from '@shared/user/user.service';
import { UserEventService } from './user-event.service';
import { UserEventValidator } from './validator/user-event.validator';

@Controller('user-event')
export class UserEventController {
    constructor(
        private readonly userEventService: UserEventService,
        private readonly userService: UserService
    ) {}

    @Get()
    async getUserEvents(@Query() query) {
        query.phone_number = query?.phone_number?.replace(' ', '+');
        const validate = UserEventValidator.validate(query);
        if (validate.error) {
            throw new BadRequestException('validation_error');
        }
        const user: UserInterface = await this.userService.getUser({
            email: query.email,
            phone_number: query.phone_number
        });
        const userEvents = await this.userEventService.getUserEvents({
            user_unique_value: user.user_unique_value,
            user_unique_key: user.user_unique_key,
            page_number: query.page_number || 0
        });

        return userEvents;
    }
}
