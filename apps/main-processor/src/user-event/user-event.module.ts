import { Module } from '@nestjs/common';
import { UserEventModelModule } from '@shared/models/user-event.model';
import { UserModule } from '@shared/user/user.module';
import { UserEventController } from './user-event.controller';
import { UserEventService } from './user-event.service';

@Module({
    imports: [UserModule, UserEventModelModule],
    controllers: [UserEventController],
    providers: [UserEventService],
    exports: [UserEventService]
})
export class UserEventModule {}
