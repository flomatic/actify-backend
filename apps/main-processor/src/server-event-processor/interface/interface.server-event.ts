import { MessageType } from 'apps/api/src/server-event/interface/interface.server-event';

export interface ServerEvent {
    type: MessageType;
    user_unique_value: string;
    user_unique_key: string;
    event_name: string;
    body: any;
    date: Date;
}
