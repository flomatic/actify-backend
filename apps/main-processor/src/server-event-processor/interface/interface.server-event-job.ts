export interface ServerEventJob {
    user_unique_value: string;
    user_unique_key: string;
    event_name: string;
    body: any;
    date: Date;
    timeout_date: Date;
}
