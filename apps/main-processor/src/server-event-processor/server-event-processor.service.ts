import { Injectable } from '@nestjs/common';
import { EnumService } from '@shared/enum/enum.service';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { SessionInterface } from '@shared/models/session.model';
import { UserInterface } from 'apps/api/src/public/interface/user.interface';
import { MessageType } from 'apps/api/src/server-event/interface/interface.server-event';
import { SchemaService } from '@shared/schema/schema.service';
import { Event } from '../socket-parser/interfaces/event';
import { UserService } from '@shared/user/user.service';
import { AnalyticsProcessor } from './../analytics/analytics.processor';
import { EventService } from './../event/event.service';
import { JourneyService } from './../journey/journey.service';
import { SessionService } from './../session/session.service';
import { UserEventService } from './../user-event/user-event.service';
import { ServerEvent } from './interface/interface.server-event';
// import { DelayedJobsService } from '../delayed-jobs/delayed-jobs.service';

@Injectable()
export class ServerEventProcessor {
    constructor(
        private readonly userEventService: UserEventService,
        // private readonly delayedJobService: DelayedJobsService,
        private readonly sessionService: SessionService,
        private readonly enumService: EnumService,
        private readonly userService: UserService,
        private readonly analyticsProcessor: AnalyticsProcessor,
        private readonly journeyService: JourneyService,
        private readonly eventService: EventService,
        private readonly logger: BLogger,
        private readonly schemaService: SchemaService
    ) {}

    async handleEvent(serverEvent: ServerEvent) {
        switch (serverEvent.type) {
        case MessageType.CREATE_USER:
            this.createUser(serverEvent);
            break;
        case MessageType.DELETE_USER:
            this.deleteUser(serverEvent);
            break;
        case MessageType.EVENT:
            this.createEvent(serverEvent);
            break;
        case MessageType.UPDATE_USER:
            this.updateUser(serverEvent);
            break;
        case MessageType.UPDATE_USER_ATTRS:
            this.updateUserAttrs(serverEvent);
            break;
        }
    }

    async createEvent(serverEvent: ServerEvent) {
        const { user_unique_value, user_unique_key, date, type } = serverEvent;
        const findLastSession = async (): Promise<SessionInterface> => {
            return await this.sessionService.findLastSession({
                user_unique_value,
                user_unique_key,
                date
            });
        };

        let lastSession = await findLastSession();
        // TODO: Not important which worker picks up the job?
        if (!lastSession) {
            await new Promise((r) => setTimeout(r, 60 * 1000)); // 1 minute
            lastSession = await findLastSession();
            if (!lastSession) {
                // TODO: Improve the logic if there is no lastSession (use agenda?)
                // TODO: Send an exception to Sentry.
                return;
            }
        }

        const event: Event = {
            session_id: lastSession.session_id,
            event_name: serverEvent.event_name,
            body: serverEvent.body,
            date: serverEvent.date
        };

        // Analyze schema
        await this.analyzeSchema(event);

        // Add to the session (user-events) and raw events (events) and save
        await this.createServerEvent(lastSession, event);

        // Process Journeys
        await this.handleJourneys(event, lastSession);

        // Process Analytics
        await this.handleAnalytics(event, lastSession);
    }

    async createUser(serverEvent: ServerEvent) {
        const user = serverEvent.body as UserInterface;
        const userAttrs = user.user_attributes;
        this.analyzeArray(userAttrs);
        await this.userService.upsertUser({
            user_unique_key: user.user_unique_key,
            user_unique_value: user.user_unique_value,
            email: user.email,
            phone_number: user.phone_number,
            user_attributes: user.user_attributes,
            last_activity: new Date(),
            create_date: new Date(serverEvent.date)
        });
    }

    deleteUser(serverEvent: ServerEvent) {
        this.userService.deleteUser({
            user_unique_key: serverEvent.user_unique_key,
            user_unique_value: serverEvent.user_unique_value
        });
    }

    async updateUser(serverEvent: ServerEvent) {
        const user = serverEvent.body as UserInterface;
        const userAttrs = user.user_attributes;
        this.analyzeArray(userAttrs);
        await this.userService.upsertUser({
            user_unique_key: user.user_unique_key,
            user_unique_value: user.user_unique_value,
            email: user.email,
            phone_number: user.phone_number,
            user_attributes: user.user_attributes,
            last_activity: new Date(),
            create_date: serverEvent.date
        });
    }

    async updateUserAttrs(serverEvent: ServerEvent) {
        const userAttrs = serverEvent.body.user_attributes;
        this.analyzeArray(userAttrs);
        await this.userService.updateAttr({
            user_unique_key: serverEvent.user_unique_key,
            user_unique_value: serverEvent.user_unique_value,
            attrs: userAttrs
        });
    }

    private async analyzeSchema(event: Event) {
        this.schemaService.analyze(event);
        this.enumService.addEnums(event);
    }

    private async analyzeArray(
        params: Array<{ variable: string; value: string }>
    ) {
        try {
            this.schemaService.analyzeUserAttrs(params);
            this.enumService.addUserAttrsEnums(params);
        } catch (e) {
            this.logger.error(ACTIONS.SERVER_EVENT_PROCESSOR_SERVICE, e);
        }
    }

    private async createServerEvent(session: SessionInterface, event: Event) {
        const isUser = session.user_unique_value && session.user_unique_key;
        if (isUser) {
            // insert event in user event (session)
            const userEventData = {
                name: event.event_name,
                body: event.body,
                time: event.date
            };
            await this.userEventService.addServerEvent(userEventData, session);
        }
        const eventData: any = {
            session_id: session.session_id,
            user_unique_key: session.user_unique_key,
            user_unique_value: session.user_unique_value,
            body: event.body,
            name: event.event_name,
            time: event.date
        };
        // insert event into events
        await this.eventService.addEvent(eventData);
    }

    async handleJourneys(event: Event, session: SessionInterface) {
        if (session) {
            await this.journeyService.handleJourneys(event, session);
        }
    }

    async handleAnalytics(event: Event, session: SessionInterface) {
        if (session) {
            await this.analyticsProcessor.handleEvent(event);
        }
    }
}
