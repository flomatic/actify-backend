import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { EnumModule } from '@shared/enum/enum.module';
import { SchemaModule } from '@shared/schema/schema.module';
import { UserModule } from '@shared/user/user.module';
import { EventModule } from '../event/event.module';
import { DelayedJobsModule } from '../journey/delayed-jobs/journey-job.module';
import { JourneyModule } from '../journey/journey.module';
import { ReplayModule } from '../replay/replay.module';
import { AnalyticsModule } from './../analytics/analytics.module';
import { SessionModule } from './../session/session.module';
import { UserEventModule } from './../user-event/user-event.module';
import { ServerEventProcessor } from './server-event-processor.service';

@Module({
    imports: [
        SessionModule,
        UserEventModule,
        DelayedJobsModule,
        JourneyModule,
        CqrsModule,
        UserModule,
        AnalyticsModule,
        EventModule,
        SchemaModule,
        EnumModule
        // ReplayModule
    ],
    providers: [ServerEventProcessor],
    exports: [ServerEventProcessor]
})
export class ServerEventProcessorModule {}
