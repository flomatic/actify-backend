import { Module } from '@nestjs/common';
import { JourneyAnalyticModelModule } from '@shared/models/journey-analytic.model';
import { JourneyAnalyticsService } from './journey-analytics.service';

@Module({
    imports: [JourneyAnalyticModelModule],
    providers: [JourneyAnalyticsService],
    exports: [JourneyAnalyticsService]
})
export class JourneyAnalyticsModule {}
