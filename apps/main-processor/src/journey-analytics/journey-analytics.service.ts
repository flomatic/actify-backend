import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { JourneyAnalyticsInterface } from '@shared/models/journey-analytic.model';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';

@Injectable()
export class JourneyAnalyticsService {
    constructor(
        @InjectModel('JourneyAnalytics')
        private readonly journeyAnalyticsModel: Model<JourneyAnalyticsInterface>
    ) {}

    async add(
        journeyId: mongoose.Types.ObjectId,
        step: { status: 'started' | 'middle'; id: string }
    ) {
        const journeyAnalytic = await this.journeyAnalyticsModel.findOne({
            journey_id: journeyId
        });
        if (journeyAnalytic) {
            if (journeyAnalytic?.analytics?.find((j) => j.step === step.id)) {
                journeyAnalytic.analytics.find((j) => j.step === step.id)
                    .value++;
            } else {
                journeyAnalytic.analytics.push({
                    step: step.id,
                    value: 1
                });
            }
            const totalEntries = journeyAnalytic.info.total_entries;
            const totalInJourneys = journeyAnalytic.info.exist_in_journey;
            const totalExitsJourney = journeyAnalytic.info.total_exits;
            journeyAnalytic.info = {
                total_entries:
                    step.status === 'started' ? totalEntries + 1 : totalEntries,
                exist_in_journey:
                    step.status === 'middle'
                        ? totalInJourneys + 1
                        : totalInJourneys,
                total_exits: totalExitsJourney
            };
            await journeyAnalytic.save();
        } else {
            await new this.journeyAnalyticsModel({
                journey_id: journeyId,
                analytics: [
                    {
                        step: step.id,
                        value: 1
                    }
                ],
                info: {
                    total_entries: step.status === 'started' ? 1 : 0,
                    exist_in_journey: step.status === 'middle' ? 1 : 0,
                    total_exits: 0
                },
                kpi: [],
                kpi_result: []
            }).save();
        }
    }

    async finish(journeyId: mongoose.Types.ObjectId, ids: string[]) {
        const journeyAnalytic = await this.journeyAnalyticsModel.findOne({
            journey_id: journeyId
        });
        if (journeyAnalytic) {
            ids.forEach((id) => {
                if (journeyAnalytic?.analytics?.find((j) => j.step === id)) {
                    journeyAnalytic.analytics.find((j) => j.step === id)
                        .value++;
                } else {
                    journeyAnalytic.analytics.push({
                        step: id,
                        value: 1
                    });
                }
            });
            const totalEntries = journeyAnalytic.info.total_entries;
            const totalInJourneys = journeyAnalytic.info.exist_in_journey;
            const totalExitsJourney = journeyAnalytic.info.total_exits;
            journeyAnalytic.info = {
                total_entries: totalEntries,
                exist_in_journey: totalInJourneys > 0 ? totalInJourneys - 1 : 0,
                total_exits: totalExitsJourney + 1
            };
            await journeyAnalytic.save();
        }
    }

    async addKpi(
        analyticsId: mongoose.Types.ObjectId,
        params: { name: string; first_step_id: string; second_step_id: string }
    ) {
        const journeyAnalytic = await this.journeyAnalyticsModel.findById(
            analyticsId
        );
        if (
            journeyAnalytic &&
            !journeyAnalytic.kpi.find((k) => k.name === params.name)
        ) {
            journeyAnalytic.kpi.push(params);
            journeyAnalytic.kpi_result.push({ name: params.name, value: 0 });
            await journeyAnalytic.save();
        }
    }

    async removeKpi(analyticsId: mongoose.Types.ObjectId, name: string) {
        const journeyAnalytic = await this.journeyAnalyticsModel.findById(
            analyticsId
        );
        if (journeyAnalytic) {
            journeyAnalytic.kpi =
                journeyAnalytic.kpi?.filter((k) => k.name !== name) || [];
            journeyAnalytic.kpi_result =
                journeyAnalytic.kpi_result?.filter((k) => k.name !== name) ||
                [];
            await journeyAnalytic.save();
        }
    }
}
