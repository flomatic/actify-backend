import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { RedisService } from '@shared/redis/redis.service';
import Agenda = require('agenda');
import redis = require('redis');

@Injectable()
export class ShadowConnection {
    agenda: Agenda;
    worker: string;
    jobName: string;
    redis: redis.RedisClient;
    cronJob: string;
    constructor(
        private readonly configService: ConfigService,
        private readonly redisService: RedisService,
        private readonly logger: BLogger
    ) {
        this.agenda = new Agenda({
            db: {
                address: this.configService.get('MONGO_AGENDA_URL'),
                collection: 'RemoveShadowConnectionsJob'
            }
        });
        this.worker = this.configService.get('HOSTNAME') || '';
        this.cronJob = this.configService.get(
            'SHADOW_CONNECTION_REMOVER_CRON_JOB'
        );
        this.jobName = ShadowConnection.name;
        this.redis = this.redisService.getReadWriteClient();
        this.handleJob();
    }

    async handleJob() {
        if (this.belongsToMe()) {
            await this.agenda.start();
            this.defineJob();
            this.schedule();
        }
    }

    belongsToMe() {
        const backendJob = this.configService.get('BACKEND_JOB');
        return backendJob === this.worker;
    }

    defineJob() {
        this.logger.info(ACTIONS.REMOVE_SHADOW_CONNECTION);
        let totalDeleted = 0;
        let totalScanned = 0;
        let index = 0;
        const size = 100;
        this.agenda.define(
            this.jobName,
            async (job: Agenda.Job, done: (error?: Error) => void) => {
                do {
                    try {
                        const values = await this.redisService.scanAsync(
                            index,
                            '*shadow_connection:*',
                            size
                        );
                        index = values.new_cursor;
                        this.logger.info(ACTIONS.REMOVE_SHADOW_CONNECTION, {
                            index: values.new_cursor,
                            length: values.keys.length
                        });
                        await Promise.all(
                            values.keys.map(async (v) => {
                                totalScanned++;
                                const connectionKey = v.replace('shadow_', '');
                                const connection = await this.redisService.get(
                                    connectionKey
                                );
                                if (!connection) {
                                    await this.redisService.clearByKey(v);
                                    totalDeleted++;
                                }
                            })
                        );
                    } catch (e) {
                        this.logger.error(ACTIONS.REMOVE_SHADOW_CONNECTION);
                        index = 0;
                    }
                } while (index != 0);
                this.logger.info(ACTIONS.REMOVE_SHADOW_CONNECTION_DONE, {
                    total_deleted: totalDeleted,
                    total_scanned: totalScanned
                });
                done();
            }
        );
    }

    async schedule() {
        this.agenda.every('24 hours', this.jobName);
    }
}
