import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { User } from '@sentry/node';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { RedisService } from '@shared/redis/redis.service';
import Agenda = require('agenda');
import redis = require('redis');

@Injectable()
export class UpdateSegmentUsers {
    agenda: Agenda;
    worker: string;
    jobName: string;
    redis: redis.RedisClient;
    cronJob: string;
    constructor(
        private readonly configService: ConfigService,
        private readonly redisService: RedisService,
        private readonly logger: BLogger
    ) {
        this.agenda = new Agenda({
            db: {
                address: this.configService.get('MONGO_AGENDA_URL'),
                collection: 'UpdateSegmentUsersJob'
            }
        });
        this.worker = this.configService.get('HOSTNAME') || '';
        this.jobName = UpdateSegmentUsers.name;
        this.redis = this.redisService.getReadWriteClient();
    }

    defineJob() {
        this.logger.info(ACTIONS.REMOVE_SHADOW_CONNECTION);
        const totalDeleted = 0;
        const totalScanned = 0;
        const index = 0;
        const size = 100;
        this.agenda.define(
            this.jobName,
            async (job: Agenda.Job, done: (error?: Error) => void) => {
                try {
                } catch (e) {
                    done(e);
                } finally {
                    done();
                }
            }
        );
    }

    calculateSegmentUsersById(segmentId: string) {
        this.agenda.now(this.jobName, { segment_id: segmentId });
    }
}
