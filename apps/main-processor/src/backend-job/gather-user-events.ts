import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { User } from '@sentry/node';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { SessionInterface } from '@shared/models/session.model';
import { UserInterface } from '@shared/models/user.model';
import { SessionDeviceService } from '@shared/session-device/session-device.service';
import { UserService } from '@shared/user/user.service';
import Agenda = require('agenda');
import { EventService } from '../event/event.service';
import { EventInterface } from '../event/interface/interface.event';
import { SessionService } from '../session/session.service';
import { UserEventService } from '../user-event/user-event.service';

@Injectable()
export class GatherUserEvents {
    agenda: Agenda;
    worker: string;
    jobName: string;
    cronJob: string;
    constructor(
        private readonly configService: ConfigService,
        private readonly userService: UserService,
        private readonly eventService: EventService,
        private readonly userEventService: UserEventService,
        private readonly sessionService: SessionService,
        private readonly sdService: SessionDeviceService,
        private readonly logger: BLogger
    ) {
        this.agenda = new Agenda({
            db: {
                address: this.configService.get('MONGO_AGENDA_URL'),
                collection: 'GatherUserEvents'
            }
        });
        this.agenda.maxConcurrency(10);
        this.worker = this.configService.get('HOSTNAME') || '';
        this.jobName = GatherUserEvents.name;
        this.agenda.start().then(() => this.defineJob());
    }

    defineJob() {
        this.agenda.define(
            this.jobName,
            async (job: Agenda.Job, done: (error?: Error) => void) => {
                try {
                    const { user_id: userId } = job.attrs.data;
                    const user = await this.userService.findUserById(userId);
                    const sessions = [];
                    if (!user) {
                        throw new Error('user_not_found');
                    }
                    const devices = user.devices;
                    await Promise.all(
                        devices?.map(async (device) => {
                            const sessionDevice = await this.sdService.findSessionsByDeviceId(
                                device.device_id
                            );
                            if (sessionDevice) {
                                sessions.push(...sessionDevice.sessions);
                            }
                        })
                    );
                    await Promise.all(
                        sessions.map(async (s) => {
                            await this.updateEvents(s, user);
                        })
                    );
                } catch (e) {
                    this.logger.error(ACTIONS.GATHER_USER_EVENTS, e);
                    done(e);
                } finally {
                    done();
                }
            }
        );
    }

    private async updateEvents(s: string, user: UserInterface) {
        const events = await this.eventService.findEventsBySessionId(s);
        Promise.all(
            events?.map(async (e) => {
                (e.user_unique_key = user.user_unique_key),
                (e.user_unique_value = user.user_unique_value),
                await e.save();
            })
        );
        await this.updateSessions(s, user, events);
    }

    async addByUserId(userId: string) {
        this.agenda.now(this.jobName, { user_id: userId });
    }

    private async updateUserEvetns(
        events: EventInterface[],
        user: UserInterface,
        session: SessionInterface
    ) {
        if (session.end_date && session.start_date) {
            const duration =
                new Date(session.end_date).getTime() -
                new Date(session.start_date).getTime();
            this.userEventService.addEvents({
                user_unique_value: user.user_unique_value,
                user_unique_key: user.user_unique_key,
                session_id: session.session_id,
                duration: duration,
                events: events?.map((e) => {
                    return { name: e.name, body: e.body, time: e.time };
                }),
                start_date: session.start_date,
                end_date: session.end_date
            });
        }
    }
    private async updateSessions(
        s: string,
        user: UserInterface,
        events: Array<EventInterface>
    ) {
        const session = await this.sessionService.findSessionBySessionId(s);
        if (session) {
            session.user_unique_key = user.user_unique_key;
            session.user_unique_value = user.user_unique_value;
            await session.save();
            await this.updateUserEvetns(events, user, session);
        }
    }
}
