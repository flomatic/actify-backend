import { Module } from '@nestjs/common';
import { RedisModule } from '@shared/redis/redis.module';
import { SegmentModule } from '@shared/segment/segment.module';
import { SessionDeviceModule } from '@shared/session-device/session-device.module';
import { UserModule } from '@shared/user/user.module';
import { JourneyModule } from 'apps/main-processor/src/journey/journey.module';
import { EventModule } from '../event/event.module';
import { SessionModule } from '../session/session.module';
import { UserEventModule } from '../user-event/user-event.module';
import { GatherUserEvents } from './gather-user-events';
import { ResetJourney } from './reset-journey.service';
import { ShadowConnection } from './shadow-connection';
import { UpdateSegmentCount } from './update-segment-count';

@Module({
    imports: [
        RedisModule,
        JourneyModule,
        SegmentModule,
        SessionModule,
        SessionDeviceModule,
        UserModule,
        EventModule,
        UserEventModule
    ],
    providers: [
        ShadowConnection,
        ResetJourney,
        UpdateSegmentCount,
        GatherUserEvents
    ],
    exports: [
        ShadowConnection,
        ResetJourney,
        UpdateSegmentCount,
        GatherUserEvents
    ]
})
export class BackendJobModule {
    constructor(private readonly shadowConnection: ShadowConnection) {}
}
