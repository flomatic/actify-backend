import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { RedisService } from '@shared/redis/redis.service';
import Agenda = require('agenda');
import { JourneyService } from 'apps/main-processor/src/journey/journey.service';
import redis = require('redis');
import { JourneyJobService } from 'apps/main-processor/src/journey/journey-job.service';

@Injectable()
export class ResetJourney {
    agenda: Agenda;
    worker: string;
    jobName: string;
    redis: redis.RedisClient;
    cronJob: string;
    constructor(
        private readonly configService: ConfigService,
        private readonly redisService: RedisService,
        private readonly journeyService: JourneyService,
        private readonly journeyJobService: JourneyJobService,
        private readonly logger: BLogger
    ) {
        this.agenda = new Agenda({
            db: {
                address: this.configService.get('MONGO_AGENDA_URL'),
                collection: 'ResetJourneyJob'
            }
        });
        this.worker = this.configService.get('HOSTNAME') || '';
        this.jobName = ResetJourney.name;
        this.redis = this.redisService.getReadWriteClient();
        this.handleJob();
    }

    async handleJob() {
        if (this.belongsToMe()) {
            await this.agenda.start();
            this.defineJob();
        }
    }

    belongsToMe() {
        const backendJob = this.configService.get('BACKEND_JOB');
        return backendJob === this.worker;
    }

    private defineJob() {
        this.agenda.define(
            this.jobName,
            async (job: Agenda.Job, done: (error?: Error) => void) => {
                try {
                    const journeyId = job.attrs.data.journey_id;
                    const journey = await this.journeyService.findJourneyById(
                        journeyId
                    );
                    journey.isLocked = true;
                    await journey.save();
                    await this.journeyJobService.cancelJobsForAJourney(
                        job.attrs.data.journey_id
                    );
                    journey.isLocked = false;
                    await journey.save();
                    done();
                } catch (error) {
                    done(error);
                    this.logger.error(ACTIONS.RESET_MODIFIED_JOURNEY, error);
                }
            }
        );
    }

    async addJob(journeyId: string) {
        this.agenda.now(this.jobName, { journey_id: journeyId });
    }
}
