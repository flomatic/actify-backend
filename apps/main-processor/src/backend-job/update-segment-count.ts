import { UserCalculateStatus } from '@entries/segment/enum/segment-user-count-status.enum';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { SegmentQueryHelper } from '@shared/segment/segment-query-helper';
import { SegmentService } from '@shared/segment/segment.service';
import Agenda = require('agenda');
/* eslint-disable-next-line max-len */
import redis = require('redis');

@Injectable()
export class UpdateSegmentCount {
    agenda: Agenda;
    worker: string;
    jobName: string;
    redis: redis.RedisClient;
    cronJob: string;
    constructor(
        private readonly configService: ConfigService,
        private readonly userSegmentationService: SegmentService,
        private readonly userSegmentCalculator: SegmentQueryHelper,
        private readonly logger: BLogger
    ) {
        this.agenda = new Agenda({
            db: {
                address: this.configService.get('MONGO_AGENDA_URL'),
                collection: 'UpdateSegmentCountJob'
            }
        });
        this.agenda.maxConcurrency(5);
        this.worker = this.configService.get('HOSTNAME') || '';
        this.cronJob = this.configService.get(
            'SHADOW_CONNECTION_REMOVER_CRON_JOB'
        );
        this.jobName = UpdateSegmentCount.name;
        this.handleJob();
    }

    async handleJob() {
        if (this.belongsToMe()) {
            await this.agenda.start();
            this.defineJob();
            this.schedule();
        }
    }

    private belongsToMe() {
        const backendJob = this.configService.get('BACKEND_JOB');
        return backendJob === this.worker;
    }

    private defineJob() {
        this.agenda.define(
            this.jobName,
            async (job: Agenda.Job, done: (error?: Error) => void) => {
                const startTime = Date.now();
                let finalTime;
                const segmentId = job.attrs.data?.segment_id;

                try {
                    const segment = await this.userSegmentationService.findSegmentationById(
                        segmentId
                    );

                    if (!segment) {
                        throw new Error('segment not found');
                    }

                    segment.user_count.status = UserCalculateStatus.CALCULATING;
                    await segment.save();

                    const {
                        count
                    } = await this.userSegmentCalculator.calculate(
                        segment as any
                    );

                    finalTime = Date.now() - startTime;

                    const userCount = {
                        value: count,
                        status: UserCalculateStatus.READY,
                        last_calculation_duration: finalTime
                    };
                    segment.user_count = userCount;
                    await segment.save();
                    this.logger.info(ACTIONS.UPDATE_SEGMENT_COUNT, {
                        segment_id: segmentId,
                        duration: finalTime
                    });
                } catch (error) {
                    console.log(error);
                    this.logger.error(ACTIONS.UPDATE_SEGMENT_COUNT, {
                        segment_id: segmentId,
                        error
                    });
                    done(error);
                } finally {
                    done();
                }
            }
        );
    }

    private async schedule() {
        this.agenda.every('30 minutes', this.jobName);
    }

    updateSegmentCountById(segmentId: string) {
        this.agenda.now(this.jobName, { segment_id: segmentId });
    }
}
