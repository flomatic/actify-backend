import { Injectable } from '@nestjs/common';
import { NotificationReportService } from './report-service';
import { NotificationReport } from './schema/report.schema';
import { TokenService } from './token.service';

@Injectable()
export class CallbackService {
    cb: (token: string) => void;
    constructor(
        private readonly tokenService: TokenService,
        private readonly reportService: NotificationReportService
    ) {}

    async register(params: {
        token: string;
        uuk: string;
        uuv: string;
        channel: string;
    }) {
        const token = await this.tokenService.verifyToken(params.token);
        if (token) {
            this.reportService.addNotification({
                token,
                ref_token: params.token,
                uuk: params.uuk,
                uuv: params.uuv
            } as NotificationReport);
            this.cb(token);
        }
    }

    recivedCallback(cb: (token: string) => void) {
        this.cb = cb;
    }
}
