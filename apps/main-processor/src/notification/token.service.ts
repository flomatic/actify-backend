import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as RandomString from 'randomstring';
import { TokenInterface } from './interface/token.interface';

@Injectable()
export class TokenService {
    constructor(
        @InjectModel('Token')
        private readonly tokenModel: Model<TokenInterface>
    ) {}

    async generateToken(exteranl_token: string): Promise<string> {
        let internalToken: string;
        const token = RandomString.generate({ length: 10 });
        const dbToken = await this.tokenModel.findOne({
            internal_token: token
        });
        if (dbToken) {
            return await this.generateToken(exteranl_token);
        } else {
            internalToken = token;
            await new this.tokenModel({
                internal_token: internalToken,
                exteranl_token
            }).save();
            return internalToken;
        }
    }

    async verifyToken(internal_token: string): Promise<string | undefined> {
        return (await this.tokenModel.findOne({ internal_token }))
            ?.external_token;
    }
}
