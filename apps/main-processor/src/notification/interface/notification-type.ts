export enum NotificationType {
    SMS = 'sms',
    EMAIL = 'email',
    PUSH_NOTIFICATION = 'push_notification'
}
