import { Document } from 'mongoose';
export interface TokenInterface extends Document {
    internal_token: string;
    external_token: string;
    created_at: Date;
    updated_at: Date;
}
