export interface EmailBody {
    message: string;
    email_address: string;
    template_id: string;
}
