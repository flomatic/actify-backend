import { RateLimit } from '@entries/ratelimit/model/ratelimit';
import { Types } from 'mongoose';
import { RateLimitCount } from '../schema/rate-limit.schema';

export interface NotificationInterface {
    token?: string;
    time_out?: Date;
    retry?: {
        in: number;
        next: 'day' | 'hour';
    };
    user_id: Types.ObjectId;
    notification: {
        body: string;
        payload: any;
    };
    type: 'sms' | 'email' | 'push_notification';
    ignore_sleep_time?: boolean;
    ignore_frequency_cap?: boolean;
    rate_limit?: RateLimit;
    group_name: string;
    total_retry?: number;
}
