export interface PushNotificationBody {
    message: string;
    token: string;
    icon?: string;
}
