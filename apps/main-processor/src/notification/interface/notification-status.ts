export enum NotificationStatus {
    SUCCESS = 'success',
    FAILED = 'failed',
    INTERACTED = 'interacted'
}
