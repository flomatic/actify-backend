import * as mongoose from 'mongoose';

export interface UserNotificationInterface extends mongoose.Document {
    email: number;
    sms: number;
    push_notification: number;
    from_date: Date;
    to_date: Date;
    user_unique_value: string;
    user_unique_key: string;
}
