import { Test } from '@nestjs/testing';
import { UserInterface } from '@shared/models/user.model';
import { mock } from 'jest-mock-extended';
import { SettingService } from '../../../../shared/setting/setting.service';
import { NotificationInterface } from './interface/job.interface';
import { JobService } from './job-service';
import { RateLimitService } from './rate-limit.service';
import { UserNotificationService } from './user-notification.service';
describe('Job Service', () => {
    let settingService: SettingService;
    let rateLimitService: RateLimitService;
    let jobService: JobService;
    let userNotificationService: UserNotificationService;

    beforeEach(async () => {
        const settingModule = await Test.createTestingModule({
            providers: [
                {
                    provide: SettingService,
                    useFactory: () => ({
                        getRateLimit: jest.fn(() => {
                            return {
                                email: 100,
                                push_notification: 100,
                                sms: 100
                            };
                        }),
                        getProviderRateLimit: jest.fn(() => {
                            return {
                                email: 1000,
                                push_notification: 1000,
                                sms: 1000
                            };
                        }),
                        getDNDTime: jest.fn(() => {
                            return {
                                from_hour: 23,
                                to_hour: 8
                            };
                        })
                    })
                }
            ]
        }).compile();

        const rateLimitModule = await Test.createTestingModule({
            providers: [
                {
                    provide: RateLimitService,
                    useFactory: () => ({
                        canSendNow: jest.fn(() => {
                            return true;
                        })
                    })
                }
            ]
        }).compile();

        const userNotificationModule = await Test.createTestingModule({
            providers: [
                {
                    provide: UserNotificationService,
                    useFactory: () => ({
                        getTodayUserNotifications: jest.fn(() => {
                            return {
                                sms: 0,
                                email: 0,
                                push_notification: 0
                            };
                        })
                    })
                }
            ]
        }).compile();

        settingService = settingModule.get<SettingService>(SettingService);

        rateLimitService = rateLimitModule.get<RateLimitService>(
            RateLimitService
        );

        userNotificationService = userNotificationModule.get<UserNotificationService>(
            UserNotificationService
        );

        const jobService = new JobService(
            null,
            null,
            null,
            null,
            null,
            rateLimitService,
            userNotificationService,
            settingService
        );

        // rateLimitService = moduleRef.get<RateLimitService>(
        //     RateLimitService
        // );
        // jobService = moduleRef.get<JobService>(JobService);

        // userNotificationService = moduleRef.get<
        //     UserNotificationService
        // >(UserNotificationService);
    });

    describe('getExecutionTime', () => {
        it('should return an array of cats', async () => {
            jest.mock('../notification/interface/job.interface.ts');
            const n: NotificationInterface = {
                token: null,
                time_out: null,
                retry: {
                    in: 1,
                    next: 'hour'
                },
                user_id: null,
                notification: null,
                type: 'sms',
                ignore_sleep_time: false,
                ignore_frequency_cap: false,
                rate_limit: {
                    sms: 0,
                    email: 0,
                    push_notification: 0
                },
                group_name: 'test',
                total_retry: 0
            };
            const user: UserInterface = mock<UserInterface>();

            // jest.spyOn(
            //     userNotificationService,
            //     'getTodayUserNotifications'
            // ).mockImplementation(async (params: any) => {
            //     return {
            //         sms: 0,
            //         email: 0,
            //         push_notification: 0
            //     } as UserNotificationInterface;
            // });

            // jest.spyOn(
            //     settingService,
            //     'getRateLimit'
            // ).mockImplementation(async () => {
            //     return {
            //         sms: 0,
            //         email: 0,
            //         push_notification: 0
            //     };
            // });

            expect(await jobService.getExecutionTime(n, user)).toBe(null);
        });
    });
});
