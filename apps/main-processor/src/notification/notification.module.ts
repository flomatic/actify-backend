import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { BullModule } from '@shared/bull/bull.module';
import { EmailModule } from '@shared/email/email.module';
import { PushNotificationModule } from '@shared/push-notification/push-notification.module';
import { SettingModule } from '@shared/setting/setting.module';
import { ShortLinkModule } from '@shared/shortlink/shortlink.module';
import { SmsModule } from '@shared/sms/sms.module';
import { UserModule } from '@shared/user/user.module';
import { VariableReplaceModule } from '@shared/variable-replace/variable-replace.module';
import { VariableModule } from 'apps/api/src/variable/variable.module';
import { UserNotificationSchema } from '../user-notification/schema/user-notification.schema';
import { CallbackService } from './callback-service';
import { JobService } from './job-service';
import { NotificationService } from './notification.service';
import { RateLimitService } from './rate-limit.service';
import { NotificationReportService } from './report-service';
import { RateLimitSchema } from './schema/rate-limit.schema';
import { ReportNotificationSchema } from './schema/report.schema';
import { TokenSchema } from './schema/token.schema';
import { InternalSMSService } from './sms.service';
import { TokenService } from './token.service';
import { UserNotificationService } from './user-notification.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: 'NotificationReport',
                schema: ReportNotificationSchema
            },
            {
                name: 'UserNotification',
                schema: UserNotificationSchema
            },
            { name: 'Token', schema: TokenSchema },
            {
                name: 'RateLimit',
                schema: RateLimitSchema
            }
        ]),
        ConfigModule,
        SmsModule,
        EmailModule,
        VariableReplaceModule,
        PushNotificationModule,
        BullModule,
        ShortLinkModule,
        SettingModule,
        VariableModule,
        UserModule,
        PushNotificationModule
    ],
    providers: [
        InternalSMSService,
        UserNotificationService,
        NotificationReportService,
        CallbackService,
        JobService,
        RateLimitService,
        NotificationService,
        TokenService
    ],
    exports: [InternalSMSService, NotificationService, TokenService]
})
export class NotificationModule {}
