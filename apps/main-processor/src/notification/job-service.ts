import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EmailService } from '@shared/email/email.service';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { UserInterface } from '@shared/models/user.model';
import { PushNotificationService } from '@shared/push-notification/push-notification.service';
import { SettingService } from '@shared/setting/setting.service';
import { SMSInterface } from '@shared/sms/interface/sms.interface';
import { UserService } from '@shared/user/user.service';
import { VariableReplaceHelper } from '@shared/variable-replace/variable-replace.helper';
import { isEmpty, isNull } from 'lodash';
import { NotificationInterface } from './interface/job.interface';
import { NotificationStatus } from './interface/notification-status';
import { RateLimitService } from './rate-limit.service';
import { InternalSMSService } from './sms.service';
import { TokenService } from './token.service';
import { UserNotificationService } from './user-notification.service';
import Agenda = require('agenda');
import { RateLimit } from '@entries/ratelimit/model/ratelimit';
import { number } from 'joi';

@Injectable()
export class JobService {
    agenda: Agenda;
    worker: string;
    timeZoneOffset: number;
    globalrateLimitRef: RateLimit;
    defaultDnD: { from_hour: number; to_hour: number };
    cb: (token: string, status: NotificationStatus) => void;

    constructor(
        private readonly configService: ConfigService,
        private readonly smsService: InternalSMSService,
        private readonly tokenService: TokenService,
        private readonly userService: UserService,
        private readonly logger: BLogger,
        private readonly variableHelper: VariableReplaceHelper,
        private readonly rateLimitService: RateLimitService,
        private readonly emailService: EmailService,
        private readonly userNotificationService: UserNotificationService,
        private readonly pushNotificationService: PushNotificationService,
        private readonly settingService: SettingService
    ) {
        this.agenda = new Agenda({
            db: {
                address: this.configService.get('MONGO_AGENDA_URL'),
                collection: 'NotificationJob'
            }
        });
        this.agenda.start();
        this.worker = this.configService.get('HOSTNAME') || '';
        this.injectSetting();
    }
    injectSetting() {
        setInterval(async () => {
            this.timeZoneOffset = await this.getTimeZoneOffset();
            this.globalrateLimitRef = await this.settingService.getTimeZoneOffset();
            this.defaultDnD = await this.settingService.getDNDTime();
        }, 60 * 1000);
    }

    private async getTimeZoneOffset() {
        return await this.settingService.getTimeZoneOffset();
    }

    addJob(notification: NotificationInterface) {
        const jobName = `notification_job::${notification.group_name}::${notification.user_id}`;
        this.defineJob(jobName);
        this.agenda.now(jobName, notification);
    }

    jobStatusChanged(
        changes: (token: string, status: NotificationStatus) => void
    ) {
        this.cb = changes;
    }

    defineJob(jobName: string) {
        this.agenda.define(
            jobName,
            async (
                job: Agenda.Job<NotificationInterface>,
                done: (error?: Error) => void
            ) => {
                const notification = job.attrs.data;
                const user = await this.userService.findUserById(
                    notification.user_id
                );
                const execDate = await this.getExecutionTime(
                    notification,
                    user
                );
                if (!execDate) {
                    done();
                    return;
                }
                let contentToSend = notification.notification.body;

                const token = await this.tokenService.generateToken(
                    notification.token
                );
                contentToSend = await this.variableHelper.replaceLinkVariables(
                    contentToSend,
                    token
                );

                // if tasks should run in 10 seconds from now we should send it immediately
                if (
                    Math.abs(execDate.getTime() - new Date().getTime()) <
                    60 * 1000
                ) {
                    try {
                        await this.sendNotification(
                            notification,
                            contentToSend,
                            user
                        );
                        await this.cb(
                            notification.token,
                            NotificationStatus.SUCCESS
                        );
                        const refRateLimit: RateLimit =
                            isEmpty(notification.rate_limit) ||
                            !notification.rate_limit.per
                                ? this.globalrateLimitRef
                                : notification.rate_limit;
                        this.rateLimitService.add(
                            notification.type,
                            notification.group_name,
                            notification.user_id.toString(),
                            refRateLimit
                        );
                        done();
                    } catch (e) {
                        this.handleRetry(job, done, e);
                    }
                } else {
                    job.schedule(execDate);
                }

                await job.save();
            }
        );
    }

    private async handleRetry(
        job: Agenda.Job<NotificationInterface>,
        done: any,
        e: any
    ) {
        const notification: NotificationInterface = job.attrs.data;
        //  by the order of Yasaman
        const totalRetry = 5;

        const totalRetriesSoFar = notification.total_retry || 0;

        if (totalRetriesSoFar < totalRetry) {
            const retryPeriod = notification.retry?.next || 'hour';
            const retryCount = notification.retry?.in || 5;
            const totalPeriodInMs =
                retryCount *
                (retryPeriod === 'hour' ? 60 * 60 * 1000 : 24 * 60 * 60 * 1000);
            const delayInMs =
                (totalPeriodInMs / totalRetry) * totalRetriesSoFar;
            if (delayInMs < 60 * 1000) {
                job.run();
            } else {
                job.schedule(
                    new Date(new Date().getTime() + delayInMs + 60 * 1000)
                );
            }
            if (!job.attrs.data.total_retry) job.attrs.data.total_retry = 0;
            job.attrs.data.total_retry++;
            job.save();
        } else {
            this.cb(notification.token, NotificationStatus.FAILED);
            done(e);
        }
    }

    async getExecutionTime(
        jobInterface: NotificationInterface,
        user: UserInterface
    ) {
        if (jobInterface.time_out) {
            if (new Date() > jobInterface.time_out) {
                return null;
            }
        }
        const rateLimit: RateLimit = isEmpty(jobInterface.rate_limit)
            ? this.globalrateLimitRef
            : jobInterface.rate_limit;

        const ignoreGlobalRateLimit =
            isEmpty(jobInterface.rate_limit) || isNull(jobInterface.rate_limit);
        const canSendNow = await this.rateLimitService.canSend(
            jobInterface.group_name,
            rateLimit,
            user._id.toString(),
            ignoreGlobalRateLimit,
            !ignoreGlobalRateLimit,
            jobInterface.ignore_frequency_cap,
            jobInterface.type
        );
        if (!canSendNow.status) {
            return canSendNow.next_date;
        }

        const execDateForDnd = await this.getNextExecutionTimeForDnd(
            jobInterface.ignore_sleep_time
        );
        return execDateForDnd;
    }

    private async getNextExecutionTimeForDnd(ignore_sleep_time) {
        if (!ignore_sleep_time) {
            return new Date();
        }
        const nowDate = new Date();
        let zeroHour = new Date();
        nowDate.setTime(nowDate.getTime() + this.timeZoneOffset * 60 * 1000);
        zeroHour = nowDate;
        zeroHour.setHours(0, 0, 0, 0);
        const nowDateHour = nowDate.getHours();
        const executionTimeForSleep = new Date(nowDate);
        const sleepTime: {
            from_hour: number;
            to_hour: number;
        } = await this.settingService.getDNDTime();
        const timePeriods = [];
        let passedTheNight = false;
        for (let i = sleepTime.from_hour; i <= sleepTime.to_hour; i++) {
            if (i == 24) {
                i = 0;
                passedTheNight = true;
            }
            timePeriods.push(i);
        }
        if (timePeriods.includes(nowDateHour)) {
            if (passedTheNight) {
                const indexOfZero = timePeriods.indexOf(0);
                const indexOfCurrent = timePeriods.indexOf(nowDateHour);
                if (indexOfCurrent < indexOfZero) {
                    executionTimeForSleep.setDate(
                        executionTimeForSleep.getDate() + 1
                    );
                }
            }
            executionTimeForSleep.setHours(sleepTime.to_hour);
            return executionTimeForSleep;
        }
        return new Date();
    }

    private async sendNotification(
        jobInterface: NotificationInterface,
        content: string,
        user: UserInterface
    ) {
        /* eslint-disable */
        switch (jobInterface.type) {
            case 'sms':
                await this.smsService.send({
                    phone_number: user.phone_number,
                    body: content
                } as SMSInterface);
                break;
            case 'email':
                await this.emailService.sendEmail({
                    email: user.email,
                    html: content,
                    subject: jobInterface.notification.payload.subject
                });
                break;
            case 'push_notification':
                await Promise.all(
                    user?.devices?.map(async (d) => {
                        if (d.firebase_token) {
                            try {
                                await this.pushNotificationService.sendPush({
                                    to: d.firebase_token,
                                    subject:
                                        jobInterface?.notification?.payload
                                            ?.subject,
                                    body: jobInterface?.notification?.body,
                                    payload:
                                        jobInterface?.notification?.payload
                                            ?.payload
                                });
                            } catch (e) {
                                this.logger.error(
                                    ACTIONS.SEND_PUSH_NOTIFICATION,
                                    e
                                );
                            }
                        }
                    })
                );
                break;
            //
        }
        /* eslint-enable */
    }
}
