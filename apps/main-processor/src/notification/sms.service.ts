import { Injectable } from '@nestjs/common';
import { SMSInterface } from '@shared/sms/interface/sms.interface';
import { SmsService } from '@shared/sms/sms.service';
import { UserService } from '@shared/user/user.service';

@Injectable()
export class InternalSMSService {
    constructor(
        private readonly userService: UserService,
        private readonly smsService: SmsService
    ) {}
    async send(
        smsInterface: SMSInterface
    ): Promise<{
        status: 'success' | 'delivered';
    }> {
        await this.smsService.sendSMS({
            phone_number: smsInterface.phone_number,
            body: smsInterface.body
        });
        return {
            status: 'success'
        };
    }
}
