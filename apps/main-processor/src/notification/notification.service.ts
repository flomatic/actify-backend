import { Injectable } from '@nestjs/common';
import { CallbackService } from './callback-service';
import { NotificationInterface } from './interface/job.interface';
import { NotificationStatus } from './interface/notification-status';
import { JobService } from './job-service';
import { NotificationReportService } from './report-service';
import { TokenService } from './token.service';

@Injectable()
export class NotificationService {
    callBacks: Array<(token: string, status: NotificationStatus) => void> = [];
    constructor(
        private readonly jobService: JobService,
        private readonly callbackService: CallbackService,
        private readonly tokenService: TokenService,
        private readonly notificationReportService: NotificationReportService
    ) {
        this.callbackService.recivedCallback(this.callBacRecieved);
        this.jobService.jobStatusChanged(this.jobStatusChanged);
    }

    async sendNotification(notification: NotificationInterface) {
        this.jobService.addJob(notification);
    }

    async callback(params: {
        token: string;
        uuk: string;
        uuv: string;
        channel: string;
    }) {
        this.callbackService.register(params);
    }

    async notificationStatusChanged(
        callBack: (token: string, status: NotificationStatus) => void
    ) {
        this.callBacks.push(callBack);
    }

    jobStatusChanged = (token: string, status: NotificationStatus) => {
        this.broadCast(token, status);
    };

    private async callBacRecieved(token: string) {
        //  this.broadCast(token, NotificationStatus.INTERACTED);
    }

    private broadCast(token: string, status: NotificationStatus) {
        this.callBacks?.forEach((callBack) => {
            callBack(token, status);
        });
    }
}
