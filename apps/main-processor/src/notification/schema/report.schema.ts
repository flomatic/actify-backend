import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NotificationReportDocument = NotificationReport & Document;

@Schema()
export class NotificationReport {
    @Prop()
    ref_token: string;

    @Prop()
    token: string;

    @Prop()
    uuk: string;

    @Prop({ index: true })
    uuv: string;

    @Prop()
    created_at?: Date;

    @Prop()
    updated_at?: Date;
}

export const ReportNotificationSchema = SchemaFactory.createForClass(
    NotificationReport
);

ReportNotificationSchema.set('timestamps', {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});

//  what  a greay way to transform data
// ReportNotificationSchema.set('toJSON', {
//     transform: (doc, ret, options) => {

//     }
// })
