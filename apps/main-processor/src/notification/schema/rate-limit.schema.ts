import { RateLimitPeriod } from '@entries/ratelimit/enum/ratelimit-period';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export type RateLimitDocument = RateLimit & Document;

export class RateLimitCount {
    @Prop({ type: Number, default: 0 })
    sms: number;
    @Prop({ type: Number, default: 0 })
    push_notification: number;
    @Prop({ type: Number, default: 0 })
    email: number;
}

@Schema()
export class RateLimit {
    @Prop({ type: String, index: true })
    group_name: string;

    @Prop({
        type: RateLimitCount
    })
    rate_limit: RateLimitCount;
    @Prop({
        type: RateLimitCount
    })
    rate_limit_ref: RateLimitCount;

    @Prop({ Type: Date, index: true })
    from_date: Date;

    @Prop({ Type: Date, index: true })
    to_date: Date;

    @Prop({ type: Date })
    created_at: Date;

    @Prop({ type: Date })
    updated_at: Date;

    @Prop({
        type: String,
        enum: [...Object.values(RateLimitPeriod)],
        default: 'hour'
    })
    time_period?: string;
}

export const RateLimitSchema = SchemaFactory.createForClass(RateLimit);
RateLimitSchema.method('canSendNow', function (this: RateLimitDocument) {
    const refRateLimit = this.rate_limit_ref;
    const rateLimit = this.rate_limit;
    const timePeriod = this.time_period;
    const spentTimeInMs = new Date().getTime() - this.from_date.getTime();
    const totalTimeInMs =
        timePeriod === RateLimitPeriod.MINUTE
            ? 60 * 1000
            : timePeriod === RateLimitPeriod.HOUR
                ? 60 * 60 * 1000
                : 24 * 60 * 60 * 1000;
    const expectedSMSRate = Math.min(
        (refRateLimit.sms * spentTimeInMs) / totalTimeInMs,
        1
    );
    const expectedEmailRate = Math.min(
        (refRateLimit.email * spentTimeInMs) / totalTimeInMs,
        1
    );
    const expectedPushRate = Math.min(
        (refRateLimit.push_notification * spentTimeInMs) / totalTimeInMs,
        1
    );
    return {
        sms: rateLimit.sms < expectedSMSRate,
        email: rateLimit.email < expectedEmailRate,
        push_notification: rateLimit.push_notification < expectedPushRate
    };
});

RateLimitSchema.set('timestamps', {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});

// //  what  a great way to transform data
// RateLimitSchema.set('toJSON', {
//     transform: (doc, ret, options) => {
//         ret.id = ret._id
//         delete ret._id
//         delete ret._v
//     }
// });
