import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TokenDocumnet = Token & Document;

@Schema()
export class Token {
    @Prop()
    ref_token: string;

    @Prop()
    token: string;

    @Prop()
    uuk: string;

    @Prop({ index: true })
    uuv: string;

    @Prop()
    created_at?: Date;

    @Prop()
    updated_at?: Date;
}

export const TokenSchema = SchemaFactory.createForClass(Token);

TokenSchema.set('timestamps', {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});

//  what  a greay way to transform data
// ReportNotificationSchema.set('toJSON', {
//     transform: (doc, ret, options) => {

//     }
// })
