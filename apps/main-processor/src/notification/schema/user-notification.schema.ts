import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserNotificationDocument = UserNotification & Document;

@Schema()
export class UserNotification {
    @Prop()
    email: number;
    @Prop()
    sms: number;
    @Prop()
    push_notification: number;
    @Prop({ index: true })
    from_date: Date;
    @Prop({ index: true })
    to_date: Date;
    @Prop({ index: true })
    user_unique_value: string;
    @Prop({ index: true })
    user_unique_key: string;
}

export const TokenSchema = SchemaFactory.createForClass(UserNotification);

TokenSchema.set('timestamps', {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});

//  what  a greay way to transform data
// ReportNotificationSchema.set('toJSON', {
//     transform: (doc, ret, options) => {

//     }
// })
