import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
    NotificationReport,
    NotificationReportDocument
} from './schema/report.schema';

@Injectable()
export class NotificationReportService {
    constructor(
        @InjectModel('NotificationReport')
        private notificationReportModel: Model<NotificationReportDocument>
    ) {}

    async addNotification(report: NotificationReport) {
        return new this.notificationReportModel(report).save();
    }
}
