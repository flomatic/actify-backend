import { RateLimitPeriod } from '@entries/ratelimit/enum/ratelimit-period';
import { RateLimit } from '@entries/ratelimit/model/ratelimit';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SettingService } from '@shared/setting/setting.service';
import { getFromToDateInDay } from 'apps/api/src/analytics/helper/date-utils';
import { Model } from 'mongoose';
import {
    getFromToDateInHour,
    getFromToDateInMinute
} from '../analytics/helper/date-utils';
import { RateLimitDocument } from './schema/rate-limit.schema';

@Injectable()
export class RateLimitService {
    globalRateLimitRef: RateLimit;
    userRateLimitRef: RateLimit;
    constructor(
        @InjectModel('RateLimit')
        private rateLimitModel: Model<RateLimitDocument>,
        private readonly settingService: SettingService
    ) {}
    async injectGlobalRateLimitRef() {
        if (!this.globalRateLimitRef && !this.userRateLimitRef) {
            this.globalRateLimitRef = await this.settingService.getGlobalRateLimit();
            this.userRateLimitRef = await this.settingService.getUserRateLimit();
        }
    }

    async canSend(
        groupName: string,
        groupRateLimit: RateLimit,
        user_id: string,
        ignore_global_rate_limit: boolean,
        ignore_group_rate_limit: boolean,
        ignore_user_rate_limit: boolean,
        type: 'sms' | 'email' | 'push_notification'
    ): Promise<{ status: boolean; next_date: Date }> {
        let nextDate = new Date();
        const globalRateLimitResult = ignore_global_rate_limit
            ? true
            : await this.canSendFor(
                'global_rate_limit',
                this.globalRateLimitRef,
                type
            );
        if (!globalRateLimitResult) {
            nextDate = this.getNextDateFromPerTime(this.globalRateLimitRef.per);
        }
        const gropRateLimitResult = ignore_group_rate_limit
            ? true
            : await this.canSendFor(groupName, groupRateLimit, type);
        if (!gropRateLimitResult) {
            nextDate = this.getNextDateFromPerTime(groupRateLimit.per);
        }

        const userRateLimit = ignore_user_rate_limit
            ? true
            : await this.canSendFor(user_id, this.userRateLimitRef, type);
        if (!userRateLimit) {
            nextDate = this.getNextDateFromPerTime(this.userRateLimitRef.per);
        }
        return {
            status:
                globalRateLimitResult && gropRateLimitResult && userRateLimit,
            next_date: nextDate
        };
    }

    private getNextDateFromPerTime(per: string) {
        const date = new Date();
        switch (per) {
        case RateLimitPeriod.DAY:
            date.setDate(date.getDate() + 1);
            return date;
        case RateLimitPeriod.HOUR:
            date.setHours(date.getHours() + 1);
            return date;
        case RateLimitPeriod.MINUTE:
            date.setMinutes(date.getMinutes() + 1);
            return date;
        default:
            return date;
        }
    }

    private async canSendFor(
        groupName: string,
        rateLimitRef: RateLimit,
        type: string
    ) {
        await this.injectGlobalRateLimitRef();
        const currentDatePeriod =
            rateLimitRef.per === RateLimitPeriod.MINUTE
                ? getFromToDateInMinute()
                : rateLimitRef.per === RateLimitPeriod.HOUR
                    ? getFromToDateInHour()
                    : getFromToDateInDay();
        const rateLimit: RateLimitDocument = await this.rateLimitModel.findOne({
            group_name: groupName,
            from_date: currentDatePeriod.from_date,
            to_date: currentDatePeriod.to_date,
            time_period: rateLimitRef.per
        });
        if (!rateLimit) {
            return true;
        } else {
            const result = await (rateLimit as any).canSendNow();
            return result[type];
        }
    }

    async add(
        type: 'sms' | 'email' | 'push_notification',
        groupName: string,
        user_id: string,
        refRateLimit: RateLimit
    ) {
        await this.injectGlobalRateLimitRef();
        await this.addRateLimit(
            type,
            'global_rate_limit',
            this.globalRateLimitRef
        );
        await this.addRateLimit(type, groupName, refRateLimit);
        await this.addRateLimit(type, user_id, refRateLimit);
    }
    private async addRateLimit(
        type: string,
        groupName: string,
        rateLimitRef: RateLimit
    ) {
        const datePeriod = this.getDatePeiodFromRateLimitType(rateLimitRef.per);
        const rateLimit = await this.rateLimitModel.findOne({
            group_name: groupName,
            from_date: datePeriod.from_date,
            to_date: datePeriod.to_date,
            time_period: rateLimitRef.per
        });
        if (rateLimit) {
            rateLimit.rate_limit[type] = rateLimit.rate_limit[type] + 1;
            rateLimit.markModified('rate_limit');
            await rateLimit.save();
        } else {
            await new this.rateLimitModel({
                group_name: groupName,
                rate_limit: {
                    sms: type === 'sms' ? 1 : 0,
                    email: type === 'email' ? 1 : 0,
                    push_notification: type === 'push_notification' ? 1 : 0
                },
                rate_limit_ref: { ...rateLimitRef },
                from_date: datePeriod.from_date,
                to_date: datePeriod.to_date,
                time_period: rateLimitRef.per
            }).save();
        }
    }
    getDatePeiodFromRateLimitType(per: RateLimitPeriod) {
        switch (per) {
        case RateLimitPeriod.DAY:
            return getFromToDateInDay();
        case RateLimitPeriod.HOUR:
            return getFromToDateInHour();
        case RateLimitPeriod.MINUTE:
            return getFromToDateInMinute();
        }
    }
}
