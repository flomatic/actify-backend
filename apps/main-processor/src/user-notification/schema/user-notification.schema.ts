import * as mongoose from 'mongoose';

export const UserNotificationSchema = new mongoose.Schema({
    email: Number,
    sms: Number,
    push_notification: Number,
    from_date: Date,
    to_date: Date
});
