import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { getFromToDateInDay } from '../analytics/helper/date-utils';
import { UserNotificationInterface } from './interface/user-notification.interface';

@Injectable()
export class UserNotificationService {
    constructor(
        @InjectModel('user-notification')
        private readonly userNotificationModel: Model<UserNotificationInterface>
    ) {}

    async add(params: {
        type: 'email' | 'push_notification' | 'sms';
        user_id: Types.ObjectId;
    }) {
        const fromToDate = getFromToDateInDay();
        let userNotification = await this.userNotificationModel.findOne({
            from_date: { $gte: fromToDate.from_date },
            to_date: { $lte: fromToDate.to_date },
            user_id: params.user_id
        });
        if (!userNotification) {
            userNotification = new this.userNotificationModel({
                sms: params.type === 'sms' ? 1 : 0,
                email: params.type === 'email' ? 1 : 0,
                push_notification: params.type === 'push_notification' ? 1 : 0,
                user_id: params.user_id
            });
            await userNotification.save();
        } else {
            /* eslint-disable */
            switch (params.type) {
                case 'push_notification':
                    userNotification.push_notification = userNotification.push_notification + 1;
                    break;
                case 'sms':
                    userNotification.sms = userNotification.sms + 1;
                    break;
                case 'email':
                    userNotification.email = userNotification.email + 1;
                    break;
                /* eslint-enable */
            }
            await userNotification.save();
        }
    }

    async getTodayUserNotifications(userId: Types.ObjectId) {
        const fromToDate = getFromToDateInDay();
        let userNotification = await this.userNotificationModel.findOne({
            from_date: { $gte: fromToDate.from_date },
            to_date: { $lte: fromToDate.to_date },
            user_id: userId
        });
        if (!userNotification) {
            userNotification = new this.userNotificationModel({
                sms: 0,
                email: 0,
                push_notification: 0,
                user_id: userId
            });
            await userNotification.save();
        } else {
            return userNotification;
        }
    }
}
