import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserNotificationSchema } from './schema/user-notification.schema';
import { UserNotificationService } from './user-notification.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'user-notification', schema: UserNotificationSchema }
        ])
    ],
    providers: [UserNotificationService],
    exports: [UserNotificationService]
})
export class UserNotificationModule {}
