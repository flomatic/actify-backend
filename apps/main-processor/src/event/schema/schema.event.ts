import * as mongoose from 'mongoose';

export const EventSchema = new mongoose.Schema({
    session_id: { type: String },
    user_unique_value: { type: String, index: true },
    user_unique_key: { type: String, index: true },
    user_id: { type: mongoose.Types.ObjectId, index: true },
    body: { type: Object },
    name: { type: String, index: true },
    time: { type: Date, index: true }
});

EventSchema.index({
    time: 1,
    name: 1
});
