import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EventInterface } from './interface/interface.event';

@Injectable()
export class EventService {
    constructor(
        @InjectModel('Event') readonly eventModel: Model<EventInterface>
    ) {}

    async findEvents(
        params: {
            uuk: string;
            uuv: string;
            eventName: string;
        },
        limit?: number
    ): Promise<EventInterface[]> {
        return await this.eventModel
            .find({
                user_unique_key: params.uuk,
                user_unique_value: params.uuv,
                name: params.eventName
            })
            .limit(limit);
    }

    async addEvent(event) {
        await new this.eventModel(event).save();
    }

    async findEventsBySessionId(s: string) {
        return this.eventModel.find({ session_id: s });
    }
}
