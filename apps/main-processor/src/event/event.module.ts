import { EventSchema } from './schema/schema.event';
import { EventService } from './event.service';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
    imports: [
        CqrsModule,
        MongooseModule.forFeature([{ name: 'Event', schema: EventSchema }])
    ],
    providers: [EventService],
    exports: [EventService]
})
export class EventModule {}
