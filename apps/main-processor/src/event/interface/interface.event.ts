import { Document, Types } from 'mongoose';
export interface EventInterface extends Document {
    session_id: string;
    user_unique_value: string;
    user_unique_key: string;
    user_id: Types.ObjectId;
    body: any;
    name: string;
    time: Date;
}
