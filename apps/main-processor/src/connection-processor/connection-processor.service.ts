import { Injectable } from '@nestjs/common';
import { IndraService } from '@shared/indra/indra.service';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import {
    ReferrerInterface,
    User,
    UserAvailabilityInterface
} from '@shared/models/user.model';
import { getReferrerDomainFromUrl } from '@shared/utils/referrer-domain';
import * as _ from 'lodash';
import { QueueName } from '../job/interface/QueueNames';
import Connection from '../socket-parser/interfaces/connection';
import { JobService } from './../job/job.service';
import { JourneyService } from './../journey/journey.service';
import { SessionService } from './../session/session.service';
import { UserEventService } from './../user-event/user-event.service';
import { UserStatsProcessor } from './../user-stats/user-stats.processor';
import { UserService } from '@shared/user/user.service';
import { SessionDeviceService } from '@shared/session-device/session-device.service';
import { VariableValue } from '@entries/variablevalue/model/variablevalue';
import { getValueOfEvent } from '../journey/helper';
import { ActivityLevel } from '@entries/activity-level/enum/activity-level';
import { GatherUserEvents } from '../backend-job/gather-user-events';

@Injectable()
export class ConnectionProcessor {
    constructor(
        private readonly userService: UserService,
        private readonly indraService: IndraService,
        private readonly userEventService: UserEventService,
        private readonly jobService: JobService,
        private readonly journeyService: JourneyService,
        private readonly logger: BLogger,
        private readonly gatherUserEvents: GatherUserEvents,
        private readonly sessionDeciceService: SessionDeviceService,
        private readonly sessionService: SessionService,
        private readonly userStatsProcessor: UserStatsProcessor
    ) {}

    async handleConnection(connection: Connection) {
        const userAvailability: UserAvailabilityInterface = {
            is_new_user: null,
            is_user: null,
            user: null,
            user_data: connection.userAgent,
            ip_address: connection.ipAddress,
            session_id: connection.sessionId,
            user_attr: connection.userAttribute,
            is_online: connection.isOnline,
            device_id: connection.device_id,
            channel: connection.channel,
            device_info: connection.deviceInfo,
            user_agent: connection.userAgent,
            connectionDate: connection.connectionDate,
            firebase_token: connection.firebaseToken
        };
        // check if session started or ended
        const { isOnline } = connection;
        if (isOnline) {
            await this.handleSessionStart(connection, userAvailability);
        } else {
            await this.handleSessionEnd(connection, userAvailability);
        }
        // this.userAvailability = {};
    }
    /*
      this section of code should be refactor to exclude it's dependency
      from IndraService.
      */
    async handleSessionStart(
        connection: Connection,
        userAvailability: UserAvailabilityInterface
    ) {
        console.log('1111');
        // (connection)
        // check if is user or guest
        const isUser = _.isObject(connection.userAttribute);
        console.log(isUser, connection.userAttribute);
        userAvailability.is_user = isUser;
        let isDbUser = !isUser ? false : true;
        if (isUser) {
            // get user from indra service then 'upsert' to our database
            // we are pretty sure that the user_unique_value is uuk because
            // we have a contract with web sdk
            const uuk: string = connection.userAttribute.value;
            userAvailability.user = {
                uuk: connection.userAttribute.key,
                uuv: connection.userAttribute.value
            };
            const dbUser = await this.userService.getUserByKeyValue({
                uuk: connection.userAttribute.key,
                uuv: connection.userAttribute.value
            });
            console.log(dbUser);
            if (!dbUser) {
                isDbUser = false;
                console.log('hee');
                // const deviceId = connection.device_id;
                // const sessionId = connection.sessionId;
                // await this.sessionDeciceService.addSessionForDeviceId(
                //     sessionId,
                //     deviceId
                // );
                // try {
                //     const indraUser = await this.indraService.getIndraUserByUUK(
                //         uuk
                //     );
                //     userAvailability.is_new_user = true;
                //     const phoneNumber = indraUser?.phone?.replace('09', '+989');
                //     const email = indraUser?.email?.toLowerCase() || '';
                //     const name =
                //         indraUser?.namePersian +
                //             ' ' +
                //             indraUser?.lastNamePersian ||
                //         indraUser?.name + ' ' + indraUser?.lastName ||
                //         '';
                //     const referrer: ReferrerInterface[] = [
                //         {
                //             name: connection.referrer || 'direct',
                //             count: 1
                //         }
                //     ];
                //     const user = {
                //         user_unique_key: connection.userAttribute.key,
                //         user_unique_value: connection.userAttribute.value,
                //         name,
                //         email,
                //         phone_number: phoneNumber,
                //         user_attributes: indraUser,
                //         last_activity: new Date(),
                //         referrer,
                //         create_date: new Date()
                //     };
                //     await this.userService.upsertUser(user);
                //     const userEvent = {
                //         user_unique_value: connection.userAttribute.value,
                //         user_unique_key: connection.userAttribute.key,
                //         session_id: connection.sessionId,
                //         events: [],
                //         start_date: new Date(),
                //         end_date: null
                //     };
                //     await this.userEventService.createNewSession(userEvent);
                // } catch (error) {
                //     userAvailability.is_new_user = false;
                //     this.logger.error(
                //         ACTIONS.CONNECTION_PROCESSOR_SERVICE,
                //         error
                //     );
                //     // log
                // }
            } else {
                // Save referrer to the user model
                const referrerArray = this.handleReferrer(
                    connection.referrer,
                    dbUser.referrer
                );

                await dbUser.update({
                    referrer: referrerArray
                });

                userAvailability.is_new_user = false;
                const userEvent = {
                    user_unique_value: connection.userAttribute.value,
                    user_unique_key: connection.userAttribute.key,
                    session_id: connection.sessionId,
                    events: [],
                    start_date: connection.connectionDate,
                    end_date: null
                };
                await this.userEventService.createNewSession(userEvent);
                await this.gatherUserEvents.addByUserId(dbUser._id);
            }
            await this.journeyService.handleJourneysForConnection(connection);
        } else {
            userAvailability.is_new_user = false;
        }
        const session = {
            session_id: connection.sessionId,
            user_unique_key: connection.userAttribute?.key,
            user_unique_value: connection.userAttribute?.value,
            ip_address: connection.ipAddress,
            start_date: new Date(),
            end_date: null,
            is_online: true,
            channel: connection.channel,
            client_attributes: {}
        };
        if (!isUser || !isDbUser) {
            const deviceId = connection.device_id;
            const sessionId = connection.sessionId;
            await this.sessionDeciceService.addSessionForDeviceId(
                sessionId,
                deviceId
            );
        }
        await this.sessionService.createNewSession(session);
        this.jobService.addJob(QueueName.USER_PROFILE, userAvailability);
        await this.userStatsProcessor.handleUserStats(userAvailability);
    }

    async handleSessionEnd(
        connection: Connection,
        userAvailability: UserAvailabilityInterface
    ) {
        // check if is user or guest
        const isUser = _.isObject(connection.userAttribute);
        userAvailability.is_new_user = false;
        userAvailability.is_user = isUser;
        if (isUser) {
            userAvailability.user = {
                uuk: connection.userAttribute.key,
                uuv: connection.userAttribute.value
            };
            await this.userService.updateUserLastActivity(
                connection.userAttribute
            );
            await this.userEventService.closeSession(connection.sessionId);
            await this.journeyService.handleJourneysForConnection(connection);
            this.jobService.addJob(QueueName.RETENTION_STATS, userAvailability);
        }
        await this.sessionService.closeSession(connection.sessionId);
        await this.userStatsProcessor.handleUserStats(userAvailability);
    }

    handleReferrer(connectionReferrer, currentReferrerArray) {
        let referrerArray = [];
        const referrerName = getReferrerDomainFromUrl(connectionReferrer);
        if (!currentReferrerArray || !currentReferrerArray.length) {
            referrerArray = [{ name: connectionReferrer, count: 1 }];
            return referrerArray;
        } else {
            const referrerItem = currentReferrerArray.find(
                (item) => item.name === referrerName
            );

            referrerArray = [...currentReferrerArray];
            if (!referrerItem) {
                referrerArray.push({ name: referrerName, count: 1 });
            } else {
                const index = referrerArray.indexOf(referrerItem);
                const count = referrerArray[index].count + 1;
                referrerArray[index].count = count;
            }

            return referrerArray;
        }
    }

    async createUserFromAttrs(params: VariableValue[], connection: Connection) {
        const getValue = (variable: string) => {
            return params.find((p) => {
                p.variable.toLowerCase() === variable.toLowerCase();
            })?.value;
        };
        const email = getValue('email');
        const phoneNumber =
            getValue('phone_number') ||
            getValue('phonenumber') ||
            getValue('mobile');
        const name =
            getValue('name') || getValue('firstname') || getValue('fist_name');
        const family =
            getValue('family') || getValue('last_name') || getValue('lastname');

        const finaleName = name ? (family ? name + ' ' + family : name) : '';

        const user: any = {
            user_unique_key: connection.userAttribute.key,
            user_unique_value: connection.userAttribute.value,
            email: email,
            phone_number: phoneNumber,
            name: finaleName,
            activity_level: ActivityLevel.MEDIUM,
            create_date: new Date(),
            last_activity: new Date(),
            user_attributes: params
        };
        await this.userService.upsertUser(user);
    }
}
