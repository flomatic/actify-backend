import { Module } from '@nestjs/common';
import { IndraModule } from '@shared/indra/indra.module';
import { JourneyModule } from '../journey/journey.module';
import { UserStatsModule } from '../user-stats/user-stats.module';
import { AnalyticsModule } from '../analytics/analytics.module';
import { JobModule } from '../job/job.module';
import { SessionModule } from '../session/session.module';
import { UserEventModule } from '../user-event/user-event.module';
import { UserModule } from '@shared/user/user.module';
import { ConnectionProcessor } from './connection-processor.service';
import { BackendJobModule } from '../backend-job/backend-job.module';
import { SessionDeviceModule } from '@shared/session-device/session-device.module';

@Module({
    imports: [
        UserModule,
        UserEventModule,
        SessionModule,
        IndraModule,
        BackendJobModule,
        SessionDeviceModule,
        JourneyModule,
        UserStatsModule,
        AnalyticsModule,
        JobModule
    ],
    providers: [ConnectionProcessor],
    exports: [ConnectionProcessor]
})
export class ConnectionModule {}
