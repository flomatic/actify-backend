import { Module } from '@nestjs/common';
import { LoggerModule } from '@shared/logger/logger.module';
import { RedisModule } from '@shared/redis/redis.module';
import { ConnectionModule } from '../connection-processor/connection-processor.module';
/* eslint-disable-next-line */
import { ServerEventProcessorModule } from '../server-event-processor/server-event-processor.module';
import { EventProcessorModule } from './../event-processor/event-processor.module';
import { SocketParserService } from './socket-parser.service';

@Module({
    imports: [
        RedisModule,
        ConnectionModule,
        EventProcessorModule,
        ServerEventProcessorModule,
        LoggerModule
    ],
    providers: [SocketParserService]
})
export class SocketParserModule {}
