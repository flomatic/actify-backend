export interface Event {
    session_id: string;
    event_name: string;
    body: any;
    date: Date;
}
