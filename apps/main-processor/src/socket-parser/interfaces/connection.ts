export default interface Connection {
    sessionId: string;
    userAttribute?: { key: string; value: string };
    channel: string;
    userAgent?: any;
    firebaseToken?: string;
    deviceInfo?: string;
    device_id: string;
    ipAddress: any;
    connectionDate: Date;
    isOnline: boolean;
    tracking_id?: string;
    referrer?: string;
}
