import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { RedisService } from '@shared/redis/redis.service';
import { ServerEvent } from 'apps/api/src/server-event/interface/interface.server-event';
import { ServerEventProcessor } from '../server-event-processor/server-event-processor.service';
import { ConnectionProcessor } from './../connection-processor/connection-processor.service';
import { EventProcessor } from './../event-processor/event-processor.service';
import Connection from './interfaces/connection';
import { Event } from './interfaces/event';
import util = require('util');
import Timeout = NodeJS.Timeout;

@Injectable()
export class SocketParserService implements OnModuleInit, OnModuleDestroy {
    clientName: string;

    socketReadTimeout: Timeout;

    constructor(
        private readonly redisService: RedisService,
        private readonly configService: ConfigService,
        private readonly logger: BLogger,
        private readonly eventProcessor: EventProcessor,
        private readonly serverEventProcessor: ServerEventProcessor,
        private readonly connectionProcessor: ConnectionProcessor
    ) {
        this.clientName = this.configService.get('HOSTNAME') || 'worker';
    }

    async onModuleInit() {
        if ((process as any).console) return;
        await this.createGroupsIfNotExist();

        let errorCounter = 0;
        const startReadTimer = async () => {
            try {
                await this.readFromSockets();
                errorCounter = 0;
            } catch (err) {
                // Exits after 5 sequence of exceptions
                if (errorCounter > 5) {
                    process.exit(1);
                }
                errorCounter++;
            }
            this.socketReadTimeout = setTimeout(startReadTimer, 1000);
        };
        startReadTimer();
    }

    onModuleDestroy(): any {
        clearTimeout(this.socketReadTimeout);
    }

    async readFromSockets() {
        const connectionPromise = util
            .promisify(this.redisService.readWriteClient.xreadgroup)
            .bind(this.redisService.readWriteClient);
        const xDelPromise = util
            .promisify(this.redisService.readWriteClient.xdel)
            .bind(this.redisService.readWriteClient);
        const xAckPromise = util
            .promisify(this.redisService.readWriteClient.xack)
            .bind(this.redisService.readWriteClient);
        const eventPromise = util
            .promisify(this.redisService.readWriteClient.xreadgroup)
            .bind(this.redisService.readWriteClient);
        const serverEventPromise = util
            .promisify(this.redisService.readWriteClient.xreadgroup)
            .bind(this.redisService.readWriteClient);

        try {
            const connectionsStream = await connectionPromise(
                'GROUP',
                'availabilities',
                this.clientName,
                'COUNT',
                1000,
                'streams',
                'user_availability',
                '>'
            );
            const promises: Array<Promise<any>> = [];
            if (connectionsStream?.[0]) {
                for (const element of connectionsStream?.[0]?.[1]) {
                    const messageId = element?.[0];
                    const connectionString = element?.[1]?.[1];
                    const isOnlineString = element?.[1]?.[3];
                    const connection: Connection = {
                        ...JSON.parse(connectionString),
                        isOnline: isOnlineString === 'true'
                    };
                    const promise = this.connectionProcessor.handleConnection(
                        connection
                    );
                    promises.push(promise);

                    await xAckPromise(
                        'user_availability',
                        'availabilities',
                        messageId
                    );
                    await xDelPromise('user_availability', messageId);
                }
                await Promise.all(promises);
            }
            const eventStream = await eventPromise(
                'GROUP',
                'procs',
                this.clientName,
                'COUNT',
                1000,
                'streams',
                'event',
                '>'
            );
            if (eventStream?.[0]) {
                for (const element of eventStream?.[0]?.[1]) {
                    const eventString = element?.[1]?.[1];
                    const connectionString = element?.[1]?.[3];
                    const messageId = element?.[0];
                    const event: Event = JSON.parse(eventString);
                    const connection: Connection = JSON.parse(connectionString);
                    event.date &&
                        this.logger.info('time to start event processing', {
                            time: Date.now() - new Date(event.date).getTime(),
                            unit: 'ms'
                        });
                    const promise = this.eventProcessor.handleEvent(
                        event,
                        connection
                    );
                    promises.push(promise);
                    await xAckPromise('event', 'procs', messageId);
                    await xDelPromise('event', messageId);
                }
                await Promise.all(promises);
            }

            const serverEventStream = await serverEventPromise(
                'GROUP',
                'server-procs',
                this.clientName,
                'COUNT',
                1000,
                'streams',
                'server-event',
                '>'
            );
            if (serverEventStream?.[0]) {
                for (const element of serverEventStream?.[0]?.[1]) {
                    const serverEventString = element?.[1]?.[1];
                    const messageId = element?.[0];
                    const event: ServerEvent = JSON.parse(serverEventString);
                    const promise = this.serverEventProcessor.handleEvent(
                        event
                    );
                    promises.push(promise);
                    await xAckPromise(
                        'server-event',
                        'server-procs',
                        messageId
                    );
                    await xDelPromise('server-event', messageId);
                }
                await Promise.all(promises);
            }
        } catch (e) {
            this.logger.error(ACTIONS.READ_FROM_REDIS, e);
            throw e;
        }
    }

    async createGroupsIfNotExist() {
        return new Promise((resolve) => {
            this.redisService.readWriteClient.xgroup(
                'create',
                'user_availability',
                'availabilities',
                '$',
                'MKSTREAM',
                () => {
                    this.redisService.readWriteClient.xgroup(
                        'create',
                        'event',
                        'procs',
                        '$',
                        'MKSTREAM',
                        () => {
                            this.redisService.readWriteClient.xgroup(
                                'create',
                                'server-event',
                                'server-procs',
                                '$',
                                'MKSTREAM',
                                () => {
                                    resolve();
                                }
                            );
                        }
                    );
                }
            );
        });
    }
}
