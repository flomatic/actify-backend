import { Injectable } from '@nestjs/common';
import {
    AnalyticsInterface,
    FlowInterface
} from '@shared/models/analytic.model';
import { UserEventInterface } from '@shared/models/user-event.model';
import { checkFilter } from '../../journey/helper';
import { ReportService } from './../report.service';
import {
    getFromToDateInDay,
    getFromToDateInHour,
    getFromToDateInMonth,
    getFromToDateInYear
} from './date-utils';

@Injectable()
export class FlowHelper {
    constructor(private readonly reportService: ReportService) {}
    async handleFlow(
        userEvents: UserEventInterface,
        analytic: AnalyticsInterface
    ) {
        const result: number[] = [];
        let isOk = false;
        if (userEvents?.events?.length || 0 > 0) {
            const analyticFlow = analytic.input as FlowInterface;
            if ((analyticFlow?.events?.length || 0) > 0) {
                analyticFlow.events.forEach((analyticEvent, index) => {
                    userEvents?.events?.forEach((inputEvent) => {
                        if (inputEvent.name === analyticEvent.event_name) {
                            const matched = checkFilter(
                                analyticEvent.event_filters,
                                inputEvent.body
                            );
                            if (
                                matched &&
                                result.length < analyticFlow.events.length
                            ) {
                                result.push(index);
                            }
                        }
                    });
                });
                if (
                    result.length > 0 &&
                    analyticFlow.events.length === result.length
                ) {
                    if (
                        result.every(
                            (r, index, rs) => !index || rs[index - 1] <= r
                        )
                    ) {
                        isOk = true;
                    }
                }
            }
        }
        if (isOk) {
            await this.insertToFlows(analytic);
        }
    }
    async insertToFlows(analytic: AnalyticsInterface) {
        await this.insertToFlow(analytic, 'yearly');
        await this.insertToFlow(analytic, 'monthly');
        await this.insertToFlow(analytic, 'daily');
        await this.insertToFlow(analytic, 'hourly');
    }
    async insertToFlow(analytic: AnalyticsInterface, period: string) {
        const fromToDate =
            period === 'yearly'
                ? getFromToDateInYear()
                : period === 'monthly'
                    ? getFromToDateInMonth()
                    : period === 'daily'
                        ? getFromToDateInDay()
                        : period === 'hourly'
                            ? getFromToDateInHour()
                            : null;
        const report = (
            await this.reportService.findQuery({
                analytic_id: analytic._id,
                time_period: period,
                from_date: { $gte: fromToDate.from_date },
                to_date: { $lte: fromToDate.to_date }
            })
        )?.[0];
        if (!report) {
            await this.reportService.create({
                group: 'all',
                analytic_id: analytic._id,
                type: 'flow',
                chart_type: analytic.chart_type,
                time_period: period,
                from_date: fromToDate.from_date,
                to_date: fromToDate.to_date,
                data: [{ value: 1, name: 'number_of_happening' }]
            });
        } else {
            (report.data as any[]).find((d) => d.name === 'number_of_happening')
                .value++;
            report.markModified('data');
            await report.save();
        }
    }
}
