import { Injectable } from '@nestjs/common';
import {
    AnalyticsInterface,
    ChartType,
    FunnelInterface,
    ReportData,
    ReportInterface
} from '@shared/models/analytic.model';
import { UserEventInterface } from '@shared/models/user-event.model';
import { checkFilter, forEachAsync } from '../../journey/helper';
import { ReportService } from './../report.service';
import {
    getFromToDateInDay,
    getFromToDateInMonth,
    getFromToDateInYear
} from './date-utils';

@Injectable()
export class FunnelHelper {
    constructor(private readonly reportService: ReportService) {}
    async handleFunnel(
        userEvents: UserEventInterface,
        analytic: AnalyticsInterface
    ) {
        const funnelNames: string[] = [];
        const analyticFunnel = analytic.input as FunnelInterface;
        await forEachAsync(userEvents?.events, async (inputEvent) => {
            await forEachAsync(analyticFunnel.events, async (analyticEvent) => {
                if (analyticEvent.event_name === inputEvent.name) {
                    const matched = checkFilter(
                        analyticEvent.event_filters,
                        inputEvent.body
                    );
                    if (matched && !funnelNames.includes(analyticEvent.name)) {
                        funnelNames.push(analyticEvent.name);
                    }
                }
            });
        });
        if (funnelNames.length > 0) {
            await this.insertToFunnels(funnelNames, analytic, 'yearly');
            await this.insertToFunnels(funnelNames, analytic, 'monthly');
            await this.insertToFunnels(funnelNames, analytic, 'daily');
        }
    }

    async insertToFunnels(
        funnelNames: string[],
        analytic: AnalyticsInterface,
        period
    ) {
        const analyticFunnel = analytic.input as FunnelInterface;

        const fromToDate =
            period === 'yearly'
                ? getFromToDateInYear()
                : period === 'monthly'
                    ? getFromToDateInMonth()
                    : period === 'daily'
                        ? getFromToDateInDay()
                        : null;
        const report: ReportInterface = (
            await this.reportService.findQuery({
                analytic_id: analytic._id,
                time_period: period,
                from_date: { $gte: fromToDate.from_date },
                to_date: { $lte: fromToDate.to_date }
            })
        )?.[0];
        if (!report) {
            const objectToSave = {
                group: 'all',
                analytic_id: analytic._id,
                type: 'funnel',
                chart_type: ChartType.FUNNEL,
                time_period: period,
                from_date: fromToDate.from_date,
                to_date: fromToDate.to_date,
                data: analyticFunnel.events.map((e) => ({
                    name: e.name,
                    value: 0
                }))
            };
            funnelNames.forEach((name) => {
                if (objectToSave.data.find((d) => d.name === name)) {
                    objectToSave.data.find((d) => d.name === name).value = 1;
                }
            });
            await this.reportService.create(objectToSave);
        } else {
            funnelNames.forEach((f) => {
                let dataIndex = -1;
                if (
                    (report.data as any[]).find((d, index) => {
                        if (d.name === f) {
                            dataIndex = index;
                            return true;
                        }
                        return false;
                    })
                ) {
                    (report.data[dataIndex] as ReportData).value += 1;
                } else {
                    (report.data as any).push({
                        name: f,
                        value: 1
                    });
                }
            });
            report.markModified('data');
            await report.save();
        }
    }
}
