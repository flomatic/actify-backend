import { Injectable } from '@nestjs/common';
import {
    AnalyticsInterface,
    EventInterface,
    EventResultTypes
} from '@shared/models/analytic.model';
import { checkFilter, getValueOfEvent } from '../../journey/helper';
import { Event } from '../../socket-parser/interfaces/event';
import { ReportService } from './../report.service';
import {
    getFromToDateInDay,
    getFromToDateInHour,
    getFromToDateInMonth,
    getFromToDateInYear
} from './date-utils';

@Injectable()
export class EventHelper {
    constructor(private readonly reportService: ReportService) {}
    async handleEvents(event: Event, analytic: AnalyticsInterface) {
        const eventAnalytic = analytic.input as EventInterface;
        if (eventAnalytic.event_name === event.event_name) {
            const isMatched = checkFilter(
                eventAnalytic.event_filters,
                event.body
            );
            if (isMatched) {
                await this.check(event, analytic);
            }
        }
    }
    async check(event: Event, analytic: AnalyticsInterface) {
        const eventAnalytic = analytic.input as EventInterface;
        const resultType = eventAnalytic.result_type;
        if (resultType === EventResultTypes.ENUM_EVENT_VALUES) {
            const values = eventAnalytic.keys.map((key) =>
                getValueOfEvent(key.key, event.body)
            );
            await this.insertForItem(analytic, 'yearly', values);
            await this.insertForItem(analytic, 'monthly', values);
            await this.insertForItem(analytic, 'daily', values);
        } else if (resultType === EventResultTypes.NUMBER_OF_EVENTS) {
            await this.insert(analytic, 'yearly');
            await this.insert(analytic, 'monthly');
            await this.insert(analytic, 'daily');
            await this.insert(analytic, 'hourly');
        } else if (resultType === EventResultTypes.SUM_EVENT_VALUES) {
            const values = eventAnalytic.keys.map((key) => ({
                name: key.name,
                value: getValueOfEvent(key.key, event.body)
            }));
            await this.insertForSum(analytic, 'yearly', values);
            await this.insertForSum(analytic, 'monthly', values);
            await this.insertForSum(analytic, 'daily', values);
            await this.insertForSum(analytic, 'hourly', values);
        }
    }
    async insert(analytic: AnalyticsInterface, period: string) {
        const fromToDate =
            period === 'yearly'
                ? getFromToDateInYear()
                : period === 'monthly'
                    ? getFromToDateInMonth()
                    : period === 'daily'
                        ? getFromToDateInDay()
                        : period === 'hourly'
                            ? getFromToDateInHour()
                            : null;
        const report = (
            await this.reportService.findQuery({
                analytic_id: analytic._id,
                time_period: period,
                from_date: { $gte: fromToDate.from_date },
                to_date: { $lte: fromToDate.to_date }
            })
        )?.[0];
        if (!report) {
            await this.reportService.create({
                group: 'all',
                analytic_id: analytic._id,
                type: 'event',
                chart_type: analytic.chart_type,
                time_period: period,
                from_date: fromToDate.from_date,
                to_date: fromToDate.to_date,
                data: [{ value: 1, name: 'number_of_happening' }]
            });
        } else {
            (report.data as any[]).find((d) => d.name === 'number_of_happening')
                .value++;
            report.markModified('data');
            await report.save();
        }
    }

    async insertForSum(
        analytic: AnalyticsInterface,
        period,
        values: Array<{ name: string; value: number }>
    ) {
        const fromToDate =
            period === 'yearly'
                ? getFromToDateInYear()
                : period === 'monthly'
                    ? getFromToDateInMonth()
                    : period === 'daily'
                        ? getFromToDateInDay()
                        : period === 'hourly'
                            ? getFromToDateInHour()
                            : null;

        const report = (
            await this.reportService.findQuery({
                analytic_id: analytic._id,
                time_period: period,
                from_date: { $gte: fromToDate.from_date },
                to_date: { $lte: fromToDate.to_date }
            })
        )?.[0];
        if (!report) {
            await this.reportService.create({
                group: 'all',
                analytic_id: analytic._id,
                type: 'event',
                chart_type: analytic.chart_type,
                time_period: period,
                from_date: fromToDate.from_date,
                to_date: fromToDate.to_date,
                data: values
            });
        } else {
            values.forEach((v) => {
                const data = (report.data as any[]).find(
                    (d) => d.name === v.name
                );
                if (data) {
                    (report.data as any[]).find(
                        (d) => d.name === v.name
                    ).value += v.value;
                } else {
                    (report.data as any[]).push({
                        name: v.name,
                        value: v.value
                    });
                }
            });
            report.markModified('data');
            await report.save();
        }
    }

    async insertForItem(
        analytic: AnalyticsInterface,
        period,
        values: string[]
    ) {
        const fromToDate =
            period === 'yearly'
                ? getFromToDateInYear()
                : period === 'monthly'
                    ? getFromToDateInMonth()
                    : period === 'daily'
                        ? getFromToDateInDay()
                        : period === 'hourly'
                            ? getFromToDateInHour()
                            : null;
        const report = (
            await this.reportService.findQuery({
                analytic_id: analytic._id,
                time_period: period,
                from_date: { $gte: fromToDate.from_date },
                to_date: { $lte: fromToDate.to_date }
            })
        )?.[0];
        if (!report) {
            await this.reportService.create({
                group: 'all',
                analytic_id: analytic._id,
                type: 'event',
                chart_type: analytic.chart_type,
                time_period: period,
                from_date: fromToDate.from_date,
                to_date: fromToDate.to_date,
                data: values.map((v) => ({ name: v, value: 1 }))
            });
        } else {
            values.forEach((v) => {
                const data = (report.data as any).find((d) => d.name === v);
                if (data) {
                    (report.data as any).find((d) => d.name === v).value++;
                } else {
                    (report.data as any).push({
                        name: v,
                        value: 1
                    });
                }
            });
            report.markModified('data');
            await report.save();
        }
    }
}
