import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AnalyticsInterface } from '@shared/models/analytic.model';
import { Model } from 'mongoose';

@Injectable()
export class AnalyticsService implements OnModuleInit, OnModuleDestroy {
    analytics: AnalyticsInterface[];
    timer;
    constructor(
        @InjectModel('Analytics')
        private readonly analyticsModel: Model<AnalyticsInterface>
    ) {}

    async onModuleInit() {
        this.analytics = await this.getAllAnalytics();
        this.timer = setTimeout(async () => {
            await this.onModuleInit();
        }, 3000);
    }

    async onModuleDestroy() {
        clearTimeout(this.timer);
    }
    async getAllAnalytics(): Promise<AnalyticsInterface[]> {
        return this.analyticsModel.find();
    }

    async getAllAnalyticsSync(): Promise<AnalyticsInterface[]> {
        if (!this.analytics) {
            this.analytics = await this.getAllAnalytics();
        }
        return this.analytics;
    }
}
