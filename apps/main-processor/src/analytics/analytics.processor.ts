import { Injectable } from '@nestjs/common';
import {
    AnalyticsInterface,
    AnalyticsTypes
} from '@shared/models/analytic.model';
import { SessionService } from '../session/session.service';
import { Event } from '../socket-parser/interfaces/event';
import { TriggedByUserSegmentationHelper } from './../journey/helpers/trigged-by-user-segmentation';
import { UserEventService } from './../user-event/user-event.service';
import { AnalyticsService } from './analytics.service';
import { EventHelper } from './helper/event-helper';
import { FlowHelper } from './helper/flow-helper';
import { FunnelHelper } from './helper/funnel-helper';

@Injectable()
export class AnalyticsProcessor {
    constructor(
        private readonly analyticsService: AnalyticsService,
        private readonly userEventService: UserEventService,
        private readonly sessionService: SessionService,
        private readonly flowHelper: FlowHelper,
        private readonly segmentationHelper: TriggedByUserSegmentationHelper,
        private readonly eventHelper: EventHelper,
        private readonly funnelHelper: FunnelHelper
    ) {}

    async handleFlowOrFunnel(sessionId: string) {
        const userEvents = await this.userEventService.getUserEventsBySessionId(
            sessionId
        );
        const session = await this.sessionService.findSessionBySessionId(
            sessionId
        );
        const analytics = await this.getAnalytics([
            AnalyticsTypes.FLOW,
            AnalyticsTypes.FUNNEL
        ]);
        for (const analytic of analytics) {
            let canPass = true;
            if (analytic.user_segmentation_id) {
                canPass = await this.segmentationHelper.isMatchForAnalytics({
                    session_id: sessionId,
                    uuk: session.user_unique_key,
                    uuv: session.user_unique_value,
                    segmentation_id: analytic.user_segmentation_id
                });
            }
            if (analytic && canPass) {
                /* eslint-disable */
                switch (analytic.type) {
                    case AnalyticsTypes.FLOW:
                        await this.flowHelper.handleFlow(userEvents, analytic);
                        break;
                    case AnalyticsTypes.FUNNEL:
                        await this.funnelHelper.handleFunnel(userEvents, analytic);
                        break;
                }
                /* eslint-enable */
            }
        }
    }

    async getAnalytics(types: AnalyticsTypes[]): Promise<AnalyticsInterface[]> {
        const analytics = await this.analyticsService.getAllAnalyticsSync();
        return (
            analytics.filter((analytic) => types.includes(analytic.type)) || []
        );
    }

    async handleEvent(event: Event) {
        const analytics = await this.getAnalytics([AnalyticsTypes.EVENT]);
        for (const analytic of analytics) {
            if (analytic) {
                let canPass = true;
                if (analytic.user_segmentation_id) {
                    const session = await this.sessionService.findSessionBySessionId(
                        event.session_id
                    );
                    canPass = await this.segmentationHelper.isMatchForAnalytics(
                        {
                            session_id: event.session_id,
                            uuk: session.user_unique_key,
                            uuv: session.user_unique_value,
                            segmentation_id: analytic.user_segmentation_id
                        }
                    );
                }
                if (canPass) {
                    await this.eventHelper.handleEvents(event, analytic);
                }
            }
        }
    }
}
