import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ReportInterface } from '@shared/models/analytic.model';
import { Model } from 'mongoose';

@Injectable()
export class ReportService {
    constructor(
        @InjectModel('Report')
        private readonly reportModel: Model<ReportInterface>
    ) {}

    async findQuery(params) {
        return await this.reportModel.find(params);
    }

    async create(params) {
        const report = new this.reportModel(params);
        await report.save();
        return report;
    }
}
