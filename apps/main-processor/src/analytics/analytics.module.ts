import { Module } from '@nestjs/common';
import {
    AnalyticReportsModeModule,
    AnalyticsModeModule
} from '@shared/models/analytic.model';
import { SessionModule } from '../session/session.module';
import { JourneyModule } from './../journey/journey.module';
import { UserEventModule } from './../user-event/user-event.module';
import { AnalyticsProcessor } from './analytics.processor';
import { AnalyticsService } from './analytics.service';
import { EventHelper } from './helper/event-helper';
import { FlowHelper } from './helper/flow-helper';
import { FunnelHelper } from './helper/funnel-helper';
import { ReportService } from './report.service';

@Module({
    imports: [
        UserEventModule,
        JourneyModule,
        SessionModule,
        AnalyticsModeModule,
        AnalyticReportsModeModule
    ],
    providers: [
        AnalyticsService,
        AnalyticsProcessor,
        FunnelHelper,
        FlowHelper,
        EventHelper,
        ReportService
    ],
    exports: [AnalyticsService, AnalyticsProcessor]
})
export class AnalyticsModule {}
