import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RetentionStatsService } from './retention-stats.service';
import { RetentionStatsSchema } from './schema/retention-stats.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'RetentionStat', schema: RetentionStatsSchema }
        ])
    ],
    providers: [RetentionStatsService],
    exports: [RetentionStatsService]
})
export class RetentionStatsModule {}
