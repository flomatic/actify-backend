import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
    getFromToDateInDay,
    getFromToDateInMonth,
    getFromToDateInYear
} from '../analytics/helper/date-utils';
import {
    RetentionStatsInterface,
    TimePeriod
} from './interface/retention-stats.interface';

@Injectable()
export class RetentionStatsService {
    constructor(
        @InjectModel('RetentionStat')
        private readonly retentionStatsModel: Model<RetentionStatsInterface>
    ) {}

    async updateRetention(params: {
        time_spend_on_session: number;
        time_between_two_session: number;
    }) {
        await this.insertData(params, TimePeriod.YEARLY);
        await this.insertData(params, TimePeriod.MONTHLY);
        await this.insertData(params, TimePeriod.DAILY);
    }

    async getRetention(params: { from_date: Date; to_date: Date }) {
        return this.retentionStatsModel.findOne(params);
    }

    async insertData(
        data: {
            time_spend_on_session: number;
            time_between_two_session: number;
        },
        period: TimePeriod
    ) {
        const TimePeriodDic = {
            [TimePeriod.YEARLY]: getFromToDateInYear(),
            [TimePeriod.MONTHLY]: getFromToDateInMonth(),
            [TimePeriod.DAILY]: getFromToDateInDay()
        };
        const fromToDate = TimePeriodDic[period] || null;
        let currentRetention: RetentionStatsInterface = await this.retentionStatsModel.findOne(
            {
                from_date: fromToDate.from_date,
                to_date: fromToDate.to_date
            }
        );
        if (!currentRetention) {
            currentRetention = new this.retentionStatsModel({
                time_period: period,
                from_date: fromToDate.from_date,
                to_date: fromToDate.to_date,
                average_session_time: {
                    value: data.time_spend_on_session,
                    count: 1
                },
                average_revisit_time: {
                    value: data.time_between_two_session,
                    count: 1
                }
            });
        } else {
            currentRetention.average_session_time.count++;
            const totalAverageSessionTime =
                currentRetention.average_session_time.value *
                    (currentRetention.average_session_time.count - 1) +
                data.time_spend_on_session;
            currentRetention.average_session_time.value = Math.round(
                totalAverageSessionTime /
                    currentRetention.average_session_time.count
            );
            currentRetention.average_revisit_time.count++;
            const totalRevisitTime =
                currentRetention.average_revisit_time.value *
                    (currentRetention.average_revisit_time.count - 1) +
                data.time_between_two_session;
            currentRetention.average_revisit_time.value = Math.round(
                totalRevisitTime / currentRetention.average_revisit_time.count
            );
        }
        await currentRetention.save();
    }
}
