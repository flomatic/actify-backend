import { RetentionStatsInterface } from './../interface/retention-stats.interface';
import { Schema, HookNextFunction } from 'mongoose';

export const RetentionStatsSchema = new Schema(
    {
        time_period: { type: String, enum: ['yearly', 'monthly', 'daily'] },
        average_session_time: {
            value: { type: Number, default: 0 },
            count: { type: Number, default: 0 }
        },
        average_revisit_time: {
            value: { type: Number, default: 0 },
            count: { type: Number, default: 0 }
        },
        average_score: { type: Number, default: 0 },
        from_date: { type: Date, index: true },
        to_date: { type: Date, index: true }
    },
    {
        timestamps: true
    }
);

RetentionStatsSchema.pre(
    'save',
    function (this: RetentionStatsInterface, next: HookNextFunction) {
        try {
            const {
                average_session_time: { value: sessionTime },
                average_revisit_time: { value: revisitTime }
            } = this;
            if (sessionTime === 0 || revisitTime === 0) {
                this.average_score = 0;
            } else {
                this.average_score = Number(
                    (sessionTime / revisitTime).toFixed(3)
                );
            }
            return next();
        } catch (error) {
            return next(error);
        }
    }
);
