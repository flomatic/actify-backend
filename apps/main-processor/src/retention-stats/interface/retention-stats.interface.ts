import { Document } from 'mongoose';

export interface RetentionStatsInterface extends Document {
    time_period: TimePeriod;
    average_session_time: {
        value: number;
        count: number;
    };
    average_revisit_time: {
        value: number;
        count: number;
    };
    average_score: number;
    from_date: Date;
    to_date: Date;
}

export enum TimePeriod {
    YEARLY = 'yearly',
    MONTHLY = 'monthly',
    DAILY = 'daily'
}
