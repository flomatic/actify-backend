import Vue from 'vue';

<% if (options.external !== false) { %>
function addScript (src) {
    return new Promise((resolve, reject) => {
        const script = document.createElement('script');
        script.setAttribute('src', src);
        script.defer = true;
        script.async = true;
        script.onerror = reject;
        script.onload = resolve;
        document.body.appendChild(script);
    });
}
<% } %>

async function init (app) {
    <% if (options.external !== false) { %>
    await addScript('<%= options.sdkUrl %>?format=iife<%= options.url ? "&url=" + encodeURIComponent(options.url) : "" %>');
    const { actify } = window;
    <% } else { %>
    const Actify = await import('@actify/sdk')
    const actify = new Actify.default({ url: '<%= options.url %>' })
    <% } %>

    if (!actify) return;

    <% if (options.autoInit !== false) { %>
    actify.init({});
    <% } %>
    return actify;
}

function proxy(fn) {
    return function(...args) {
        if (!this._fns) this._fns = []
        this._fns.push([fn, ...args])
    }
}

export default function ({ app }, inject) {
    if (process.server) return;
    const proxyKeys = ['init', 'event', 'setErrorHandler', 'startSessionRecording', 'stopSessionRecording']
    Vue.prototype.$actify = app.$actify = proxyKeys.reduce((carry, item) => {
        carry[item] = proxy(item)
        return carry
    })
    init(app).then(actify => {
        const proxyActify = app.$actify;
        ((proxyActify || {})._fns || []).forEach(([fn, ...args]) => {
            actify[fn](...args)
        })
        Vue.prototype.$actify = app.$actify = actify;
    })
};
