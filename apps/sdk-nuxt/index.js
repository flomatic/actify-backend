import path from 'path';

export default function actifyModule(moduleOptions) {
    const options = {
        ...this.options.actify,
        ...moduleOptions
    }

    this.addPlugin({
        src: path.resolve(__dirname, 'plugin.js'),
        options,
        mode: 'client'
    })
}

module.exports.meta = require('./package.json')
