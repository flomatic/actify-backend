declare const process: NodeJS.Process & { console: boolean };

process.env.ENV = 'console';
process.console = true;

import { BootstrapConsole, ConsoleModule } from 'nestjs-console';
import { Module } from '@nestjs/common';
import { AppModule as ApiModule } from 'apps/api/src/app.module';
import { EmailModule } from '@shared/email/email.module';

@Module({
    imports: [ApiModule, EmailModule, ConsoleModule]
})
export class AppModule {}

const bootstrap = new BootstrapConsole({
    module: AppModule,
    useDecorators: true
});
bootstrap.init().then(async (app) => {
    try {
        // init your app
        await app.init();
        // boot the cli
        await bootstrap.boot();
        process.exit(0);
    } catch (e) {
        process.exit(1);
    }
});
