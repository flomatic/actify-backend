var allowedKeys = {
    37: 'left',
    38: 'up',
    39: 'right',
    40: 'down',
    65: 'a',
    66: 'b'
};

var konamiCode = [
    'up',
    'up',
    'down',
    'down',
    'left',
    'right',
    'left',
    'right',
    'b',
    'a'
];

var konamiCodePosition = 0;

document.addEventListener('keydown', function (e) {
    var key = allowedKeys[e.keyCode];
    var requiredKey = konamiCode[konamiCodePosition];

    if (key == requiredKey) {
        konamiCodePosition++;

        if (konamiCodePosition == konamiCode.length) {
            activateFirstCheats();
            activateSecondCheats();
            konamiCodePosition = 0;
        }
    } else {
        konamiCodePosition = 0;
    }
});

function activateFirstCheats() {
    var today = new Date();
    var possibleEmoji = ['🐹', '🐢', '🌼', '🍉'];
    if (
        today.getMonth() === 9 &&
        [31, 30, 29, 28].indexOf(today.getDate()) !== -1
    ) {
        possibleEmoji = ['👻', '🎃', '🧟‍♀️'];
    }
    if (
        today.getMonth() === 11 &&
        [21, 22, 23, 24, 25, 26].indexOf(today.getDate()) !== -1
    ) {
        possibleEmoji = ['❄️', '🎅', '🎁'];
    }
    document.body.addEventListener('click', function (event) {
        var randomNumber = Math.round(Math.random() * possibleEmoji.length);
        var span = document.createElement('span');
        span.textContent = possibleEmoji[randomNumber];
        span.className = 'emoji click-emoji';

        span.style.left = event.clientX + 'px';

        span.style.top = event.clientY + 'px';

        span.style.position = 'fixed';
        document.body.appendChild(span);
    });
}

function activateSecondCheats() {
    document.addEventListener('keyup', function (event) {
        if (event.keyCode === 67) {
            var clickEmoji = document.getElementsByClassName('click-emoji');
            var totalEmoji = clickEmoji.length;

            Array.from(clickEmoji).forEach(function (emoji, index) {
                var maximumDelay = totalEmoji.length > 10 ? 1000 : 400;
                if (index === 0) {
                    emoji.style['animation-delay'] = 0 + 'ms';
                } else {
                    emoji.style['animation-delay'] =
                        Math.round(Math.random() * maximumDelay) + 'ms';
                }

                emoji.style['animation-duration'] =
                    Math.max(Math.round(Math.random() * 700), 100) + 'ms';

                emoji.addEventListener('animationend', function () {
                    document.body.removeChild(emoji);
                });

                emoji.classList.add('fall-down');
            });
        }
    });
}
