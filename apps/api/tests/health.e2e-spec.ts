import * as request from 'supertest';
import { healthKeys as keys, url } from './constants';

describe('HEALTH', () => {
    it('Should pass healthcheck', () => {
        return request(url)
            .get('/health')
            .expect(200)
            .expect(({ body }) => {
                expect(body.status).toEqual(keys.OK);
                expect(body.info[keys.MEMORY_HEAP][keys.STATUS]).toEqual(
                    keys.UP
                );
                expect(body.info[keys.MEMORY_RSS][keys.STATUS]).toEqual(
                    keys.UP
                );
                expect(body.info[keys.MONGO][keys.STATUS]).toEqual(keys.UP);
                expect(body.info[keys.GOOGLE][keys.STATUS]).toEqual(keys.UP);
            });
    });
});
