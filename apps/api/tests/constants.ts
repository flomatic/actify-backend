import * as config from 'config';

const { port, prefix: apiPrefix } = Object.assign({}, config.get('app'));

export const url = `http://localhost:${port}`;

export const prefix = apiPrefix;
export const healthKeys = Object.freeze({
    OK: 'ok',
    UP: 'up',
    MEMORY_HEAP: 'memory_heap',
    MEMORY_RSS: 'memory_rss',
    MONGO: 'mongo',
    GOOGLE: 'google',
    STATUS: 'status'
});
