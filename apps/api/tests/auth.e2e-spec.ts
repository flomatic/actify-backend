import { HttpStatus } from '@nestjs/common';
import * as config from 'config';
import * as mongoose from 'mongoose';
import { LoginDTO, RegisterDTO } from '@entries/auth/dto/auth.dto';
import * as request from 'supertest';
import { prefix, url } from './constants';

const MONGO_URI = config.get('mongo.uri');
const app = `${url}/${prefix}`;

beforeAll(async () => {
    await mongoose.connect(MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    await mongoose.connection.db.dropDatabase();
});

afterAll(async (done) => {
    await mongoose.disconnect(done);
});

describe('AUTH', () => {
    const clientUser: LoginDTO = {
        email: 'user@test.com',
        password: 'password'
    };

    const clientAdmin: RegisterDTO = {
        email: 'admin@test.com',
        password: 'password'
    };

    let clientToken: string;
    let adminToken: string;
    let clientId: string;

    // /register
    it('Should register', () => {
        return request(app)
            .post('/auth/register')
            .set('Accept', 'application/json')
            .send(clientUser)
            .expect(({ body }) => {
                expect(body.token).toBeDefined();
                expect(body.client.email).toEqual(clientUser.email);
                expect(body.client.password).toBeUndefined();
            })
            .expect(HttpStatus.CREATED);
    });

    it('Should register an admin user', () => {
        return request(app)
            .post('/auth/register')
            .set('Accept', 'application/json')
            .send(clientAdmin)
            .expect(({ body }) => {
                expect(body.token).toBeDefined();
                expect(body.client.email).toEqual(clientAdmin.email);
                expect(body.client.password).toBeUndefined();
            })
            .expect(HttpStatus.CREATED);
    });

    it('Should reject duplicate registration', () => {
        return request(app)
            .post('/auth/register')
            .set('Accept', 'application/json')
            .send(clientUser)
            .expect(({ body }) => {
                expect(body.message).toEqual('User already exists');
                expect(body.statusCode).toEqual(HttpStatus.BAD_REQUEST);
            })
            .expect(HttpStatus.BAD_REQUEST);
    });

    // /login
    it('Should login user', () => {
        return request(app)
            .post('/auth/login')
            .set('Accept', 'application/json')
            .send(clientUser)
            .expect(({ body }) => {
                clientToken = body.token;
                clientId = body.client.id;
                expect(body.token).toBeDefined();
                expect(body.client.email).toEqual(clientUser.email);
                expect(body.client.password).toBeUndefined();
            })
            .expect(HttpStatus.CREATED);
    });

    it('Should login admin', () => {
        return request(app)
            .post('/auth/login')
            .set('Accept', 'application/json')
            .send(clientAdmin)
            .expect(({ body }) => {
                adminToken = body.token;
                expect(body.token).toBeDefined();
                expect(body.client.email).toEqual(clientAdmin.email);
                expect(body.client.password).toBeUndefined();
            })
            .expect(HttpStatus.CREATED);
    });

    // /self
    it('Should reject getting self user info if not logged in', () => {
        return request(app)
            .get('/auth/user')
            .set('Accept', 'application/json')
            .expect(HttpStatus.UNAUTHORIZED);
    });

    it('Should get self user info as client', () => {
        return request(app)
            .get('/auth/user')
            .set('Accept', 'application/json')
            .set('Authorization', `Bearer ${clientToken}`)
            .expect(({ body }) => {
                expect(body.email).toEqual(clientUser.email);
                expect(body.password).toBeUndefined();
            });
    });

    it('Should get self user info as admin', () => {
        return request(app)
            .get('/auth/user')
            .set('Accept', 'application/json')
            .set('Authorization', `Bearer ${adminToken}`)
            .expect(({ body }) => {
                expect(body.email).toEqual(clientAdmin.email);
                expect(body.password).toBeUndefined();
            });
    });

    it('Should be able to delete a user as admin', () => {
        return request(app)
            .delete('/auth/user')
            .set('Accept', 'application/json')
            .set('Authorization', `Bearer ${adminToken}`)
            .expect(200)
            .send({ id: clientId })
            .expect(({ body }) => {
                expect(body.success).toBeTruthy();
            });
    });

    // "/auth"
    it('Should get list of all users as admin', () => {
        return request(app)
            .get('/auth')
            .set('Accept', 'application/json')
            .set('Authorization', `Bearer ${adminToken}`)
            .expect(200)
            .expect(({ body }) => {
                expect(body).toBeInstanceOf(Array);
                expect(body.length).toBeGreaterThan(0);
                expect(body[0].email).toBeDefined();
                expect(body[0].password).toBeUndefined();
            });
    });

    it('Should reject listing users as client', () => {
        return request(app)
            .get('/auth')
            .set('Accept', 'application/json')
            .set('Authorization', `Bearer ${clientToken}`)
            .expect(HttpStatus.UNAUTHORIZED);
    });
});
