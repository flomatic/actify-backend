ServiceName: actify-api
MinCPU/pod: 1Core
MaxCPU/pod: 2Core
MinMemory/pod: 2GB
MaxMemory/pod: 2GB
minReplica: 1
maxReplica:1
MaxRequestPerSec: 100rps
MinCountInstances: 1
MaxCountInstances: 1
Edge: false
Dependencies: MongoDB
HealthCheckAddr: [ROOT]/api/v1/health
HealthCheckAddrVerbose: [ "Healthy": 200 status code ]
Upstream Dependencies: []
Downstream Dependencies: [ "actify-admin-panel" ]
