import * as Joi from 'joi';

export const SystemVariableValidation = Joi.object({
    variable: Joi.string().trim().min(3).max(60).required(),
    type: Joi.string()
        .valid('string', 'number', 'boolean', 'date', 'object')
        .required(),
    value: Joi.required()
        .when('type', { is: 'string', then: Joi.string() })
        .when('type', { is: 'number', then: Joi.number() })
        .when('type', { is: 'boolean', then: Joi.boolean() })
        .when('type', { is: 'date', then: Joi.date() })
        .when('type', { is: 'object', then: Joi.object() })
});
