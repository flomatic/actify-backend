import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SystemVariableDTO } from '@shared/models/system-variables.model';
import { Model } from 'mongoose';

@Injectable()
export class SystemVariablesService {
    constructor(
        @InjectModel('SystemVariables')
        private readonly systemVariablesModel: Model<SystemVariableDTO>
    ) {}

    async getAllSystemVariables(): Promise<Array<SystemVariableDTO>> {
        return await this.systemVariablesModel.find({}, { __v: 0 });
    }

    async addASystemVariable(systemVariableDto) {
        try {
            const newVariable = new this.systemVariablesModel(
                systemVariableDto
            );
            await newVariable.save();
            return newVariable;
        } catch (err) {
            if (11000 === err.code || 11001 === err.code) {
                throw new BadRequestException('Variable name must be unique');
            }
            throw new BadRequestException(err);
        }
    }

    async editASystemVariable(
        id: string,
        systemVariableDto: SystemVariableDTO
    ): Promise<any> {
        try {
            const editedVariable = await this.systemVariablesModel.findByIdAndUpdate(
                id,
                systemVariableDto,
                {
                    new: true,
                    lean: true
                }
            );

            if (!editedVariable) {
                throw new BadRequestException('Variable not found');
            }

            return editedVariable;
        } catch (err) {
            if (11000 === err.code || 11001 === err.code) {
                throw new BadRequestException('Variable name must be unique');
            }
            throw new BadRequestException(err);
        }
    }

    async removeASystemVariable(id: string): Promise<any> {
        const removedVariable = await this.systemVariablesModel.findByIdAndRemove(
            id
        );
        if (!removedVariable) {
            throw new BadRequestException('Variable not found');
        }

        return removedVariable;
    }
}
