import { Module } from '@nestjs/common';
import { SystemVariablesModelModule } from '@shared/models/system-variables.model';
import { SystemVariablesController } from './system-variables.controller';
import { SystemVariablesService } from './system-variables.service';

@Module({
    imports: [SystemVariablesModelModule],
    controllers: [SystemVariablesController],
    providers: [SystemVariablesService],
    exports: [SystemVariablesService]
})
export class SystemVariablesModule {}
