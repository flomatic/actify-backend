import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { SystemVariableDTO } from '@shared/models/system-variables.model';
import { OperationByIdDTO } from '@entries/general/dto/general.dto';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { SystemVariablesService } from './system-variables.service';
import { SystemVariableValidation } from './system-variables.validation';
import { IdValidation } from '@shared/validators/validation.id';

@ApiTags('System Variables')
@Controller()
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class SystemVariablesController {
    constructor(
        private readonly globalVariablesService: SystemVariablesService
    ) {}

    @Get('/api/v1/system-variables')
    @ApiOperation({ summary: 'Returns a list of all system variables.' })
    async getAllSystemVariables() {
        return this.globalVariablesService.getAllSystemVariables();
    }

    @Post('/api/v1/system-variables')
    @ApiOperation({ summary: 'Adds a system variable.' })
    async addASystemVariable(@Body() systemVariableDto: SystemVariableDTO) {
        const { error } = SystemVariableValidation.validate(systemVariableDto);
        if (error) {
            throw new BadRequestException(error);
        }

        return this.globalVariablesService.addASystemVariable(
            systemVariableDto
        );
    }

    @Put('/api/v1/system-variables/:id')
    @ApiOperation({ summary: 'Edits a system variable.' })
    async editASystemVariable(
        @Param() { id }: OperationByIdDTO,
        @Body() systemVariableDto: SystemVariableDTO
    ) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        const { error } = SystemVariableValidation.validate(systemVariableDto);
        if (error) {
            throw new BadRequestException(error);
        }

        return this.globalVariablesService.editASystemVariable(
            id,
            systemVariableDto
        );
    }

    @Delete('/api/v1/system-variables/:id')
    @ApiOperation({ summary: 'Removes a system variable.' })
    async removeASystemVariable(@Param() { id }: OperationByIdDTO) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        return this.globalVariablesService.removeASystemVariable(id);
    }
}
