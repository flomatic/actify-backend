import { ClientModelModule } from '@entries/client/model/client.model';
import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ClientService } from './client.service';
import { LoggingInterceptor } from './logging.interceptor';

@Module({
    imports: [ClientModelModule],
    providers: [
        ClientService,
        {
            provide: APP_INTERCEPTOR,
            useClass: LoggingInterceptor
        }
    ],
    exports: [ClientService]
})
export class SharedModule {}
