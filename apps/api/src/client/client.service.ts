import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { Model, Types as MongooseTypes } from 'mongoose';
import {
    ChangePasswordDTO,
    LoginDTO,
    RegisterDTO
} from '../../../../entries/auth/dto/auth.dto';
import { Client } from '../types/clients';

@Injectable()
export class ClientService {
    constructor(@InjectModel('Client') private userModel: Model<Client>) {}

    private sanitizeClient(clients: Client[]) {
        const sanitized = clients.map((client) => {
            const {
                email,
                createdAt,
                updatedAt,
                admin,
                _id
            } = client.toObject();
            return {
                email,
                createdAt,
                updatedAt,
                role: admin ? 'Admin' : 'Client',
                id: _id
            };
        });

        return sanitized;
    }

    async create(userDTO: RegisterDTO) {
        const { email } = userDTO;
        const user = await this.userModel.findOne({ email });
        if (user) {
            throw new HttpException(
                'User already exists',
                HttpStatus.BAD_REQUEST
            );
        }

        const createdUser = new this.userModel(userDTO);
        await createdUser.save();
        return this.sanitizeClient([createdUser])[0];
    }

    // tslint:disable-next-line: variable-name
    async changePassword(changePasswordDTO: ChangePasswordDTO, _id: string) {
        const client = await this.userModel.findById({ _id });

        if (!client) {
            throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
        }

        client.password = changePasswordDTO.newPassword;
        await client.save();

        return { id: client._id };
    }

    async findByLogin(userDTO: LoginDTO) {
        const { email, password } = userDTO;
        const user = await this.userModel.findOne({ email });
        const unauthorizedError = new HttpException(
            'Invalid credentials',
            HttpStatus.UNAUTHORIZED
        );

        if (!user) {
            throw unauthorizedError;
        }

        if (await bcrypt.compare(password, user.password)) {
            return this.sanitizeClient([user])[0];
        }

        throw unauthorizedError;
    }

    async deleteById(userId: string) {
        const objectId = MongooseTypes.ObjectId(userId);
        const deletedClient = await this.userModel.findOneAndDelete({
            _id: objectId
        });

        if (!deletedClient) {
            throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
        }

        return { success: true };
    }

    async findByPayload(payload: any) {
        const { email } = payload;
        return await this.userModel.findOne({ email });
    }

    async findAll() {
        const allClients = await this.userModel.find();
        return this.sanitizeClient([...allClients]);
    }
}
