import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JourneySchemaController } from './journey-schema.controller';
import { JourneySchemaSchema } from './journey-schema.schema';
import { JourneySchemaService } from './journey-schema.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'Schema', schema: JourneySchemaSchema }
        ])
    ],
    controllers: [JourneySchemaController],
    providers: [JourneySchemaService],
    exports: [JourneySchemaService]
})
export class JourneySchemaModule {}
