import { Controller, Get, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { JourneySchemaService } from './journey-schema.service';

@ApiTags('Journey Event Schemas')
@Controller()
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class JourneySchemaController {
    constructor(private readonly journeySchemaService: JourneySchemaService) {}

    @Get('/api/v1/journey-schemas')
    @ApiOperation({
        summary: 'Returns schemas for all events.'
    })
    async getAllSchemas() {
        return this.journeySchemaService.getAllSchemas();
    }
}
