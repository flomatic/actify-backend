import * as mongoose from 'mongoose';

export const JourneySchemaSchema = new mongoose.Schema({
    event_name: { type: String, index: true },
    fields: { type: Object }
});