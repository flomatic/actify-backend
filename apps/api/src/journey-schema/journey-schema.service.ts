import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { JourneySchemaInterface } from './journey-schema.interface';

@Injectable()
export class JourneySchemaService {
    constructor(
        @InjectModel('Schema')
        private readonly journeySchemaModel: Model<JourneySchemaInterface>
    ) {}

    async getAllSchemas(): Promise<JourneySchemaInterface[]> {
        const res = await this.journeySchemaModel.find({}, { __v: 0, _id: 0 });
        return res;
    }

    async getEventVariablesByName(event_name: string): Promise<any> {
        const res = await this.journeySchemaModel
            .findOne({ event_name }, { __v: 0, _id: 0 })
            .lean();
        return res;
    }
}
