import * as mongoose from 'mongoose';

export interface JourneySchemaInterface extends mongoose.Document {
    event_name: string;
    fields: any;
}
