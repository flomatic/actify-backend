import { Module } from '@nestjs/common';
import { JourneyTemplateController } from './journey-template.controller';
import { JourneyTemplateService } from './journey-template.service';

@Module({
    controllers: [JourneyTemplateController],
    providers: [JourneyTemplateService],
    exports: [JourneyTemplateService]
})
export class JourneyTemplateModule {}
