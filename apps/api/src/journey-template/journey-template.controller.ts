// eslint-disable-next-line max-len
import { JourneyBlockTemplateInterface } from '@entries/journey-block/interface/journey-block.interface';
import { Controller, Get, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiTags
} from '@nestjs/swagger';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { JourneyTemplateService } from './journey-template.service';

@ApiTags('Journey Block Templates')
@Controller()
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class JourneyTemplateController {
    constructor(
        private readonly journeyTemplateService: JourneyTemplateService
    ) {}

    @Get('/api/v1/journey-block-template')
    @ApiOperation({ summary: 'Returns all journey block templates.' })
    @ApiResponse({ status: 200, type: [JourneyBlockTemplateInterface] })
    getJourneyTemplates() {
        return this.journeyTemplateService.getAllJourneyBlockTemplates();
    }
}
