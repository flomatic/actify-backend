// eslint-disable-next-line max-len
import { JourneyBlockTemplates } from '@entries/journey-block/data/journey-block-template.data';
// eslint-disable-next-line max-len
import { JourneyBlockTemplateInterface } from '@entries/journey-block/interface/journey-block.interface';
import { Injectable } from '@nestjs/common';

@Injectable()
export class JourneyTemplateService {
    getAllJourneyBlockTemplates(): JourneyBlockTemplateInterface[] {
        return JourneyBlockTemplates;
    }
}
