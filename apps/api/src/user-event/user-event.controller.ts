import {
    BadRequestException,
    Controller,
    Get,
    Query,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UserEventDTO } from '@shared/models/user-event.model';
import { UserInterface } from '@shared/models/user.model';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { UserService } from './../user/user.service';
import { UserEventService } from './user-event.service';
import { UserEventValidator } from './validation/user-event.validator';

@ApiTags('User Events')
@Controller()
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class UserEventController {
    constructor(
        private readonly userEventService: UserEventService,
        private readonly userService: UserService
    ) {}

    @Get('/api/v1/user-event')
    @ApiOperation({
        summary: 'Returns events for a specific user (paginated).'
    })
    async getUserEvents(@Query() userEventDto: UserEventDTO) {
        userEventDto.phone_number = userEventDto?.phone_number?.replace(
            ' ',
            '+'
        );
        const { error } = UserEventValidator.validate(userEventDto);
        if (error) {
            throw new BadRequestException(error);
        }
        const user: UserInterface = await this.userService.getUser({
            email: userEventDto.email,
            phone_number: userEventDto.phone_number
        });
        const userEvents = await this.userEventService.getUserEvents({
            user_unique_value: user.user_unique_value,
            user_unique_key: user.user_unique_key,
            page_number: userEventDto.page_number || 0,
            per_page: userEventDto.per_page,
            start_date: userEventDto.start_date,
            end_date: userEventDto.end_date
        });

        return userEvents;
    }
}
