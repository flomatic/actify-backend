import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UserEventInterface } from '@shared/models/user-event.model';
import { Model } from 'mongoose';

@Injectable()
export class UserEventService {
    constructor(
        @InjectModel('UserEvent')
        private readonly userEventModel: Model<UserEventInterface>
    ) {}

    async getUserEvents(params: {
        user_unique_key: string;
        user_unique_value: string;
        page_number?: number;
        per_page?: number;
        start_date?: string;
        end_date?: string;
    }) {
        const pageNumber = +params.page_number || 1;
        const perPage = +params.per_page || 5;
        const skipValue = Number((pageNumber - 1) * perPage);

        const dbQueryItems: any = {
            user_unique_key: params.user_unique_key,
            user_unique_value: params.user_unique_value
        };

        if (params.start_date) {
            const startDate = new Date(params.start_date).setHours(0, 0, 0, 0);
            dbQueryItems.start_date = { $gte: startDate };
        }

        if (params.end_date) {
            const endDate = new Date(params.end_date).setHours(23, 59, 59, 999);
            dbQueryItems.$or = [
                { end_date: { $lte: endDate } },
                { end_date: null }
            ];
        }

        const result = await this.userEventModel
            .find(dbQueryItems)
            .sort({ _id: -1 })
            .skip(skipValue)
            .limit(perPage)
            .lean();

        return {
            result: result || [],
            pageNumber
        };
    }
}
