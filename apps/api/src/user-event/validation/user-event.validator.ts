import Joi = require('joi');

export const UserEventValidator = Joi.object({
    phone_number: Joi.string().regex(/^(\+\d{1,3}[- ]?)?\d{10}$/),
    email: [Joi.string().email(), Joi.number()],
    page_number: Joi.number().max(500),
    per_page: Joi.number().max(500),
    start_date: Joi.string().allow(''),
    end_date: Joi.string().allow(''),
    tab_id: Joi.string().allow('')
}).xor('phone_number', 'email');
