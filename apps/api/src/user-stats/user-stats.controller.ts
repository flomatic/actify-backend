import { Controller, Get, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    TimePeriod,
    UserStatsInterface
} from '@shared/models/user-stats.model';
import { getFromToDateInDay } from '../analytics/helper/date-utils';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { UserService } from '../user/user.service';
import { UserStatsService } from './user-stats.service';

@ApiTags('User Stats')
@Controller()
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class UserStatsController {
    constructor(
        private readonly userStatsService: UserStatsService,
        private readonly userService: UserService
    ) {}

    @Get('/api/v1/user-stats')
    @ApiOperation({ summary: 'Get user stats' })
    async getReport() {
        const toDayUserStats = getFromToDateInDay();
        const userStats: UserStatsInterface[] = await this.userStatsService
            .getModel()
            .find({
                from_date: { $gte: toDayUserStats.from_date },
                to_date: { $lte: toDayUserStats.to_date }
            })
            .sort({ from_date: 1 });
        const usersCount = await this.userService.countAllUsers();
        const userStatsReport = {
            today_new_users: 0,
            today_sessions: 0,
            total_users: usersCount,
            today_unique_visitors: 0,
            current_online_users: 0,
            data: [] as {
                from_date: Date;
                to_date: Date;
                time_period: TimePeriod;
                total_session: number;
                total_unique_visit: number;
                online_users: number;
                new_users: number;
            }[]
        };
        const startDate = new Date();
        startDate.setHours(0, 0, 0, 0);

        for (let i = 0; i <= 23; i++) {
            const fromDate = new Date(startDate);
            fromDate.setHours(startDate.getHours() + i, 0, 0, 0);
            const toDate = new Date(startDate);
            toDate.setHours(startDate.getHours() + i + 1, 0, 0, 0);
            userStatsReport.data.push({
                from_date: fromDate,
                to_date: toDate,
                time_period: TimePeriod.HOURLY,
                total_session: 0,
                total_unique_visit: 0,
                online_users: 0,
                new_users: 0
            });
        }

        userStats?.forEach((userStat, i) => {
            let index = -1;
            userStatsReport.data.find((u, v) => {
                const isMatch =
                    u.from_date.toISOString() ===
                        userStat.from_date.toISOString() &&
                    u.to_date.toISOString() === userStat.to_date.toISOString();
                if (isMatch) {
                    index = v;
                }
                return isMatch;
            });
            if (index !== -1) {
                userStatsReport.data[index].new_users = userStat.new_users.find(
                    (n) => n.channel === 'browser'
                ).value;
                userStatsReport.data[
                    index
                ].online_users = userStat.maximum_online_users.find(
                    (n) => n.channel === 'browser'
                ).value;
                userStatsReport.data[
                    index
                ].total_session = userStat.total_session.find(
                    (t) => t.channel === 'browser'
                ).value;
                userStatsReport.data[
                    index
                ].total_unique_visit = userStat.total_unique_visit.find(
                    (v) => v.channel === 'browser'
                ).value;
                if (userStats.length === i + 1) {
                    userStatsReport.current_online_users =
                        userStatsReport.data[index].online_users;
                }

                userStatsReport.today_new_users +=
                    userStatsReport.data[index].new_users;
                userStatsReport.today_sessions +=
                    userStatsReport.data[index].total_session;
                userStatsReport.today_unique_visitors +=
                    userStatsReport.data[index].total_unique_visit;
            }
        });
        return userStatsReport;
    }
}
