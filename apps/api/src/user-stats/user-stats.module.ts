import { Module } from '@nestjs/common';
import { UserStatsModelModule } from '@shared/models/user-stats.model';
import { UserEventModule } from './../user-event/user-event.module';
import { UserModule } from './../user/user.module';
import { UserStatsController } from './user-stats.controller';
import { UserStatsService } from './user-stats.service';

@Module({
    imports: [UserEventModule, UserModule, UserStatsModelModule],
    controllers: [UserStatsController],
    providers: [UserStatsService],
    exports: [UserStatsService]
})
export class UserStatsModule {}
