import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UserStatsInterface } from '@shared/models/user-stats.model';
import { Model } from 'mongoose';

@Injectable()
export class UserStatsService {
    constructor(
        @InjectModel('UserStats')
        private readonly userStatsModel: Model<UserStatsInterface>
    ) {}

    async findQuery(params) {
        return await this.userStatsModel.find(params);
    }

    getModel() {
        return this.userStatsModel;
    }

    async create(params) {
        const userStats = new this.userStatsModel(params);
        await userStats.save();
        return userStats;
    }

    async findOneByQuery(params) {
        return await this.userStatsModel.findOne(params);
    }
}
