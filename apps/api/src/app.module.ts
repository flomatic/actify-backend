import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { TerminusModule } from '@nestjs/terminus';
import { ConfigModule } from '@shared/config.module';
import { RedisModule } from '@shared/redis/redis.module';
import { SentryModule } from '@shared/sentry/sentry.module';
import { SettingModule } from '@shared/setting/setting.module';
import { CommandModule } from 'nestjs-command';
import { AnalyticsCommand } from './analytics/analytics.command';
import { AnalyticsModule } from './analytics/analytics.module';
import { ReportGeneratorCommand } from './analytics/report-generator.command';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthCommand } from './auth/auth.command';
import { AuthModule } from './auth/auth.module';
import { CampaignCommand } from './campaign/campaign.command';
import { CampaignModule } from './campaign/campaign.module';
import { ConfigurationModule } from './configuration/configuration.module';
import { HealthController } from './health/health.controller';
import { JourneyAnalyticsModule } from './journey-analytics/journey-analytics.module';
import { JourneyEnumModule } from './journey-enum/journey-enum.module';
import { JourneySchemaModule } from './journey-schema/journey-schema.module';
import { JourneyTemplateModule } from './journey-template/journey-template.module';
import { JourneyConsole } from './journey/journey.console';
import { JourneyModule } from './journey/journey.module';
import { LocationCommand } from './location/location.command';
import { LocationModule } from './location/location.module';
import { MessageTemplateModule } from './message-template/message-template.module';
import { PublicModule } from './public/public.module';
import { SchemaModule } from './schema/schema.module';
import { SharedModule } from './client/client.module';
import { SystemVariablesModule } from './system-variables/system-variables.module';
import { TagModule } from './tag/tag.module';
import { UserEventModule } from './user-event/user-event.module';
import { UserStatsModule } from './user-stats/user-stats.module';
import { UserModule } from './user/user.module';
import { VariableModule } from './variable/variable.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { SegmentModule } from '@shared/segment/segment.module';
import { SegmentCommand } from '@shared/segment/segment.command';
import { SegmentApiModule } from './segment/segment.module';

@Module({
    imports: [
        ConfigModule,
        MongooseModule.forRootAsync({
            useFactory: async (configService: ConfigService) => {
                return {
                    uri: configService.get('MONGO_URI'),
                    useNewUrlParser: true,
                    useCreateIndex: true,
                    useUnifiedTopology: true,
                    useFindAndModify: false
                };
            },
            inject: [ConfigService]
        }),
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '..', 'static'),
            serveRoot: '/static',
            exclude: ['/api*'],
            serveStaticOptions: {}
        }),
        SentryModule,
        CommandModule,
        JourneyModule,
        SharedModule,
        AnalyticsModule,
        AuthModule,
        UserStatsModule,
        UserEventModule,
        RedisModule,
        UserModule,
        JourneyTemplateModule,
        JourneySchemaModule,
        JourneyEnumModule,
        JourneyAnalyticsModule,
        SchemaModule,
        SegmentApiModule,
        SegmentModule,
        SettingModule,
        LocationModule,
        TerminusModule,
        AnalyticsModule,
        SystemVariablesModule,
        CampaignModule,
        MessageTemplateModule,
        // ReplayModule,
        PublicModule,
        TagModule,
        VariableModule,
        ConfigurationModule
    ],
    controllers: [AppController, HealthController],
    providers: [
        AppService,
        JourneyConsole,
        AnalyticsCommand,
        ReportGeneratorCommand,
        SegmentCommand,
        AuthCommand,
        LocationCommand,
        CampaignCommand
    ]
})
export class AppModule {}
