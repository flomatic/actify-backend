import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Post,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { AdminGuard } from 'apps/api/src/guards/admin.guard';
import { Payload } from 'apps/api/src/types/payload';
import { Client } from 'apps/api/src/utilities/client.decorator';
import { ClientService } from '../client/client.service';
import {
    ChangePasswordDTO,
    DeleteDTO,
    LoginDTO,
    RegisterDTO
} from '../../../../entries/auth/dto/auth.dto';
import { AuthService } from './auth.service';
import {
    ChangePasswordDTOValidation,
    RegisterDTOValidation
} from './validator/auth.validator';

@ApiTags('Auth')
@Controller()
@UseInterceptors(SentryInterceptor)
export class AuthController {
    constructor(
        private clientService: ClientService,
        private authService: AuthService
    ) {}

    @Get('/api/v1/auth')
    @UseGuards(AuthGuard('jwt'), AdminGuard)
    @ApiBearerAuth('access_token')
    @ApiOperation({
        summary: 'Returns an array of all admin-panel users.'
    })
    async findAll() {
        return await this.clientService.findAll();
    }

    @Post('/api/v1/auth/login')
    @ApiOperation({
        summary: 'Logs a user in, using email and password.'
    })
    async login(@Body() clientDto: LoginDTO) {
        const client = await this.clientService.findByLogin(clientDto);
        const payload: Payload = { email: client.email };
        const token = await this.authService.signPayload(payload);

        return { client, token };
    }

    @Post('/api/v1/auth/register')
    @UseGuards(AuthGuard('jwt'), AdminGuard)
    @ApiOperation({ summary: 'Registers a new admin-panel user.' })
    @ApiBearerAuth('access_token')
    async register(@Body() registerDto: RegisterDTO) {
        const validate = RegisterDTOValidation.validate(registerDto);
        if (validate.error) {
            throw new BadRequestException(validate.error, 'Invalid parameters');
        }

        const client = await this.clientService.create(registerDto);
        const payload = { email: client.email };
        const token = await this.authService.signPayload(payload);

        return { client, token };
    }

    @Get('/api/v1/auth/user')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth('access_token')
    @ApiOperation({
        summary: 'Returns the current admin-panel user.'
    })
    async getUser(@Client() client: any) {
        const role = client.admin ? 'admin' : 'client';
        return { result: { email: client.email, role } };
    }

    @Delete('/api/v1/auth/user')
    @UseGuards(AuthGuard('jwt'), AdminGuard)
    @ApiBearerAuth('access_token')
    @ApiOperation({ summary: 'Deletes a specific admin-panel user.' })
    @ApiBearerAuth()
    async deleteUser(@Body() client: DeleteDTO) {
        return await this.clientService.deleteById(client.id);
    }

    @Post('/api/v1/auth/change-password')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth('access_token')
    @ApiOperation({ summary: "Changes a user's password" })
    async changePassword(@Body() changePasswordDTO: ChangePasswordDTO) {
        const validate = ChangePasswordDTOValidation.validate(
            changePasswordDTO
        );
        if (validate.error) {
            throw new BadRequestException(validate.error, 'Invalid parameters');
        }

        const { email, oldPassword } = changePasswordDTO;
        const loginInfo: LoginDTO = { email, password: oldPassword };
        const { id } = await this.clientService.findByLogin(loginInfo);
        return await this.clientService.changePassword(changePasswordDTO, id);
    }
}
