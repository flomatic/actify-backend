import { Command, Console } from 'nestjs-console';
import { AuthService } from './auth.service';

@Console()
export class AuthCommand {
    constructor(private readonly authService: AuthService) {}

    @Command({ command: 'build_sample_admin', description: 'create admin' })
    async create() {
        try {
            await this.authService.create({
                email: 'admin@actify.ir',
                password: 'admin@actify.ir',
                admin: true
            });
        } catch (e) {
            //
        }
    }
}
