import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';
import { AuthService } from '../auth.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
    constructor(
        private authService: AuthService,
        private configService: ConfigService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.get('JWT_CLIENT_ACCESS_TOKEN_SECRET')
        });
    }

    async validate(payload: any, done: VerifiedCallback) {
        const client = await this.authService.validateClient(payload);
        if (!client) {
            return done(
                new HttpException(
                    'Unauthorized access',
                    HttpStatus.UNAUTHORIZED
                ),
                false
            );
        }

        return done(null, client, payload.iat);
    }
}
