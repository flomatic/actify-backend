import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';
import { AuthService } from '../auth.service';

@Injectable()
export class AccessTokenStrategy extends PassportStrategy(
    Strategy,
    'jwt_organization'
) {
    constructor(
        private authService: AuthService,
        private configService: ConfigService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.get('JWT_ORG_ACCESS_TOKEN_SECRET'),
            ignoreExpiration: false
        });
    }

    async validate(payload: any, done: VerifiedCallback) {
        const organization = await this.authService.validateOrganization(
            payload
        );
        if (!organization) {
            return done(
                new HttpException(
                    'Unauthorized access',
                    HttpStatus.UNAUTHORIZED
                ),
                false
            );
        }

        return done(null, organization);
    }
}
