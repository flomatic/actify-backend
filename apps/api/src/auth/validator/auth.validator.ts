import * as Joi from 'joi';

export const RegisterDTOValidation = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(6).required(),
    admin: Joi.boolean()
});

export const ChangePasswordDTOValidation = Joi.object({
    email: Joi.string().email().required(),
    oldPassword: Joi.string().max(120).required(),
    newPassword: Joi.string().min(8).max(120).required()
});
