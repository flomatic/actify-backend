import { Logger, Module } from '@nestjs/common';
import { OrganizationModule } from '../organization/organization.module';
import { SharedModule } from '../client/client.module';
import { AccessTokenStrategy } from './auth-strategy/organization.strategy';
import { AuthCommand } from './auth.command';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './auth-strategy/jwt.strategy';

@Module({
    controllers: [AuthController],
    imports: [SharedModule, Logger, OrganizationModule],
    providers: [AuthService, JwtStrategy, AuthCommand, AccessTokenStrategy],
    exports: [AuthCommand, AuthService]
})
export class AuthModule {}
