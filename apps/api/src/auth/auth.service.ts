import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Payload } from 'apps/api/src/types/payload';
import { sign } from 'jsonwebtoken';
import { OrganizationService } from '../organization/organization.service';
import { ClientService } from '../client/client.service';

@Injectable()
export class AuthService {
    constructor(
        private readonly clientService: ClientService,
        private readonly configService: ConfigService,
        private readonly organizationService: OrganizationService
    ) {}

    async signPayload(payload: Payload) {
        // TODO: Insert secret from config
        const secret = this.configService.get<string>(
            'JWT_CLIENT_ACCESS_TOKEN_SECRET'
        );
        // TODO: Move the expiration time to config
        return sign(payload, secret, { expiresIn: '8h' });
    }

    async validateClient(payload: Payload) {
        return await this.clientService.findByPayload(payload);
    }

    async create(params: { email: string; password: string; admin: boolean }) {
        return await this.clientService.create(params);
    }

    async validateOrganization(payload: { name: string }) {
        return await this.organizationService.validateOrganization(payload);
    }
}
