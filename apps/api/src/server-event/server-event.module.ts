import { Module } from '@nestjs/common';
import { RedisModule } from '@shared/redis/redis.module';
import { ServerEventService } from './server-event.service';

@Module({
    imports: [RedisModule],
    providers: [ServerEventService],
    exports: [ServerEventService]
})
export class ServerEventModule {}
