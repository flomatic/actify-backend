import { Injectable } from '@nestjs/common';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { RedisService } from '@shared/redis/redis.service';

@Injectable()
export class ServerEventService {
    constructor(
        private readonly logger: BLogger,
        private readonly redisService: RedisService
    ) {}

    async sendEvent(event) {
        const messageString: string = JSON.stringify(event);
        try {
            return await this.redisService.readWriteClient.xadd(
                'server-event',
                '*',
                'message',
                messageString
            );
        } catch (e) {
            this.logger.error(ACTIONS.SERVER_EVENT_SERVICE, e);
        }
    }
}
