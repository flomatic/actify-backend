import * as Joi from 'joi';

export const ServerEventValidation = Joi.object({
    user_unique_value: Joi.string().required(),
    user_unique_key: Joi.string().required(),
    type: Joi.string().required(),
    event_name: Joi.string().required(),
    body: Joi.object().required()
});
