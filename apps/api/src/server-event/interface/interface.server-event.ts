export interface ServerEvent {
    user_unique_value: string;
    user_unique_key: string;
    type: MessageType;
    event_name: string;
    body: any;
    date: Date;
}

export enum MessageType {
    EVENT = 'event',
    CREATE_USER = 'create_user',
    DELETE_USER = 'delete_user',
    UPDATE_USER = 'update_user',
    UPDATE_USER_ATTRS = 'update_user_attributes'
}
