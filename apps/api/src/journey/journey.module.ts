import { JourneyModelModule } from '@entries/journey/model/journey';
import { Module } from '@nestjs/common';
import { BackendJobModule } from 'apps/main-processor/src/backend-job/backend-job.module';
import { JourneyConsole } from './journey.console';
import { JourneyController } from './journey.controller';
import { JourneyService } from './journey.service';

@Module({
    imports: [JourneyModelModule, BackendJobModule],
    controllers: [JourneyController],
    providers: [JourneyConsole, JourneyService],
    exports: [JourneyConsole, JourneyService]
})
export class JourneyModule {}
