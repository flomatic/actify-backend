import { Command, Console, createSpinner } from 'nestjs-console';
import { JourneyService } from './journey.service';

@Console()
export class JourneyConsole {
    constructor(private readonly journeyService: JourneyService) {}

    @Command({
        command: 'create-journeys',
        description: 'create sample journeys'
    })
    async create() {
        const spinner = createSpinner({ text: 'Creating journeys' });
        const journeys = [
            {
                name: 'journey_1',
                status: 'draft',
                fields: [
                    {
                        id: 1,
                        name: 'specific_users',
                        category: 'trigger',
                        path: [
                            {
                                from_node: 'default',
                                to_id: 2,
                                to_node: 'default'
                            }
                        ],
                        inputs: {
                            user: [
                                {
                                    key: 'email',
                                    value: 'tehranserver24@gmail.com'
                                }
                            ]
                        }
                    },
                    {
                        id: 2,
                        name: 'send_sms',
                        category: 'action',
                        path: [
                            {
                                from_node: 'sent',
                                to_id: 3,
                                to_node: 'default'
                            }
                        ],
                        inputs: {
                            body: 'salam khoobi ?',
                            ignore_sleep_time: false
                        }
                    },
                    {
                        id: 3,
                        name: 'wait_for_event',
                        category: 'flow_control',
                        inputs: {
                            event_name: 'khoobam',
                            number_of_happening: 1,
                            time_out: {
                                type: 'minutes',
                                value: '1'
                            },
                            filters: [
                                {
                                    key: 1,
                                    operation: {
                                        variable: 'khoobam.number.value',
                                        condition: 'greater_or_equal',
                                        value: 5
                                    }
                                },
                                {
                                    key: 2,
                                    operation: {
                                        operator: 'or'
                                    }
                                },
                                {
                                    key: 3,
                                    operation: {
                                        variable: 'khoobam.string.value',
                                        condition: 'equal',
                                        value: '5'
                                    }
                                }
                            ]
                        },
                        path: [
                            {
                                from_node: 'happened',
                                to_id: 2,
                                to_node: 'default'
                            },
                            {
                                from_node: 'not_happened',
                                to_id: 5,
                                to_node: 'default'
                            }
                        ]
                    },
                    {
                        id: 5,
                        name: 'send_sms',
                        category: 'action',
                        path: [],
                        inputs: {
                            body: 'khoda ro shokr',
                            ignore_sleep_time: true
                        }
                    }
                ]
            },
            {
                name: 'journey_2',
                status: 'draft',
                fields: [
                    {
                        id: 1,
                        name: 'getting_offline',
                        category: 'trigger',
                        path: [
                            {
                                from_node: 'default',
                                to_id: 2,
                                to_node: 'default'
                            }
                        ],
                        inputs: {
                            access_from: 'browser'
                        }
                    },
                    {
                        id: 2,
                        name: 'send_sms',
                        category: 'action',
                        path: [],
                        inputs: {
                            body: 'koja rafti ?',
                            ignore_sleep_time: true
                        }
                    }
                ]
            }
        ];
        await this.journeyService.createJourney(journeys);
        spinner.succeed('Done...');
    }
}
