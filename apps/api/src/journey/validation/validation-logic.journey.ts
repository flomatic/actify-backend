/* eslint-disable-next-line max-len */
import { JourneyBlockCategory } from '@entries/journey-block/enum/journey-block-category.enum';
import { JourneyFieldAction } from '@entries/journey/enum/journey-field-action';
import { Journey } from '@entries/journey/model/journey';

export const validateJourneyForPublish = (
    journey: Journey
): { hasError: boolean; message: string } => {
    // Length of the journey fields should be longer than the minimum
    if (journey.fields.length < 2) {
        return {
            hasError: true,
            message: 'A journey should have at least two fields.'
        };
    }

    // Each journey should have one trigger
    const triggers = journey.fields.filter(
        (field: any) => field.category === JourneyBlockCategory.TRIGGER
    );
    if (triggers.length !== 1) {
        return {
            hasError: true,
            message: 'Only one trigger per journey is allowed.'
        };
    }

    // Check for other errors
    let hasError = false;
    let message = '';
    journey.fields.forEach((field) => {
        let matchedItemsCount = 0;
        journey.fields.forEach((f) =>
            f.path.forEach((p) => {
                if (p.to_id === field.id) {
                    matchedItemsCount++;
                }
            })
        );
        if (
            matchedItemsCount === 0 &&
            field.name !== JourneyFieldAction.GETTING_ONLINE &&
            field.name !== JourneyFieldAction.GETTING_OFFLINE
        ) {
            hasError = true;
            message = 'Invalid path connections.';
        }
    });
    return {
        hasError,
        message
    };
};
