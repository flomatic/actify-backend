import { HttpMethod } from '@entries/httpmethod/enum/httpmethod';
import { JourneyFieldAction } from '@entries/journey/enum/journey-field-action';
import { JourneyStatus } from '@entries/journey/enum/journey-status';
import { MessageTemplateType } from '@shared/models/message-template.model';
import * as Joi from 'joi';

const timeOut = Joi.object({
    type: Joi.string().allow(
        'iso_date_string',
        'days',
        'hours',
        'minutes',
        'seconds'
    ),
    value: [Joi.string(), Joi.number()]
}).required();

const DateType = Joi.object({
    from: Joi.string()
        .valid('now', 'iso_date_string', 'year', 'month', 'day')
        .required(),
    from_value: Joi.any().optional(),
    to: Joi.string()
        .valid('now', 'iso_date_string', 'year', 'month', 'day')
        .required(),
    to_value: Joi.any().optional()
});

const eventFilter = Joi.object({
    key: Joi.number(),
    operation: [
        Joi.object({
            variable: Joi.string(),
            value: [Joi.string(), Joi.number()],
            condition: Joi.string().when('value', {
                is: Joi.number().valid(true),
                then: Joi.allow('equal', 'lower_or_equal', 'greater_or_equal'),
                otherwise: Joi.allow('contains', 'equal')
            })
        }),
        Joi.object({
            operator: Joi.string().allow('and', 'or')
        })
    ]
});

const messageTemplateVariable = Joi.object({
    name: Joi.string().empty().max(100),
    variable: Joi.string().empty().max(1000)
});

const filterWithEvent = Joi.object({
    event_name: Joi.string(),
    number_of_happening: Joi.number().min(0).max(10),
    filters: Joi.array().items(eventFilter),
    date: DateType
});

const waitForEvent = Joi.object({
    event_name: Joi.string(),
    time_out: timeOut,
    number_of_happening: Joi.number().min(1).max(10),
    filters: Joi.array().items(eventFilter)
});

const waitForTime = Joi.object({
    time_out: timeOut
});

const userInteraction = Joi.object({
    template_id: Joi.string()
        .regex(/^[0-9a-fA-F]{24}$/)
        .required(),
    type: Joi.string().allow(...Object.values(MessageTemplateType)).optional(),
    variables: Joi.array().items(messageTemplateVariable).optional()
});

const callApi = Joi.object({
    method: Joi.string().allow(...Object.values(HttpMethod)),
    url: Joi.string().uri(),
    headers: Joi.array().items(
        Joi.object({
            key: Joi.string().allow('').optional(),
            value: Joi.string().allow('').optional()
        }).optional()
    ),
    query: Joi.array().items(
        Joi.object({
            key: Joi.string().allow('').optional(),
            value: Joi.string().allow('').optional()
        }).optional()
    ),
    data: Joi.when('method', {
        is: HttpMethod.POST,
        then: Joi.array().items(
            Joi.object({
                key: Joi.string(),
                value: Joi.string()
            }).optional()
        )
    }),
    variables: Joi.array().items(
        Joi.object({
            key: Joi.string(),
            variable: Joi.string()
        }).optional()
    )
});

const filterWithApiVariables = Joi.object({
    key: Joi.number(),
    operation: Joi.array().items(
        Joi.object({
            variable: [Joi.string(), Joi.number()],
            value: [Joi.string(), Joi.number()],
            condition: Joi.string().when('value', {
                is: Joi.number().valid(true),
                then: Joi.allow('equal', 'lower_or_equal', 'greater_or_equal'),
                otherwise: Joi.allow('contains', 'equal')
            })
        }),
        Joi.object({
            operator: Joi.string().allow('and', 'or')
        })
    )
});

const userSegmentation = Joi.object({
    id: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
});

export const JourneyValidation = Joi.object({
    name: Joi.string().required(),
    description: Joi.string().allow('').max(5000).optional(),
    tags: Joi.array().items(Joi.string()).optional(),
    status: Joi.string().allow(...Object.values(JourneyStatus)),
    fields: Joi.array().items(
        Joi.object({
            id: Joi.string(),
            name: Joi.string(),
            category: Joi.string(),
            path: Joi.array().items(
                Joi.object({
                    from_node: Joi.string().default('default'),
                    to_id: Joi.string(),
                    to_node: Joi.string()
                })
            ),
            inputs: Joi.optional()
                .when('name', {
                    is: JourneyFieldAction.FILTER_WITH_USER_SEGMENTATION,
                    then: userSegmentation
                })
                .when('name', {
                    is: JourneyFieldAction.FILTER_WITH_EVENT,
                    then: filterWithEvent
                })
                .when('name', {
                    is: JourneyFieldAction.WAIT_FOR_EVENT,
                    then: waitForEvent
                })
                .when('name', {
                    is: JourneyFieldAction.WAIT_FOR_TIME,
                    then: waitForTime
                })
                .when('name', {
                    is: JourneyFieldAction.USER_INTERACTION,
                    then: userInteraction
                })
                .when('name', {
                    is: JourneyFieldAction.CALL_API,
                    then: callApi
                })
                .when('name', {
                    is: JourneyFieldAction.FILTER_WITH_API_VARIABLES,
                    then: filterWithApiVariables
                })
        })
    ),
    meta: Joi.object(),
    rate_limit: Joi.object({
        email: Joi.number().min(0).optional(),
        sms: Joi.number().min(0).optional(),
        push_notification: Joi.number().min(0).optional()
    }).optional(),
    ignore_dnd: Joi.boolean().optional(),
    ignore_rate_limit: Joi.boolean().optional()
});
