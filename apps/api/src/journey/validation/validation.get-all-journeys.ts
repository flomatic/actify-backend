import { JourneyStatus } from '@entries/journey/enum/journey-status';
import * as Joi from 'joi';

export const GetAllJourneysValidation = Joi.object({
    page: Joi.number().min(1).optional(),
    itemsPerPage: Joi.number().min(1).optional(),
    name: Joi.string().optional(),
    status: Joi.string()
        .valid(...Object.values(JourneyStatus))
        .optional(),
    sortBy: Joi.string().optional(),
    sortDesc: Joi.string().valid(1, -1).optional(),
    tag: Joi.string().optional()
});
