import { JourneyPaginationParamsDTO } from '@entries/journey/dto/journey-pagination-params';
import { Journey } from '@entries/journey/model/journey';
import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    Query,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { OperationByIdDTO } from '@entries/general/dto/general.dto';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { IdValidation } from '@shared/validators/validation.id';
import { AdminGuard } from 'apps/api/src/guards/admin.guard';
import { JourneyService } from './journey.service';
import { validateJourneyForPublish } from './validation/validation-logic.journey';
import { JourneyValidation } from './validation/validation.journey';
import { GetAllJourneysValidation } from './validation/validation.get-all-journeys';
import { JourneyJobService } from 'apps/main-processor/src/journey/journey-job.service';
import { ResetJourney } from 'apps/main-processor/src/backend-job/reset-journey.service';

@ApiTags('Journey')
@Controller()
@UseInterceptors(SentryInterceptor)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
export class JourneyController {
    constructor(
        private readonly journeyService: JourneyService,
        private readonly resetJourney: ResetJourney
    ) {}

    @Get('/api/v1/journey')
    @ApiOperation({ summary: 'Returns all journeys.' })
    async getAllJourneys(
        @Query() paginationParams: JourneyPaginationParamsDTO
    ) {
        const { error } = GetAllJourneysValidation.validate(paginationParams);
        if (error) {
            throw new BadRequestException(error);
        }
        return await this.journeyService.getAllJourneys(paginationParams);
    }

    @Get('/api/v1/journey/:id')
    @ApiOperation({ summary: 'Returns a journey by id.' })
    async getAJourney(@Param() { id }: OperationByIdDTO) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        const result = await this.journeyService.getAJourney(id);
        if (!result) {
            throw new BadRequestException('No journeys found for this id');
        }

        return result;
    }

    @Post('/api/v1/journey')
    @ApiOperation({ summary: 'Adds a new journey.' })
    async addJourney(@Body() journey: Journey) {
        const { error } = JourneyValidation.validate(journey);
        if (error) {
            throw new BadRequestException(error);
        }

        const { hasError, message } = validateJourneyForPublish(journey);
        if (hasError) {
            throw new BadRequestException(message);
        }

        return this.journeyService.addJourney(journey);
    }

    @Put('/api/v1/journey/:id')
    @ApiOperation({ summary: 'Updates a journey by id.' })
    async updateJourney(
        @Param() { id }: OperationByIdDTO,
        @Body() journey: Journey
    ) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        const { error } = JourneyValidation.validate(journey);
        if (error) {
            throw new BadRequestException(error);
        }

        const { hasError, message } = validateJourneyForPublish(journey);
        if (hasError) {
            throw new BadRequestException(message);
        }
        await this.journeyService.updateJourney(journey, id);
        await this.resetJourney.addJob(id);
        return { status: 'ok' };
    }

    @Delete('/api/v1/journey/:id')
    @UseGuards(AdminGuard)
    @ApiOperation({ summary: 'Deletes a journey by id.' })
    async deleteJourney(@Param() { id }: OperationByIdDTO) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }
        await this.journeyService.deleteJourney(id);
        await this.resetJourney.addJob(id);

        return { status: 'ok' };
    }
}
