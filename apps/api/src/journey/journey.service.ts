import { JourneyPaginationParamsDTO } from '@entries/journey/dto/journey-pagination-params';
import { Journey, JourneyDocument } from '@entries/journey/model/journey';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
// import { ResetJourney } from '@shared/backend-job/reset-journey.service';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import * as escRegExp from 'escape-string-regexp';
import { Model } from 'mongoose';

@Injectable()
export class JourneyService {
    constructor(
        @InjectModel('Journey')
        private readonly journeyModel: Model<JourneyDocument>,
        private readonly logger: BLogger // private readonly resetJourney: ResetJourney
    ) {}

    async getAllJourneys(params: JourneyPaginationParamsDTO) {
        try {
            const name = params.name ? escRegExp(params.name) : null;
            const pageNumber = +params.page || 1;
            const perPage = +params.itemsPerPage || 10;
            const skipValue = Number((pageNumber - 1) * perPage);
            const status = params.status;
            const tag = params.tag;

            const queryModel = {};

            if (name) queryModel['name'] = new RegExp(name, 'gi');
            if (status) queryModel['status'] = status;
            if (tag) queryModel['tags'] = tag;

            const total = await this.journeyModel.count(queryModel);
            const result = await this.journeyModel
                .find(queryModel, { __v: 0, isLocked: 0 })
                .skip(skipValue)
                .limit(perPage)
                // .populate('tags')
                .lean();

            return {
                result,
                total,
                page: pageNumber
            } as { result: JourneyDocument[]; total: number; page: number };
        } catch (err) {
            this.logger.error(ACTIONS.JOURNEY_SERVICE, err);
        }
    }

    async createJourney(journeys: any[]) {
        await Promise.all(
            journeys.map(async (journey) => {
                await this.journeyModel.updateOne(
                    {
                        name: journey.name
                    },
                    {
                        $set: journey
                    },
                    {
                        upsert: true
                    }
                );
            })
        );
    }

    async getAJourney(id: string): Promise<JourneyDocument> {
        try {
            return await this.journeyModel.findById(id, {
                __v: 0,
                isLocked: 0
            });
            // .populate('tags');
        } catch (err) {
            this.logger.error(ACTIONS.JOURNEY_SERVICE, err);
        }
    }

    async addJourney(journeyDTO: Journey): Promise<any> {
        let name: string = journeyDTO.name;
        let foundJourney: JourneyDocument = null;

        while (!foundJourney) {
            foundJourney = await this.journeyModel.findOne({ name });
            if (foundJourney) {
                name = incrementJourneyName(name);
                foundJourney = null;
            } else {
                break;
            }
        }

        const journeyData = Object.assign(journeyDTO, { name });
        const newJourney = new this.journeyModel(journeyData);
        await newJourney.save();

        return newJourney;
    }

    async updateJourney(journeyData: Journey, id: string): Promise<any> {
        const journey = await this.journeyModel.findByIdAndUpdate(
            { _id: id },
            journeyData,
            {
                new: true
            }
        );

        if (!journey) throw new BadRequestException('Cannot find journey');
        // this.resetJourney.addJob(id);
        return journey;
    }

    // tslint:disable-next-line: variable-name
    async deleteJourney(_id: string): Promise<any> {
        const journey = await this.journeyModel.findOneAndDelete(
            { _id },
            { select: '_id' }
        );

        if (!journey) throw new BadRequestException('Cannot find journey');
        return journey;
    }
}

// Helpers
const incrementJourneyName = (name) => {
    const parts = name.match(/_([1-9])*$/g);
    if (parts && !!parts[0]) {
        // tslint:disable-next-line: radix
        const code = parseInt(parts[0].split('_')[1]);
        return name.replace(parts[0], `_${code + 1}`);
    }
    return `${name}_1`;
};
