import * as Joi from 'joi';
import { VariableType } from '@shared/models/variable.entries';

export const VariableRequestValidation = Joi.object({
    type: Joi.string()
        .valid(...Object.values(VariableType))
        .required()
});
