import { Module } from '@nestjs/common';
import { SchemaModule } from '@shared/schema/schema.module';
import { UserModule } from '@shared/user/user.module';
import { JourneySchemaModule } from '../journey-schema/journey-schema.module';
import { SystemVariablesModule } from '../system-variables/system-variables.module';
import { VariableController } from './variable.controller';
import { VariableService } from './variable.service';

@Module({
    imports: [
        SchemaModule,
        UserModule,
        JourneySchemaModule,
        SystemVariablesModule
    ],
    controllers: [VariableController],
    providers: [VariableService]
})
export class VariableModule {}
