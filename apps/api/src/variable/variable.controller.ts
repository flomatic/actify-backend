import {
    BadRequestException,
    Controller,
    Get,
    Param,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { GetVariablesDTO, VariableType } from '@shared/models/variable.entries';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { VariableService } from './variable.service';
import { VariableRequestValidation } from './validator/variable.validator';

@ApiTags('Variables')
@UseGuards(AuthGuard('jwt'))
@Controller()
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class VariableController {
    constructor(private readonly variableService: VariableService) {}

    @Get('/api/v1/variable/:type')
    @ApiOperation({
        summary: 'Returns a list of all variables by type.'
    })
    async getVariables(@Param() params: GetVariablesDTO) {
        const { error } = VariableRequestValidation.validate(params);
        if (error) {
            throw new BadRequestException(error);
        }

        try {
            /* eslint-disable */
            switch (params.type) {
                case VariableType.USER_ATTRIBUTE:
                    return this.variableService.getUserAttributes();
                case VariableType.USER_DEVICE_ATTRIBUTE:
                    return this.variableService.getUserDeviceAttributes();
                case VariableType.USER_INFO:
                    return this.variableService.getUserInfoVariables();
                case VariableType.SYSTEM:
                    return this.variableService.getSystemVariables();
                default:
                    break;
            }
            /* eslint-enable */
        } catch (error) {
            throw new BadRequestException(error);
        }
    }

    @Get('/api/v1/variable/event/:event_name')
    @ApiOperation({
        summary: 'Returns a list of all event variables by event name.'
    })
    async getEventVariablesByName(@Param('event_name') event_name: string) {
        try {
            return await this.variableService.getEventVariablesByName(
                event_name
            );
        } catch (error) {
            throw new BadRequestException(error);
        }
    }
}
