import { Injectable } from '@nestjs/common';
import { SchemaService } from '@shared/schema/schema.service';
import * as _ from 'lodash';
import { JourneySchemaService } from '../journey-schema/journey-schema.service';
import { SystemVariablesService } from '../system-variables/system-variables.service';

@Injectable()
export class VariableService {
    constructor(
        private readonly schemaService: SchemaService,
        private readonly systemVariableService: SystemVariablesService,
        private readonly journeySchemaService: JourneySchemaService
    ) {}

    async getUserAttributes(): Promise<any> {
        const result = await this.schemaService.findSchemaByName(
            'user_attributes'
        );
        const data = this.formatAttributes(result);
        return this.formatResponse(data, true);
    }

    async getUserDeviceAttributes(): Promise<any> {
        const result = await this.schemaService.findSchemaByName(
            'user_device_attributes'
        );
        const data = this.formatAttributes(result);
        return this.formatResponse(data, true);
    }

    async getUserInfoVariables() {
        const result = await this.schemaService.findSchemaByName('user_info');
        const data = this.formatAttributes(result);
        return this.formatResponse(data, true);
    }

    async getSystemVariables(): Promise<any> {
        const result = await this.systemVariableService.getAllSystemVariables();
        const data = result.map((variable) => variable.variable) || [];
        return this.formatResponse(data, true);
    }

    async getEventVariables(): Promise<any> {
        const result = await this.journeySchemaService.getAllSchemas();
        const data = result.map((schema) => schema.event_name) || [];
        return this.formatResponse(data, false);
    }

    async getEventVariablesByName(event_name: string): Promise<any> {
        const result = await this.journeySchemaService.getEventVariablesByName(
            event_name
        );
        const data = this.formatAttributes(result);
        return this.formatResponse(data, true);
    }

    async getJourneyVariables(): Promise<any> {
        // TODO: Replace placeholder(s)!
        return this.formatResponse([], true);
    }

    formatAttributes(obj: unknown): string[] {
        const properties = _.get(obj, 'fields.properties', null);
        if (!properties) return [];
        return (
            Object.keys(properties).filter(
                (item) => item !== 'tab_session_id'
            ) || []
        );
    }

    formatResponse(data, finished: boolean) {
        return {
            finished,
            data
        };
    }
}
