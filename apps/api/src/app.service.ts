import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
    pingRoot() {
        return {
            works: true
        };
    }
}
