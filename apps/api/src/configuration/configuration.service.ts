import { Injectable } from '@nestjs/common';
import {
    VariableType,
    VariableTypeLabel
} from '@shared/models/variable.entries';
import { Organization } from '@shared/models/configuration.model';

@Injectable()
export class ConfigurationService {
    // constructor() {}

    async getCurrentConfig(): Promise<any> {
        const config = {};

        const categories = this.getVariableCategories();
        const organization = this.getOrganizationConfig();
        if (categories && categories.length) {
            Object.assign(config, { variables: { categories }, organization });
        }

        return config;
    }

    getVariableCategories() {
        return Object.entries(VariableType).map((item) => {
            return {
                type: item[1],
                label: VariableTypeLabel[item[0]] || ''
            };
        });
    }

    getOrganizationConfig(): Organization {
        // TODO: Fetch from the database and remove the hardcoded stuff
        return {
            name: {
                short: 'Alibaba',
                complete: 'Alibaba Travels Co.'
            },
            icon: {
                url:
                    'https://cdn.alibaba.ir/ostorage/frontend-cdn/actify/icons/alibaba.svg'
            }
        };
    }
}
