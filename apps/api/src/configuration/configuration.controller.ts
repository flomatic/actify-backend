import { Controller, Get, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { ConfigurationService } from './configuration.service';

@ApiTags('Configuration')
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@Controller()
@UseInterceptors(SentryInterceptor)
export class ConfigurationController {
    constructor(private readonly configurationService: ConfigurationService) {}

    @Get('/api/v1/configuration')
    @ApiOperation({
        summary: 'Returns the config for the current organization.'
    })
    async getAllSystemVariables() {
        return this.configurationService.getCurrentConfig();
    }
}
