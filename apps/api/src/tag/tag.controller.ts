import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    UseGuards,
    UseInterceptors,
    UsePipes
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiParam,
    ApiResponse,
    ApiTags
} from '@nestjs/swagger';
import { OperationByIdDTO } from '@entries/general/dto/general.dto';
import {
    AddTagDTO,
    AddTagResponseDTO,
    EditTagDTO
} from '@entries/tag/dto/tag.dto';
import { PipeJoiValidation } from '@shared/pipe/pipe.joi-validation';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { TagService } from './tag.service';
import {
    AddTagValidation,
    EditTagValidation
} from './validator/validation.tag';

@ApiTags('Tags')
@UseGuards(AuthGuard('jwt'))
@Controller()
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class TagController {
    constructor(private readonly tagService: TagService) {}

    @Get('/api/v1/tag')
    @ApiResponse({ type: [AddTagResponseDTO] })
    @ApiOperation({ summary: 'Returns a list of all available tags.' })
    async getAllTags() {
        return await this.tagService.getAllTags();
    }

    @Post('/api/v1/tag')
    @UsePipes(new PipeJoiValidation(AddTagValidation))
    @ApiResponse({ type: AddTagResponseDTO })
    @ApiOperation({ summary: 'Creates a tag' })
    async addTag(@Body() params: AddTagDTO) {
        return await this.tagService.addTag(params);
    }

    @Patch('/api/v1/tag/:id')
    @ApiResponse({ type: AddTagResponseDTO })
    @ApiParam({ name: 'id', required: true, type: 'string' })
    @ApiOperation({ summary: 'Edits a tag' })
    async editTag(
        @Param() { id }: OperationByIdDTO,
        @Body() params: EditTagDTO
    ) {
        const { error } = EditTagValidation.validate(params);
        if (error) {
            throw new BadRequestException(error);
        }
        return await this.tagService.editTag(id, params);
    }

    @Delete('/api/v1/tag/:id')
    @ApiResponse({ type: AddTagResponseDTO })
    @ApiOperation({ summary: 'Removes a tag' })
    async removeTag(@Param() { id }: OperationByIdDTO) {
        return await this.tagService.removeTag(id);
    }
}
