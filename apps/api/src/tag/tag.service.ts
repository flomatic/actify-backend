import {
    BadRequestException,
    Injectable,
    InternalServerErrorException
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
    AddTagDTO,
    AddTagResponseDTO,
    EditTagDTO
} from '@entries/tag/dto/tag.dto';
import { TagDocument } from '@entries/tag/model/tag.model';

@Injectable()
export class TagService {
    constructor(
        @InjectModel('Tag')
        private readonly tagModel: Model<TagDocument>
    ) {}

    async getAllTags(): Promise<any> {
        try {
            return await this.tagModel.find({}).lean();
        } catch (error) {
            throw new BadRequestException(error);
        }
    }

    async addTag(params: AddTagDTO): Promise<AddTagResponseDTO> {
        try {
            const tag = new this.tagModel(params);
            const { _id, name, description } = await tag.save();
            return { _id, name, description };
        } catch (e) {
            if (e.code === 11000) {
                throw new BadRequestException(
                    `Tag with name ${params.name} already exists`
                );
            } else {
                throw new InternalServerErrorException(
                    "can't/ write to the database"
                );
            }
        }
    }

    async editTag(id, params: EditTagDTO): Promise<AddTagResponseDTO> {
        try {
            const editedTag = await this.tagModel
                .findByIdAndUpdate(id, params, { new: true })
                .lean();
            if (!editedTag) {
                throw new BadRequestException('Cannot find tag');
            }
            return editedTag;
        } catch (error) {
            throw new BadRequestException(error);
        }
    }

    async removeTag(id): Promise<AddTagResponseDTO> {
        try {
            const removedTag = await this.tagModel.findByIdAndRemove(id).lean();
            if (!removedTag) {
                throw new BadRequestException('Cannot find tag');
            }
            return removedTag;
        } catch (error) {
            throw new BadRequestException(error);
        }
    }
}
