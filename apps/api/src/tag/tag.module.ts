import { Module } from '@nestjs/common';
import { TagModelModule } from '@entries/tag/model/tag.model';
import { TagController } from './tag.controller';
import { TagService } from './tag.service';

@Module({
    imports: [TagModelModule],
    providers: [TagService],
    controllers: [TagController],
    exports: []
})
export class TagModule {}
