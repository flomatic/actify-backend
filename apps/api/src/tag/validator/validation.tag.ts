import * as Joi from 'joi';

export const AddTagValidation = Joi.object({
    name: Joi.string().trim().lowercase().empty().max(150).required(),
    description: Joi.string().empty().max(4000).optional()
});

export const EditTagValidation = Joi.object({
    name: Joi.string().trim().lowercase().empty().max(150).optional(),
    description: Joi.string().empty().max(4000).optional()
});
