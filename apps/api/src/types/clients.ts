import { Document } from 'mongoose';

export interface Client extends Document {
    email: string;
    password: string;
}
