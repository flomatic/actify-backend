import { Controller, Get, UseInterceptors } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { RootPingResponseDTO } from '@entries/ping/dto/ping.dto';

@ApiTags('Ping')
@Controller()
@UseInterceptors(SentryInterceptor)
export class AppController {
    constructor(private readonly appService: AppService) {}

    @Get()
    @ApiOperation({
        summary: 'Pings the application and returns a status code of 200.'
    })
    @ApiResponse({ status: 200, type: RootPingResponseDTO })
    pingRoot() {
        return this.appService.pingRoot();
    }
}
