import {
    BadRequestException,
    Controller,
    Get,
    Query,
    UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    AnalyticsInterface,
    GetReportDto,
    ReportInterface
} from '@shared/models/analytic.model';
import { Types } from 'mongoose';
import { AnalyticsService } from './analytics.service';
import reportConvertModel from './convert-model/report.convert-model';
import { ReportService } from './report.service';
import { GetAnalyticValidation } from './validation/validation.get-analytic';

@ApiTags('Analytics')
@Controller()
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
export class ReportController {
    constructor(
        private readonly reportService: ReportService,
        private readonly analyticsService: AnalyticsService
    ) {}

    @Get('/api/v1/analytics/report')
    @ApiOperation({ summary: 'get report for analytics' })
    async getReport(@Query() getReportDto: GetReportDto) {
        if (typeof getReportDto.format === 'string') {
            getReportDto.format = [getReportDto.format];
        }
        const isValid = GetAnalyticValidation.validate(getReportDto);
        if (isValid.error) {
            throw new BadRequestException(isValid.error);
        }
        if (!Types.ObjectId.isValid(getReportDto.analytic_id)) {
            throw new BadRequestException('analytic_id is not valid');
        }
        const dbResponse: ReportInterface[] = await this.reportService.findQuery(
            {
                analytic_id: getReportDto.analytic_id,
                from_date: { $gte: getReportDto.from_date },
                to_date: { $lte: getReportDto.to_date },
                $or: getReportDto.format.map((format) => ({
                    time_period: format
                }))
            }
        );
        const analytic: AnalyticsInterface = await this.analyticsService.findById(
            getReportDto.analytic_id
        );
        const response = reportConvertModel(dbResponse, getReportDto, analytic);
        return response;
    }
}
