import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateAnalyticsDto } from '@shared/models/analytic.model';
import { OperationByIdDTO } from '@entries/general/dto/general.dto';
import { AnalyticsService } from './analytics.service';
import { isAnalyticLogicallyValid } from './validation/validation-logic-analytic';
import { AnalyticValidation } from './validation/validation.analytic';

@ApiTags('Analytics')
@Controller()
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
export class AnalyticsController {
    constructor(private readonly analyticsService: AnalyticsService) {}

    @Post('/api/v1/analytics')
    @ApiOperation({ summary: 'create an analytic graph' })
    async createAnalytic(@Body() createAnalytics: CreateAnalyticsDto) {
        const isValid = AnalyticValidation.validate(createAnalytics);
        if (isValid.error) {
            throw new BadRequestException(isValid.error);
        }
        const isLogicallyValid = isAnalyticLogicallyValid(createAnalytics);
        if (isLogicallyValid) {
            return await this.analyticsService.addAnalytic(createAnalytics);
        }
    }

    @Get('/api/v1/analytics/:id')
    @ApiOperation({ summary: 'get analytics' })
    async getJourneyAnalytics(@Param() operationByIdDto: OperationByIdDTO) {
        const id = operationByIdDto.id;
        if (!id || id === '{id}') {
            return await this.analyticsService.getAllAnalytics();
        } else {
            return await this.analyticsService.findById(id);
        }
    }

    @Delete('/api/v1/analytics/:id')
    @ApiOperation({ summary: 'delete analytic by id' })
    async deleteAnalyticById(@Param() operationByIdDto: OperationByIdDTO) {
        const id = operationByIdDto.id;
        if (!id || id === '{id}') {
            throw new BadRequestException('id is not provided');
        } else {
            return await this.analyticsService.deleteById(id);
        }
    }
}
