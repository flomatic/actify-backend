import * as mongoose from 'mongoose';

export class DeleteReportCommand {
    constructor(public analyticId: mongoose.Types.ObjectId) {}
}
