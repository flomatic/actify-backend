import {
    AnalyticsInterface,
    GetReportDto,
    ReportDto,
    ReportInterface
} from '@shared/models/analytic.model';

export default (
    reportInterface: ReportInterface[],
    request: GetReportDto,
    analytic: AnalyticsInterface
) => {
    const reportDto = {
        analytic_id: analytic._id,
        analytic_name: analytic.name,
        type: analytic.type,
        chart_type: analytic.chart_type,
        from_date: request.from_date,
        to_date: request.to_date,
        response: {}
    };
    request.format.forEach((f) => (reportDto.response[f] = []));
    reportInterface?.forEach((report) => {
        reportDto.response[report.time_period].push({
            data: report.data,
            from_date: report.from_date,
            to_date: report.to_date
        });
    });
    return reportDto as ReportDto;
};
