export function getFromToDateInHour(): {
    from_date: Date;
    to_date: Date;
    } {
    const date = new Date();
    const fromDate = new Date();
    fromDate.setHours(date.getHours(), 0, 0, 0);
    const toDate = new Date();
    toDate.setHours(date.getHours() + 1, 0, 0, 0);
    return { from_date: fromDate, to_date: toDate };
}

export function getFromToDateInDay(): {
    from_date: Date;
    to_date: Date;
    } {
    const fromDate = new Date();
    fromDate.setHours(0, 0, 0, 0);
    const toDate = new Date();
    toDate.setHours(0, 0, 0, 0);
    toDate.setDate(toDate.getDate() + 1);
    return { from_date: fromDate, to_date: toDate };
}

export function getFromToDateInMonth(): {
    from_date: Date;
    to_date: Date;
    } {
    const fromDate = new Date();
    fromDate.setHours(0, 0, 0, 0);
    const toDate = new Date();
    toDate.setHours(0, 0, 0, 0);
    toDate.setDate(toDate.getMonth() + 1);
    return { from_date: fromDate, to_date: toDate };
}

export function getFromToDateInYear(): {
    from_date: Date;
    to_date: Date;
    } {
    const date = new Date();
    const fromDate = new Date();
    fromDate.setHours(0, 0, 0, 0);
    const toDate = new Date();
    toDate.setHours(0, 0, 0, 0);
    toDate.setHours(date.getFullYear() + 1);
    return { from_date: fromDate, to_date: toDate };
}
