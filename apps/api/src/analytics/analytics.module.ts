import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import {
    AnalyticReportsModeModule,
    AnalyticsModeModule
} from '@shared/models/analytic.model';
import { UserEventModule } from './../user-event/user-event.module';
import { AnalyticsCommand } from './analytics.command';
import { AnalyticsController } from './analytics.controller';
import { AnalyticsService } from './analytics.service';
import { DeleteReportCommand } from './command/delete-report.command';
import { DeleteReportsHandler } from './handler/delete-res-analytics.handler';
import { ReportGeneratorCommand } from './report-generator.command';
import { ReportController } from './report.controller';
import { ReportService } from './report.service';

@Module({
    imports: [
        CqrsModule,
        UserEventModule,
        AnalyticsModeModule,
        AnalyticReportsModeModule
    ],
    controllers: [ReportController, AnalyticsController],
    providers: [
        AnalyticsService,
        ReportService,
        AnalyticsCommand,
        DeleteReportCommand,
        ReportGeneratorCommand,
        DeleteReportsHandler
    ],
    exports: [AnalyticsService, ReportService, AnalyticsCommand]
})
export class AnalyticsModule {}
