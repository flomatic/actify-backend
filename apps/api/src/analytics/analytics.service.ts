import {
    BadRequestException,
    Injectable,
    InternalServerErrorException,
    NotFoundException
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { InjectModel } from '@nestjs/mongoose';
import { AnalyticsInterface } from '@shared/models/analytic.model';
import { Model } from 'mongoose';
import { DeleteReportCommand } from './command/delete-report.command';

@Injectable()
export class AnalyticsService {
    analytics: AnalyticsInterface[];
    constructor(
        private readonly commandBus: CommandBus,
        @InjectModel('Analytics')
        private readonly analyticsModel: Model<AnalyticsInterface>
    ) {
        this.init();
    }

    async init() {
        this.analytics = await this.getAllAnalytics();
        setInterval(async () => {
            this.analytics = await this.getAllAnalytics();
        }, 3000);
    }

    async getAllAnalytics(): Promise<AnalyticsInterface[]> {
        return this.analyticsModel.find({}, { __v: 0 });
    }

    async getAllAnalyticsSync(): Promise<AnalyticsInterface[]> {
        if (!this.analytics) {
            this.analytics = await this.getAllAnalytics();
        }
        return this.analytics;
    }

    async insertAll(data: any[]) {
        await this.analyticsModel.insertMany(data);
    }

    async addAnalytic(createAnalytics: any) {
        try {
            const analytic = new this.analyticsModel(createAnalytics);
            await analytic.save();
            return analytic;
        } catch (e) {
            if (e.code === 11000) {
                throw new BadRequestException(
                    `analytic with name ${createAnalytics.name} already exists`
                );
            } else {
                throw new InternalServerErrorException(
                    "can't/ write to the database"
                );
            }
        }
    }

    async deleteById(id: string) {
        const deletedItem = await this.analyticsModel.findByIdAndDelete(id);
        if (!deletedItem) {
            throw new NotFoundException(`item with ${id} not found`);
        } else {
            this.commandBus.execute(new DeleteReportCommand(deletedItem._id));
            return deletedItem;
        }
    }

    async findById(id: string) {
        return await this.analyticsModel.findById(id);
    }
}
