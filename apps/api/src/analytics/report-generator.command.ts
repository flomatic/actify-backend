import { AnalyticsInterface } from '@shared/models/analytic.model';
import { Command, Console } from 'nestjs-console';
import { AnalyticsService } from './analytics.service';
import { ReportService } from './report.service';

@Console()
export class ReportGeneratorCommand {
    time: TimeClass;
    dataToBeInserted: any[] = [];
    analytics: AnalyticsInterface[];
    constructor(
        private readonly analyticsService: AnalyticsService,
        private readonly reportService: ReportService
    ) {
        this.time = new TimeClass();
    }

    @Command({
        command: 'build_dummy-report',
        description: 'create dummy report to test report module'
    })
    async create() {
        this.analytics = await this.analyticsService.getAllAnalyticsSync();
        let currentHour: { from_date: Date; to_date: Date };
        let currentDay: { from_date: Date; to_date: Date };
        let currentMonth: { from_date: Date; to_date: Date };
        let currentYear: { from_date: Date; to_date: Date };
        for (const analytic of this.analytics) {
            this.time.reset();
            do {
                currentHour = this.time.getNextHour();
                if (this.time.isTheFirstHourOfTheDay(currentHour.from_date)) {
                    currentDay = this.time.getCurrentDay();
                    if (this.time.isTheFirstDayOfMonth(currentHour.from_date)) {
                        currentMonth = this.time.getCurrentMonth();
                        if (
                            this.time.isTheFirstMonthOfTheYear(
                                currentHour.to_date
                            )
                        ) {
                            currentYear = this.time.getCurrentYear();
                            if (this.dataToBeInserted.length !== 0) {
                                await this.reportService.bulkInsert(
                                    this.dataToBeInserted
                                );
                            }
                            this.dataToBeInserted = [];
                        }
                    }
                }
                this.insertData(
                    analytic,
                    currentHour,
                    currentDay,
                    currentMonth,
                    currentYear
                );
            } while (this.time.isTheLastYear(currentHour.to_date));
        }
    }

    async insertData(
        analytic,
        currentHour,
        currentDay,
        currentMonth,
        currentYear
    ) {
        if (analytic.name === 'Domestic Flight Funnel') {
            this.insertDataToFunnel(
                analytic,
                currentHour,
                currentDay,
                currentMonth,
                currentYear
            );
        } else if (
            analytic.name === 'Searched For Hotel And Flight To Shiraz'
        ) {
            this.insertDataToFlow(
                analytic,
                currentHour,
                currentDay,
                currentMonth,
                currentYear
            );
        } else if (
            analytic.name === 'Most Searched Cities For Domestic Hotels'
        ) {
            this.insertDataToEventItem(
                analytic,
                currentHour,
                currentDay,
                currentMonth,
                currentYear
            );
        } else if (analytic.name === 'Most Searched Cities For Turkey Hotels') {
            this.insertDataToEventItem2(
                analytic,
                currentHour,
                currentDay,
                currentMonth,
                currentYear
            );
        } else if (analytic.name === 'Total Bus Seat Sold') {
            this.insertDataToEventSum(
                analytic,
                currentHour,
                currentDay,
                currentMonth,
                currentYear
            );
        }
    }

    getValueByPercent(percent, value) {
        return (Math.ceil(Math.random() * percent) * value) / 100;
    }

    insertDataToFunnel(
        analytic,
        currentHour,
        currentDay,
        currentMonth,
        currentYear
    ) {
        if (this.time.isTheFirstHourOfTheDay(currentHour.from_date)) {
            let data = {
                group: 'all',
                analytic_id: analytic._id,
                type: 'funnel',
                time_period: 'daily',
                chart_type: 'funnel_chart',
                data: [
                    {
                        name: 'Search Domestic Flight',
                        value: 0
                    },
                    {
                        name: 'Select Domestic Flight',
                        value: 0
                    },
                    {
                        name: 'Add To Card Domestic Flight',
                        value: 0
                    },
                    {
                        name: 'Payment Domestic Flight',
                        value: 0
                    }
                ],
                from_date: new Date(currentDay.from_date),
                to_date: new Date(currentDay.to_date)
            };
            this.dataToBeInserted.push(data);
            if (this.time.isTheFirstDayOfMonth(currentHour.from_date)) {
                data = {
                    group: 'all',
                    analytic_id: analytic._id,
                    type: 'funnel',
                    time_period: 'monthly',
                    chart_type: 'funnel_chart',
                    data: [
                        {
                            name: 'Search Domestic Flight',
                            value: 0
                        },
                        {
                            name: 'Select Domestic Flight',
                            value: 0
                        },
                        {
                            name: 'Add To Card Domestic Flight',
                            value: 0
                        },
                        {
                            name: 'Payment Domestic Flight',
                            value: 0
                        }
                    ],
                    from_date: new Date(currentMonth.from_date),
                    to_date: new Date(currentMonth.to_date)
                };
                this.dataToBeInserted.push(data);
                if (this.time.isTheFirstMonthOfTheYear(currentHour.from_date)) {
                    data = {
                        group: 'all',
                        analytic_id: analytic._id,
                        type: 'funnel',
                        time_period: 'yearly',
                        chart_type: 'funnel_chart',
                        data: [
                            {
                                name: 'Search Domestic Flight',
                                value: 0
                            },
                            {
                                name: 'Select Domestic Flight',
                                value: 0
                            },
                            {
                                name: 'Add To Card Domestic Flight',
                                value: 0
                            },
                            {
                                name: 'Payment Domestic Flight',
                                value: 0
                            }
                        ],
                        from_date: new Date(currentYear.from_date),
                        to_date: new Date(currentYear.to_date)
                    };
                    this.dataToBeInserted.push(data);
                }
            }
        }
        const data = {
            group: 'all',
            analytic_id: analytic._id,
            type: 'funnel',
            time_period: 'hourly',
            chart_type: 'funnel_chart',
            data: [
                {
                    name: 'Search Domestic Flight',
                    value: this.getValueByPercent(100, 1000)
                },
                {
                    name: 'Select Domestic Flight',
                    value: this.getValueByPercent(70, 1000)
                },
                {
                    name: 'Add To Card Domestic Flight',
                    value: this.getValueByPercent(30, 1000)
                },
                {
                    name: 'Payment Domestic Flight',
                    value: this.getValueByPercent(10, 1000)
                }
            ],
            from_date: new Date(currentHour.from_date),
            to_date: new Date(currentHour.to_date)
        };
        let index = -1;
        let insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'daily' &&
                d.from_date.toISOString() ===
                    currentDay.from_date.toISOString() &&
                d.to_date.toISOString() === currentDay.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            insertedAverage.data[1].value += data.data[1].value;
            insertedAverage.data[2].value += data.data[2].value;
            insertedAverage.data[3].value += data.data[3].value;
            this.dataToBeInserted[index] = insertedAverage;
        }
        index = -1;
        insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'monthly' &&
                d.from_date.toISOString() ===
                    currentMonth.from_date.toISOString() &&
                d.to_date.toISOString() === currentMonth.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            insertedAverage.data[1].value += data.data[1].value;
            insertedAverage.data[2].value += data.data[2].value;
            insertedAverage.data[3].value += data.data[3].value;
            this.dataToBeInserted[index] = insertedAverage;
        }
        index = -1;
        insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'yearly' &&
                d.from_date.toISOString() ===
                    currentYear.from_date.toISOString() &&
                d.to_date.toISOString() === currentYear.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            insertedAverage.data[1].value += data.data[1].value;
            insertedAverage.data[2].value += data.data[2].value;
            insertedAverage.data[3].value += data.data[3].value;
            this.dataToBeInserted[index] = insertedAverage;
        }
    }

    insertDataToFlow(
        analytic,
        currentHour,
        currentDay,
        currentMonth,
        currentYear
    ) {
        if (this.time.isTheFirstHourOfTheDay(currentHour.from_date)) {
            let data = {
                group: 'all',
                analytic_id: analytic._id,
                type: 'flow',
                time_period: 'daily',
                chart_type: 'linear_chart',
                data: [
                    {
                        name: 'sum of number',
                        value: 0
                    }
                ],
                from_date: new Date(currentDay.from_date),
                to_date: new Date(currentDay.to_date)
            };
            this.dataToBeInserted.push(data);

            if (this.time.isTheFirstDayOfMonth(currentDay.from_date)) {
                data = {
                    group: 'all',
                    analytic_id: analytic._id,
                    type: 'flow',
                    time_period: 'monthly',
                    chart_type: 'linear_chart',
                    data: [
                        {
                            name: 'sum of number',
                            value: 0
                        }
                    ],
                    from_date: new Date(currentMonth.from_date),
                    to_date: new Date(currentMonth.to_date)
                };
                this.dataToBeInserted.push(data);
                if (
                    this.time.isTheFirstMonthOfTheYear(currentMonth.from_date)
                ) {
                    data = {
                        group: 'all',
                        analytic_id: analytic._id,
                        type: 'flow',
                        time_period: 'yearly',
                        chart_type: 'linear_chart',
                        data: [
                            {
                                name: 'sum of number',
                                value: 0
                            }
                        ],
                        from_date: new Date(currentYear.from_date),
                        to_date: new Date(currentYear.to_date)
                    };
                    this.dataToBeInserted.push(data);
                }
            }
        }
        const data = {
            group: 'all',
            analytic_id: analytic._id,
            type: 'flow',
            time_period: 'hourly',
            chart_type: 'linear_chart',
            data: [
                {
                    name: 'sum of number',
                    value: this.getValueByPercent(100, 8000)
                }
            ],
            from_date: new Date(currentHour.from_date),
            to_date: new Date(currentHour.to_date)
        };
        this.dataToBeInserted.push(data);

        let index = -1;
        let insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'daily' &&
                d.from_date.toISOString() ===
                    currentDay.from_date.toISOString() &&
                d.to_date.toISOString() === currentDay.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            this.dataToBeInserted[index] = insertedAverage;
        }
        index = -1;
        insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'monthly' &&
                d.from_date.toISOString() ===
                    currentMonth.from_date.toISOString() &&
                d.to_date.toISOString() === currentMonth.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            this.dataToBeInserted[index] = insertedAverage;
        }
        index = -1;
        insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'yearly' &&
                d.from_date.toISOString() ===
                    currentYear.from_date.toISOString() &&
                d.to_date.toISOString() === currentYear.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            this.dataToBeInserted[index] = insertedAverage;
        }
    }

    insertDataToEventItem(
        analytic,
        currentHour,
        currentDay,
        currentMonth,
        currentYear
    ) {
        if (this.time.isTheFirstHourOfTheDay(currentHour.from_date)) {
            let data = {
                group: 'all',
                analytic_id: analytic._id,
                type: 'event',
                time_period: 'daily',
                chart_type: 'pie_chart',
                data: [
                    {
                        name: 'Tehran',
                        value: 0
                    },
                    {
                        name: 'Shiraz',
                        value: 0
                    },
                    {
                        name: 'Isfahan',
                        value: 0
                    },
                    {
                        name: 'Kashan',
                        value: 0
                    }
                ],
                from_date: new Date(currentDay.from_date),
                to_date: new Date(currentDay.to_date)
            };
            this.dataToBeInserted.push(data);

            if (this.time.isTheFirstDayOfMonth(currentDay.from_date)) {
                data = {
                    group: 'all',
                    analytic_id: analytic._id,
                    type: 'event',
                    time_period: 'monthly',
                    chart_type: 'pie_chart',
                    data: [
                        {
                            name: 'Tehran',
                            value: 0
                        },
                        {
                            name: 'Shiraz',
                            value: 0
                        },
                        {
                            name: 'Isfahan',
                            value: 0
                        },
                        {
                            name: 'Kashan',
                            value: 0
                        }
                    ],
                    from_date: new Date(currentMonth.from_date),
                    to_date: new Date(currentMonth.to_date)
                };
                this.dataToBeInserted.push(data);
                if (
                    this.time.isTheFirstMonthOfTheYear(currentMonth.from_date)
                ) {
                    data = {
                        group: 'all',
                        analytic_id: analytic._id,
                        type: 'event',
                        time_period: 'yearly',
                        chart_type: 'pie_chart',
                        data: [
                            {
                                name: 'Tehran',
                                value: 0
                            },
                            {
                                name: 'Shiraz',
                                value: 0
                            },
                            {
                                name: 'Isfahan',
                                value: 0
                            },
                            {
                                name: 'Kashan',
                                value: 0
                            }
                        ],
                        from_date: new Date(currentYear.from_date),
                        to_date: new Date(currentYear.to_date)
                    };
                    this.dataToBeInserted.push(data);
                }
            }
        }
        const data = {
            group: 'all',
            analytic_id: analytic._id,
            type: 'event',
            time_period: 'hourly',
            chart_type: 'pie_chart',
            data: [
                {
                    name: 'Tehran',
                    value: this.getValueByPercent(100, 500)
                },
                {
                    name: 'Shiraz',
                    value: this.getValueByPercent(100, 300)
                },
                {
                    name: 'Isfahan',
                    value: this.getValueByPercent(100, 200)
                },
                {
                    name: 'Kashan',
                    value: this.getValueByPercent(100, 100)
                }
            ],
            from_date: currentHour.from_date,
            to_date: currentHour.to_date
        };
        this.dataToBeInserted.push(data);

        let index = -1;
        let insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'daily' &&
                d.from_date.toISOString() ===
                    currentDay.from_date.toISOString() &&
                d.to_date.toISOString() === currentDay.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            insertedAverage.data[1].value += data.data[1].value;
            insertedAverage.data[2].value += data.data[2].value;
            insertedAverage.data[3].value += data.data[3].value;

            this.dataToBeInserted[index] = insertedAverage;
        }
        index = -1;
        insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'monthly' &&
                d.from_date.toISOString() ===
                    currentMonth.from_date.toISOString() &&
                d.to_date.toISOString() === currentMonth.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            insertedAverage.data[1].value += data.data[1].value;
            insertedAverage.data[2].value += data.data[2].value;
            insertedAverage.data[3].value += data.data[3].value;
            this.dataToBeInserted[index] = insertedAverage;
        }

        index = -1;
        insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'yearly' &&
                d.from_date.toISOString() ===
                    currentYear.from_date.toISOString() &&
                d.to_date.toISOString() === currentYear.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            insertedAverage.data[1].value += data.data[1].value;
            insertedAverage.data[2].value += data.data[2].value;
            insertedAverage.data[3].value += data.data[3].value;
            this.dataToBeInserted[index] = insertedAverage;
        }
    }

    insertDataToEventItem2(
        analytic,
        currentHour,
        currentDay,
        currentMonth,
        currentYear
    ) {
        if (this.time.isTheFirstHourOfTheDay(currentHour.from_date)) {
            let data = {
                group: 'all',
                analytic_id: analytic._id,
                type: 'event',
                time_period: 'daily',
                chart_type: 'pie_chart',
                data: [
                    {
                        name: 'Istanbul',
                        value: 0
                    },
                    {
                        name: 'Ankara',
                        value: 0
                    },
                    {
                        name: 'Izmir',
                        value: 0
                    },
                    {
                        name: 'Bodrum',
                        value: 0
                    }
                ],
                from_date: new Date(currentDay.from_date),
                to_date: new Date(currentDay.to_date)
            };
            this.dataToBeInserted.push(data);

            if (this.time.isTheFirstDayOfMonth(currentDay.from_date)) {
                data = {
                    group: 'all',
                    analytic_id: analytic._id,
                    type: 'event',
                    time_period: 'monthly',
                    chart_type: 'pie_chart',
                    data: [
                        {
                            name: 'Istanbul',
                            value: 0
                        },
                        {
                            name: 'Ankara',
                            value: 0
                        },
                        {
                            name: 'Izmir',
                            value: 0
                        },
                        {
                            name: 'Bodrum',
                            value: 0
                        }
                    ],
                    from_date: new Date(currentMonth.from_date),
                    to_date: new Date(currentMonth.to_date)
                };
                this.dataToBeInserted.push(data);
                if (
                    this.time.isTheFirstMonthOfTheYear(currentMonth.from_date)
                ) {
                    data = {
                        group: 'all',
                        analytic_id: analytic._id,
                        type: 'event',
                        time_period: 'yearly',
                        chart_type: 'pie_chart',
                        data: [
                            {
                                name: 'Istanbul',
                                value: 0
                            },
                            {
                                name: 'Ankara',
                                value: 0
                            },
                            {
                                name: 'Izmir',
                                value: 0
                            },
                            {
                                name: 'Bodrum',
                                value: 0
                            }
                        ],
                        from_date: new Date(currentYear.from_date),
                        to_date: new Date(currentYear.to_date)
                    };
                    this.dataToBeInserted.push(data);
                }
            }
        }
        const data = {
            group: 'all',
            analytic_id: analytic._id,
            type: 'event',
            time_period: 'hourly',
            chart_type: 'pie_chart',
            data: [
                {
                    name: 'Istanbul',
                    value: this.getValueByPercent(100, 200)
                },
                {
                    name: 'Ankara',
                    value: this.getValueByPercent(100, 100)
                },
                {
                    name: 'Izmir',
                    value: this.getValueByPercent(100, 80)
                },
                {
                    name: 'Bodrum',
                    value: this.getValueByPercent(100, 30)
                }
            ],
            from_date: currentHour.from_date,
            to_date: currentHour.to_date
        };
        this.dataToBeInserted.push(data);

        let index = -1;
        let insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'daily' &&
                d.from_date.toISOString() ===
                    currentDay.from_date.toISOString() &&
                d.to_date.toISOString() === currentDay.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            insertedAverage.data[1].value += data.data[1].value;
            insertedAverage.data[2].value += data.data[2].value;
            insertedAverage.data[3].value += data.data[3].value;

            this.dataToBeInserted[index] = insertedAverage;
        }
        index = -1;
        insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'monthly' &&
                d.from_date.toISOString() ===
                    currentMonth.from_date.toISOString() &&
                d.to_date.toISOString() === currentMonth.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            insertedAverage.data[1].value += data.data[1].value;
            insertedAverage.data[2].value += data.data[2].value;
            insertedAverage.data[3].value += data.data[3].value;
            this.dataToBeInserted[index] = insertedAverage;
        }

        index = -1;
        insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'yearly' &&
                d.from_date.toISOString() ===
                    currentYear.from_date.toISOString() &&
                d.to_date.toISOString() === currentYear.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            insertedAverage.data[1].value += data.data[1].value;
            insertedAverage.data[2].value += data.data[2].value;
            insertedAverage.data[3].value += data.data[3].value;
            this.dataToBeInserted[index] = insertedAverage;
        }
    }

    insertDataToEventSum(
        analytic,
        currentHour,
        currentDay,
        currentMonth,
        currentYear
    ) {
        if (this.time.isTheFirstHourOfTheDay(currentHour.from_date)) {
            let data = {
                group: 'all',
                analytic_id: analytic._id,
                type: 'event',
                time_period: 'daily',
                chart_type: 'linear_chart',
                data: [
                    {
                        name: 'number of seats',
                        value: 0
                    }
                ],
                from_date: new Date(currentDay.from_date),
                to_date: new Date(currentDay.to_date)
            };
            this.dataToBeInserted.push(data);

            if (this.time.isTheFirstDayOfMonth(currentDay.from_date)) {
                data = {
                    group: 'all',
                    analytic_id: analytic._id,
                    type: 'event',
                    time_period: 'monthly',
                    chart_type: 'linear_chart',
                    data: [
                        {
                            name: 'number of seats',
                            value: 0
                        }
                    ],
                    from_date: new Date(currentMonth.from_date),
                    to_date: new Date(currentMonth.to_date)
                };
                this.dataToBeInserted.push(data);
                if (
                    this.time.isTheFirstMonthOfTheYear(currentMonth.from_date)
                ) {
                    data = {
                        group: 'all',
                        analytic_id: analytic._id,
                        type: 'event',
                        time_period: 'yearly',
                        chart_type: 'linear_chart',
                        data: [
                            {
                                name: 'number of seats',
                                value: 0
                            }
                        ],
                        from_date: new Date(currentYear.from_date),
                        to_date: new Date(currentYear.to_date)
                    };
                    this.dataToBeInserted.push(data);
                }
            }
        }
        const data = {
            group: 'all',
            analytic_id: analytic._id,
            type: 'event',
            time_period: 'hourly',
            chart_type: 'linear_chart',
            data: [
                {
                    name: 'number of seats',
                    value: this.getValueByPercent(100, 50)
                }
            ],
            from_date: new Date(currentHour.from_date),
            to_date: new Date(currentHour.to_date)
        };
        this.dataToBeInserted.push(data);

        let index = -1;
        let insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'daily' &&
                d.from_date.toISOString() ===
                    currentDay.from_date.toISOString() &&
                d.to_date.toISOString() === currentDay.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            this.dataToBeInserted[index] = insertedAverage;
        }

        index = -1;
        insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'monthly' &&
                d.from_date.toISOString() ===
                    currentMonth.from_date.toISOString() &&
                d.to_date.toISOString() === currentMonth.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            this.dataToBeInserted[index] = insertedAverage;
        }
        index = -1;
        insertedAverage = this.dataToBeInserted.find((d, i) => {
            if (
                d.analytic_id === analytic._id &&
                d.time_period === 'yearly' &&
                d.from_date.toISOString() ===
                    currentYear.from_date.toISOString() &&
                d.to_date.toISOString() === currentYear.to_date.toISOString()
            ) {
                index = i;
                return d;
            }
        });
        if (index !== -1) {
            insertedAverage.data[0].value += data.data[0].value;
            this.dataToBeInserted[index] = insertedAverage;
        }
    }
}

class TimeClass {
    currentDate: Date = new Date('2018-01-01T00:00:00.000Z');

    getNextHour(): { from_date: Date; to_date: Date } {
        const from_date = new Date(this.currentDate);
        this.currentDate.setHours(this.currentDate.getHours() + 1, 0, 0, 0);
        const to_date = this.currentDate;
        return { from_date, to_date };
    }

    getCurrentDay() {
        const from_date = new Date(this.currentDate);
        const to_date = new Date(this.currentDate);
        from_date.setHours(0, 0, 0, 0);
        to_date.setDate(this.currentDate.getDate() + 1);
        to_date.setHours(0, 0, 0, 0);
        return { from_date, to_date };
    }

    getCurrentMonth(): { from_date: Date; to_date: Date } {
        const from_date = new Date(this.currentDate);
        const to_date = new Date(this.currentDate);
        from_date.setHours(0, 0, 0, 0);
        to_date.setHours(0, 0, 0, 0);
        to_date.setMonth(from_date.getMonth() + 1);
        return { from_date, to_date };
    }

    reset() {
        this.currentDate = new Date('2018-01-01T00:00:00.000Z');
    }

    getCurrentYear() {
        const from_date = new Date(this.currentDate);
        const to_date = new Date(this.currentDate);
        from_date.setHours(0, 0, 0, 0);
        to_date.setHours(0, 0, 0, 0);
        to_date.setFullYear(from_date.getFullYear() + 1);
        return { from_date, to_date };
    }

    isTheFirstHourOfTheDay(date: Date) {
        if (date.getHours() === 0) {
            return true;
        }
        return false;
    }

    isTheFirstDayOfMonth(date: Date) {
        if (date.getDate() === 1) {
            return true;
        }
        return false;
    }

    isTheFirstMonthOfTheYear(date: Date) {
        if (date.getMonth() === 0) {
            return true;
        }
        return false;
    }

    isTheLastYear(date: Date) {
        if (date.getFullYear() < 2021) {
            return true;
        }
        return false;
    }
}
