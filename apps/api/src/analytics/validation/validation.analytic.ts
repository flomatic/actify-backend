import * as Joi from 'joi';

const eventFilters = Joi.array().items(
    Joi.object({
        key: Joi.number(),
        operation: [
            Joi.object({
                variable: [Joi.string(), Joi.number()],
                value: [Joi.string(), Joi.number()],
                condition: Joi.string().when('variable', {
                    is: Joi.number().valid(true),
                    then: Joi.allow(
                        'equal',
                        'lower_or_equal',
                        'greater_or_equal'
                    ),
                    otherwise: Joi.allow('contains', 'equal')
                })
            }),
            Joi.object({
                operator: Joi.string().allow('and', 'or')
            })
        ]
    })
);

const resultTypes = Joi.object().allow(
    'number_of_event',
    'sum_event_values',
    'event_values'
);

const eventType = Joi.object({
    event_name: Joi.string(),
    event_filters: eventFilters,
    result_type: resultTypes,
    keys: Joi.array().items(
        Joi.object({
            key: Joi.string(),
            // if result type is sum_event_values, name must be mandatory
            name: Joi.string().optional()
        })
    )
});

const flowType = Joi.object({
    events: Joi.array().items({
        event_name: Joi.string(),
        event_filters: eventFilters
    })
});

const funnelType = Joi.object({
    events: Joi.array().items({
        name: Joi.string(),
        event_name: Joi.string(),
        event_filters: eventFilters
    })
});

export const AnalyticValidation = Joi.object({
    group: Joi.string().default('all').optional(),
    name: Joi.string().required(),
    type: Joi.string().allow('flow', 'event', 'funnel'),
    user_segmentation_id: Joi.string()
        .optional()
        .regex(/^[0-9a-fA-F]{24}$/),
    chart_type: Joi.string().allow(
        'linear_chart',
        'bar_item_chart',
        'bar_time_line_chart',
        'pie_chart',
        'funnel_chart'
    ),
    input: [eventType, flowType, funnelType],
    meta: Joi.object().optional()
});
