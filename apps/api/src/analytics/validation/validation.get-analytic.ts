import Joi = require('joi');

export const GetAnalyticValidation = Joi.object({
    from_date: Joi.date().iso(),
    to_date: Joi.date().iso(),
    analytic_id: Joi.string(),
    format: Joi.array()
        .min(1)
        .items(Joi.string().allow('hourly', 'daily', 'monthly', 'yearly'))
});
