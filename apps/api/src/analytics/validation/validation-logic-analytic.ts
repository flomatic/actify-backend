import { BadRequestException } from '@nestjs/common';
import {
    AnalyticsTypes,
    ChartType,
    CreateAnalyticsDto,
    EventInterface,
    EventResultTypes,
    FlowInterface,
    FunnelInterface
} from '@shared/models/analytic.model';

export const isAnalyticLogicallyValid = (
    createAnalytics: CreateAnalyticsDto
) => {
    const analyticsType = createAnalytics.type;
    const chartType = createAnalytics.chart_type;
    const input = createAnalytics.input;

    if (analyticsType === AnalyticsTypes.FUNNEL) {
        if (chartType !== ChartType.FUNNEL) {
            throw new BadRequestException(
                'funnel analytic only allow funnel_chart'
            );
        }
        if (!input.hasOwnProperty('events')) {
            throw new BadRequestException(
                'funnel analytic input is not correct'
            );
        }
        const hasNotNames = (input as FunnelInterface).events.some(
            (e) => !e.hasOwnProperty('name')
        );
        if (hasNotNames) {
            throw new BadRequestException(
                'funnel analytic input is not correct or funnel names not provided'
            );
        }
        return true;
    }

    if (analyticsType === AnalyticsTypes.FLOW) {
        if (![ChartType.BAR_TIME_LINE, ChartType.LINEAR].includes(chartType)) {
            throw new BadRequestException(
                'flow analytic only allow bar_time_line_chart and linear_chart'
            );
        }
        if (!input.hasOwnProperty('events')) {
            throw new BadRequestException('flow analytic input is not correct');
        }
        const hasNames = (input as FlowInterface).events.every((e) =>
            e.hasOwnProperty('name')
        );
        if (hasNames) {
            throw new BadRequestException('flow analytic input has no name');
        }
        return true;
    }

    if (analyticsType === AnalyticsTypes.EVENT) {
        if (ChartType.FUNNEL === chartType) {
            throw new BadRequestException(
                "event analytic doesn't support funnel_chart"
            );
        }
        if (input.hasOwnProperty('events')) {
            throw new BadRequestException(
                'event analytic input is not correct'
            );
        }
        const eventInterface = input as EventInterface;

        if (eventInterface.result_type === EventResultTypes.NUMBER_OF_EVENTS) {
            if (
                ![ChartType.LINEAR, ChartType.BAR_TIME_LINE].includes(chartType)
            ) {
                throw new BadRequestException(
                    'event analytic with number_of_events only allow time_line charts'
                );
            }
        } else {
            if ((eventInterface?.keys?.length || 0) === 0) {
                throw new BadRequestException(
                    'event analytic should have keys'
                );
            }
        }

        if (eventInterface.result_type === EventResultTypes.SUM_EVENT_VALUES) {
            const hasNotNames = eventInterface.keys.some(
                (e) => !e.hasOwnProperty('name')
            );
            if (hasNotNames) {
                throw new BadRequestException(
                    'sum_event_values result type should have name'
                );
            }
        }
        return true;
    }
};
