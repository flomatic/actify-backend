import { Command, Console } from 'nestjs-console';
import { AnalyticsService } from './analytics.service';

@Console()
export class AnalyticsCommand {
    constructor(private readonly analyticsService: AnalyticsService) {}

    @Command({
        command: 'build_sample_analytics',
        description: 'create sample analytics'
    })
    async create() {
        const analytics = [
            {
                name: 'Domestic Flight Funnel',
                type: 'funnel',
                chart_type: 'funnel_chart',
                input: {
                    events: [
                        {
                            name: 'Search Domestic Flight',
                            event_name: 'funnel_test_1'
                        },
                        {
                            name: 'Select Domestic Flight',
                            event_name: 'funnel_test_2'
                        },
                        {
                            name: 'Add To Card Domestic Flight',
                            event_name: 'funnel_test_3'
                        },
                        {
                            name: 'Payment Domestic Flight',
                            event_name: 'funnel_test_4'
                        }
                    ]
                }
            },
            {
                name: 'Searched For Hotel And Flight To Shiraz',
                type: 'flow',
                chart_type: 'linear_chart',
                input: {
                    events: [
                        {
                            event_name: 'flow_test_1',
                            event_filters: [
                                {
                                    key: 1,
                                    operation: {
                                        variable: 'flow_test_1.number',
                                        condition: 'equal',
                                        value: 100
                                    }
                                },
                                {
                                    key: 2,
                                    operation: {
                                        operator: 'and'
                                    }
                                },
                                {
                                    key: 3,
                                    operation: {
                                        variable: 'flow_test_1.string',
                                        condition: 'equal',
                                        value: '100'
                                    }
                                }
                            ]
                        },
                        {
                            event_name: 'flow_test_1',
                            event_filters: [
                                {
                                    key: 1,
                                    operation: {
                                        variable: 'flow_test_1.number',
                                        condition: 'equal',
                                        value: 100
                                    }
                                },
                                {
                                    key: 2,
                                    operation: {
                                        operator: 'or'
                                    }
                                },
                                {
                                    key: 3,
                                    operation: {
                                        variable: 'flow_test_1.string',
                                        condition: 'equal',
                                        value: '100'
                                    }
                                }
                            ]
                        }
                    ]
                }
            },
            {
                name: 'Most Searched Cities For Domestic Hotels',
                type: 'event',
                chart_type: 'pie_chart',
                input: {
                    event_name: 'event_item_step_1',
                    event_filters: [
                        {
                            key: 1,
                            operation: {
                                variable: 'event_item_step.number',
                                condition: 'equal',
                                value: 100
                            }
                        }
                    ],
                    result_type: 'event_values',
                    keys: [{ key: 'event_item_step.string' }]
                }
            },
            {
                name: 'Most Searched Cities For Turkey Hotels',
                type: 'event',
                chart_type: 'bar_item_chart',
                input: {
                    event_name: 'event_item_step_2',
                    event_filters: [
                        {
                            key: 1,
                            operation: {
                                variable: 'event_item_step_2.number',
                                condition: 'equal',
                                value: 100
                            }
                        }
                    ],
                    result_type: 'event_values',
                    keys: [{ key: 'event_item_step.string' }]
                }
            },

            {
                name: 'Total Bus Seat Sold',
                type: 'event',
                chart_type: 'linear_chart',
                input: {
                    event_name: 'event_sum_test',
                    name: 'event_sum_test',
                    event_filters: [
                        {
                            key: 1,
                            operation: {
                                variable: 'event_sum_test.number',
                                condition: 'equal',
                                value: 100
                            }
                        }
                    ],
                    result_type: 'sum_event_values',
                    keys: [
                        {
                            key: 'event_sum_test.number',
                            name: 'number of seats'
                        }
                    ]
                }
            }
        ];
        await this.analyticsService.insertAll(analytics);
    }
}
