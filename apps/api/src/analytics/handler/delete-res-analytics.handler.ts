import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { DeleteReportCommand } from './../command/delete-report.command';
import { ReportService } from './../report.service';

@CommandHandler(DeleteReportCommand)
export class DeleteReportsHandler
implements ICommandHandler<DeleteReportCommand> {
    constructor(private readonly reportService: ReportService) {}

    async execute(command: DeleteReportCommand) {
        const analyticId = command.analyticId;
        await this.reportService.deleteAllByAnalyticId(analyticId);
    }
}
