import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SchemaInterface } from '@shared/schema/interface/interface.Schema';
import { addedDiff } from 'deep-object-diff';
import { merge } from 'lodash';
import * as mongoose from 'mongoose';
import { Event } from '../../../main-processor/src/socket-parser/interfaces/event';
import toJsonSchema = require('to-json-schema');

@Injectable()
export class SchemaService {
    constructor(
        @InjectModel('Schema')
        private readonly schemaModel: mongoose.Model<SchemaInterface>
    ) {}

    async analyze(event: Event) {
        const eventName = event.event_name;
        let schema: any = await this.findSchemaByName(eventName);
        if (!schema) {
            // create schema based on this event;
            const schemaFromJson = this.getSchemaFromJson(event);
            schema = new this.schemaModel({
                event_name: eventName,
                fields: schemaFromJson
            });
            await new this.schemaModel(schema).save();
        } else {
            const newSchema = this.getSchemaFromJson(event);
            const oldSchema = schema.fields;
            const diff: any = addedDiff(oldSchema, newSchema);
            schema.fields = merge(oldSchema, diff);
            await this.schemaModel.updateOne(
                { event_name: schema.event_name },
                { $set: schema }
            );
        }
    }

    async findAll() {
        return this.schemaModel.find({});
    }

    async analyzeUserAttrs(attrs: Array<{ variable: string; value: string }>) {
        const eventName = 'user_attributes';
        const value = {};
        attrs.forEach((attr) => {
            value[attr.variable] = attr.value;
        });
        let schema: any = await this.findSchemaByName(eventName);
        if (!schema) {
            // create schema based on this event;
            const schemaFromJson = toJsonSchema(value);
            schema = new this.schemaModel({
                event_name: eventName,
                fields: schemaFromJson
            });
            await new this.schemaModel(schema).save();
        } else {
            const newSchema = toJsonSchema(value);
            const oldSchema = schema.fields;
            const diff: any = addedDiff(oldSchema, newSchema);
            schema.fields = merge(oldSchema, diff);
            await this.schemaModel.updateOne(
                { event_name: schema.event_name },
                { $set: schema }
            );
        }
    }

    async analyzeUserDevice(deviceInfo: {
        type: string;
        browser: any;
        browser_version: any;
        os: any;
        os_version: any;
        last_used_date: Date;
        firebase_token: string;
        device_id: string;
        brand: any;
        model: any;
        manufacturer: any;
        app_version: string;
    }) {
        const eventName = 'user_device_attributes';
        let schema: any = await this.findSchemaByName(eventName);
        if (!schema) {
            const schemaFromJson = toJsonSchema(deviceInfo);
            schema = new this.schemaModel({
                event_name: eventName,
                fields: schemaFromJson
            });
            await new this.schemaModel(schema).save();
        } else {
            const newSchema = toJsonSchema(deviceInfo);
            const oldSchema = schema.fields;
            const diff: any = addedDiff(oldSchema, newSchema);
            schema.fields = merge(oldSchema, diff);
            await this.schemaModel.updateOne(
                { event_name: schema.event_name },
                { $set: schema }
            );
        }
    }

    async findSchemaByName(name: string) {
        return this.schemaModel.findOne({ event_name: name });
    }

    async findSchemaById(id: mongoose.Types.ObjectId) {
        return await this.schemaModel.findById(id);
    }

    getSchemaFromJson(event: Event) {
        if (!event.body) return {};
        return toJsonSchema(event.body);
    }
}
