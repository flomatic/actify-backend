import { Controller, Get, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { SchemaService } from './schema.service';

@ApiTags('Schema')
@Controller()
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class SchemaController {
    constructor(private readonly schemaService: SchemaService) {}

    @Get('/api/v1/schema')
    @ApiOperation({
        summary: 'Return list of schemas'
    })
    async schemas() {
        return this.schemaService.findAll();
    }
}
