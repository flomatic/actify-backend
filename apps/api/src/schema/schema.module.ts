import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SchemaSchema } from '@shared/schema/schema/schema.schema';
import { SchemaController } from './schema.controller';
import { SchemaService } from './schema.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Schema', schema: SchemaSchema }])
    ],
    controllers: [SchemaController],
    providers: [SchemaService],
    exports: [SchemaService]
})
export class SchemaModule {}
