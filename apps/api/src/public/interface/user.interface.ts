import { UserAttr } from './user-attr.interface';

export interface UserInterface {
    user_unique_value: string;
    user_unique_key: string;
    name: string;
    email: string;
    phone_number: string;
    user_attributes: Array<UserAttr>;
    joined_date: Date;
}
