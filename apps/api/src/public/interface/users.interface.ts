import { UserInterface } from '@shared/models/user.model';

export interface UsersInterface {
    users: Array<UserInterface>;
}
