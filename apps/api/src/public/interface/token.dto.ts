import { ApiProperty } from '@nestjs/swagger';
export class GenerateTokensDTO {
    @ApiProperty({ description: 'Your organization name' })
    name: string;

    @ApiProperty({ description: 'Your organization API secret' })
    secret: string;
}

export class RefreshTokensDTO {
    @ApiProperty({ description: 'Your organization name' })
    access_token: string;

    @ApiProperty({ description: 'Your organization API secret' })
    refresh_token: string;
}
