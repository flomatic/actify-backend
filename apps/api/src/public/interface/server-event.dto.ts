import { ApiProperty } from '@nestjs/swagger';
import { MessageType } from '../../server-event/interface/interface.server-event';

export class ServerEventDTO {
    @ApiProperty({ description: 'Use unique Key', default: 'uun' })
    user_unique_key: string;
    @ApiProperty({ description: 'Use unique value', default: '770012699' })
    user_unique_value: string;
    @ApiProperty({
        description: 'Type of the event',
        default: 'event',
        enum: ['event']
    })
    type: MessageType;
    @ApiProperty({
        description: 'Name of the event',
        default: 'SWAGGER_TEST : Server Event'
    })
    event_name: string;
    @ApiProperty({
        description: 'Event body attributes',
        default: { testAttr: 'test', working: true, count: 12 }
    })
    body: any;
}
