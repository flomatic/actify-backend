import * as Joi from 'joi';

export const UserAttrValidation = Joi.object({
    variable: Joi.string().max(60).required(),
    value: Joi.any().required()
});
