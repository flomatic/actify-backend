import * as Joi from 'joi';

export const DeleteUserValidation = Joi.object({
    user_unique_key: Joi.string().required(),
    user_unique_value: Joi.string().required()
});
