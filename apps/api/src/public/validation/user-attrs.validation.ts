import * as Joi from 'joi';
import { UserAttrValidation } from './user-attr.validation';

export const UserAttrsValidation = Joi.object({
    user_unique_key: Joi.string().required(),
    user_unique_value: Joi.string().required(),
    user_attributes: Joi.array().items(UserAttrValidation).min(1)
});
