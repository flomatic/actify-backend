import * as Joi from 'joi';

export const GenerateTokenValidation = Joi.object({
    name: Joi.string().required(),
    secret: Joi.string().required()
});

export const RefreshTokenValidation = Joi.object({
    access_token: Joi.string().required(),
    refresh_token: Joi.string().required()
});
