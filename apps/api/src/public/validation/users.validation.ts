import * as Joi from 'joi';
import { UserValidation } from './user.validation';

export const UsersValidation = Joi.array().items(UserValidation).max(3000);
