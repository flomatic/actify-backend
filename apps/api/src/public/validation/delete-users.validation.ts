import * as Joi from 'joi';
import { DeleteUserValidation } from './delete-user.validation';

export const DeleteUsersValidation = Joi.array()
    .items(DeleteUserValidation)
    .min(1)
    .max(3000);
