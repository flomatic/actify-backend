import * as Joi from 'joi';
import { UserAttrValidation } from './user-attr.validation';

export const UserValidation = Joi.object({
    user_unique_key: Joi.string().required(),
    user_unique_value: Joi.string().required(),
    name: Joi.string().required(),
    email: Joi.string().required(),
    phone_number: Joi.string().required(),
    user_attributes: Joi.array().items(UserAttrValidation).default([])
});
