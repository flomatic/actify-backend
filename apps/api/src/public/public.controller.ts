import {
    BadGatewayException,
    BadRequestException,
    Body,
    Controller,
    Delete,
    Post,
    UseGuards,
    UseInterceptors,
    UsePipes
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { PipeJoiValidation } from '@shared/pipe/pipe.joi-validation';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { AuthService } from '../auth/auth.service';
import { OrganizationService } from '../organization/organization.service';
import {
    MessageType,
    ServerEvent
} from '../server-event/interface/interface.server-event';
import { ServerEventService } from '../server-event/server-event.service';
import { ServerEventValidation } from '../server-event/validator/validator.server-event';
import { ServerEventDTO } from './interface/server-event.dto';
import { GenerateTokensDTO, RefreshTokensDTO } from './interface/token.dto';
import { UserInterface } from './interface/user.interface';
import { DeleteUserValidation } from './validation/delete-user.validation';
import { DeleteUsersValidation } from './validation/delete-users.validation';
import {
    GenerateTokenValidation,
    RefreshTokenValidation
} from './validation/tokens.validation';
import { UserAttrsValidation } from './validation/user-attrs.validation';
import { UserValidation } from './validation/user.validation';
import { UsersValidation } from './validation/users.validation';

@ApiTags('Public')
@UseInterceptors(SentryInterceptor)
@Controller()
export class PublicController {
    constructor(
        private readonly serverEventService: ServerEventService,
        private readonly organizationService: OrganizationService,
        private readonly authService: AuthService
    ) {}

    @Post('/api/v1/public/event')
    @UseGuards(AuthGuard('jwt_organization'))
    @ApiBearerAuth('public_access_token')
    @ApiOperation({
        summary: 'Sends a server event to the processor'
    })
    async sendEvent(@Body() event: ServerEventDTO) {
        const { error } = ServerEventValidation.validate(event);
        if (error) throw new BadRequestException(error);

        try {
            const date = new Date();
            const status = await this.serverEventService.sendEvent({
                ...event,
                date
            });
            // TODO: Format response
            return {
                status: Boolean(status) ? 'success' : 'failed',
                timestamp: Date.now()
            };
        } catch (error) {
            throw new BadGatewayException(error);
        }
    }

    @Post('/api/v1/public/user/create')
    @UseGuards(AuthGuard('jwt_organization'))
    @ApiBearerAuth('public_access_token')
    @ApiOperation({ summary: 'Creates a user in Actify' })
    async createUser(@Body() body: any) {
        const { error } = UserValidation.validate(body);
        if (error) throw new BadRequestException(error);
        const user = body as UserInterface;
        const event: ServerEvent = {
            type: MessageType.CREATE_USER,
            user_unique_key: user.user_unique_key,
            user_unique_value: user.user_unique_value,
            event_name: 'user',
            body: user,
            date: new Date()
        };
        const status = await this.serverEventService.sendEvent(event);
        return {
            status: Boolean(status) ? 'success' : 'failed',
            timestamp: Date.now()
        };
    }

    @Post('/api/v1/public/user/bulk-insert')
    @UseGuards(AuthGuard('jwt_organization'))
    @ApiBearerAuth('public_access_token')
    @ApiOperation({ summary: 'Creates a user in Actify' })
    async bulkInsert(@Body() body: any) {
        const { error } = UsersValidation.validate(body);
        if (error) throw new BadRequestException(error);
        const users = body as Array<UserInterface>;
        users.forEach((user) => {
            const event: ServerEvent = {
                type: MessageType.CREATE_USER,
                user_unique_key: user.user_unique_key,
                user_unique_value: user.user_unique_value,
                event_name: 'user',
                body: user,
                date: new Date()
            };
            this.serverEventService.sendEvent(event);
        });
        return {
            status: 'success',
            timestamp: Date.now()
        };
    }

    @Post('/api/v1/public/user/user-attributes')
    @UseGuards(AuthGuard('jwt_organization'))
    @ApiBearerAuth('public_access_token')
    @ApiOperation({ summary: 'Update user attributes' })
    async update(@Body() body: any) {
        const { error } = UserAttrsValidation.validate(body);
        if (error) throw new BadRequestException(error);
        const event: ServerEvent = {
            type: MessageType.UPDATE_USER_ATTRS,
            user_unique_key: body.user_unique_key,
            user_unique_value: body.user_unique_value,
            event_name: 'user',
            body: body,
            date: new Date()
        };
        this.serverEventService.sendEvent(event);
        return {
            status: 'success',
            timestamp: Date.now()
        };
    }

    @Delete('/api/v1/public/user/bulk-delete')
    @UseGuards(AuthGuard('jwt_organization'))
    @ApiBearerAuth('public_access_token')
    @ApiOperation({ summary: 'Update user attributes' })
    async bulkDelete(@Body() body: any) {
        const { error } = DeleteUsersValidation.validate(body);
        if (error) throw new BadRequestException(error);
        const users = body as any[];
        users.forEach((user) => {
            const event: ServerEvent = {
                type: MessageType.DELETE_USER,
                user_unique_key: user.user_unique_key,
                user_unique_value: user.user_unique_value,
                event_name: 'user',
                body: {},
                date: new Date()
            };
            this.serverEventService.sendEvent(event);
        });
        return {
            status: 'success',
            timestamp: Date.now()
        };
    }

    @Delete('/api/v1/public/user')
    @UseGuards(AuthGuard('jwt_organization'))
    @ApiBearerAuth('public_access_token')
    @ApiOperation({ summary: 'Deletes a user' })
    async delete(@Body() body: any) {
        const { error } = DeleteUserValidation.validate(body);
        if (error) throw new BadRequestException(error);
        const event: ServerEvent = {
            type: MessageType.DELETE_USER,
            user_unique_key: body.user_unique_key,
            user_unique_value: body.user_unique_value,
            event_name: 'user',
            body: {},
            date: new Date()
        };
        const status = await this.serverEventService.sendEvent(event);
        return {
            status: Boolean(status) ? 'success' : 'failed',
            timestamp: Date.now()
        };
    }

    @Post('/api/v1/public/tokens/generate')
    @UsePipes(new PipeJoiValidation(GenerateTokenValidation))
    @ApiOperation({ summary: 'Generate a pair of tokens' })
    async generateTokens(@Body() params: GenerateTokensDTO) {
        return this.organizationService.generateTokens(params);
    }

    @Post('/api/v1/public/tokens/refresh')
    @UsePipes(new PipeJoiValidation(RefreshTokenValidation))
    @ApiOperation({ summary: 'Refreshes a pair of tokens' })
    async refreshTokens(@Body() params: RefreshTokensDTO) {
        return this.organizationService.refreshTokens(params);
    }
}
