import { Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { OrganizationModule } from '../organization/organization.module';
import { ServerEventModule } from '../server-event/server-event.module';
import { PublicController } from './public.controller';

@Module({
    imports: [ServerEventModule, OrganizationModule, AuthModule],
    controllers: [PublicController],
    providers: []
})
export class PublicModule {}
