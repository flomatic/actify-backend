import {
    BadRequestException,
    Controller,
    Get,
    Param,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { GetLocationByCountryDTO } from './location.dto';
import { LocationService } from './location.service';
import { CountryNameValidation } from './validation/location.validation';

@ApiTags('Location')
@UseGuards(AuthGuard('jwt'))
@Controller()
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class LocationController {
    constructor(private readonly locationService: LocationService) {}

    @Get('/api/v1/location/countries')
    @ApiOperation({ summary: 'Returns a list of all countries' })
    async getCountries() {
        return this.locationService.getCountries();
    }

    @Get('/api/v1/location/:country')
    @ApiOperation({
        summary: 'Returns a list of cities and timezones for a country'
    })
    async getCities(@Param() locationCityDto: GetLocationByCountryDTO) {
        const { error } = CountryNameValidation.validate(locationCityDto);
        if (error) {
            throw new BadRequestException(error);
        }

        return this.locationService.getCities(locationCityDto.country);
    }
}
