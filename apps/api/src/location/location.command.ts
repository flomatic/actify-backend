import { Command, Console } from 'nestjs-console';
import { LocationService } from './location.service';

@Console()
export class LocationCommand {
    constructor(private readonly locationService: LocationService) {}

    @Command({
        command: 'build_sample_locations',
        description: 'Creates sample location data'
    })
    async create() {
        const sampleLocations = [
            {
                country: 'iran',
                cities: [
                    {
                        city: 'tehran',
                        time_zone: '+4:30'
                    },
                    {
                        city: 'mashhad',
                        time_zone: '+4:30'
                    },
                    {
                        city: 'isfahan',
                        time_zone: '+4:30'
                    },
                    {
                        city: 'yazd',
                        time_zone: '+4:30'
                    },
                    {
                        city: 'shiraz',
                        time_zone: '+4:30'
                    }
                ]
            },
            {
                country: 'united states',
                cities: [
                    {
                        city: 'new york',
                        time_zone: '-4:00'
                    },
                    {
                        city: 'washington',
                        time_zone: '-7:00'
                    }
                ]
            }
        ];

        await this.locationService.addSampleLocationData(sampleLocations);
    }
}
