import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LocationCommand } from './location.command';
import { LocationController } from './location.controller';
import { LocationService } from './location.service';
import { LocationSchema } from './schema/location.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'location', schema: LocationSchema }
        ])
    ],
    controllers: [LocationController],
    providers: [LocationService, LocationCommand],
    exports: [LocationService, LocationCommand]
})
export class LocationModule {}
