import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { LocationInterface } from './interface/location.interface';

@Injectable()
export class LocationService {
    constructor(
        @InjectModel('location')
        private readonly locationModel: Model<LocationInterface>
    ) {}

    async getCountries() {
        const countries = await this.locationModel.find(
            {},
            { country: 1, _id: 0 }
        );
        return countries.map((item) => item.country);
    }

    async getCities(country) {
        const foundCountry = await this.locationModel.findOne(
            { country },
            { _id: 0 }
        );
        if (!foundCountry) {
            throw new BadRequestException('Country could not be found');
        }
        return foundCountry.cities || [];
    }

    async addSampleLocationData(locations: any[]) {
        return await this.locationModel.insertMany(locations);
    }
}
