import { Schema } from 'mongoose';

const citySchema = new Schema(
    {
        city: { type: String },
        time_zone: { type: String }
    },
    { _id: false }
);

export const LocationSchema = new Schema({
    country: { type: String, index: true, unique: true },
    cities: [citySchema]
});
