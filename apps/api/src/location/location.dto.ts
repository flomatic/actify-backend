import { ApiProperty } from '@nestjs/swagger';

export class GetLocationByCountryDTO {
    @ApiProperty({ type: 'string' })
    country: string;
}
