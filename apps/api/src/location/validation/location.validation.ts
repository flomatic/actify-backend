import * as Joi from 'joi';

export const CountryNameValidation = Joi.object({
    country: Joi.string().max(128).min(4).required()
});
