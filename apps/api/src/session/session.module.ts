import { Module } from '@nestjs/common';
import { SessionModelModule } from '@shared/models/session.model';
import { SessionService } from './session.service';

@Module({
    imports: [SessionModelModule],
    providers: [SessionService],
    exports: [SessionService]
})
export class SessionModule {}
