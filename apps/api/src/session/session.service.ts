import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SessionInterface } from '@shared/models/session.model';
import { uniq } from 'lodash';
import { Model } from 'mongoose';

@Injectable()
export class SessionService {
    constructor(
        @InjectModel('Session')
        private readonly sessionModel: Model<SessionInterface>
    ) {}

    async findUserStatus({
        user_unique_key,
        user_unique_value
    }): Promise<string[]> {
        // Get all online sessions
        const onlineSessions = await this.sessionModel
            .find({ user_unique_key, user_unique_value, is_online: true })
            .lean();

        // If there's no online session, return an empty array, indicating an offline status
        if (!onlineSessions || !onlineSessions.length) {
            return [];
        }

        // Make a unique array of this user's online sessions and then channels
        const channels = uniq(
            onlineSessions.map((item) => item.channel.toLowerCase())
        );

        return channels;
    }
}
