import {
    Controller,
    Get,
    Param,
    ParseIntPipe,
    Query,
    UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiParam,
    ApiTags
} from '@nestjs/swagger';
import { ReplayService } from './replay.service';
@ApiTags('Session Replay')
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@Controller()
export class ReplayController {
    constructor(private readonly replayService: ReplayService) {}

    @Get('/api/v1/replay/sessions')
    @ApiOperation({ summary: 'Returns all session replays.' })
    getSessions(
        @Query('limit', ParseIntPipe) limit: number,
        @Query('offset', ParseIntPipe) offset: number
    ) {
        return this.replayService.findSessions(limit, offset);
    }

    @Get('/api/v1/replay/sessions/:id')
    @ApiParam({ name: 'id', required: true, type: 'string' })
    @ApiOperation({ summary: 'Returns a session replay by id.' })
    getSession(@Param('id') id: string) {
        return this.replayService.findSession(id);
    }
}
