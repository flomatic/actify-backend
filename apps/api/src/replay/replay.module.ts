import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ReplayController } from './replay.controller';
import { ReplayService } from './replay.service';
import { ReplaySchema } from './schema/replay.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Replay', schema: ReplaySchema }])
    ],
    providers: [ReplayService],
    controllers: [ReplayController],
    exports: [ReplayService]
})
export class ReplayModule {}
