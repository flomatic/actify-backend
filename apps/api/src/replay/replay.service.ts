import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ReplayInterface } from './schema/replay.schema';

interface ReplayEventType {
    tab_session_id: string;
    user_agent: string;
    events: any[];
    url: string;
    context: { [key: string]: any };
}

@Injectable()
export class ReplayService {
    constructor(
        @InjectModel('Replay')
        private readonly replayModel: Model<ReplayInterface>
    ) {}

    async findSessions(limit = 30, offset = 0) {
        return this.replayModel
            .find({})
            .populate('session_id')
            .skip(offset)
            .limit(limit)
            .exec();
    }

    async findSession(id) {
        return this.replayModel.findById(id).select('+events').exec();
    }
}
