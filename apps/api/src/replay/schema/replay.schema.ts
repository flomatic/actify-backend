import * as mongoose from 'mongoose';
import { Document } from 'mongoose';

export interface ReplayInterface extends Document {
    _id: string;
    session_id: string;
    events: any[];
    url: string;
    context: { [key: string]: any };
}

export const ReplaySchema = new mongoose.Schema({
    _id: { type: String },
    session_id: { type: String, index: true, ref: 'Session' },
    events: {
        type: Array,
        select: false
    },
    url: { type: String },
    context: {
        type: mongoose.Schema.Types.Mixed,
        default: () => ({})
    }
});
