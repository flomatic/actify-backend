import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    Query,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { OperationByIdDTO } from '@entries/general/dto/general.dto';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { IdValidation } from '@shared/validators/validation.id';
import { Segment } from '@entries/segment/model/segment.model';
import { SegmentPaginationParamsDTO } from '@entries/segment/dto/segment-pagination.dto';
import { UpdateSegmentCount } from 'apps/main-processor/src/backend-job/update-segment-count';
import { SegmentService } from '@shared/segment/segment.service';
import { SegmentQueryHelper } from '@shared/segment/segment-query-helper';
import { SegmentValidation } from './validation/segment.validation';
import { SegmentUsersPaginationDTO } from '@entries/segment/dto/segment-users-pagination.dto';

@ApiTags('Segment')
@Controller()
@ApiBearerAuth('access_token')
@UseGuards(AuthGuard('jwt'))
@UseInterceptors(SentryInterceptor)
export class SegmentController {
    constructor(
        private readonly segmentService: SegmentService,
        private readonly userSegmentationCalculator: SegmentQueryHelper,
        private readonly updateSegmentCount: UpdateSegmentCount
    ) {}

    @Get('/api/v1/segment')
    // TODO: Add swagger response
    @ApiOperation({ summary: 'Returns  all user segmentations' })
    async getAllUserSegmentations(
        @Query() paginationParams: SegmentPaginationParamsDTO
    ) {
        return await this.segmentService.getAllSegmentations(paginationParams);
    }

    @Get('/api/v1/segment/:id')
    // TODO: Add swagger response
    @ApiOperation({ summary: 'Returns a user segmentation by id' })
    async getUserSegmentation(@Param() { id }: OperationByIdDTO) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }
        return await this.segmentService.getASegmentation(id);
    }

    @Get('/api/v1/segment/count/:id')
    // TODO: Add swagger response
    @ApiOperation({ summary: 'Returns count information for a segment by id' })
    async getSegmentCount(@Param() { id }: OperationByIdDTO) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }
        return await this.segmentService.getSegmentCount(id);
    }

    @Post('/api/v1/segment')
    // TODO: Add swagger response
    @ApiOperation({ summary: 'Creates a user segmentation' })
    async addUserSegmentation(@Body() addUserSegmentationDTO: Segment) {
        const { error } = SegmentValidation.validate(addUserSegmentationDTO);
        if (error) {
            throw new BadRequestException(error);
        }

        const result = await this.segmentService.addSegmentation(
            addUserSegmentationDTO
        );
        this.updateSegmentCount.updateSegmentCountById(result._id);
        return result;
    }

    @Put('/api/v1/segment/:id')
    // TODO: Add swagger response
    @ApiOperation({ summary: 'Updates a user segmentation' })
    async editUserSegmentation(
        @Param() { id }: OperationByIdDTO,
        @Body() editUserSegmentationDTO: Segment
    ) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        const { error } = SegmentValidation.validate(editUserSegmentationDTO);
        if (error) {
            throw new BadRequestException(error);
        }
        const result = await this.segmentService.editUserSegmentation(
            editUserSegmentationDTO,
            id
        );
        this.updateSegmentCount.updateSegmentCountById(id);
        return result;
    }

    @Delete('/api/v1/segment/:id')
    // TODO: Add swagger response
    @ApiOperation({ summary: 'Deletes a user segmentation' })
    async deleteUserSegmentation(@Param() { id }: OperationByIdDTO) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }
        return await this.segmentService.deleteUserSegmentation(id);
    }

    @Get('/api/v1/segment/:id/users')
    // TODO: Add swagger response
    @ApiOperation({
        summary: 'Returns a list of current users in this campaign'
    })
    async getAllSegmentUsersPaginated(
        @Param() { id }: OperationByIdDTO,
        @Query() paginationParams: SegmentUsersPaginationDTO
    ) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }
        const segmentUsers = await this.segmentService.getAllSegmentUsersPaginated(
            id,
            paginationParams
        );
        return segmentUsers;
    }
}
