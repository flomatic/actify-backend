import { Module } from '@nestjs/common';
import { SegmentModule } from '@shared/segment/segment.module';
import { BackendJobModule } from 'apps/main-processor/src/backend-job/backend-job.module';
import { SegmentController } from './segment.controller';

@Module({
    imports: [SegmentModule, BackendJobModule],
    controllers: [SegmentController],
    providers: [],
    exports: []
})
export class SegmentApiModule {}
