import { Condition } from '@entries/operation/enum/condition';
import { OperationType } from '@entries/operation/enum/operation-type';
import * as Joi from 'joi';

const Members = Joi.object({
    type: Joi.string().allow('all', 'custom').required(),
    users: Joi.array().items(
        Joi.object({
            key: Joi.string().required(),
            value: Joi.string().required()
        })
    )
});

const DateType = Joi.object({
    from: Joi.string()
        .valid('now', 'iso_date_string', 'year', 'month', 'day')
        .required(),
    from_value: Joi.any().optional(),
    to: Joi.string()
        .valid('now', 'iso_date_string', 'year', 'month', 'day')
        .required(),
    to_value: Joi.any().optional()
});

const CriteriaOperation = Joi.object({
    type: Joi.string().valid(
        'activity_level',
        'last_visited',
        'channel',
        'location',
        'joined_date'
    ),
    value: Joi.required()
        .when('type', {
            is: 'activity_level',
            then: Joi.string().valid('high', 'medium', 'low')
        })
        .when('type', { is: 'last_visited', then: DateType })
        .when('type', {
            is: 'channel',
            then: Joi.string().valid('android', 'ios', 'browser')
        })
        .when('type', { is: 'joined_date', then: DateType })
});

const CriteriaOperator = Joi.object({
    operator: Joi.string().allow('and', 'or')
});

const Criteria = Joi.object({
    key: Joi.number().required(),
    operation: [CriteriaOperation, CriteriaOperator]
});

const Filter = Joi.object({
    key: Joi.number(),
    operation: [
        Joi.object({
            variable: Joi.string(),
            value: Joi.any(),
            type: Joi.string().allow(...Object.values(OperationType)),
            condition: Joi.string().allow(...Object.values(Condition))
        }),
        Joi.object({
            operator: Joi.string().allow('and', 'or')
        })
    ]
});

// const VariableValue = Joi.object({
//     variable: Joi.string(),
//     value: Joi.any()
// });

export const SegmentValidation = Joi.object({
    name: Joi.string().max(32).min(4).required(),
    description: Joi.string().allow('').max(5000).optional(),
    category: Joi.string().valid('dynamic', 'static').required(),
    members: Members,
    criteria: Joi.array().items(Criteria),
    user_attrs: Joi.array().items(Filter),
    device_attrs: Joi.array().items(Filter),
    events: Joi.array().items(
        Joi.object({
            event_name: Joi.string().required(),
            filters: Joi.array().items(Filter),
            number_of_happening: Joi.number(),
            operator: Joi.string().allow('and', 'or').required(),
            date: DateType
        })
    ),
    user_count: Joi.object().optional()
});
