import { Module } from '@nestjs/common';
import { EnumModelModule } from '@shared/models/enum.model';
import { JourneyEnumController } from './journey-enum.controller';
import { JourneyEnumService } from './journey-enum.service';

@Module({
    imports: [EnumModelModule],
    controllers: [JourneyEnumController],
    providers: [JourneyEnumService],
    exports: [JourneyEnumService]
})
export class JourneyEnumModule {}
