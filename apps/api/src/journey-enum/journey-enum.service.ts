import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { EnumInterface } from '@shared/models/enum.model';
import { Model } from 'mongoose';

@Injectable()
export class JourneyEnumService {
    constructor(
        @InjectModel('Enum')
        private readonly journeyEnumModel: Model<EnumInterface>,
        private readonly logger: BLogger
    ) {}

    async dbResponse(eventName: string): Promise<any[]> {
        try {
            const res = await this.journeyEnumModel.find(
                { name: new RegExp('^' + eventName, 'i') },
                {
                    __v: 0,
                    _id: 0
                }
            );

            // TODO: Taking out tabSessionId. This must be considered later.
            const noTabId = res.filter(
                (item) => !item.name.includes('tabSessionId')
            );

            return noTabId;
        } catch (err) {
            this.logger.error(ACTIONS.JOURNEY_ENUM_SERVICE, err);
        }
    }

    async getEnumValues(eventName: string, enumName: string): Promise<any[]> {
        const res = await this.dbResponse(eventName);

        const items = res.filter((item) => {
            return item.name.split(`${eventName}.`)[1] === enumName;
        });

        return items;
    }
}
