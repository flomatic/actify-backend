import {
    Controller,
    Get,
    Param,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { EnumValuesDTO } from '@shared/models/enum.model';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { JourneyEnumService } from './journey-enum.service';

@ApiTags('Journey Enums')
@UseGuards(AuthGuard('jwt'))
@Controller()
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class JourneyEnumController {
    constructor(private readonly journeyEnumService: JourneyEnumService) {}

    @Get('/api/v1/journey-enum/:eventName/:enumName')
    @ApiOperation({
        summary: 'Returns enum values for a given event by name.'
    })
    async getEnumValues(@Param() enumValuesDTO: EnumValuesDTO) {
        return this.journeyEnumService.getEnumValues(
            enumValuesDTO.eventName,
            enumValuesDTO.enumName
        );
    }
}
