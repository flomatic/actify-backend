import { Module } from '@nestjs/common';
import { CampaignModelModule } from '@shared/models/campaign.model';
import { PushNotificationModule } from '@shared/push-notification/push-notification.module';
import { CampaignCommand } from './campaign.command';
import { CampaignController } from './campaign.controller';
import { CampaignService } from './campaign.service';

@Module({
    imports: [CampaignModelModule, PushNotificationModule],
    controllers: [CampaignController],
    providers: [CampaignService, CampaignCommand],
    exports: [CampaignService, CampaignCommand]
})
export class CampaignModule {}
