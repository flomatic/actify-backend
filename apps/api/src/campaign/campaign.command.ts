import {
    CampaignMode,
    CampaignStatus,
    CampaignTimeScheduleType,
    CampaignType
} from '@shared/models/campaign.model';
import { Command, Console } from 'nestjs-console';
import { CampaignService } from './campaign.service';

@Console()
export class CampaignCommand {
    constructor(private readonly campaignService: CampaignService) {}

    @Command({
        command: 'build_sample_campaigns',
        description:
            'Build a couple of sample campaigns and insert into the database'
    })
    async create() {
        const sampleCampaigns = [
            {
                name: 'test_campaign_SMS',
                type: CampaignType.SMS_TYPE,
                segmentation_id: '123',
                payload: 'testing',
                status: CampaignStatus.SCHEDULED,
                mode: CampaignMode.TEST_MODE,
                time_schedule: {
                    type: CampaignTimeScheduleType.ONE_TIME,
                    date: new Date()
                },
                ignore_sleep_time: false
            }
        ];
        await this.campaignService.addOrUpdate(sampleCampaigns);
    }
}
