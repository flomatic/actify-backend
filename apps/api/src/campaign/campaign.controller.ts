import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    Query,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    CampaignDocument,
    CampaignModel,
    CampaignPaginationParamsDTO,
    CampaignStatus
} from '@shared/models/campaign.model';
import { OperationByIdDTO } from '@entries/general/dto/general.dto';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { CampaignService } from './campaign.service';
import { CampaignTimeValidation } from './campaign.utils';
import { CampaignValidation } from './campaign.validation';
import { IdValidation } from '@shared/validators/validation.id';

@ApiTags('Campaign')
@UseGuards(AuthGuard('jwt'))
@Controller()
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class CampaignController {
    constructor(private readonly campaignService: CampaignService) {}

    @Get('/api/v1/campaign')
    @ApiOperation({ summary: 'Returns all campaigns.' })
    async getAllCampaigns(
        @Query() paginationParams: CampaignPaginationParamsDTO
    ) {
        return this.campaignService.getAllCampaign(paginationParams);
    }

    @Get('/api/v1/campaign/:id')
    @ApiOperation({ summary: 'Returns a campaign by id.' })
    async getACampaign(@Param() { id }: OperationByIdDTO) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        return this.campaignService.getACampaign(id);
    }

    @Post('/api/v1/campaign/')
    @ApiOperation({ summary: 'Adds a new campaign.' })
    async addACampaign(@Body() campaignDTO: CampaignModel) {
        const { error } = CampaignValidation.validate(campaignDTO);
        if (error) {
            throw new BadRequestException(error);
        }
        const isTimeValid = CampaignTimeValidation(campaignDTO.time_schedule);
        if (!isTimeValid) {
            throw new BadRequestException('time is not valid');
        }
        return this.campaignService.addACampaign(campaignDTO);
    }

    @Put('/api/v1/campaign/:id')
    @ApiOperation({ summary: 'Edits a campaign.' })
    async editACampaign(
        @Param() { id }: OperationByIdDTO,
        @Body() campaignDTO: CampaignDocument
    ) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }
        const { error } = CampaignValidation.validate(campaignDTO);
        if (error) {
            throw new BadRequestException(error);
        }
        if (campaignDTO.status === CampaignStatus.SCHEDULED) {
            const isTimeValid = CampaignTimeValidation(
                campaignDTO.time_schedule
            );
            if (!isTimeValid) {
                throw new BadRequestException('time is not valid');
            }
        }
        let campaign = await this.campaignService.finCampaignById(id);
        if (campaign.status !== CampaignStatus.RUNNING) {
            if (campaignDTO.status === CampaignStatus.SCHEDULED) {
                campaign.last_running_date = null;
                campaign.total_running_count = 0;
                campaign.next_running_date = null;
            }
            campaign = Object.assign(campaign, campaignDTO);
            await campaign.save();
            return campaign;
        } else {
            throw new BadRequestException('cant not edit running campaign');
        }
    }

    @Delete('/api/v1/campaign/:id')
    @ApiOperation({ summary: 'Removes a campaign.' })
    async removeACampaign(@Param() { id }: OperationByIdDTO) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        const campaign = await this.campaignService.finCampaignById(id);
        if (campaign.status !== CampaignStatus.RUNNING) {
            await campaign.deleteOne();
        } else {
            throw new BadRequestException('cant not edit running campaign');
        }
    }
}
