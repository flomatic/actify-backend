import * as Joi from 'joi';

export const CampaignValidation = Joi.object({
    name: Joi.string().trim().min(3).max(60).required(),
    type: Joi.string().valid(
        'sms_type',
        'email_type',
        'push_notification_type'
    ),
    segmentation_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
    test_segmentation_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
    template_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
    mode: Joi.string().valid('normal_mode', 'test_mode'),
    ignore_sleep_time: Joi.boolean().optional(),
    ignore_rate_limit: Joi.boolean().optional(),
    rate_limit: Joi.object({
        email: Joi.number().min(1).max(10000),
        push_notification: Joi.number().min(1).max(10000),
        sms: Joi.number().min(1).max(10000)
    }).optional(),
    time_out: Joi.date().optional(),
    retry: Joi.object({
        in: Joi.number(),
        next: Joi.object().valid('day', 'hour')
    }).optional(),
    status: Joi.string().valid('running', 'scheduled', 'stopped'),
    time_schedule: Joi.object({
        type: Joi.string().valid('one_time', 'recurring'),
        date: Joi.date(),
        every: Joi.object({
            type: Joi.string().valid('day', 'week', 'month', 'year'),
            number: Joi.number(),
            from: Joi.date()
        })
    }).xor('date', 'every')
});
