import {
    CampaignTimeSchedule,
    CampaignTimeScheduleType
} from '@shared/models/campaign.model';

export const CampaignTimeValidation = (ts: CampaignTimeSchedule) => {
    const nowDate = new Date().getTime() - 20 * 1000;
    if (ts.type === CampaignTimeScheduleType.ONE_TIME) {
        const tsDate = new Date(ts.date).getTime();
        if (tsDate < nowDate) {
            return false;
        }
    } else if (ts.type === CampaignTimeScheduleType.RECURRING) {
        const tsDate = new Date(ts.every.from).getTime();
        if (tsDate < nowDate) {
            return false;
        }
    }
    return true;
};
