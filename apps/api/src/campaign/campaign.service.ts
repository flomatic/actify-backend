import {
    BadRequestException,
    Injectable,
    NotFoundException
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import {
    CampaignDocument,
    CampaignModel,
    CampaignPaginationParamsDTO,
    CampaignStatus
} from '@shared/models/campaign.model';
import * as escRegExp from 'escape-string-regexp';
import { Model } from 'mongoose';

Injectable();
export class CampaignService {
    constructor(
        private readonly logger: BLogger,
        @InjectModel('Campaign')
        private readonly campaignModel: Model<CampaignDocument>
    ) {}

    async getAllCampaign(params: CampaignPaginationParamsDTO) {
        try {
            const name = params.name ? escRegExp(params.name) : null;
            const pageNumber = +params.page || 1;
            const perPage = +params.itemsPerPage || 10;
            const skipValue = Number((pageNumber - 1) * perPage);
            const status = params.status;
            const type = params.type;

            const queryModel = {};

            if (name) queryModel['name'] = new RegExp(name, 'gi');
            if (status) queryModel['status'] = status;
            if (type) queryModel['type'] = type;

            const total = await this.campaignModel.count(queryModel);
            const result = await this.campaignModel
                .find(queryModel, { __v: 0 })
                .sort({ _id: -1 })
                .skip(skipValue)
                .limit(perPage)
                .lean();

            return {
                result,
                total,
                page: pageNumber
            } as {
                result: CampaignDocument[];
                total: number;
                page: number;
            };
        } catch (err) {
            this.logger.error(ACTIONS.CAMPAIGN_SERVICE, err);
        }
    }

    async getACampaign(id: string): Promise<CampaignModel> {
        const foundCampaign = await this.campaignModel
            .findById(id, {
                __v: 0,
                _id: 0,
                total_running_count: 0,
                next_running_date: 0,
                last_running_date: 0,
                test_segmentation: 0,
                createdAt: 0,
                updatedAt: 0
            })
            .lean();
        if (!foundCampaign) {
            throw new NotFoundException('Campaign not found');
        }

        return foundCampaign;
    }

    async finCampaignById(id: string): Promise<CampaignDocument> {
        return await this.campaignModel.findById(id);
    }

    async addACampaign(campaignDTO) {
        try {
            if (campaignDTO.status === CampaignStatus.RUNNING) {
                throw new BadRequestException(
                    'Cannot modify an ongoing campaign'
                );
            }

            const newCampaign = new this.campaignModel(campaignDTO);
            await newCampaign.save();
            return newCampaign;
        } catch (err) {
            if (11000 === err.code || 11001 === err.code) {
                throw new BadRequestException('Campaign name must be unique');
            }
            throw new BadRequestException(err);
        }
    }

    async editACampaign(id: string, campaignDTO): Promise<any> {
        try {
            const editedCampaign = await this.campaignModel.findByIdAndUpdate(
                id,
                campaignDTO,
                {
                    new: true,
                    lean: true
                }
            );

            if (!editedCampaign) {
                throw new NotFoundException('Campaign not found');
            }

            if (campaignDTO.status === CampaignStatus.RUNNING) {
                throw new BadRequestException(
                    'Cannot modify an ongoing campaign'
                );
            }

            return editedCampaign;
        } catch (err) {
            if (11000 === err.code || 11001 === err.code) {
                throw new BadRequestException('Campaign name must be unique');
            }
            throw new BadRequestException(err);
        }
    }

    async removeACampaign(id: string): Promise<any> {
        const removedCampaign = await this.campaignModel.findByIdAndRemove(id);
        if (!removedCampaign) {
            throw new NotFoundException('Campaign not found');
        }

        return removedCampaign;
    }

    async addOrUpdate(sampleCampaigns: any[]) {
        return await this.campaignModel.insertMany(sampleCampaigns);
    }
}
