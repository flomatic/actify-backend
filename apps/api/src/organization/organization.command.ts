import { Command, Console } from 'nestjs-console';
import { OrganizationService } from './organization.service';

@Console()
export class OrganizationCommand {
    constructor(private readonly organizationService: OrganizationService) {}

    @Command({
        command: 'add_organization <name> <secret>',
        description: 'Create an organization'
    })
    async create(name: string, secret: string) {
        await this.organizationService.addOrganization({
            name,
            enable: true,
            secret: secret || 'super_secret_secret'
        });
    }
}
