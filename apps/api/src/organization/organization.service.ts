import {
    BadGatewayException,
    BadRequestException,
    Injectable
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { hash, compare } from 'bcrypt';
import { get } from 'lodash';
import { sign, TokenExpiredError, verify } from 'jsonwebtoken';
import { OrganizationInterface } from '@entries/organization/model/organization.model';

@Injectable()
export class OrganizationService {
    accessTokenSecret: string;
    refreshTokenSecret: string;

    constructor(
        @InjectModel('Organization')
        private readonly organizationModel: Model<OrganizationInterface>,
        private readonly configService: ConfigService
    ) {
        this.accessTokenSecret = this.configService.get<string>(
            'JWT_ORG_ACCESS_TOKEN_SECRET'
        );
        this.refreshTokenSecret = this.configService.get<string>(
            'JWT_ORG_REFRESH_TOKEN_SECRET'
        );
    }

    async addOrganization(params) {
        try {
            const attrs = { ...params };
            const hashedSecret = await hash(attrs.secret, 10);
            attrs.secret = hashedSecret;
            const org: OrganizationInterface = new this.organizationModel(
                attrs
            );
            await org.save();
            return org;
        } catch (error) {
            throw new BadGatewayException(error);
        }
    }

    async validateOrganization(payload) {
        try {
            const name = payload.name;
            if (!name) return null;
            /* TODO: Check for enabled and disabled organizations when we move to a domain  */
            /* An also, the licence expiration date feature should be added */
            return await this.organizationModel.findOne({ name });
        } catch (error) {
            throw new BadRequestException(error);
        }
    }

    async generateTokens({ name, secret }) {
        const secureErrorMsg = 'Invalid api or secret key';

        const organization = await this.organizationModel.findOne({ name });
        if (!organization) {
            throw new BadRequestException(secureErrorMsg);
        }

        const isValid = await compare(secret, organization.secret);
        if (!isValid) {
            throw new BadRequestException(secureErrorMsg);
        }

        return this.generatePairOfTokens(name);
    }

    async refreshTokens({ access_token, refresh_token }) {
        const secureErrorMessage = 'Invalid tokens';
        try {
            const decodedAccessToken = verify(
                access_token,
                this.accessTokenSecret
            );
            const name = get(decodedAccessToken, 'name', null);
            if (!name || !name.length) {
                throw new BadRequestException(secureErrorMessage);
            }

            return this.generatePairOfTokens(name);
        } catch (e) {
            if (e instanceof TokenExpiredError) {
                const decodedRefreshToken = verify(
                    refresh_token,
                    this.refreshTokenSecret,
                    (err, decoded) => {
                        if (err) {
                            throw new BadRequestException(secureErrorMessage);
                        }
                        return decoded;
                    }
                );
                const name = get(decodedRefreshToken, 'name', null);
                if (!name || !name.length) {
                    throw new BadRequestException(secureErrorMessage);
                }

                return this.generatePairOfTokens(name);
            } else {
                throw new BadRequestException(secureErrorMessage);
            }
        }
    }

    generatePairOfTokens(name) {
        // TODO: Move the expiration times to config
        const access_token = sign({ name }, this.accessTokenSecret, {
            expiresIn: '8h'
        });
        const refresh_token = sign({ name }, this.refreshTokenSecret, {
            expiresIn: '12d'
        });
        return { access_token, refresh_token };
    }
}
