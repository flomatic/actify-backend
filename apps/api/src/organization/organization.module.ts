import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { OrganizationModelModule } from '@entries/organization/model/organization.model';
import { OrganizationCommand } from './organization.command';
import { OrganizationService } from './organization.service';

@Module({
    imports: [OrganizationModelModule, ConfigModule],
    providers: [OrganizationService, OrganizationCommand],
    exports: [OrganizationService, OrganizationCommand]
})
export class OrganizationModule {}
