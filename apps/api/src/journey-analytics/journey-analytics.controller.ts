import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    JourneyAnalyticsAddKpiDTO,
    JourneyAnalyticsRemoveKpiDTO
} from '@shared/models/journey-analytic.model';
import { OperationByIdDTO } from '@entries/general/dto/general.dto';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { JourneyAnalyticsService } from './journey-analytics.service';
import {
    JourneyKpiParamsValidation,
    JourneyKpiRemoveValidation
} from './journey-analytics.validator';
import { IdValidation } from '@shared/validators/validation.id';

@ApiTags('Journey Analytics')
@Controller()
@UseInterceptors(SentryInterceptor)
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
export class JourneyAnalyticsController {
    constructor(
        private readonly journeyAnalyticsService: JourneyAnalyticsService
    ) {}

    @Get('/api/v1/journey-analytics/:id')
    @ApiOperation({ summary: 'Returns a journey-analytics by id.' })
    async getJourneyAnalytics(@Param() operationByIdDto: OperationByIdDTO) {
        const isValid = IdValidation(operationByIdDto.id);

        if (!isValid) {
            throw new BadRequestException('Invalid id');
        }

        return this.journeyAnalyticsService.getAJourneyAnalytics(
            operationByIdDto.id
        );
    }

    @Post('/api/v1/journey-analytics/kpi/:id')
    @ApiOperation({ summary: 'Adds a single KPI to the journey analytics.' })
    async addAKpi(
        @Param() operationByIdDto: OperationByIdDTO,
        @Body() journeyAnalyticsAddKpiDTO: JourneyAnalyticsAddKpiDTO
    ) {
        const { id } = operationByIdDto;
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        const kpiParamsValidation = JourneyKpiParamsValidation.validate(
            operationByIdDto
        );
        if (kpiParamsValidation.error) {
            throw new BadRequestException(
                kpiParamsValidation.error,
                'Validation Error'
            );
        }
        return this.journeyAnalyticsService.addKpi(
            id,
            journeyAnalyticsAddKpiDTO
        );
    }

    @Delete('/api/v1/journey-analytics/kpi/:id')
    @ApiOperation({
        summary: 'Removes a single KPI from the journey analytics.'
    })
    async removeAKpi(
        @Param() operationByIdDto: OperationByIdDTO,
        @Body() journeyAnalyticsRemoveKpiDTO: JourneyAnalyticsRemoveKpiDTO
    ) {
        const { id } = operationByIdDto;
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        const kpiParamsValidation = JourneyKpiRemoveValidation.validate(
            journeyAnalyticsRemoveKpiDTO
        );
        if (kpiParamsValidation.error) {
            throw new BadRequestException(
                kpiParamsValidation.error,
                'Validation Error'
            );
        }
        return this.journeyAnalyticsService.removeKpi(
            id,
            journeyAnalyticsRemoveKpiDTO.name
        );
    }
}
