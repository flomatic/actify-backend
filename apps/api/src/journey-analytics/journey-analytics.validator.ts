import * as Joi from 'joi';

export const JourneyKpiParamsValidation = Joi.object({
    name: Joi.string().required(),
    first_step_id: Joi.string().required(),
    second_step_id: Joi.string().required()
});

export const JourneyKpiRemoveValidation = Joi.object({
    name: Joi.string().required()
});
