import {
    BadRequestException,
    Injectable,
    NotFoundException
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { JourneyAnalyticsInterface } from '@shared/models/journey-analytic.model';
import { Model } from 'mongoose';

@Injectable()
export class JourneyAnalyticsService {
    constructor(
        @InjectModel('JourneyAnalytics')
        private readonly journeyAnalyticsModel: Model<JourneyAnalyticsInterface>
    ) {}

    async addKpi(
        analyticsId: string,
        params: { name: string; first_step_id: string; second_step_id: string }
    ) {
        const journeyAnalytic = await this.journeyAnalyticsModel.findOne({
            journey_id: analyticsId
        });

        if (!journeyAnalytic) {
            throw new NotFoundException(
                'Cannot find analytics for this journey'
            );
        }

        if (journeyAnalytic.kpi.find((k) => k.name === params.name)) {
            throw new BadRequestException('Duplicate KPI name');
        }

        journeyAnalytic.kpi.push(params);
        journeyAnalytic.kpi_result.push({ name: params.name, value: 0 });
        await journeyAnalytic.save();

        return journeyAnalytic;
    }

    async removeKpi(analyticsId: string, name: string) {
        const journeyAnalytic = await this.journeyAnalyticsModel.findOne({
            journey_id: analyticsId
        });

        if (!journeyAnalytic) {
            throw new NotFoundException(
                'Cannot find analytics for this journey'
            );
        }

        const kpi = journeyAnalytic.kpi.find((k) => k.name === name);
        if (!kpi) {
            throw new BadRequestException('Cannot find kpi');
        }

        journeyAnalytic.kpi =
            journeyAnalytic.kpi?.filter((k) => k.name !== name) || [];
        journeyAnalytic.kpi_result =
            journeyAnalytic.kpi_result?.filter((k) => k.name !== name) || [];
        await journeyAnalytic.save();

        return journeyAnalytic;
    }

    async getAJourneyAnalytics(analyticsId: string) {
        const journeyAnalytic = await this.journeyAnalyticsModel.findOne({
            journey_id: analyticsId
        });

        if (!journeyAnalytic) {
            throw new BadRequestException(
                'Cannot find analytics for this journey'
            );
        }

        return journeyAnalytic;
    }
}
