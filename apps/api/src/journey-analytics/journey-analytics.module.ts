import { Module } from '@nestjs/common';
import { JourneyAnalyticModelModule } from '@shared/models/journey-analytic.model';
import { JourneyAnalyticsController } from './journey-analytics.controller';
import { JourneyAnalyticsService } from './journey-analytics.service';

@Module({
    imports: [JourneyAnalyticModelModule],
    controllers: [JourneyAnalyticsController],
    providers: [JourneyAnalyticsService],
    exports: [JourneyAnalyticsService]
})
export class JourneyAnalyticsModule {}
