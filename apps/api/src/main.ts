import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { NestjsLogger } from '@shared/logger/logger.service';
import * as chalk from 'chalk';
import * as helmet from 'helmet';
import * as Swagger from 'swagger-ui-express';
import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    const configService = await app.get<ConfigService>(ConfigService);

    const port = configService.get('API_PORT', 3002);
    const host = configService.get('HOST', 'localhost');

    app.useLogger(app.get(NestjsLogger));
    app.use(helmet());
    app.enableCors();

    const options = new DocumentBuilder()
        .setTitle('Actify Panel API')
        .setDescription(
            'The backend API to power our Actify event logging system.'
        )
        .setVersion('1.0')
        .addBearerAuth(
            { type: 'http', in: 'header', scheme: 'Bearer' },
            'access_token'
        )
        .addBearerAuth(
            { type: 'http', in: 'header', scheme: 'Bearer' },
            'public_access_token'
        )
        .build();

    const document = SwaggerModule.createDocument(app, options);

    app.use(
        '/swagger',
        Swagger.serve,
        Swagger.setup(document, {
            customCssUrl: '/static/swagger.theme.css',
            customJs: '/static/swagger.theme.js',
            swaggerOptions: {
                persistAuthorization: true,
                docExpansion: 'none'
            }
        })
    );

    SwaggerModule.setup('swagger', app, document);

    await app.listen(port, host);

    const url = await app.getUrl();

    console.log(`
        Status:   ${chalk.blueBright('Running')}
        Address:  ${chalk.green(url)}
        Docs:     ${chalk.green(`${url}/swagger`)}
    `);
}
bootstrap();
