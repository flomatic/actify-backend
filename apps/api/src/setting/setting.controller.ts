import {
    BadRequestException,
    Body,
    Controller,
    Get,
    Put,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { SettingsUpdateDTO } from '@shared/setting/interface/setting.interface';
import { SettingService } from '@shared/setting/setting.service';
import { SettingsUpdateValidator } from './setting.validator';

@ApiTags('Settings')
@Controller()
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class SettingController {
    constructor(private readonly settingService: SettingService) {}

    @Get('/api/v1/setting')
    @ApiOperation({
        summary: 'Returns setting for the current organization.'
    })
    async getSettings() {
        try {
            return this.settingService.getSettings();
        } catch (error) {
            throw new BadRequestException(error);
        }
    }

    @Put('/api/v1/setting')
    @ApiOperation({
        summary: 'Updates one record of the settings'
    })
    async updateSettings(@Body() payload: SettingsUpdateDTO) {
        try {
            const { error } = SettingsUpdateValidator.validate(payload);
            if (error) {
                throw new BadRequestException(error);
            }

            const setting = await this.settingService.updateASetting(payload);

            return setting;
        } catch (error) {
            throw new BadRequestException(error);
        }
    }
}
