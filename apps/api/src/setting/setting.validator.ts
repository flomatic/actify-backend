import * as Joi from 'joi';
import { SETTING_NAME } from '@shared/setting/interface/setting.interface';

const allowedTypes = Object.values(SETTING_NAME);

const userRateLimit = Joi.object({
    push_notification: Joi.number().required(),
    email: Joi.number().required(),
    sms: Joi.number().required()
});

const rateLimit = Joi.object({
    push_notification: Joi.number().required(),
    email: Joi.number().required(),
    sms: Joi.number().required()
});

const providerRateLimit = Joi.object({
    push_notification: Joi.number().required(),
    email: Joi.number().required(),
    sms: Joi.number().required()
});

const dndTime = Joi.object({
    from_hour: Joi.number().required(),
    to_hour: Joi.number().required()
});

export const SettingsUpdateValidator = Joi.object({
    name: Joi.string()
        .valid(...(allowedTypes || []))
        .required(),
    value: Joi.required()
        .when('name', {
            is: SETTING_NAME.RATE_LIMIT,
            then: rateLimit
        })
        .when('name', {
            is: SETTING_NAME.DND_TIME,
            then: dndTime
        })
        .when('name', {
            is: SETTING_NAME.USER_RATE_LIMIT,
            then: userRateLimit
        })
});
