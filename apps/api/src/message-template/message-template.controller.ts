import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiParam,
    ApiTags
} from '@nestjs/swagger';
import {
    MessageTemplateOperationDTO,
    MessageTemplateType
} from '@shared/models/message-template.model';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { MessageTemplateService } from './message-template.service';
import { MessageTemplateValidation } from './validator/message-template.validation';
import { IdValidation } from '@shared/validators/validation.id';
import { OperationByIdDTO } from '@entries/general/dto/general.dto';
import { Types } from 'mongoose';

@ApiTags('Message Templates')
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@Controller()
@UseInterceptors(SentryInterceptor)
export class MessageTemplateController {
    constructor(
        private readonly messageTemplateService: MessageTemplateService
    ) {}

    @Get('/api/v1/message-template/type/:type')
    @ApiParam({
        name: 'type',
        required: true,
        enum: [...Object.values(MessageTemplateType)]
    })
    @ApiOperation({
        summary: 'Returns all message templates for a specific type.'
    })
    async getAllMessageTemplates(@Param() { type }: any) {
        return this.messageTemplateService.getAllMessageTemplate(type);
    }

    @Get('/api/v1/message-template/:id')
    @ApiParam({ name: 'id', required: true, type: 'string' })
    @ApiOperation({ summary: 'Returns a message template by id.' })
    async getAMessageTemplateById( @Param() { id }: OperationByIdDTO ) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        return this.messageTemplateService.findById(Types.ObjectId(id));
    }

    @Post('/api/v1/message-template')
    @ApiOperation({ summary: 'Adds a new message template.' })
    async addAMessageTemplate(
        @Body() messageTemplateDto: MessageTemplateOperationDTO
    ) {
        const { error } = MessageTemplateValidation.validate(
            messageTemplateDto
        );
        if (error) {
            throw new BadRequestException(error);
        }

        return this.messageTemplateService.addAMessageTemplate(
            messageTemplateDto
        );
    }

    @Put('/api/v1/message-template/:id')
    @ApiParam({ name: 'id', required: true, type: 'string' })
    @ApiOperation({ summary: 'Edits a message template.' })
    async editAMessageTemplate(
        @Param() { id }: OperationByIdDTO,
        @Body() messageTemplateDto: MessageTemplateOperationDTO
    ) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        const { error } = MessageTemplateValidation.validate(
            messageTemplateDto
        );
        if (error) {
            throw new BadRequestException(error);
        }

        return this.messageTemplateService.editAMessageTemplate(
            id,
            messageTemplateDto
        );
    }

    @Delete('/api/v1/message-template/:id')
    @ApiParam({ name: 'id', required: true, type: 'string' })
    @ApiOperation({ summary: 'Removes a message template.' })
    async removeAMessageTemplate(@Param() { id }: OperationByIdDTO) {
        if (!IdValidation(id)) {
            throw new BadRequestException('Invalid id');
        }

        return this.messageTemplateService.removeAMessageTemplate(id);
    }
}
