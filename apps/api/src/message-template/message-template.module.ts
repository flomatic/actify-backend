import { Module } from '@nestjs/common';
import { MessageTemplateModelModule } from '@shared/models/message-template.model';
import { MessageTemplateController } from './message-template.controller';
import { MessageTemplateService } from './message-template.service';

@Module({
    imports: [MessageTemplateModelModule],
    controllers: [MessageTemplateController],
    providers: [MessageTemplateService],
    exports: [MessageTemplateService]
})
export class MessageTemplateModule {}
