import {
    BadRequestException,
    Injectable,
    NotFoundException
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MessageTemplateInterface } from '@shared/models/message-template.model';
import { Model } from 'mongoose';

Injectable();
export class MessageTemplateService {
    constructor(
        @InjectModel('MessageTemplate')
          private readonly messageTemplateModel: Model<MessageTemplateInterface>
    ) {}

    async getAllMessageTemplate(
        type: any
    ): Promise<MessageTemplateInterface[]> {
        return await this.messageTemplateModel.find({ type }, { __v: 0 });
    }

    async findById(templateId) {
        try {
            const foundMessageTemplate = await this.messageTemplateModel
                .findById(
                    templateId ,
                    {
                        __v: 0,
                        createdAt: 0,
                        updatedAt: 0
                    }
                );
    
            if (!foundMessageTemplate) {
                throw new NotFoundException('Message template not found');
            }
    
            return foundMessageTemplate;
        } catch (error) {
            //
        }
    }

    async addAMessageTemplate(messageTemplateDto) {
        try {
            const newMessageTemplate = new this.messageTemplateModel(
                messageTemplateDto
            );
            await newMessageTemplate.save();
            return newMessageTemplate;
        } catch (err) {
            if (11000 === err.code || 11001 === err.code) {
                throw new BadRequestException(
                    'Message template name must be unique'
                );
            }
            throw new BadRequestException(err);
        }
    }

    async editAMessageTemplate(id: string, messageTemplateDto): Promise<any> {
        try {
            const editedMessageTemplate = await this.messageTemplateModel.findByIdAndUpdate(
                id,
                messageTemplateDto,
                {
                    new: true,
                    lean: true
                }
            );

            if (!editedMessageTemplate) {
                throw new NotFoundException('Message template not found');
            }

            return editedMessageTemplate;
        } catch (err) {
            if (11000 === err.code || 11001 === err.code) {
                throw new BadRequestException(
                    'Message template name must be unique'
                );
            }
            throw new BadRequestException(err);
        }
    }

    async removeAMessageTemplate(id: string): Promise<any> {
        const removedMessageTemplate = await this.messageTemplateModel.findByIdAndRemove(
            id
        );
        if (!removedMessageTemplate) {
            throw new NotFoundException('Message template not found');
        }

        return removedMessageTemplate;
    }
}
