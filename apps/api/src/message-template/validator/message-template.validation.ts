import * as Joi from 'joi';
import { MessageTemplateType } from '@shared/models/message-template.model';

const allowedTypes = Object.values(MessageTemplateType);

export const MessageTemplateValidation = Joi.object({
    name: Joi.string().trim().min(3).max(60).required(),
    type: Joi.string()
        .valid(...(allowedTypes || []))
        .required(),
    tags: Joi.array().items(Joi.string()).optional(),
    description: Joi.string().allow('').optional(),
    variables: Joi.array()
        .items({
            name: Joi.string().empty().max(100),
            fallback: Joi.string().allow('').max(3000).optional()
        })
        .optional(),
    data: Joi.required()
        .when('type', {
            is: MessageTemplateType.SMS,
            then: Joi.object().keys({
                body: Joi.string().allow('').required()
            })
        })
        .when('type', {
            is: MessageTemplateType.PUSH_NOTIFICATION,
            then: Joi.object().keys({
                subject: Joi.string().allow('').required(),
                body: Joi.string().min(3).required()
            })
        })
        .when('type', {
            is: MessageTemplateType.EMAIL,
            then: Joi.object().keys({
                subject: Joi.string().allow('').optional(),
                from: Joi.string().allow('').optional(),
                reply_to: Joi.string().allow('').optional(),
                design: Joi.object().required(),
                chunks: Joi.object().required(),
                text: Joi.string().allow('').optional(),
                html: Joi.string().min(3).required()
            })
        })
});
