import { Injectable, ServiceUnavailableException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UserInterface } from '@shared/models/user.model';
import { SessionService } from 'apps/api/src/session/session.service';
import * as escRegExp from 'escape-string-regexp';
import { Model } from 'mongoose';

@Injectable()
export class UserService {
    constructor(
        @InjectModel('User') readonly userModel: Model<UserInterface>,
        private readonly sessionService: SessionService
    ) {}

    async getUsers(query: {
        page?: number;
        itemsPerPage?: number;
        term?: string;
        // sortBy?: string;
        // sortDesc?: number;
    }) {
        try {
            const term = query.term ? escRegExp(query.term) : null;
            const searchTerm = term && term.trim().replace(/^[+]98|^0|/g, '');
            const pageNumber = +query.page || 1;
            const perPage = +query.itemsPerPage || 10;
            const skipValue = Number((pageNumber - 1) * perPage);

            /* eslint-disable */
            const queryModel = searchTerm
                ? {
                      $or: [
                          { email: new RegExp(searchTerm, 'gi') },
                          { phone_number: new RegExp(searchTerm, 'gi') },
                          { user_unique_value: new RegExp(searchTerm, 'gi') }
                      ]
                  }
                : {};
            /* eslint-enable */

            const total = await this.userModel.count(queryModel);
            const q = this.userModel.find(queryModel);

            // const sortTerm = query.sortBy;
            // const sortQuery = {};
            // sortQuery[sortTerm] = query.sortDesc;

            const sortQuery = { last_activity: -1 };
            const result = await q
                .sort(sortQuery)
                .skip(skipValue)
                .limit(perPage)
                .lean();

            return {
                result,
                total,
                page: pageNumber
            };
        } catch (err) {
            throw new ServiceUnavailableException(err);
        }
    }

    // TODO: REDUNDANT
    async getUser(params: { email?: string; phone_number?: string }) {
        const user = await this.userModel.aggregate([
            {
                $match: {
                    $or: [
                        { email: params?.email },
                        { phone_number: params?.phone_number }
                    ]
                }
            }
        ]);
        return user?.[0] || {};
    }

    async getUserById(
        id: string
    ): Promise<UserInterface & { online_channels: string[] }> {
        const user: UserInterface & {
            online_channels: string[];
        } = await this.userModel.findById(id, { __v: 0 }).lean();
        const { user_unique_key, user_unique_value } = user;
        user.online_channels = await this.sessionService.findUserStatus({
            user_unique_key,
            user_unique_value
        });
        return user;
    }

    countAllUsers() {
        return this.userModel.find({}).count().exec();
    }
}
