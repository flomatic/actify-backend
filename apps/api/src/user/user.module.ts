import { Module } from '@nestjs/common';
import { EnumModule } from '@shared/enum/enum.module';
import { UserModelModule } from '@shared/models/user.model';
import { SchemaModule } from '@shared/schema/schema.module';
import { SessionModule } from '../session/session.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
    imports: [UserModelModule, SessionModule, SchemaModule, EnumModule],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService]
})
export class UserModule {}
