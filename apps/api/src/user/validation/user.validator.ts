import Joi = require('joi');

export const UserValidator = Joi.object({
    phone_number: Joi.string().regex(/^(\+\d{1,3}[- ]?)?\d{10}$/),
    email: Joi.string().email(),
    page_number: Joi.number().max(100)
}).xor('phone_number', 'email');

export const UsersValidator = Joi.object({
    page: Joi.number().min(1),
    itemsPerPage: Joi.number(),
    term: Joi.string().optional().allow(''),
    sortBy: Joi.string().allow(''),
    sortDesc: Joi.number().allow(1, -1)
});
