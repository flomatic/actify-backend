import {
    BadRequestException,
    Controller,
    Get,
    Param,
    Query,
    UseGuards,
    UseInterceptors
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiParam,
    ApiTags
} from '@nestjs/swagger';
import { EnumService } from '@shared/enum/enum.service';
import { GetUsersDTO } from '@shared/models/user.model';
import { SchemaService } from '@shared/schema/schema.service';
import { SentryInterceptor } from '@shared/sentry/sentry.interceptor';
import { UserService } from './user.service';
import { UsersValidator } from './validation/user.validator';
import { IdValidation } from '@shared/validators/validation.id';
import { OperationByIdDTO } from '@entries/general/dto/general.dto';

@ApiTags('User')
@Controller()
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth('access_token')
@UseInterceptors(SentryInterceptor)
export class UserController {
    constructor(
        private readonly userService: UserService,
        private readonly schemaService: SchemaService,
        private readonly enumService: EnumService
    ) {}

    @Get('/api/v1/user')
    @ApiOperation({ summary: 'Returns a set of users (paginated).' })
    getUsers(@Query() getUsersDto: GetUsersDTO) {
        const validate = UsersValidator.validate(getUsersDto);
        if (validate.error) {
            throw new BadRequestException('Validation Error');
        }
        return this.userService.getUsers(getUsersDto);
    }

    @Get('/api/v1/user/user-info-schema')
    async getUserInfo() {
        return (await this.schemaService.findSchemaByName('user_info')) || {};
    }

    @Get('/api/v1/user/user-device-schema')
    async getUserDeviceSchema() {
        return (
            (await this.schemaService.findSchemaByName(
                'user_device_attributes'
            )) || {}
        );
    }

    @Get('/api/v1/user/user-device-schema/:variable')
    @ApiParam({
        name: 'variable',
        type: 'string',
        description: 'Name of the variable',
        required: true
    })
    async getPossibleValuesForUserDeviceSchema(
        @Param('variable') variable: string
    ) {
        return await this.enumService.getUserDeviceValues(variable);
    }

    @Get('/api/v1/user/user_attributes')
    async getUserAttributes() {
        return (
            (await this.schemaService.findSchemaByName('user_attributes')) || {}
        );
    }

    @Get('/api/v1/user/user_attributes/:variable')
    @ApiParam({
        name: 'variable',
        type: 'string',
        description: 'Name of the variable',
        required: true
    })
    async getPossibleValuesForUserAttributes(
        @Param('variable') variable: string
    ) {
        return await this.enumService.getUserDeviceValues(variable);
    }

    @Get('/api/v1/user/:id')
    @ApiOperation({ summary: 'Returns a user by id.' })
    @ApiParam({ name: 'id', type: 'string', required: true })
    async getUser(@Param() operationByIdDto: OperationByIdDTO) {
        if (!IdValidation(operationByIdDto.id)) {
            throw new BadRequestException('Invalid id');
        }
        const user = await this.userService.getUserById(operationByIdDto.id);
        return user;
    }
}
