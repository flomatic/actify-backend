import { Actify } from './actify';

declare global {
    interface Window {
        actify: any;
        ACTIFY_WEBSOCKET_URL?: string;
    }
}

export default Actify;

// export function init({ url, onError }: ActifyOptions) {
//
// }
