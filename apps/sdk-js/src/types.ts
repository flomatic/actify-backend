export interface ActifyOptions {
    url: string;
    onError?: (err: Error, ...args) => void;
}

export interface InitData {
    user?: {
        key: string;
        value: Record<string, unknown>;
    };
}
