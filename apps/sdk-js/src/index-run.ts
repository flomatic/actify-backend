import Actify from './index';

const { warn } = console;

function init() {
    if (!window.ACTIFY_WEBSOCKET_URL) {
        warn('[ACTIFY]', 'No url provided');
        return;
    }

    if (typeof window === 'undefined') {
        warn(
            '[ACTIFY]',
            "Wrong environment. make sure you're using actify in browser"
        );
        return;
    }

    const { localStorage, sessionStorage } = window;
    if (!localStorage || !sessionStorage) {
        throw new Error(`SessionStorage & LocalStorage is not supported`);
    }

    window.actify = new Actify({ url: window.ACTIFY_WEBSOCKET_URL });
}

init();
