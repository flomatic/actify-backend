import { MESSAGE_TYPES } from './utils/constants';
import { record } from 'rrweb';

export class SessionRecorder {
    private recordStopper?: () => void;

    constructor(
        protected readonly localSessionId: string,
        protected readonly tabSessionId: string,
        protected readonly emitter: (obj: any) => void
    ) {}

    startSessionRecording(ctx, interval = 10 * 1000) {
        let totalEvents = 0;
        let events = [];

        const submitEvents = () => {
            if (totalEvents > 3000 || events.length === 0) {
                return;
            }
            this.emitter({
                type: MESSAGE_TYPES.EVENT,
                session_id: this.localSessionId,
                event_name: 'replay_event',
                body: {
                    tab_session_id: this.tabSessionId,
                    user_agent: navigator.userAgent,
                    events,
                    context: { ...ctx }
                }
            });
            totalEvents += events.length;
            events = [];
        };

        this.recordStopper = record({
            emit(event) {
                events.push(event);

                if (events.length > 50) {
                    submitEvents();
                }
            }
        });

        setInterval(submitEvents, interval);
    }

    stopSessionRecording() {
        this.recordStopper && this.recordStopper();
    }
}
