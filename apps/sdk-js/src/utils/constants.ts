export enum MESSAGE_TYPES {
    INIT = 'init',
    CLOSE = 'close',
    EVENT = 'event',
    PING = 'ping',
    ERROR = 'error',
    PONG = 'pong'
}

export enum CHANNELS {
    BROWSER = 'browser'
}

export enum SERVER_MESSAGES {
    INVALID_SESSION = 'session_id_is_not_valid',
    SET_INIT = 'please_set_init_first',
    INVALID_INIT = 'init_is_not_valid'
}

export enum STORAGE_KEYS {
    USER_SESSION_ID = 'actify._session_id',
    TAB_SESSION_ID = 'actify._tab_session_id',
    DEVICE_ID = 'actify._device_id'
}

export enum CLIENT_MESSAGES {
    SESSION_ID_NOT_FOUND = 'User session id could not be found.',
    INVALID_EVENT_NAME = 'Invalid event name.',
    TAB_SESSION_ID_NOT_FOUND = 'Browser tab session id could not be found.'
}
