import { SessionRecorder } from './session-recorder';
import { ActifyOptions, InitData } from './types';
import {
    CHANNELS,
    CLIENT_MESSAGES,
    MESSAGE_TYPES,
    SERVER_MESSAGES,
    STORAGE_KEYS
} from './utils/constants';
import { generateUUID } from './utils/generate-uuid';

export class Actify {
    private connection: WebSocket;
    private sessionRecorder: SessionRecorder;

    constructor(private readonly options: ActifyOptions) {
        if (!options.onError) {
            options.onError = (err) => console.error('[ACTIFY]', err.message);
        }
        try {
            this.connection = new WebSocket(options.url);
            this.connection.onerror = this.onError.bind(this);
            this.connection.onopen = this.onOpen.bind(this);
            this.sessionRecorder = new SessionRecorder(
                this.getLocalSessionId(),
                this.getTabSessionId(),
                this.sendJSON.bind(this)
            );
        } catch (err) {
            this.options.onError(err);
        }
    }

    event(name = '', payload) {
        const localSessionId = this.getLocalSessionId();
        if (!localSessionId || !localSessionId.length) {
            throw new Error(CLIENT_MESSAGES.SESSION_ID_NOT_FOUND);
        }

        if (!name || !name.length) {
            throw new Error(CLIENT_MESSAGES.INVALID_EVENT_NAME);
        }

        const tabSessionId = this.getTabSessionId();
        if (!tabSessionId || !tabSessionId.length) {
            throw new Error(CLIENT_MESSAGES.TAB_SESSION_ID_NOT_FOUND);
        }

        const pageUrl = window.location.href || '';

        const eventData = {
            type: MESSAGE_TYPES.EVENT,
            session_id: localSessionId,
            event_name: name,
            body: {
                tab_session_id: tabSessionId,
                page_url: pageUrl,
                ...payload
            }
        };

        this.sendJSON(eventData);
    }

    async init(data?: InitData) {
        await this.waitForConnection();
        const agent = navigator.userAgent || 'Unknown';

        let deviceId = this.getDeviceId();
        if (!deviceId) {
            deviceId = generateUUID();
            localStorage.setItem(STORAGE_KEYS.DEVICE_ID, deviceId);
        }

        const initData = {
            type: MESSAGE_TYPES.INIT,
            body: {
                user_agent: agent,
                channel: CHANNELS.BROWSER,
                device_id: deviceId,
                tracking_id: this.getUtmSource(),
                referrer: this.getReferrer(),
                user_attr: undefined,
                old_session_id: undefined
            }
        };

        if (data && data.user) {
            initData.body.user_attr = data.user;
        }

        const localSessionId = this.getLocalSessionId();
        if (localSessionId && localSessionId.length) {
            initData.body.old_session_id = localSessionId;
        }

        this.sendJSON(initData);
    }

    setErrorHandler(handler: ActifyOptions['onError']) {
        this.options.onError = handler;
    }

    private onError(event) {
        const { onError } = this.options;
        onError(new Error('Websocket connection error'), event);
    }

    private async onOpen() {
        await this.waitUntilDocumentReady();
        this.connection.onmessage = this.onMessage.bind(this);
        // this.init();
    }

    private onMessage(ev) {
        try {
            const message = JSON.parse(ev.data);
            if (message.type === MESSAGE_TYPES.INIT) {
                const userSessionId = message.body.session_id;
                if (userSessionId && userSessionId.length) {
                    localStorage.setItem(
                        STORAGE_KEYS.USER_SESSION_ID,
                        userSessionId
                    );
                }

                const tabSessionId = this.getTabSessionId();
                if (!tabSessionId) {
                    sessionStorage.setItem(
                        STORAGE_KEYS.TAB_SESSION_ID,
                        generateUUID()
                    );
                }
            }

            if (message.type === MESSAGE_TYPES.PING) {
                const response = {
                    type: MESSAGE_TYPES.PONG,
                    body: {}
                };
                this.sendJSON(response);
            }

            if (message.type === MESSAGE_TYPES.ERROR) {
                const serverMsg = message.body.message;
                if (serverMsg === SERVER_MESSAGES.INVALID_SESSION) {
                    this.init();
                }
                throw new Error(serverMsg);
            }
        } catch (err) {
            this.options.onError(err);
        }
    }

    private sendJSON(obj) {
        return this.connection.send(JSON.stringify(obj));
    }

    private waitUntilDocumentReady() {
        return new Promise((resolve) => {
            const isReady = () =>
                document.readyState === 'complete' ||
                document.readyState === 'interactive';
            if (isReady()) {
                return resolve();
            }
            document.addEventListener('readystatechange', () => {
                isReady() && resolve();
            });
        });
    }

    private waitForConnection() {
        return new Promise((resolve) => {
            if (this.connection?.readyState === 1) return resolve();
            this.connection.addEventListener('open', () => {
                resolve();
            });
        });
    }

    private getLocalSessionId = () =>
        localStorage.getItem(STORAGE_KEYS.USER_SESSION_ID) || null;
    private getTabSessionId = () =>
        sessionStorage.getItem(STORAGE_KEYS.TAB_SESSION_ID) || null;
    private getDeviceId = () =>
        localStorage.getItem(STORAGE_KEYS.DEVICE_ID) || null;
    private getReferrer = () => document.referrer || null;
    private getTrackingId = () => {
        const url = window.location.href;
        const name = 'tracking_id';
        const reg = new RegExp('[?&]' + name + '=([^&#]*)', 'i');
        const queryString = reg.exec(url);
        return queryString ? queryString[1] : null;
    };

    startSessionRecording(...args) {
        return (this.sessionRecorder.startSessionRecording as any)(...args);
    }

    stopSessionRecording() {
        return this.sessionRecorder.stopSessionRecording();
    }
}
