import typescript from 'rollup-plugin-typescript';
import { terser } from 'rollup-plugin-terser';
import resolve from 'rollup-plugin-node-resolve'
import path from 'path';
import _ from 'lodash';

const baseConfig = (format, merge = {}) => {
    return _.merge({
        input: path.resolve(__dirname, './src/index.ts'),
        output: {
            format: format,
            file: path.resolve(__dirname, `./dist/actify-sdk.${format}.js`)
        },
        plugins: [
            resolve({mainFields: ['module', 'main', 'browser']}),
            typescript({ typescript: require('typescript') }),
            terser()
        ]
    }, merge)
}

export default [
    baseConfig('umd', { output: { name: 'actify' } }),
    baseConfig('es'),
    baseConfig('cjs'),
    baseConfig(
        'iife',
        { input: path.resolve(__dirname, './src/index-run.ts'), output: { name: 'actify' } }
    )
]
