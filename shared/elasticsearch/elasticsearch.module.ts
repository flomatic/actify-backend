import { ConfigModule } from '@shared/config.module';
import { ConfigService } from '@nestjs/config';
import { ElasticsearchModule as EsModule } from '@nestjs/elasticsearch';

export const ElasticsearchModule = EsModule.registerAsync({
    imports: [ConfigModule],
    useFactory: async (configService: ConfigService) => ({
        node: configService.get('ELASTICSEARCH_URL')
    }),
    inject: [ConfigService]
});
