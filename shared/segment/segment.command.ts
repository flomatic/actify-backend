import { SegmentCategory } from '@entries/segment/enum/segment-category';
import { Command, Console } from 'nestjs-console';
import { SegmentService } from './segment.service';

@Console()
export class SegmentCommand {
    constructor(private readonly segmentService: SegmentService) {}

    @Command({
        command: 'create_static_segmentations',
        description: 'Creates static user segmentations'
    })
    async create() {
        const systemSegmentation = [
            {
                name: 'all users',
                description: '',
                members: {
                    type: 'all',
                    user: []
                },
                category: SegmentCategory.SYSTEM,
                criteria: [],
                events: []
            },
            {
                name: 'android users',
                description: '',
                members: {
                    type: 'all',
                    user: []
                },
                category: SegmentCategory.SYSTEM,
                criteria: [
                    {
                        key: 1,
                        operation: {
                            type: 'channel',
                            value: 'android'
                        }
                    }
                ],
                events: []
            },
            {
                name: 'ios users',
                description: '',
                members: {
                    type: 'all',
                    user: []
                },
                category: SegmentCategory.SYSTEM,
                criteria: [
                    {
                        key: 1,
                        operation: {
                            type: 'channel',
                            value: 'ios'
                        }
                    }
                ],
                events: []
            },
            {
                name: 'web users',
                description: '',
                members: {
                    type: 'all',
                    user: []
                },
                category: SegmentCategory.SYSTEM,
                criteria: [
                    {
                        key: 1,
                        operation: {
                            type: 'channel',
                            value: 'web'
                        }
                    }
                ],
                events: []
            }
        ];

        await this.segmentService.addOrUpdateSegmentations(systemSegmentation);
    }
}
