export const SegmentStructure = {
    structure: {
        name: 'string',
        members: {
            type: ['all', 'custom'],
            user: [
                {
                    key: 'string',
                    value: 'string'
                }
            ]
        },
        criteria: [
            {
                key: 'number',
                operation: {
                    type: [
                        'activity_level',
                        'last_visited',
                        'channel',
                        'location',
                        'joined_date'
                    ],
                    value: [
                        ['high', 'medium', 'low'],
                        '|',
                        {
                            time: {
                                type: ['iso_date_string', 'days', 'hours'],
                                value: ['number', 'string']
                            }
                        },
                        '|',
                        ['browser', 'android', 'ios'],
                        '|',
                        {
                            country: 'string',
                            city: 'string',
                            time_zone: 'string'
                        }
                    ]
                }
            },
            '|',
            {}
        ]
    },
    interfaces: {
        MembersInterface: {
            type: 'MemberType',
            user: [
                {
                    key: 'string',
                    value: 'string'
                }
            ]
        }
    }
};
