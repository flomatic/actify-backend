import { SegmentModelModule } from '@entries/segment/model/segment.model';
import { Module } from '@nestjs/common';
import { UserModule } from '@shared/user/user.module';
import { EventModule } from 'apps/main-processor/src/event/event.module';
import { SegmentQueryHelper } from './segment-query-helper';
import { SegmentCommand } from './segment.command';
import { SegmentService } from './segment.service';

@Module({
    imports: [SegmentModelModule, UserModule, EventModule],
    providers: [SegmentService, SegmentCommand, SegmentQueryHelper],
    exports: [SegmentService, SegmentCommand, SegmentQueryHelper]
})
export class SegmentModule {}
