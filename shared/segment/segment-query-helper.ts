import { ActivityLevel } from '@entries/activity-level/enum/activity-level';
import { Channel } from '@entries/channel/enum/channel';
import { DateRange } from '@entries/daterange/model/daterange';
import { Filter } from '@entries/filter/model/filter';
import { Condition } from '@entries/operation/enum/condition';
import { LogicalConjunction } from '@entries/operation/enum/logical-conjunction';
import { OperationType } from '@entries/operation/enum/operation-type';
import { Operation } from '@entries/operation/model/operation';
import { OperationOperator } from '@entries/operation/model/operationoperator';
import { SegmentCategory } from '@entries/segment/enum/segment-category';
import { SegmentMembersType } from '@entries/segment/enum/segment-member-type';
import { SegmentOperationType } from '@entries/segment/enum/segment-operation-type';
import { Segment } from '@entries/segment/model/segment.model';
import { Injectable } from '@nestjs/common';
import { EventFilter } from '@shared/models/analytic.model';
import { UserService } from '@shared/user/user.service';
import { EventService } from 'apps/main-processor/src/event/event.service';
import { string } from 'joi';

@Injectable()
export class SegmentQueryHelper {
    constructor(
        private readonly userService: UserService,
        private readonly eventService: EventService
    ) {}
    async calculate(segment: Segment) {
        const userQuery = this.getUserQuery(segment);
        const criteriaQuery = this.getCriteria(segment);
        const userAttrQuery = this.getUserAttrQuery(segment);
        const deviceAttrsQuery = this.getDeviceAttrQuery(segment);
        const eventQuery = SegmentQueryHelper.getEventQuery(segment);
        if (segment.events && segment.events.length > 0) {
            const agg = [
                {
                    $match: {
                        user_unique_value: { $ne: null }
                    }
                },
                eventQuery,
                {
                    $lookup: {
                        from: 'users',
                        let: {
                            user_unique_value: '$_id'
                        },

                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $eq: [
                                            '$user_unique_value',
                                            '$$user_unique_value'
                                        ]
                                    }
                                }
                            },
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            this.getUserQuery(segment, true),
                                            this.getCriteria(segment, true),
                                            this.getUserAttrQuery(
                                                segment,
                                                true
                                            ),
                                            this.getDeviceAttrQuery(
                                                segment,
                                                true
                                            )
                                        ].filter((item) => item)
                                    }
                                }
                            }
                        ],
                        as: 'user'
                    }
                },
                {
                    $project: {
                        _id: 0,
                        user: { $arrayElemAt: ['$user', 0] },
                        event: { $arrayElemAt: ['$events', 0] }
                    }
                },
                {
                    $project: {
                        _id: '$user._id',
                        user: 1,
                        event: 1
                    }
                }
            ];
            const data = await this.eventService.eventModel
                .aggregate(agg)
                .count('count');
            if (data && data.length > 0) {
                return {
                    count: data[0].count
                };
            }
            return {
                count: 0
            };
        } else {
            const query = {
                $and: [
                    userQuery,
                    userAttrQuery,
                    criteriaQuery,
                    deviceAttrsQuery
                ].filter((item) => item)
            };
            if (query.$and.length === 0) {
                delete query.$and;
            }
            const data = await this.userService.userModel.find(query).count();
            return {
                count: data || 0
            };
        }
    }

    async getUserForSegmentation(
        segment: Segment,
        limit: number,
        skip: number
    ) {
        const userQuery = this.getUserQuery(segment);
        const criteriaQuery = this.getCriteria(segment);
        const userAttrQuery = this.getUserAttrQuery(segment);
        const deviceAttrsQuery = this.getDeviceAttrQuery(segment);
        const eventQuery = SegmentQueryHelper.getEventQuery(segment);
        if (segment.events && segment.events.length > 0) {
            const agg = [
                {
                    $match: {
                        user_unique_value: { $ne: null }
                    }
                },
                eventQuery,
                {
                    $lookup: {
                        from: 'users',
                        let: {
                            user_unique_value: '$_id'
                        },

                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $eq: [
                                            '$user_unique_value',
                                            '$$user_unique_value'
                                        ]
                                    }
                                }
                            },
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            this.getUserQuery(segment, true),
                                            this.getCriteria(segment, true),
                                            this.getUserAttrQuery(
                                                segment,
                                                true
                                            ),
                                            this.getDeviceAttrQuery(
                                                segment,
                                                true
                                            )
                                        ].filter((item) => item)
                                    }
                                }
                            }
                        ],
                        as: 'user'
                    }
                },
                {
                    $project: {
                        _id: 0,
                        user: { $arrayElemAt: ['$user', 0] },
                        event: { $arrayElemAt: ['$events', 0] }
                    }
                },
                {
                    $project: {
                        _id: '$user._id',
                        user: 1,
                        event: 1
                    }
                }
            ];
            return await this.eventService.eventModel
                .aggregate(agg)
                .limit(limit)
                .skip(skip);
        } else {
            const query = {
                $and: [
                    userQuery,
                    userAttrQuery,
                    criteriaQuery,
                    deviceAttrsQuery
                ].filter((item) => item)
            };

            if (query.$and.length === 0) {
                delete query.$and;
            }
            const result = await this.userService.userModel
                .find(query)
                .limit(limit)
                .skip(skip);

            return result.map((r) => ({
                _id: r._id,
                user: r,
                event: null
            }));
        }
    }

    getUserAttrQuery(segment: Segment, expressionMode = false) {
        if (!segment.user_attrs || segment.device_attrs.length === 0) {
            return;
        }
        let prefix = '';
        if (expressionMode) {
            prefix = '$user_attributes.';
        } else {
            prefix = 'user_attributes.';
        }
        const query = { $and: [] };
        segment?.user_attrs.forEach((u) => {
            query.$and.push(SegmentQueryHelper.getEventValues(prefix, u));
        });
        return query?.$and?.length == 0 ? undefined : query;
    }

    getDeviceAttrQuery(segment: Segment, expressionMode = false) {
        if (!segment.device_attrs || segment.device_attrs.length === 0) {
            return;
        }
        let prefix = '';
        if (expressionMode) {
            prefix = '$devices.';
        } else {
            prefix = 'devices.';
        }
        const query = { $and: [] };
        segment?.device_attrs.forEach((u) => {
            query.$and.push(SegmentQueryHelper.getEventValues(prefix, u));
        });
        return query?.$and?.length == 0 ? undefined : query;
    }
    static getEventQuery(segment: Segment, expressionMode = false) {
        if (!segment.events || segment.events.length === 0) {
            return {};
        }
        const query = { $or: [] };
        segment.events?.forEach((e) => {
            const { from, to } = this.calculateTime(e.date);
            query.$or.push({
                name: e.event_name,
                //
                ...this.getFilterQuery(e.filters, true),
                time: {
                    $gte: from,
                    $lt: to
                }
            });
        });
        let eventConditionMatchQuery: any = { $and: [] };
        segment.events?.forEach((e, i) => {
            const eventText = e.event_name + '_' + e.number_of_happening;
            const hasNext =
                e.operator != null &&
                e.operator != undefined &&
                i + 1 < segment.events.length;
            if (hasNext) {
                if (e.operator === LogicalConjunction.OR) {
                    eventConditionMatchQuery = {
                        $or: [eventConditionMatchQuery, { $and: [] }]
                    };
                }
            }
            let queryToInsert = null;
            if (e.number_of_happening === 0) {
                queryToInsert = { 'events.name': { $ne: e.event_name } };
            } else {
                queryToInsert = { 'events.name_count': { $eq: eventText } };
            }

            if (queryToInsert) {
                if (eventConditionMatchQuery.$or) {
                    eventConditionMatchQuery.$or?.[1]?.$and.push(queryToInsert);
                } else {
                    eventConditionMatchQuery.$and?.push(queryToInsert);
                }
            }
        });
        eventConditionMatchQuery = {
            $match: eventConditionMatchQuery
        };

        return [
            query?.$or?.length === 0
                ? {
                    $match: query
                }
                : undefined,
            {
                $group: {
                    _id: {
                        user_unique_value: '$user_unique_value',
                        event_name: '$name'
                    },
                    events: {
                        $push: {
                            session_id: '$session_id',
                            body: '$body',
                            name: '$name',
                            time: '$time'
                        }
                    }
                }
            },
            {
                $project: {
                    _id: 0,
                    event: { $arrayElemAt: ['$events', 0] },
                    user_unique_value: '$_id.user_unique_value',
                    name: '$_id.event_name',
                    count: { $size: '$events' }
                }
            },
            {
                $group: {
                    _id: '$user_unique_value',
                    events: {
                        $push: {
                            count: '$count',
                            name: '$name',
                            event: '$event'
                        }
                    }
                }
            },
            {
                eventConditionMatchQuery
            }
        ];
    }

    static getFilterQuery(filters: Array<Filter>, expressionMode = false) {
        // and happen first or happend second
        let query: any = { $and: [] };
        // let orNumbers = 0;
        filters = filters.sort((f, s) => f.key - s.key);
        filters.forEach((f, i) => {
            const isOp = (f.operation as any)?.operator !== undefined;
            if (!isOp) {
                // let _query = query;
                // for (i = 0; i <= orNumbers; i++) {
                //     _query = _query.$or[1];
                // }
                if (query.$or) {
                    query.$or?.[1]?.$and.push(this.getEventValues('body.', f));
                } else {
                    query.$and.push(this.getEventValues('body.', f));
                }
            } else {
                const operator = (f.operation as OperationOperator).operator;
                if (operator === LogicalConjunction.AND) {
                } else {
                    // orNumbers++;
                    query = {
                        $or: [query, { $and: [] }]
                    };
                }
            }
        });
        return query?.$and?.length === 0 ? undefined : query;
    }

    static getEventValues(
        attachedField: string,
        filter: Filter,
        expressionMode = false
    ): any {
        if (!filter.operation) {
            return null;
        }
        const operatopn: Operation = filter.operation as Operation;
        const condition = operatopn.condition;
        const variable = operatopn.variable;
        const value = operatopn.value;
        operatopn.type;
        const returnObj = {};
        /* eslint-disable */
        switch (condition) {
            case Condition.EQUAL:
                returnObj[attachedField + variable] = value;
                return returnObj;
            case Condition.GREATER_OR_EQUAL:
                returnObj[attachedField + variable] = value;
                return { $gte: returnObj };
            case Condition.LOWER_OR_EQUAL:
                returnObj[attachedField + variable] = value;
                return { $lte: returnObj };
            case Condition.CONTAINS:
                returnObj[attachedField + variable] = `/${value}/`;
                return returnObj;
            case Condition.LOWER:
                returnObj[attachedField + variable] = value;
                return { $lt: returnObj };
            case Condition.GREATER:
                returnObj[attachedField + variable] = value;
                return { $gt: returnObj };
            case Condition.NONE_OF:
                return {
                    $nin: [attachedField + variable, { $ifNull: [value, []] }]
                };
            case Condition.ONE_OF:
                return {
                    $in: [attachedField + variable, { $ifNull: [value, []] }]
                };
            case Condition.NOT_EQUAL:
                returnObj[attachedField + variable] = value;
                return { $ne: returnObj };
            /* eslint-enable */
        }
    }

    getUserQuery(segment: Segment, expressionMode = false) {
        if (segment.category === SegmentCategory.DYNAMIC || !segment.members) {
            return;
        }
        if (expressionMode) {
            const user = { $or: [] };

            segment.members?.users?.forEach((u) => {
                if (u.key === 'email') {
                    user.$or.push({
                        $eq: ['$email', u.value]
                    });
                } else if (u.key === 'phone_number') {
                    user.$or.push({
                        $eq: ['$phone_number', u.value]
                    });
                } else {
                    user.$or['$and'] = [
                        {
                            $eq: ['$user_unique_key', u.key]
                        },
                        {
                            $eq: ['$user_unique_value', u.value]
                        }
                    ];
                }
            });
            return user.$or.length > 0 ? user : undefined;
        } else {
            let query = {};
            if (segment.members?.type === SegmentMembersType.CUSTOM) {
                const keyValues = segment.members.users;
                query = {
                    $or: keyValues.map((keyValue) => {
                        if (keyValue.key === 'email') {
                            return { email: keyValue.value };
                        } else if (keyValue.key === 'phone_number') {
                            return { phone_number: keyValue.value };
                        } else {
                            return {
                                user_unique_key: keyValue.key,
                                user_unique_value: keyValue.value
                            };
                        }
                    })
                };
            } else return;
            return (query as any)?.$or?.length > 0 ? query : undefined;
        }
    }

    getCriteria(segment: Segment, expressionMode = false) {
        if (!segment.criteria || segment.criteria.length === 0) {
            return undefined;
        }
        const sortedCriteria = segment.criteria?.sort(
            (filter1, filter2) => filter1.key - filter2.key
        );

        const lastIndex = sortedCriteria.length - 1;
        let query: any = { $and: [] };
        // let numberOfOrs = 0;
        sortedCriteria?.forEach((c, i) => {
            const isOperator = (c.operation as any).operator != undefined;
            if (isOperator) {
                const operator = ((c.operation as any)
                    .operator as OperationOperator).operator;
                if (operator == LogicalConjunction.OR) {
                    query = {
                        $or: [query, { $and: [] }]
                    };
                }
            } else {
                const keyValue = this.getCriteriaKeyValue(
                    c.operation,
                    expressionMode
                );
                if (query.$or) {
                    query.$or?.[1]?.$and.push(keyValue);
                } else {
                    query.$and.push(keyValue);
                }
            }
        });
        return query?.$and?.length == 0 ? undefined : query;
    }

    getCriteriaKeyValue(operation: any, expressionMode = false) {
        const type: SegmentOperationType = operation.type;
        const value: ActivityLevel | DateRange | Channel | Location =
            operation.value;
        if (type === SegmentOperationType.ACTIVITY_LEVEL) {
            return expressionMode
                ? { $eq: ['$activity_level', value] }
                : {
                    activity_level: value
                };
        } else if (type === SegmentOperationType.CHANNEL) {
            return expressionMode
                ? {
                    $in: [value, { $ifNull: ['$devices.type', []] }]
                }
                : {
                    'devices.type': value
                };
        } else if (type === SegmentOperationType.JOINED_DATE) {
            const DateRange = value as DateRange;
            const { from, to } = SegmentQueryHelper.calculateTime(DateRange);
            return expressionMode
                ? {
                    $and: [
                        { $gte: ['$created_at', from] },
                        { $lt: ['$created_at', to] }
                    ]
                }
                : { created_at: { $gte: from, $lt: to } };
        } else if (type === SegmentOperationType.LAST_VISITED) {
            const DateRange = value as DateRange;
            const { from, to } = SegmentQueryHelper.calculateTime(DateRange);
            return expressionMode
                ? {
                    $and: [
                        { $gte: ['$updated_at', from] },
                        { $lt: ['$updated_at', to] }
                    ]
                }
                : { updated_at: { $gte: from, $lt: to } };
        }
    }

    private static calculateTime(dateRange: DateRange) {
        let dateOne: Date = new Date();
        let dateTwo: Date = new Date();
        const exec = (date, value) => {
            if (date === 'now') {
                return new Date();
            } else if (date === 'iso_date_string') {
                return new Date(value);
            } else if (date === 'year') {
                const date = new Date();
                date.setFullYear(date.getFullYear() - value);
                return date;
            } else if (date === 'month') {
                const date = new Date();
                date.setTime(date.getTime() - value * 30 * 24 * 60 * 60 * 1000);
                return date;
            } else if (date === 'day') {
                const date = new Date();
                date.setTime(date.getTime() - value * 24 * 60 * 60 * 1000);
                return date;
            } else if (date === 'week') {
                const date = new Date();
                date.setTime(date.getTime() - value * 7 * 24 * 60 * 60 * 1000);
            }
        };
        dateOne = exec(dateRange.from, dateRange.from_value);
        dateTwo = exec(dateRange.to, dateRange.to_value);
        return {
            from: dateOne,
            to: dateTwo
        };
    }
}
