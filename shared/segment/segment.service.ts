import { SegmentPaginationParamsDTO } from '@entries/segment/dto/segment-pagination.dto';
import { SegmentCategory } from '@entries/segment/enum/segment-category';
import { SegmentDocument } from '@entries/segment/model/segment.model';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import * as escRegExp from 'escape-string-regexp';
import { Model } from 'mongoose';
import { SegmentQueryHelper } from './segment-query-helper';

@Injectable()
export class SegmentService {
    constructor(
        @InjectModel('Segment')
        private readonly segmentModel: Model<SegmentDocument>,
        private readonly logger: BLogger,
        private readonly segmentQueryHelper: SegmentQueryHelper
    ) {}

    async getAllSegmentations(params: SegmentPaginationParamsDTO) {
        try {
            const name = params.name ? escRegExp(params.name) : null;
            const pageNumber = +params.page || 1;
            const perPage = +params.itemsPerPage || 10;
            const skipValue = Number((pageNumber - 1) * perPage);
            const category = params.category;

            const queryModel = {};

            if (name) queryModel['name'] = new RegExp(name, 'gi');
            if (category) queryModel['category'] = category;

            const total = await this.segmentModel.count(queryModel);
            const result = await this.segmentModel
                .find(queryModel, { __v: 0 })
                .sort({ _id: -1 })
                .skip(skipValue)
                .limit(perPage)
                .lean();

            return {
                result,
                total,
                page: pageNumber
            } as {
                result: SegmentDocument[];
                total: number;
                page: number;
            };
        } catch (err) {
            this.logger.error(ACTIONS.USER_SEGMENTATION_SERVICE, err);
        }
    }

    async getASegmentation(id: string) {
        const foundSegment = await this.segmentModel.findById(id, {
            __v: 0,
            createdAt: 0,
            updatedAt: 0
        });
        if (!foundSegment) {
            throw new BadRequestException('Segment not found');
        }
        return foundSegment;
    }

    async getSegmentCount(id: string) {
        const foundSegment = await this.segmentModel.findById(id);
        if (!foundSegment) {
            throw new BadRequestException('Segment not found');
        }
        return foundSegment.user_count;
    }

    async addSegmentation(userSegmentationDTO: any) {
        return await new this.segmentModel(userSegmentationDTO).save();
    }

    async editUserSegmentation(userSegmentationDTO: any, id: string) {
        const updatedSegmentation = await this.segmentModel.findOneAndUpdate(
            {
                _id: id,
                category: { $ne: SegmentCategory.SYSTEM }
            },
            userSegmentationDTO,
            { new: true }
        );
        if (!updatedSegmentation) {
            throw new BadRequestException('Segment not found');
        }
        return updatedSegmentation;
    }

    async deleteUserSegmentation(id: string) {
        const deletedSegmentation = await this.segmentModel.findOneAndDelete({
            _id: id,
            category: { $ne: SegmentCategory.SYSTEM }
        });

        if (!deletedSegmentation) {
            throw new BadRequestException('Segment not found');
        }
        return deletedSegmentation;
    }

    async addOrUpdateSegmentations(userSegmentations: any[]) {
        await this.segmentModel.insertMany(userSegmentations);
    }

    async findSegmentationById(id: any) {
        return await this.segmentModel.findById(id);
    }

    async findQuery(params) {
        return await this.segmentModel.find(params);
    }

    async create(params) {
        const userStats = new this.segmentModel(params);
        await userStats.save();
        return userStats;
    }

    async findOneByQuery(params) {
        return await this.segmentModel.findOne(params);
    }

    async getAllSegmentUsersPaginated(id, paginationParams) {
        const segment = await this.findSegmentationById(id);
        if (!segment) throw new BadRequestException('Segment not found');

        const limit = Number(paginationParams.itemsPerPage || 15);
        const skip = Number(limit * ((paginationParams.page || 1) - 1));

        const result = await this.segmentQueryHelper.getUserForSegmentation(
            segment,
            limit,
            skip
        );

        return result;
    }
}
