import { Command, Console } from 'nestjs-console';
import { EmailService } from './email.service';

@Console()
export class EmailCommand {
    constructor(private readonly emailService: EmailService) {}

    @Command({
        description: 'send test email to ...',
        command: 'send_email <email>'
    })
    async sendEmailTo(email: string) {
        await this.emailService.sendEmail({
            email,
            subject: 'this is a test',
            html: '<p>this is a test</p>'
        });
    }
}
