import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import {
    MessageTemplateInterface,
    SendEmailDto
} from '@shared/models/message-template.model';
import { Model } from 'mongoose';

@Injectable()
export class EmailService {
    constructor(
        @InjectModel('MessageTemplate')
        private readonly messageTemplateModel: Model<MessageTemplateInterface>,
        private readonly mailerService: MailerService,
        private readonly configService: ConfigService,
        private readonly logger: BLogger
    ) {}

    async sendEmail(params: SendEmailDto): Promise<any> {
        try {
            const silence = this.configService.get('SILENCE_OF_THE_LAMBS');
            if (!silence || silence === 'false') {
                const { email, subject, from, text, reply_to, html } = params;
                const result = await this.mailerService.sendMail({
                    to: email,
                    subject,
                    from,
                    replyTo: reply_to,
                    text,
                    html
                });
                return result;
            }
        } catch (error) {
            this.logger.error(ACTIONS.EMAIL_SERVICE, error);
        }
    }
}
