import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MessageTemplateModelModule } from '@shared/models/message-template.model';
import { UserModule } from '@shared/user/user.module';
import { EmailCommand } from './email.command';
import { EmailService } from './email.service';
@Module({
    imports: [
        MailerModule.forRootAsync({
            useFactory: (configService: ConfigService) => ({
                transport: {
                    host: configService.get('EMAIL_HOST'),
                    port: configService.get('EMAIL_PORT'),
                    auth: {
                        user: configService.get('EMAIL_AUTH_USER'),
                        pass: configService.get('EMAIL_AUTH_PASS')
                    }
                },
                defaults: {
                    from: `"No Reply" <${configService.get('EMAIL_AUTH_USER')}>`
                },
                preview: true
            }),
            inject: [ConfigService]
        }),
        UserModule,
        MessageTemplateModelModule
    ],
    providers: [EmailService, EmailCommand],
    exports: [EmailService, EmailCommand]
})
export class EmailModule {}
