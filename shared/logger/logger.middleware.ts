import { Injectable, NestMiddleware } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from './logger.service';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
    constructor(
        private readonly config: ConfigService,
        private readonly loggerService: BLogger
    ) {}

    use(req: any, _: any, next: () => void) {
        if (this.config.get('LOG_REQUESTS')) {
            this.loggerService.info(ACTIONS.HTTP_CALL, {
                headers: req.headers,
                params: req.formData,
                body: req.body,
                route: req.url
            });
        }
        next();
    }
}
