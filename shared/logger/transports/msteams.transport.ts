import Axios from 'axios';
import * as Transport from 'winston-transport';

export class MsteamsTransport extends Transport {
    private webhookUrl: string;

    constructor(opts) {
        super(opts);
        this.webhookUrl = opts.webhookUrl;
    }

    async log(info: any, next: () => void) {
        const { message, level, timestamp, ...ctx } = info;
        const json = JSON.stringify(ctx, null, 4);
        const text = `${timestamp}: <b>${level.toUpperCase()}
        </b> ${message} <br> <pre>${json}<pre>`;
        await Axios.post(this.webhookUrl, { text });
        next();
    }
}
