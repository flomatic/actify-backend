import { Global, Module } from '@nestjs/common';
import { LoggerMiddleware } from './logger.middleware';
import { BLogger, NestjsLogger } from './logger.service';
import { ElasticsearchModule } from '@shared/elasticsearch/elasticsearch.module';

@Global()
@Module({
    imports: [ElasticsearchModule],
    providers: [LoggerMiddleware, BLogger, NestjsLogger],
    exports: [LoggerMiddleware, BLogger]
})
export class LoggerModule {}
