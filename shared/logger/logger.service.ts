import { Injectable, LoggerService } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { MsteamsTransport } from '@shared/logger/transports/msteams.transport';
import * as winston from 'winston';
import { ElasticsearchTransport } from 'winston-elasticsearch';

@Injectable()
export class BLogger {
    protected logger: winston.Logger;

    constructor(
        private readonly config: ConfigService,
        private readonly esService: ElasticsearchService
    ) {
        const isDev = config.get('ENV') === 'development';

        const msTeamsTransport = new MsteamsTransport({
            level: 'error',
            webhookUrl: config.get('MSTEAMS_WEBHOOK'),
            format: winston.format.prettyPrint()
        });

        this.logger = winston.createLogger({
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.json()
            ),
            level: isDev ? 'debug' : 'info',
            transports: isDev
                ? [
                    new winston.transports.Console({
                        format: winston.format.simple()
                    })
                    // msTeamsTransport
                ]
                : [
                    new winston.transports.Console({
                        format: winston.format.simple()
                    }),
                    new ElasticsearchTransport({
                        level: 'info',
                        indexPrefix: 'actify',
                        client: esService,
                        elasticsearchVersion: 6
                    }),
                    msTeamsTransport
                ],
            exitOnError: false
        });
        this.logger.on('error', (error) => {
            console.error('Error caught', error);
        });
    }

    protected log(
        level: string,
        message: string,
        meta?: Record<string, unknown>
    ) {
        this.logger[level](message, { meta });
    }

    info(message: string, meta?: Record<string, unknown>) {
        this.logger.info(message, meta);
    }

    error(message: string, meta?: any) {
        if (meta instanceof Error) {
            meta = {
                message: meta.message,
                name: meta.name,
                stack: meta.stack
            };
        }
        this.logger.error(message, meta);
    }

    warn(message: any, meta?: Record<string, unknown>) {
        this.logger.warn(message, meta);
    }
}

export enum ACTIONS {
    HTTP_CALL = 'HTTP_CALL',
    REDIS_CONNECTION = 'REDIS_CONNECTION',
    ORIGIN_CHECK = 'ORIGIN_CHECK',
    WEB_SOCKET = 'WEB_SOCKET',
    READ_FROM_REDIS = 'READ_FROM_REDIS',
    DEVICE_INFO = 'DEVICE_INFO',
    EMAIL_SERVICE = 'EMAIL_SERVICE',
    CAMPAIGN_SERVICE = 'CAMPAIGN_SERVICE',
    JOURNEY_ENUM_SERVICE = 'JOURNEY_ENUM_SERVICE',
    JOURNEY_TEMPLATE_SERVICE = 'JOURNEY_TEMPLATE_SERVICE',
    JOURNEY_SERVICE = 'JOURNEY_SERVICE',
    RESET_MODIFIED_JOURNEY = 'RESET_MODIFIED_JOURNEY',
    USER_SEGMENTATION_SERVICE = 'USER_SEGMENTATION_SERVICE',
    CONNECTION_PROCESSOR_SERVICE = 'CONNECTION_PROCESSOR_SERVICE',
    DELAYED_JOB_SERVICE = 'DELAYED_JOB_SERVICE',
    SERVER_EVENT_PROCESSOR_SERVICE = 'SERVER_EVENT_PROCESSOR_SERVICE',
    SERVER_EVENT_SERVICE = 'SERVER_EVENT_SERVICE',
    USER_SERVICE = 'USER_SERVICE',
    SHORT_LINK = 'SHORT_LINK',
    SEND_SMS = 'SEND_SMS',
    REMOVE_SHADOW_CONNECTION = 'REMOVE_SHADOW_CONNECTION',
    REMOVE_SHADOW_CONNECTION_DONE = 'REMOVE_SHADOW_CONNECTION_DONE',
    SEND_PUSH_NOTIFICATION = 'SEND_PUSH_NOTIFICATION',
    CAMPAIGN_ERROR = 'CAMPAIGN_ERROR',
    UPDATE_SEGMENT_COUNT = 'UPDATE_SEGMENT_COUNT',
    GATHER_USER_EVENTS = 'GATHER_USER_EVENTS'
}

export enum LEVELS {
    ERROR = 'error',
    INFO = 'info'
}

@Injectable()
export class NestjsLogger implements LoggerService {
    constructor(private readonly logger: BLogger) {}

    error(message: any, trace?: string, context?: string): any {
        this.logger.error(message, { trace, context });
    }

    log(message: any, context?: string): any {
        this.logger.info(message, { context });
    }

    warn(message: any, context?: string): any {
        this.logger.warn(message, { context });
    }
}
