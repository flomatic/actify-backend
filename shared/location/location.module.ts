import { LocationService } from './location.service';
import { LocationSchema } from './schema/location.schema';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'location', schema: LocationSchema }
        ])
    ],
    providers: [LocationService],
    exports: [LocationService]
})
export class LocationModule {}
