import * as mongoose from 'mongoose';

export const LocationSchema = new mongoose.Schema({
    country: { type: String, index: true },
    cities: [
        {
            city: { type: String },
            time_zone: { type: String }
        }
    ]
});
