import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { LocationInterface } from './interface/location.interface';

@Injectable()
export class LocationService {
    constructor(
        @InjectModel('location')
        private readonly locationModel: Model<LocationInterface>
    ) {}

    async addIfNeeds(location: {
        country: string;
        city: string;
        time_zone: string;
    }) {
        let cityLocation = await this.locationModel.findOne({
            country: location.country
        });
        if (!cityLocation) {
            cityLocation = new this.locationModel({
                country: location.country,
                cities: [
                    {
                        city: location.city,
                        time_zone: location.time_zone
                    }
                ]
            });
        } else {
            const city = cityLocation.cities.find(
                (c) => c.city === location.city
            );
            if (!city) {
                cityLocation.cities.push({
                    city: location.city,
                    time_zone: location.time_zone
                });
            }
        }
        await cityLocation.save();
    }

    async getCountries() {
        return await this.locationModel.find({}, { country: 1 });
    }

    async getLocationByLocationId(id: Types.ObjectId) {
        return await this.locationModel.find({ _id: id });
    }

    async getLocationByCountryName(country: string) {
        return await this.locationModel.find({ country });
    }
}
