import { Document } from 'mongoose';

export interface LocationInterface extends Document {
    country: string;
    cities: Array<{
        city: string;
        time_zone: string;
    }>;
}
