import {
    ArgumentsHost,
    Catch,
    ExceptionFilter,
    HttpException
} from '@nestjs/common';
import { BLogger } from '@shared/logger/logger.service';
import { SentryService } from '@ntegral/nestjs-sentry';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    constructor(private logger: BLogger, private sentry: SentryService) {}

    catch(exception: Error /* , host: ArgumentsHost */): any {
        let status = null;
        if (exception instanceof HttpException) {
            status = exception.getStatus();
            if (status.toString()[0] !== '5') {
                return;
            }
        }
        this.sentry.instance().captureException(exception);
        this.logger.error(exception.message, { status });
    }
}
