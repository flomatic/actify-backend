import { Module } from '@nestjs/common';

import { IndraService } from './indra.service';

@Module({
    providers: [IndraService],
    exports: [IndraService]
})
export class IndraModule {}
