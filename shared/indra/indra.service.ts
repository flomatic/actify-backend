import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import { GetParams, Network } from '@shared/utils/network';

@Injectable()
export class IndraService {
    network: Network;
    constructor(
        private readonly configService: ConfigService,
        private readonly logger: BLogger
    ) {
        const indraURL: string = this.configService.get('INDRA_UPM');
        if (!indraURL) {
            throw new Error('indra url not provided');
        }
        this.network = new Network(indraURL);
    }

    async getIndraUserByUUK(uuk: string) {
        const getParams: GetParams = {
            params: {
                userUniqueNumber: +uuk
            }
        };
        try {
            const result = await this.network.get(
                '/api/v1/userprofile',
                getParams
            );
            return result.data.result;
        } catch (e) {
            this.logger.error(ACTIONS.HTTP_CALL, e);
        }
    }
}
