import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import axios from 'axios';
import * as lodash from 'lodash';

@Injectable()
export class ShortLinkService {
    providerId: string;
    constructor(
        private readonly logger: BLogger,
        private readonly configService: ConfigService
    ) {}

    async generate(link: string): Promise<any> {
        const data = {
            target: link,
            reuse: true
        };
        const headers = {
            'X-API-Key': this.configService.get('SHORT_LINK_API_KEY'),
            'Content-Type': 'application/json'
        };
        try {
            const response: any = await axios.post(
                `${this.configService.get('SHORT_LINK_URL')}/api/url/submit`,
                data,
                { headers }
            );
            return lodash.get(response, 'data.shortUrl');
        } catch (err) {
            this.logger.error(ACTIONS.SHORT_LINK, err);
        }
    }
}
