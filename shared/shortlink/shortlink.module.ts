import { Module } from '@nestjs/common';
import { ConfigModule } from '@shared/config.module';
import { ShortLinkService } from './shortlink.service';

@Module({
    imports: [ConfigModule],
    providers: [ShortLinkService],
    exports: [ShortLinkService]
})
export class ShortLinkModule {}
