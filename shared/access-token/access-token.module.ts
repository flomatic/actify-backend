import { Module } from '@nestjs/common';
import { AccessTokenService } from './access-token.service';

@Module({
    imports: [],
    providers: [AccessTokenService],
    exports: [AccessTokenService]
})
export class AccessTokenModule {}
