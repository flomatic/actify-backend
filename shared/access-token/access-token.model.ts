import { Prop, Schema } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

@Schema()
export class AccessTokenModel {
    @ApiProperty({})
    @Prop()
    key: string;

    @ApiProperty({})
    @Prop()
    value: string;

    @ApiProperty()
    @Prop()
    expire_date: Date;
}

export class GenerateTokenDto {
    @ApiProperty({
        description: 'organization api key'
    })
    api_key: string;
}
