import axios, { AxiosInstance } from 'axios';

export interface GetParams {
    headers?: any;
    params?: any;
}
export class Network {
    static DEFAULT_HEADERS = {};
    axiosInstance: AxiosInstance;

    constructor(
        baseURL: string,
        timeOut = 10000,
        headers: any = Network.DEFAULT_HEADERS
    ) {
        this.axiosInstance = axios.create({
            baseURL,
            timeout: timeOut,
            headers
        });
    }
    async get(url: string, getParams?: GetParams) {
        return await this.axiosInstance.get(url, getParams);
    }
}
