import { TimeOut } from '@entries/timeout/model/timeout';

export function getDateFromNow(time: {
    days?: number;
    hours?: number;
    minutes?: number;
    seconds?: number;
}) {
    const nowDate = new Date();
    return new Date(
        nowDate.getTime() +
            (time.days || 0) * 24 * 60 * 60 * 1000 +
            (time.hours || 0) * 60 * 60 * 1000 +
            (time.minutes || 0) * 60 * 1000
    );
}

export function getDateFromTimeOut(timeOut: TimeOut): Date {
    if (timeOut.type === 'iso_date_string') {
        return new Date(timeOut.value);
    } else if (timeOut.type === 'days') {
        return new Date(
            new Date().getTime() +
                (timeOut.value as number) * 24 * 60 * 60 * 1000
        );
    } else if (timeOut.type === 'hours') {
        return new Date(
            new Date().getTime() + (timeOut.value as number) * 60 * 60 * 1000
        );
    } else if (timeOut.type === 'minutes') {
        return new Date(
            new Date().getTime() + (timeOut.value as number) * 60 * 1000
        );
    } else if (timeOut.type === 'seconds') {
        return new Date(
            new Date().getTime() + (timeOut.value as number) * 1000
        );
    }
}
