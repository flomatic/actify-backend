import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as Bull from 'bull';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const Queue = require('bull');

interface RedisConfig {
    host: string;
    port: string;
    password?: string;
    enableReadyCheck?: boolean;
}
@Injectable()
export class BullService {
    queuesMap: Array<{ name: string; queue: Bull.Queue }> = [];
    constructor(private readonly configService: ConfigService) {}

    getQueue(queueName: string, ignorePrefix = false): Bull.Queue {
        const prefix = ignorePrefix
            ? ''
            : this.configService.get('HOSTNAME') || '';
        const queue: Bull.Queue = this.queuesMap.find(
            (q) => q.name === prefix + queueName
        )?.queue;
        if (!queue) {
            const redisHost = this.configService.get('REDIS_HOST');
            const redisPort = this.configService.get('REDIS_PORT');
            const redisProd = this.configService.get('REDIS_PROD');
            let redisConfig: RedisConfig = {
                host: redisHost,
                port: redisPort
            };
            if (redisProd === 'true') {
                const redisRWPort = this.configService.get('REDIS_RW_PORT');
                const redisPassword = this.configService.get('REDIS_PASSWORD');
                redisConfig = {
                    ...redisConfig,
                    port: redisRWPort,
                    password: redisPassword,
                    enableReadyCheck: false
                };
            }
            const createdQueue = new Queue(prefix + queueName, {
                redis: redisConfig,
                enableReadyCheck: false
            });
            this.queuesMap.push({
                name: prefix + queueName,
                queue: createdQueue
            });
            return createdQueue;
        } else {
            return queue;
        }
    }
    addJob(
        data: any,
        queueName: string,
        config?: {
            timeout?: number;
            exec_date?: Date;
        }
    ) {
        const queue: Bull.Queue = this.getQueue(queueName);
        if (queue) {
            queue.add(data, {
                timeout: config?.timeout || 60 * 60 * 1000,
                removeOnComplete: true,
                delay: config?.exec_date
                    ? config.exec_date.getTime() - new Date().getTime()
                    : 0,
                backoff: 4,
                removeOnFail: true
            } as Bull.JobOptions);
        }
    }

    defineProcess(queueName: string, job: (job: Bull.Job) => Promise<void>) {
        const queue: Bull.Queue = this.getQueue(queueName);
        queue.process(job);
    }
}
