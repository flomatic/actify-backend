import { Module } from '@nestjs/common';
import { BullService } from './bull.service';

@Module({
    providers: [BullService],
    exports: [BullService]
})
export class BullModule {}
