import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from '@shared/logger/logger.service';
import axios from 'axios';
import { SMSInterface } from './interface/sms.interface';
import lodash = require('lodash');

@Injectable()
export class SmsService {
    providerId: string;
    constructor(
        private readonly configService: ConfigService,
        private readonly logger: BLogger
    ) {}

    private async getProviderId() {
        if (!this.providerId) {
            const result = await axios.get(
                `${this.configService.get(
                    'SMS_SERVICE'
                )}/management/sms/providers`
            );
            this.providerId = lodash.get(result, 'data.result[0].id');
        }
        return this.providerId;
    }

    async sendSMS(message: SMSInterface): Promise<void> {
        if (
            !this.configService.get('SILENCE_OF_THE_LAMBS') ||
            this.configService.get('SILENCE_OF_THE_LAMBS') === 'false'
        ) {
            const providerId = await this.getProviderId();
            if (providerId) {
                const data = {
                    messages: [
                        {
                            text: message.body,
                            to: message.phone_number,
                            providerId
                        }
                    ]
                };
                await axios.post(
                    `${this.configService.get('SMS_SERVICE')}/sms/messages`,
                    data,
                    {
                        timeout: 3000
                    }
                );
            }
        }
        // if (this.configService.get('ENV') !== 'production') {
        this.logger.info(ACTIONS.SEND_SMS, {
            phone_number:
                '*********' +
                message.phone_number.substr(message.phone_number.length - 4),
            body: message.body
        });
        // }
    }
}
