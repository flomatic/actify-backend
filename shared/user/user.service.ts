import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BLogger } from '@shared/logger/logger.service';
import { UserInterface } from '@shared/models/user.model';
import * as _ from 'lodash';
import { Model, Types } from 'mongoose';

@Injectable()
export class UserService {
    constructor(
        private readonly logger: BLogger,
        @InjectModel('User') readonly userModel: Model<UserInterface>
    ) {}

    async upsertUser(user): Promise<any> {
        const query = {
            user_unique_key: user.user_unique_key,
            user_unique_value: user.user_unique_value
        };
        const update = {
            $set: {
                updated_at: new Date(),
                ..._.omit(user, 'create_date', 'email', 'phone_number')
            },
            $setOnInsert: {
                created_at: user.create_date,
                email: user.email,
                phone_number: user.phone_number
            }
        };
        const options = { upsert: true };
        await this.userModel.collection.updateOne(query, update, options);
    }

    async updateFireBaseToken(token: string, unique_device_id: string) {
        const user = await this.userModel.findOne({
            'devices.device_id': unique_device_id
        });
        if (user) {
            const index = user?.devices?.findIndex(
                (d) => d.device_id === unique_device_id
            );
            if (index && index != -1) {
                user!.devices[index]!.firebase_token = token;
                user.markModified('devices');
            }
            await user.save();
        }
    }

    async findUserById(userId: Types.ObjectId) {
        return this.userModel.findById(userId);
    }

    async deleteUser(params: {
        user_unique_key: string;
        user_unique_value: string;
    }) {
        return await this.userModel.deleteOne({
            user_unique_key: params.user_unique_key,
            user_unique_value: params.user_unique_value
        });
    }

    async updateUserLastActivity(userAttribute: {
        key: string;
        value: string;
    }) {
        const query = {
            user_unique_key: userAttribute.key,
            user_unique_value: userAttribute.value
        };
        const update = {
            $set: { last_activity: new Date() }
        };
        const options = { upsert: false };
        return this.userModel.collection.updateOne(query, update, options);
    }

    async updateAttr(params: {
        user_unique_key: string;
        user_unique_value: string;
        attrs: Array<{
            variable: string;
            value: string;
        }>;
    }) {
        const user = await this.getUserByKeyValue({
            uuk: params.user_unique_key,
            uuv: params.user_unique_value
        });
        console.log(params);
        if (user) {
            params.attrs?.forEach((attr) => {
                const _attr = user.user_attributes.find(
                    (a) => a.variable === attr.variable
                );
                if (!_attr) {
                    user.user_attributes.push(attr);
                } else {
                    const index = user.user_attributes.indexOf(_attr);
                    user.user_attributes[index].value = attr.value;
                }
            });
            user.markModified('user_attributes');
            await user.save();
        }
    }

    async getUsersWithPagination(userQuery: any, pageNumber = 1) {
        const pageSize = 100;
        return (
            (await this.userModel
                .find(userQuery)
                .limit(pageSize)
                .skip((pageNumber - 1) * pageSize)) || null
        );
    }

    async queyCount(userQuery: any): Promise<number> {
        return await this.userModel.find(userQuery).count();
    }

    async getUser(params: { email?: string; phone_number?: string }) {
        const user = await this.userModel.aggregate([
            {
                $match: {
                    $or: [
                        { email: params?.email },
                        { phone_number: params?.phone_number }
                    ]
                }
            }
        ]);
        return user?.[0] || {};
    }

    async getUserByKeyValue(params: { uuk: string; uuv: string }) {
        return await this.userModel.findOne({
            user_unique_key: params.uuk,
            user_unique_value: params.uuv
        });
    }
}
