import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { LocationUserInterface } from '@shared/models/user.model';
import { Model, Types } from 'mongoose';

@Injectable()
export class LocationUserService {
    constructor(
        @InjectModel('LocationUser')
        private readonly locationUserModel: Model<LocationUserInterface>
    ) {}

    async getUserLocation(id: Types.ObjectId) {
        return (
            await this.locationUserModel
                .find({ user_id: id })
                .sort({ hit: 1 })
                .limit(5)
                .lean()
        )?.[0]?.location;
    }

    async addLocationUser(params: {
        user_id: Types.ObjectId;
        location: {
            city: string;
            country: string;
            time_zone: string;
        };
    }) {
        await this.locationUserModel.collection.updateOne(
            {
                user_id: params.user_id,
                location: params.location
            },
            {
                $inc: { hit: 1 }
            },
            { upsert: true }
        );
    }
}
