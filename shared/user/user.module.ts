import { Module } from '@nestjs/common';
import { BullModule } from '@shared/bull/bull.module';
import { GeoModule } from '@shared/geo-ip/geo-module';
import { LocationModule } from '@shared/location/location.module';
import {
    LocationUserModelModule,
    UserModelModule
} from '@shared/models/user.model';
import { LocationUserService } from './location.user.service';
import { UserService } from './user.service';

@Module({
    imports: [
        GeoModule,
        LocationModule,
        UserModelModule,
        LocationUserModelModule,
        BullModule
    ],
    providers: [UserService, LocationUserService],
    exports: [UserService, LocationUserService]
})
export class UserModule {}
