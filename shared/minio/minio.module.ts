import { Client as MinioClient } from 'minio';
import { ConfigService } from '@nestjs/config';

export const MinioModule = {
    provide: MinioClient,
    useFactory(configService: ConfigService) {
        const config = {
            endPoint: configService.get('MINIO_ENDPOINT'),
            // port: +configService.get('MINIO_PORT') || undefined,
            useSSL: !!+configService.get('MINIO_SSL'),
            accessKey: configService.get('MINIO_ACCESS_KEY'),
            secretKey: configService.get('MINIO_SECRET_KEY')
        };
        return new MinioClient(config);
    },
    inject: [ConfigService]
};
