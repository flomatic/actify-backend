import { RateLimit } from '@entries/ratelimit/model/ratelimit';
import { MongooseModule, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Document, Types } from 'mongoose';

export class CampaignTimeSchedule {
    @ApiProperty({
        default: 'recurring',
        enum: ['recurring', 'one_time']
    })
    @Prop({ type: String, enum: ['recurring', 'one_time'] })
    type: CampaignTimeScheduleType;

    @ApiProperty({
        default: { type: 'day', number: '3', from: new Date() },
        enum: ['recurring', 'one_time']
    })
    @Prop({
        index: true,
        unique: true,
        type: {
            type: String,
            enum: ['day', 'week', 'month', 'year']
        },
        number: { type: Number },
        from: { type: Date }
    })
    every?: {
        type: 'day' | 'week' | 'month' | 'year';
        number?: number;
        from: Date;
    };

    @Prop({ type: Date })
    date: Date;
}

@Schema()
export class CampaignModel {
    @ApiProperty({ default: 'test_segment' })
    @Prop({ index: true, unique: true, type: String })
    name: string;

    @ApiProperty({
        default: 'sms_type',
        enum: ['sms_type', 'push_notification_type', 'email_type']
    })
    @Prop({
        enum: ['sms_type', 'push_notification_type', 'email_type'],
        default: 'sms_type',
        type: String
    })
    type: CampaignType;

    @ApiProperty({
        default: '5fc434154404c8d141e84669',
        type: String
    })
    @Prop({ type: Types.ObjectId })
    segmentation_id: Types.ObjectId;

    @ApiProperty()
    @Prop({ type: Date })
    time_out: Date;

    @ApiProperty({
        default: {
            in: 1,
            next: 'day'
        },
        description: 'day | hour'
    })
    @Prop({
        in: { type: Number, default: 1 },
        next: { type: String, default: 'hour' }
    })
    retry: {
        in: number;
        next: 'day' | 'hour';
    };

    @ApiProperty({
        default: '5f90e0a3c4b66ffd47fec426',
        type: String
    })
    @Prop({ type: Types.ObjectId })
    template_id: Types.ObjectId;

    @ApiProperty({
        default: 'normal_mode',
        description: 'normal_mode,test_mode'
    })
    @Prop({ type: String, enum: ['normal_mode', 'test_mode'] })
    mode: CampaignMode;

    @Prop({
        type: String,
        enum: ['running', 'scheduled', 'stopped'],
        default: 'scheduled'
    })
    status: CampaignStatus;

    @ApiProperty({
        type: CampaignTimeSchedule,
        description: 'campaign scheduling'
    })
    @Prop({ type: CampaignTimeSchedule })
    time_schedule: CampaignTimeSchedule;

    @ApiPropertyOptional({
        default: {
            email: 100,
            sms: 100,
            push_notification: 100
        },
        type: RateLimit,
        description: 'rate limit'
    })
    @Prop({ type: RateLimit })
    rate_limit?: RateLimit;

    @ApiPropertyOptional({
        default: false,
        type: Boolean,
        description: 'send notification without respect to DND'
    })
    @Prop({ type: Boolean })
    ignore_sleep_time?: boolean;

    @ApiProperty({
        default: false,
        type: Boolean,
        description: 'send notification without respect to rate limit'
    })
    @Prop({ type: Boolean })
    ignore_rate_limit?: boolean;

    @Prop({ type: Date })
    last_running_date: Date;

    @Prop({ type: Date })
    next_running_date: Date;

    @Prop({ type: Number, default: 0 })
    total_running_count: number;

    @ApiPropertyOptional({
        default: '5fc434154404c8d141e84669',
        type: String
    })
    @Prop({ type: Types.ObjectId })
    test_segmentation_id?: Types.ObjectId;
}
export type CampaignDocument = CampaignModel & Document;

export const CampaignSchema = SchemaFactory.createForClass(CampaignModel);

export enum CampaignStatus {
    RUNNING = 'running',
    SCHEDULED = 'scheduled',
    STOPPED = 'stopped'
}

export enum CampaignTimeScheduleType {
    ONE_TIME = 'one_time',
    RECURRING = 'recurring'
}

export enum CampaignType {
    SMS_TYPE = 'sms_type',
    EMAIL_TYPE = 'email_type',
    PUSH_NOTIFICATION_TYPE = 'push_notification_type'
}

export enum CampaignMode {
    NORMAL_MODE = 'normal_mode',
    TEST_MODE = 'test_mode'
}

export class CampaignPaginationParamsDTO {
    @ApiProperty({ type: 'string', required: false })
    name?: string;

    @ApiProperty({
        type: 'string',
        required: false,
        enum: [...Object.values(CampaignStatus)]
    })
    status?: CampaignStatus;

    @ApiProperty({
        type: 'string',
        required: false,
        enum: [...Object.values(CampaignTimeScheduleType)]
    })
    type?: CampaignTimeScheduleType;

    @ApiProperty({ type: 'number', example: 1 })
    page: number;

    @ApiProperty({ type: 'number', example: 10 })
    itemsPerPage: number;

    @ApiProperty({ type: 'string', required: false })
    sortBy?: string;

    @ApiProperty({ type: 'number', required: false, enum: [1, -1] })
    sortDesc?: number;
}

export interface SMSPayload {
    body: string;
}

export interface EMAILPayload {
    body: string;
}

export interface PushNotificationPayload {
    body: string;
}

export interface OperationByIdDto {
    id: string;
}

export const CampaignModelModule = MongooseModule.forFeature([
    { schema: CampaignSchema, name: 'Campaign' }
]);
