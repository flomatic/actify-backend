import { MongooseModule } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { Document } from 'mongoose';

export const UserEventSchema = new mongoose.Schema({
    user_unique_value: { type: String, index: true },
    session_id: { type: String, index: true },
    events: [
        {
            name: { type: String, index: true },
            body: { type: Object, index: true },
            time: { type: Date }
        }
    ],
    start_date: { type: Date },
    end_date: { type: Date },
    duration: { type: Number }
});

UserEventSchema.pre(
    'save',
    function (this: UserEventInterface, next: mongoose.HookNextFunction) {
        try {
            const newEndDate: Date = this.end_date || new Date();
            const duration = newEndDate.getTime() - this.start_date.getTime();
            this.duration = duration;
            return next();
        } catch (error) {
            return next(error);
        }
    }
);

export const UserEventModelModule = MongooseModule.forFeature([
    { schema: UserEventSchema, name: 'UserEvent' }
]);

export class UserEventDTO {
    @ApiProperty({ type: 'string' })
    id: string;

    @ApiProperty({ type: 'string' })
    email: string;

    @ApiProperty({ type: 'string', required: false })
    phone_number?: string;

    @ApiProperty({ type: 'string', required: false })
    start_date?: string;

    @ApiProperty({ type: 'string', required: false })
    end_date?: string;

    @ApiProperty({ type: 'number', required: false, example: 1 })
    per_page?: number;

    @ApiProperty({ type: 'number', required: false, example: 10 })
    page_number?: number;
}

export interface UserEventInterface extends Document {
    user_unique_value: string;
    user_unique_key: string;
    session_id: string;
    duration: number;
    events: [
        {
            name: string;
            body: any;
            time: Date;
        }?
    ];
    start_date: Date;
    end_date: Date;
}
