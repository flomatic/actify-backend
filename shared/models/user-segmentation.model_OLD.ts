import { Filter } from '@entries/filter/model/filter';
import { MongooseModule } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export const UserSegmentationSchema = new mongoose.Schema(
    {
        name: { type: String },
        description: { type: String },
        category: {
            type: String,
            enum: ['dynamic', 'static', 'system'],
            default: 'dynamic'
        },
        members: {
            type: { type: String, enum: ['all', 'custom'] },
            user: [
                {
                    key: String,
                    value: String
                }
            ]
        },
        user_attrs: [{ type: Object }],
        device_attrs: [{ type: Object }],
        criteria: [{ type: Object }],
        events: [{ type: Object }]
    },
    { timestamps: true }
);

export const UserSegmentationModelModule = MongooseModule.forFeature([
    { schema: UserSegmentationSchema, name: 'UserSegmentation' }
]);

export interface UserSegmentationInterface extends mongoose.Document {
    name: string;
    description: string;
    category: SegmentationCategory;
    members: MembersInterface;
    criteria: SegmentCriteriaInterface[];
    user_attrs: VariableValue[];
    device_attrs: VariableValue[];
    events: EventInterface[];
}

export enum SegmentationCategory {
    DYNAMIC = 'dynamic',
    STATIC = 'static',
    SYSTEM = 'system'
}

export interface VariableValue {
    variable: string;
    value: string;
}

export interface EventInterface {
    event_name: string;
    filters: [Filter];
    date?: DateType;
}

export interface SegmentationPaginationParamsDTO {
    name?: string;
    description?: string;
    category?: SegmentationCategory;
    page: number;
    itemsPerPage: number;
    sortBy?: string;
    sortDesc?: number;
}

export class UserSegmentationDTO {
    name: string;
    description?: string;
    category: SegmentationCategory;
    members: MembersInterface;
    user_attrs: VariableValue[];
    device_attrs: VariableValue[];
    criteria: SegmentCriteriaInterface[];
    events: EventInterface[];
}

export interface UserSegmentationTotalAffectedDTO {
    category: SegmentationCategory;
    members: MembersInterface;
    criteria: SegmentCriteriaInterface[];
    user_attrs: VariableValue[];
    device_attrs: VariableValue[];
    events: EventInterface[];
}

export class OperationByIDDTO {
    id: string;
}

// Members
export interface MembersInterface {
    type: MemberType;
    user: KeyValueInterface[];
}
export enum MemberType {
    ALL = 'all',
    CUSTOM = 'custom'
}
export interface KeyValueInterface {
    key: string;
    value: string;
}

// Criteria
export interface SegmentCriteriaInterface {
    key: number;
    operation:
        | {
              type: OperationType;
              value: ActivityLevel | DateType | Channel | Location;
          }
        | {
              operator: 'and' | 'or';
          };
}

export enum ActivityLevel {
    HIGH = 'high',
    MEDIUM = 'medium',
    LOW = 'low'
}

export class DateType {
    from: 'now' | 'iso_date_string' | 'year' | 'month' | 'day';
    from_value: any;
    to: 'now' | 'iso_date_string' | 'year' | 'month' | 'day';
    to_value: any;
}

export interface Location {
    country: string;
    city: string;
    time_zone: string;
}

export enum Channel {
    BROWSER = 'browser',
    ANDROID = 'android',
    IOS = 'ios'
}

export enum OperationType {
    ACTIVITY_LEVEL = 'activity_level',
    LAST_VISITED = 'last_visited',
    CHANNEL = 'channel',
    LOCATION = 'location',
    JOINED_DATE = 'joined_date'
}
