import { ApiProperty } from '@nestjs/swagger';

export interface Variable {
    type: VariableType;
    value: {
        variable: string;
    };
}

export enum VariableType {
    USER_ATTRIBUTE = 'userAttribute',
    USER_DEVICE_ATTRIBUTE = 'userDeviceAttribute',
    USER_INFO = 'userInfo',
    SYSTEM = 'system'
}

export enum VariableTypeLabel {
    USER_ATTRIBUTE = 'User Attribute',
    USER_DEVICE_ATTRIBUTE = 'User Device Attribute',
    USER_INFO = 'User Info',
    SYSTEM = 'System'
}

export class GetVariablesDTO {
    @ApiProperty({ enum: [...Object.values(VariableType)] })
    type: VariableType;
}
