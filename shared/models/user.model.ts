import { MongooseModule } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { Document, HookNextFunction, Schema, Types } from 'mongoose';

export const UserSchema = new Schema(
    {
        user_unique_value: { type: String, index: true },
        user_unique_key: { type: String, index: true },
        user_attributes: { type: Array, default: {} },
        email: { type: String, index: true },
        phone_number: { type: String, index: true },
        name: { type: String },
        location: {
            country: { type: String },
            city: { type: String },
            time_zone: { type: String }
        },
        sources: [
            {
                type: String,
                enum: ['push_notification', 'sms', 'email']
            }
        ],
        engagements: {
            type: Object,
            campaign: {
                been_in_campaigns: { type: Number, default: 0 },
                total_message_count: { type: Number, default: 0 }
            },
            journey: {
                been_in_journeys: { type: Number, default: 0 },
                total_message_count: { type: Number, default: 0 }
            },
            last_message_date: { type: Date }
        },
        average_session_time: {
            value: { type: Number, default: 0 },
            count: { type: Number, default: 0 }
        },
        average_revisit_time: {
            value: { type: Number, default: 0 },
            count: { type: Number, default: 0 }
        },
        retention_score: { type: Number, default: 0 },
        devices: [{ type: Object, default: {} }],
        last_activity: { type: Date, index: true },
        create_date: { type: Date },
        activity_level: {
            type: String,
            enum: ['high', 'medium', 'low'],
            default: 'medium'
        },
        referrer: [{ type: Object, default: {} }],
        created_at: { type: Date },
        updated_at: { type: Date }
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    }
);
UserSchema.index({ user_unique_value: 1, email: 1, phone_number: 1 });
UserSchema.index({
    user_unique_value: 1,
    user_attributes: 1,
    email: 1,
    phone_number: 1,
    location: 1,
    devices: 1,
    last_activity: 1,
    created_at: 1,
    updated_at: 1,
    activity_level: 1
});

UserSchema.post('find', function (users: UserInterface[]) {
    users?.forEach((user) => {
        if (user && !user.engagements) {
            user.engagements = {
                campaign: {
                    total_message_sent: 0,
                    been_in_campaigns: 0
                },
                journey: {
                    total_message_sent: 0,
                    been_in_journeys: 0
                },
                last_message_date: null
            };
        }
    });
});

UserSchema.post('findOne', function (user: UserInterface) {
    if (user && !user.engagements) {
        user.engagements = {
            campaign: {
                total_message_sent: 0,
                been_in_campaigns: 0
            },
            journey: {
                total_message_sent: 0,
                been_in_journeys: 0
            },
            last_message_date: null
        };
    }
});

UserSchema.pre('save', function (this: UserInterface, next: HookNextFunction) {
    try {
        const {
            average_session_time: { value: sessionTime },
            average_revisit_time: { value: revisitTime }
        } = this;
        if (sessionTime === 0 || revisitTime === 0) {
            this.retention_score = 0;
        } else {
            this.retention_score = Number(
                (sessionTime / revisitTime).toFixed(3)
            );
        }
        return next();
    } catch (error) {
        return next(error);
    }
});

export const LocationUserSchema = new mongoose.Schema(
    {
        user_id: { type: mongoose.Types.ObjectId },
        location: {
            city: String,
            country: String,
            time_zone: String
        },
        hit: { type: Number },
        last_connection_date: Date
    },
    { timestamps: true }
);

export interface User {
    user_unique_value: string;
    user_unique_key: string;
    email: string;
    phone_number: string;
    name: string;
    location: {
        city: string;
        country: string;
        time_zone: string;
    };
    sources: Array<string>;
    devices: DeviceInfoInterface[];
    engagements: {
        campaign: {
            been_in_campaigns: number;
            total_message_sent: number;
        };
        journey: {
            been_in_journeys: number;
            total_message_sent: number;
        };
        last_message_date: Date;
    };
    user_attributes: Array<{ variable: string; value: any }>;
    last_activity: Date;
    create_date: Date;
    activity_level: ActivityLevel;
    average_session_time: {
        value: number;
        count: number;
    };
    average_revisit_time: {
        value: number;
        count: number;
    };
    referrer: ReferrerInterface[];
    retention_score: number;
    created_at: Date;
    updated_at: Date;
}

export type UserInterface = User & Document;

export interface ReferrerInterface {
    name: string;
    count: number;
}

export interface DeviceInfoInterface {
    type: string;
    browser?: string;
    browser_version?: string;
    os: string;
    last_used_date: Date;
    device_id: string;
    brand?: string;
    model?: string;
    firebase_token: string;
    manufacturer?: string;
    os_version?: string;
    app_version?: string;
}

export interface LocationUserInterface extends Document {
    user_id: Types.ObjectId;
    location: {
        city: string;
        country: string;
        time_zone: string;
    };
    hit: number;
    last_connection_date: Date;
}

export interface UserAvailabilityInterface {
    is_online?: boolean;
    is_user?: boolean;
    is_new_user?: boolean;
    user_data?: any;
    session_id?: string;
    user?: {
        uuk: string;
        uuv: string;
    };
    user_agent?: string;
    channel?: string;
    firebase_token?: string;
    ip_address?: string;
    user_attr?: any;
    device_id: string;
    device_info?: string;
    connectionDate?: Date;
}

export enum ActivityLevel {
    HIGH = 'high',
    MEDIUM = 'medium',
    LOW = 'low'
}

export class GetUsersDTO {
    @ApiProperty({ type: 'number', example: 1 })
    page: number;

    @ApiProperty({ type: 'number', example: 10 })
    itemsPerPage: number;

    @ApiProperty({
        type: 'string',
        required: false,
        description: 'Could be unique identifier, phone number or email'
    })
    term?: string;

    @ApiProperty({
        type: 'string',
        description: 'last_visited for example',
        required: false
    })
    sortBy: string;

    @ApiProperty({
        type: 'number',
        enum: [1, -1],
        description: '(1: ascending), (-1: descending)',
        required: false
    })
    sortDesc: number;
}

export interface UsersInterface {
    [index: number]: UserInterface[];
    total: number;
}

export interface OverviewInterface {
    total: number;
    knownUsers: number;
}

export interface OperationByIdDto {
    id: string;
}

export const UserModelModule = MongooseModule.forFeature([
    { schema: UserSchema, name: 'User' }
]);
export const LocationUserModelModule = MongooseModule.forFeature([
    { schema: LocationUserSchema, name: 'LocationUser' }
]);
