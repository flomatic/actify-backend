import { MongooseModule } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Document } from 'mongoose';

export const SessionSchema = new mongoose.Schema({
    session_id: { type: String, index: true },
    user_unique_value: { type: String, index: true },
    user_unique_key: { type: String },
    ip_address: { type: String },
    start_date: { type: Date },
    end_date: { type: Date },
    channel: { type: String, enum: ['android', 'ios', 'browser'] },
    client_attributes: { type: Object },
    is_online: { type: Boolean }
});

export const SessionModelModule = MongooseModule.forFeature([
    { schema: SessionSchema, name: 'Session' }
]);

export interface SessionInterface extends Document {
    session_id: string;
    user_unique_value?: string;
    user_unique_key?: string;
    ip_address: string;
    start_date: Date;
    end_date: Date;
    channel: string;
    is_online: boolean;
    client_attributes?: any;
}
