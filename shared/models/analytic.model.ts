import { Filter } from '@entries/filter/model/filter';
import { MongooseModule } from '@nestjs/mongoose';
import { Document, Schema, Types } from 'mongoose';

export const AnalyticsSchema = new Schema(
    {
        group: { type: String },
        name: { type: String },
        type: { type: String, enum: ['flow', 'event', 'funnel'] },
        chart_type: {
            type: String,
            enum: [
                'linear_chart',
                'bar_item_chart',
                'bar_time_line_chart',
                'pie_chart',
                'funnel_chart'
            ]
        },
        input: { type: Object },
        user_segmentation_id: { type: String },
        updated_at: Date,
        created_at: Date
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

export interface AnalyticsInterface extends Document {
    group: string;
    name: string;
    type: AnalyticsTypes;
    chart_type: ChartType;
    user_segmentation_id?: string;
    input: EventInterface | FlowInterface | FunnelInterface;
    created_at: Date;
    updated_at: Date;
}

export enum AnalyticsTypes {
    FLOW = 'flow',
    EVENT = 'event',
    FUNNEL = 'funnel'
}

export enum ChartType {
    LINEAR = 'linear_chart',
    BAR_ITEM = 'bar_item_chart',
    BAR_TIME_LINE = 'bar_time_line_chart',
    PIE = 'pie_chart',
    FUNNEL = 'funnel_chart'
}

export interface EventInterface {
    event_name: string;
    event_filters: [Filter];
    result_type: EventResultTypes;
    keys: [{ key: string; name?: string }];
}

export interface CreateAnalyticsDto {
    group: string;
    name: string;
    type: AnalyticsTypes;
    // tslint:disable-next-line: variable-name
    user_segmentation_id?: string;
    // tslint:disable-next-line: variable-name
    chart_type: ChartType;
    meta?: any;
    input: EventInterface | FlowInterface | FunnelInterface;
}

export interface GetReportDto {
    // tslint:disable-next-line: variable-name
    from_date: Date;
    // tslint:disable-next-line: variable-name
    to_date: Date;
    // tslint:disable-next-line: variable-name
    analytic_id: string;
    format: ResponsePeriod[];
}

export enum ResponsePeriod {
    HOURLY = 'hourly',
    DAILY = 'daily',
    MONTHLY = 'monthly',
    YEARLY = 'yearly'
}

export enum EventResultTypes {
    NUMBER_OF_EVENTS = 'number_of_event',
    SUM_EVENT_VALUES = 'sum_event_values',
    ENUM_EVENT_VALUES = 'event_values'
}

export interface EventFilter {
    key: number;
    operation:
        | {
              variable: string;
              condition:
                  | 'equal'
                  | 'greater_or_equal'
                  | 'lower_or_equal'
                  | 'contains';
              value: string | number;
          }
        | {
              operator: 'and' | 'or';
          };
}

export interface FlowInterface {
    events: Array<{
        event_name: string;
        event_filters: [Filter];
    }>;
}

export interface FunnelInterface {
    events: Array<{
        name: string;
        event_name: string;
        event_filters: [Filter];
    }>;
}

export const ReportSchema = new Schema(
    {
        group: { type: String },
        analytic_id: { type: Types.ObjectId },
        type: { type: String, enum: ['flow', 'event', 'funnel'] },
        chart_type: {
            type: String,
            enum: [
                'bar_item_chart',
                'bar_time_line_chart',
                'pie_chart',
                'funnel_chart',
                'linear_chart'
            ]
        },
        time_period: {
            type: String,
            enum: ['hourly', 'daily', 'monthly', 'yearly']
        },
        data: [],
        from_date: { type: Date, index: true },
        to_date: { type: Date, index: true },
        updated_at: Date,
        created_at: Date
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

export interface ReportInterface extends Document {
    group: string;
    analytic_id: Types.ObjectId;
    type: 'flow' | 'event' | 'funnel';
    time_period: 'hourly' | 'daily' | 'monthly' | 'yearly';
    chart_type: ChartType;
    data: [ReportData];
    from_date: Date;
    to_date: Date;
    updated_at: Date;
    created_at: Date;
}

export interface ReportDto {
    // tslint:disable-next-line: variable-name
    analytic_id: string;
    type: string;
    // tslint:disable-next-line: variable-name
    analytic_name: string;
    // tslint:disable-next-line: variable-name
    chart_type: string;
    // tslint:disable-next-line: variable-name
    from_date: Date;
    // tslint:disable-next-line: variable-name
    to_date: Date;
    response: {
        hourly: ItemTimeStamp[];
        daily: ItemTimeStamp[];
        monthly: ItemTimeStamp[];
        yearly: ItemTimeStamp[];
    };
}

// tslint:disable-next-line: max-classes-per-file
export interface ItemTimeStamp {
    data: any;
    // tslint:disable-next-line: variable-name
    from_date: Date;
    // tslint:disable-next-line: variable-name
    to_date: Date;
}

export interface ReportData {
    name: string;
    value: any;
}

export const AnalyticsModeModule = MongooseModule.forFeature([
    { schema: AnalyticsSchema, name: 'Analytics' }
]);
export const AnalyticReportsModeModule = MongooseModule.forFeature([
    { schema: ReportSchema, name: 'Report' }
]);
