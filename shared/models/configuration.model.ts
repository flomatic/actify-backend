export interface Configuration {
    variables: {
        categories: VariableCategory[];
    };
    organization: Organization;
}

export interface VariableCategory {
    type: string;
    label: string;
}

// TODO: Move to the correct module
export interface Organization {
    name: {
        short?: string;
        complete?: string;
    };
    icon?: {
        url: string;
    };
}
