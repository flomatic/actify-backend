import { MongooseModule } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export const UserStatsSchema = new mongoose.Schema(
    {
        from_date: { type: Date, index: true },
        to_date: { type: Date, index: true },
        time_period: {
            type: String,
            enum: ['hourly', 'daily', 'monthly', 'yearly', 'total'],
            index: true
        },
        total_session: [
            {
                channel: { type: String, enum: ['browser', 'android', 'ios'] },
                value: { type: Number }
            }
        ],
        total_unique_visit: [
            {
                channel: { type: String, enum: ['browser', 'android', 'ios'] },
                value: { type: Number }
            }
        ],
        minimum_online_users: [
            {
                channel: { type: String, enum: ['browser', 'android', 'ios'] },
                value: { type: Number }
            }
        ],
        maximum_online_users: [
            {
                channel: { type: String, enum: ['browser', 'android', 'ios'] },
                value: { type: Number }
            }
        ],
        new_users: [
            {
                channel: { type: String, enum: ['browser', 'android', 'ios'] },
                value: { type: Number }
            }
        ]
    },
    { timestamps: true }
);

export interface UserStatsInterface extends mongoose.Document {
    from_date: Date;
    to_date: Date;
    time_period: TimePeriod;
    total_session: [ChannelValues];
    total_unique_visit: [ChannelValues];
    minimum_online_users: [ChannelValues];
    maximum_online_users: [ChannelValues];
    new_users: [ChannelValues];
}
export enum TimePeriod {
    HOURLY = 'hourly',
    DAILY = 'daily',
    MONTHLY = 'monthly',
    YEARLY = 'yearly',
    TOTAL = 'total'
}

export interface ChannelValues {
    channel: 'browser' | 'android' | 'ios';
    value: number;
}

export const UserStatsModelModule = MongooseModule.forFeature([
    { schema: UserStatsSchema, name: 'UserStats' }
]);
