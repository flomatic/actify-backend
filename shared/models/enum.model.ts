import { MongooseModule } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import * as mongoose from 'mongoose';

export const EnumSchema = new mongoose.Schema({
    name: { type: String, index: true },
    value: { type: String },
    source: { type: String, index: true },
    weight: { type: Number, default: 1 }
});

EnumSchema.index({ name: 1, value: 1 });

export interface EnumInterface extends mongoose.Document {
    name: string;
    value: string;
    source: string;
    weight: number;
}

export class EnumDTO {
    @ApiProperty({ type: 'string' })
    eventName: string;
}

export class EnumValuesDTO {
    @ApiProperty({ type: 'string' })
    eventName: string;

    @ApiProperty({ type: 'string' })
    enumName: string;
}

export const EnumModelModule = MongooseModule.forFeature([
    { schema: EnumSchema, name: 'Enum' }
]);
