import { MongooseModule } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import * as mongoose from 'mongoose';

export const JourneyAnalyticsSchema = new mongoose.Schema({
    journey_id: { type: mongoose.Types.ObjectId, index: true },
    analytics: [
        {
            step: { type: String },
            value: { type: Number, default: 0 }
        }
    ],
    info: {
        total_entries: Number,
        total_exits: Number,
        exist_in_journey: Number
    },
    kpi: [
        {
            name: { type: String },
            first_step_id: { type: String },
            second_step_id: { type: String }
        }
    ],
    kpi_result: [
        {
            name: { type: String },
            value: Number
        }
    ],
    updated_at: Date,
    created_at: Date
});

export interface JourneyAnalyticsInterface extends mongoose.Document {
    journey_id: mongoose.Types.ObjectId;
    analytics: [
        {
            step: string;
            value: number;
        }
    ];
    info: {
        total_entries: number;
        total_exits: number;
        exist_in_journey: number;
    };
    kpi: Array<{
        name: string;
        first_step_id: string;
        second_step_id: string;
    }>;
    kpi_result: Array<{
        name: string;
        value: number;
    }>;
    updated_at: Date;
    created_at: Date;
}

JourneyAnalyticsSchema.pre(
    'save',
    function (this: JourneyAnalyticsInterface, next) {
        this.kpi?.forEach((k) => {
            const firstId = k.first_step_id;
            const secondId = k.second_step_id;
            const firstValue = this.analytics?.find((a) => a.step === firstId)
                ?.value;
            const secondValue = this.analytics?.find((a) => a.step === secondId)
                ?.value;
            const percent = (secondValue / firstValue) * 100;
            if (!this.kpi_result) {
                this.kpi_result = [];
            }
            if (this.kpi_result?.find((kr) => kr.name === k.name)) {
                this.kpi_result.find(
                    (kr) => kr.name === k.name
                ).value = percent;
            } else {
                this.kpi_result.push({
                    name: k.name,
                    value: percent
                });
            }
        });
        const now = new Date();
        this.updated_at = now;
        if (!this.created_at) {
            this.created_at = now;
        }
        next();
    }
);

export class JourneyAnalyticsAddKpiDTO {
    @ApiProperty({ type: 'string' })
    name: string;

    @ApiProperty({ type: 'string' })
    first_step_id: string;

    @ApiProperty({ type: 'string' })
    second_step_id: string;
}
export class JourneyAnalyticsRemoveKpiDTO {
    @ApiProperty({ type: 'string' })
    name: string;
}

export const JourneyAnalyticModelModule = MongooseModule.forFeature([
    { schema: JourneyAnalyticsSchema, name: 'JourneyAnalytics' }
]);
