import { MongooseModule } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document, Schema } from 'mongoose';

export const MessageTemplateSchema = new Schema(
    {
        name: { type: String, unique: true, required: true },
        type: {
            type: String,
            enum: ['sms', 'email', 'push_notification'],
            required: true
        },
        tags: [{ type: String }], // TODO: use real values
        description: { type: String },
        variables: [{ type: Object }],
        data: { type: Object, required: true }
    },
    { timestamps: true }
);

export interface MessageTemplateInterface extends Document {
    name: string;
    type: MessageTemplateType;
    tags?: string[];
    description?: string;
    variables?: MessageTemplateVariable[];
    data: SMSTemplateData | EmailTemplateData | PushTemplateData;
}

export class MessageTemplateOperationDTO {
    @ApiProperty({
        description: 'Name of the template',
        default: 'Test template with Swagger'
    })
    name: string;
    @ApiProperty({
        description: 'Type of the template; could be sms, email or push',
        default: 'sms'
    })
    type: MessageTemplateType;
    @ApiProperty({ description: 'An array of tags' })
    tags?: string[];
    @ApiProperty({
        description: 'A description for the template',
        default: 'Test template'
    })
    description?: string;
    @ApiProperty({
        description: 'A list of all the required variables for this template',
        example: [{ name: 'name', fallback: 'customer' }]
    })
    variables?: MessageTemplateVariable[];
    @ApiProperty({
        description: 'Data of the template',
        default: {
            subject: 'Test template subject',
            body: 'Test template body'
        }
    })
    data: SMSTemplateData | EmailTemplateData | PushTemplateData;
}

export enum MessageTemplateType {
    SMS = 'sms',
    EMAIL = 'email',
    PUSH_NOTIFICATION = 'push_notification'
}

export interface SMSTemplateData {
    subject: string;
    body: string;
}

export interface EmailTemplateData {
    subject: string;
    from?: string;
    reply_to?: string;
    design: unknown;
    chunks: unknown;
    html: string;
}

export interface PushTemplateData {
    subject: string;
    body: string;
}
export interface MessageTemplateTypeDto {
    type: MessageTemplateType;
}

export interface MessageTemplateVariable {
    name: string;
    fallback?: string;
}

export class SendEmailDto {
    email: string;
    subject?: string;
    from?: string;
    reply_to?: string;
    text?: string;
    html: string;
}

export const MessageTemplateModelModule = MongooseModule.forFeature([
    { schema: MessageTemplateSchema, name: 'MessageTemplate' }
]);
