import { MongooseModule } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document, Schema } from 'mongoose';

export const SystemVariablesSchema = new Schema(
    {
        variable: { unique: true, type: String },
        value: { type: Schema.Types.Mixed },
        type: { type: String }
    },
    { timestamps: true }
);
export const SystemVariablesModelModule = MongooseModule.forFeature([
    { schema: SystemVariablesSchema, name: 'SystemVariables' }
]);

export class SystemVariableDTO extends Document {
    @ApiProperty({ type: 'string' })
    variable: string;

    @ApiProperty({ type: 'object' })
    value: any;

    @ApiProperty({ type: 'string' })
    type: string;
}
