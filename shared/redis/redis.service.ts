import redis = require('redis');
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ACTIONS, BLogger } from '../logger/logger.service';
import { isJsonString } from '../utils/stringUtils';

@Injectable()
export class RedisService {
    readClient: redis.RedisClient;
    readWriteClient: any;

    constructor(
        private readonly configService: ConfigService,
        private readonly loggerService: BLogger
    ) {
        const redisHost = this.configService.get('REDIS_HOST');
        const redisPort = this.configService.get('REDIS_PORT');
        const redisRWPort = this.configService.get('REDIS_RW_PORT');
        const redisPassword = this.configService.get('REDIS_PASSWORD');
        const redisProd = this.configService.get('REDIS_PROD');
        let readConfig = {};
        let readWriteConfig = {};
        if (redisProd === 'true') {
            readConfig = {
                host: redisHost,
                port: redisPort,
                password: redisPassword,
                no_ready_check: true
            };
            readWriteConfig = {
                host: redisHost,
                port: redisRWPort,
                password: redisPassword,
                no_ready_check: true
            };
        }
        this.readClient = redis.createClient(readConfig);
        this.readWriteClient = redis.createClient(readWriteConfig);

        this.readClient.on('error', (err) => {
            this.loggerService.error(ACTIONS.REDIS_CONNECTION, err);
        });
        this.readWriteClient.on('error', (err) => {
            this.loggerService.error(ACTIONS.REDIS_CONNECTION, err);
        });
    }

    getNewInstance(): redis.RedisClient {
        const redisHost = this.configService.get('REDIS_HOST');
        const redisPort = this.configService.get('REDIS_PORT');
        const redisProd = this.configService.get('REDIS_PROD');
        let redisConfig: any = {
            host: redisHost,
            port: redisPort
        };
        if (redisProd === 'true') {
            const redisPassword =
                this.configService.get('REDIS_PASSWORD') || undefined;
            redisConfig = {
                ...redisConfig,
                password: redisPassword,
                no_ready_check: true
            };
        }

        const client = redis.createClient(redisConfig);
        client.config('SET', 'notify-keyspace-events', 'Ex');

        client.on('error', (err) => {
            this.loggerService.error(ACTIONS.REDIS_CONNECTION, err);
        });
        return client;
    }

    getReadClient(): redis.RedisClient {
        return this.readClient;
    }

    getReadWriteClient(): redis.RedisClient {
        return this.readWriteClient;
    }

    async set(key: string, value: string | any): Promise<void> {
        if (typeof value !== 'string') {
            value = JSON.stringify(value);
        }
        return new Promise((resolve) => {
            this.readWriteClient.set(key, value.toString(), () => {
                resolve();
            });
        });
    }

    async clearByKey(key: string): Promise<boolean> {
        return this.readWriteClient.del(key);
    }

    async setWithTTL(key: string, value: string | any, timeInSeconds: number) {
        if (typeof value !== 'string') {
            value = JSON.stringify(value);
        }
        this.readWriteClient.set(key, value, 'EX', timeInSeconds);
    }

    async get(key: string): Promise<any> {
        return new Promise((resolve) => {
            this.readClient.get(key, (error, value) =>
                error
                    ? resolve(null)
                    : resolve(isJsonString(value) ? JSON.parse(value) : value)
            );
        });
    }

    async scanAsync(
        cursor: number,
        pattern: string,
        count: number
    ): Promise<{ new_cursor: number; keys: string[] }> {
        return new Promise((resolve, reject) => {
            this.readClient.scan(
                cursor.toString(),
                'MATCH',
                pattern,
                'COUNT',
                count.toString(),
                (error, reply) => {
                    if (!reply) {
                        resolve({
                            new_cursor: 0,
                            keys: []
                        });
                    } else {
                        const newCursor = reply[0];
                        const keys = reply[1];
                        resolve({
                            new_cursor: Number(newCursor),
                            keys
                        });
                    }
                }
            );
        });
    }

    checkHealth() {
        return new Promise((resolve, reject) => {
            this.getReadClient().send_command('ping', (err, response) => {
                if (response === 'PONG') {
                    resolve('ok');
                } else {
                    reject(err);
                }
            });
        });
    }
}
