import { Injectable, OnModuleInit } from '@nestjs/common';
import ip2loc = require('ip2location-nodejs');

@Injectable()
export class GeoService implements OnModuleInit {
    async getIpDetail(ipAddress: string) {
        return ip2loc.IP2Location_get_all(ipAddress);
    }

    onModuleInit(): any {
        ip2loc.IP2Location_init('./geo-ip-db/IP2LOCATION-LITE-DB11.BIN');
    }
}
