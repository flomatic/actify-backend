import { GeoService } from './geo-service';
import { Module } from '@nestjs/common';

@Module({
    providers: [GeoService],
    exports: [GeoService]
})
export class GeoModule {}
