import { CallApi } from '@entries/journey/model/call-api';
import { UserInteraction } from '@entries/journey/model/user-interaction';
import { Injectable } from '@nestjs/common';
import {
    MessageTemplateInterface,
    MessageTemplateType
} from '@shared/models/message-template.model';
import { User, UserInterface } from '@shared/models/user.model';
import { ShortLinkService } from '@shared/shortlink/shortlink.service';
import { MessageTemplateService } from 'apps/api/src/message-template/message-template.service';
import { SystemVariablesService } from 'apps/api/src/system-variables/system-variables.service';
import { JourneyJob } from 'apps/main-processor/src/journey/delayed-jobs/journey-job';
import {
    forEachAsync,
    getValueOfEvent
} from 'apps/main-processor/src/journey/helper';
import { Event } from 'apps/main-processor/src/socket-parser/interfaces/event';
import { Types } from 'mongoose';

@Injectable()
export class VariableReplaceHelper {
    systemVariables: any;
    constructor(
        private readonly messageTemplateService: MessageTemplateService,
        private readonly systemVariablesService: SystemVariablesService,
        private readonly shortLinkService: ShortLinkService
    ) {
        this.systemVariablesService.getAllSystemVariables().then((res) => {
            this.systemVariables = res;
        });
    }

    private generalVariables(body, user, job) {
        body = this.replaceSystemVariables(body);
        body = this.replaceUserVariables(body, user);
        if (job) {
            body = this.replaceJourneyVariables(body, job);
        }
        return body;
    }

    async getVariablesForTemplate(
        template_id: Types.ObjectId,
        user: UserInterface,
        event?: Event,
        job?: JourneyJob,
        userInteraction?: UserInteraction
    ) {
        const template = await this.messageTemplateService.findById(
            template_id
        );
        if (!template) {
            throw new Error('template not found');
        }
        let body = this.getInitBody(template);
        if (userInteraction) {
            template.variables?.forEach((v, i) => {
                const found = userInteraction.variables?.find(
                    (_v) => _v.name === v.name
                );
                if (found) {
                    const regex = new RegExp('\\${' + v.name + '}', 'gi');
                    body = body.replace(regex, '${' + found.variable + '}');
                }
            });
        }
        try {
            body = this.generalVariables(body, user, job);
            body = this.replaceEventVariables(body, event);
        } catch (e) {
            body = this.fallBackBody(template, body);
        }
        if (body === this.getInitBody(template)) {
            body = this.fallBackBody(template, body);
        }
        return body;
    }

    getInitBody(template: MessageTemplateInterface) {
        const body =
            template.type === MessageTemplateType.EMAIL
                ? (template as any).data.html
                : (template as any).data.body;
        return body;
    }

    fallBackBody(template, body: any): any {
        template.variables?.forEach((v) => {
            body = body
                .replace(v.name, v.fallback)
                .replace('${', '')
                .replace('}', '');
        });
        return body;
    }

    replaceCallApi(apiParams: CallApi, user: User, job?: JourneyJob): CallApi {
        const replacedUrl = this.generalVariables(apiParams.url, user, job);

        const replacedHeaders = apiParams.headers.map(
            async ({ key, value }) => {
                return {
                    key,
                    value: this.generalVariables(value, user, job)
                };
            }
        );
        const replacedQuery = apiParams.query.map(async ({ key, value }) => {
            return {
                key,
                value: this.generalVariables(value, user, job)
            };
        });
        const replacedData = apiParams.data.map(async ({ key, value }) => {
            return {
                key,
                value: this.generalVariables(value, user, job)
            };
        });
        const replacedParams: CallApi = { ...apiParams };
        Object.assign(replacedParams, {
            url: replacedUrl,
            headers: replacedHeaders,
            query: replacedQuery,
            data: replacedData
        });
        return replacedParams;
    }

    replaceJourneyVariables(body: any, job: JourneyJob) {
        if (!job) {
            return body;
        }
        const regex = /\${journey::(.*?)\}/g;

        const variables = body.match(regex);

        variables?.forEach((v) => {
            const variable = v.replace('${journey::', '').replace('}', '');
            const value = job.getVariable(variable, '');
            body = body.replace(
                new RegExp('\\${journey::' + variable + '}'),
                value
            );
        });
        return body;
    }

    replaceEventVariables(body: any, event: Event): Promise<any> {
        if (!event) {
            return body;
        }
        const regex = /\${event::(.*?)\}/g;

        const variables = body.match(regex);
        variables?.forEach((v) => {
            const key = v.replace('${event::', '').replace('}', '');
            const value = getValueOfEvent(key, event.body);
            body = body.replaceAll(v, value);
        });

        return body;
    }

    private replaceSystemVariables(body: string) {
        const regex = /\${system::(.*?)\}/g;
        const variables = body.match(regex);

        variables?.forEach((v) => {
            const key = v.replace('${system::', '').replace('}', '');
            const value = this.systemVariables?.find((s) => s.variable === key)
                ?.value;
            body = body.replace(v, value);
        });
        return body;
    }

    replaceUserVariables(body: string, user: UserInterface): string {
        const user_attr_regex = /\${userAttribute::(.*?)\}/g;
        const user_device_attr_regex = /\${userDeviceAttribute::(.*?)\}/g;
        const user_info_regex = /\${userInfo::(.*?)\}/g;
        const userAttrsVariables = body.match(user_attr_regex);
        const userInfoVariables = body.match(user_info_regex);
        const userDeviceAttrsVariables = body.match(user_device_attr_regex);
        userDeviceAttrsVariables?.forEach((v) => {
            const key = v
                .replace('${userDeviceAttribute::', '')
                .replace('}', '');
            const value = user.devices.find(
                (d) => d[key] != null && d[key] != undefined
            )?.[key];
            body = body.replace(v, value);
        });

        userAttrsVariables?.forEach((v) => {
            const key = v.replace('${userAttribute::', '').replace('}', '');
            const value = user.user_attributes.find(
                (d) => d[key] != null && d[key] != undefined
            )?.[key];
            body = body.replace(v, value);
        });

        userInfoVariables?.forEach((v) => {
            const key = v.replace('${userInfo::', '').replace('}', '');
            const value = user[key];
            body = body.replace(v, value);
        });
        return body;
    }

    async replaceLinkVariables(body: string, token: string) {
        const regex = /\${link::(.*?)\}/g;
        const variables = body.match(regex);
        await forEachAsync(variables, async (v) => {
            let link = v.replace('${link::', '').replace('}', '');
            link = link + '?tracking_id=' + token;
            const shortLink = await this.shortLinkService.generate(link);
            if (shortLink) {
                link = shortLink;
            }
            body = body.replace(v, link);
        });
        return body;
    }

    // replaceJourneyVariables(body: string, job: JourneyJob): string {
    //     const apiRegex = /\${filter_with_api_variables_(.*?)\}/g;
    //     const eventHappenedRegex = /\${filter_with_event_(.*?)\}/g;
    //     const waitForEventRegex = /\${wait_for_event_(.*?)\}/g;
    //     const apiVariables = body.match(apiRegex);
    //     const eventHappenedVariables = body.match(eventHappenedRegex);
    //     const waitForEventVariables = body.match(waitForEventRegex);

    //     apiVariables?.forEach((v) => {
    //         const variable = v
    //             .replace('${filter_with_api_variables_', '')
    //             .replace('}', '');
    //         const splitVariable = variable.split('::');
    //         const fieldId = splitVariable[0];
    //         const key = splitVariable[1];
    //         const variables = job.getVariablesForFieldId(fieldId);
    //         variables.forEach((_v) => {
    //             if (_v.key === key) {
    //                 body = body.replace(v, _v.value);
    //             }
    //         });
    //     });

    //     eventHappenedVariables?.forEach((v) => {
    //         const variable = v
    //             .replace('${filter_with_event_', '')
    //             .replace('}', '');
    //         const splitVariable = variable.split('::');
    //         const fieldId = splitVariable[0];
    //         const key = splitVariable[2];
    //         const event = job.getVariablesForFieldId(fieldId);
    //         const valueOfEvent = getValueOfEvent(key, event);
    //         body = body.replace(v, valueOfEvent);
    //     });

    //     waitForEventVariables?.forEach((v) => {
    //         const variable = v
    //             .replace('${wait_for_event_', '')
    //             .replace('}', '');
    //         const splitVariable = variable.split('::');
    //         const fieldId = splitVariable[0];
    //         const key = splitVariable[2];
    //         const event = job.getVariablesForFieldId(fieldId);
    //         const valueOfEvent = getValueOfEvent(key, event);
    //         body = body.replace(v, valueOfEvent);
    //     });

    //     return body;
    // }
}
