import { Module } from '@nestjs/common';
import { ShortLinkModule } from '@shared/shortlink/shortlink.module';
import { MessageTemplateModule } from 'apps/api/src/message-template/message-template.module';
import { SystemVariablesModule } from 'apps/api/src/system-variables/system-variables.module';
import { VariableReplaceHelper } from './variable-replace.helper';

@Module({
    imports: [ShortLinkModule, SystemVariablesModule, MessageTemplateModule],
    providers: [VariableReplaceHelper],
    exports: [VariableReplaceHelper]
})
export class VariableReplaceModule {}
