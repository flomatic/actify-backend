import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as admin from 'firebase-admin';
import * as path from 'path';
import { PushNotificationInterface } from './interface/interface.push-notification';
@Injectable()
export class PushNotificationService {
    app: admin.app.App;
    messaging: admin.messaging.Messaging;
    constructor(private readonly configService: ConfigService) {
        admin.initializeApp({
            credential: admin.credential.cert(
                path.join(process.cwd() + '/' + 'firebase-development.json')
            )
        });
        this.messaging = admin.messaging();
    }

    async sendPush(params: PushNotificationInterface): Promise<any> {
        if (
            !this.configService.get('SILENCE_OF_THE_LAMBS') ||
            this.configService.get('SILENCE_OF_THE_LAMBS') === 'false'
        ) {
            const payload = {
                notification: { title: params.subject, body: params.body }
                // this line produce error
                // data: params?.payload
            };
            return this.messaging.sendToDevice(params.to, payload);
        }
    }
}
