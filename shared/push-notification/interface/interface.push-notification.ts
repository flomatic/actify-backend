export interface PushNotificationInterface {
    to: string;
    subject: string;
    body: string;
    payload?: { [key: string]: string };
}
