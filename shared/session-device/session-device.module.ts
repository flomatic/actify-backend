import { SessionDeviceModeModule } from '@entries/session-device/session-device';
import { Module } from '@nestjs/common';
import { SessionDeviceService } from './session-device.service';

@Module({
    imports: [SessionDeviceModeModule],
    providers: [SessionDeviceService],
    exports: [SessionDeviceService]
})
export class SessionDeviceModule {}
