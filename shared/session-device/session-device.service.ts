import { SessionDeviceDocument } from '@entries/session-device/session-device';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class SessionDeviceService {
    constructor(
        @InjectModel('SessionDevice')
        private readonly sessionDeviceModel: Model<SessionDeviceDocument>
    ) {}

    async addSessionForDeviceId(deviceId: string, sessionId: string) {
        const sessionDevice = await this.sessionDeviceModel.findOne({
            device_id: deviceId
        });
        if (!sessionDevice) {
            return await new this.sessionDeviceModel({
                device_id: deviceId,
                sessions: [sessionId]
            }).save();
        } else {
            const hasCurrentSessionId = sessionDevice.sessions?.find(
                (s) => s === sessionId
            );
            if (!hasCurrentSessionId) {
                sessionDevice.sessions.push(sessionId);
                sessionDevice.markModified('sessions');
                await sessionDevice.save();
            }
        }
    }

    async findSessionsByDeviceId(deviceId: string) {
        return await this.sessionDeviceModel.findOne({ device_id: deviceId });
    }
}
