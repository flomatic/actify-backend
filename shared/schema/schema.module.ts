import { SchemaService } from './schema.service';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SchemaSchema } from './schema/schema.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Schema', schema: SchemaSchema }])
    ],
    providers: [SchemaService],
    exports: [SchemaService]
})
export class SchemaModule {}
