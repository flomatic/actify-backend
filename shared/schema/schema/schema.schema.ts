import * as mongoose from 'mongoose';

export const SchemaSchema = new mongoose.Schema({
    event_name: { type: String, index: true },
    fields: { type: Object }
});
