import * as mongoose from 'mongoose';

export enum FieldType {
    String = 'string',
    Number = 'number',
    ISODate = 'iso-date'
}

export interface SchemaInterface extends mongoose.Document {
    event_name: string;
    fields: any;
}
