import { Module } from '@nestjs/common';
import { SettingService } from './setting.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SettingSchema } from './schema/setting.schema';
import { SettingController } from 'apps/api/src/setting/setting.controller';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'setting', schema: SettingSchema }])
    ],
    controllers: [SettingController],
    providers: [SettingService],
    exports: [SettingService]
})
export class SettingModule {}
