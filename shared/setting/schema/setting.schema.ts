import * as mongoose from 'mongoose';

export const SettingSchema = new mongoose.Schema({
    name: { type: String, unique: true },
    value: Object
});
