import { ApiProperty } from '@nestjs/swagger';
import * as mongoose from 'mongoose';

export enum SETTING_NAME {
    USER_RATE_LIMIT = 'user_rate_limit',
    RATE_LIMIT = 'rate_limit',
    TIME_ZONE_OFFSET = 'time_zone_offset',
    PROVIDER_RATE_LIMIT = 'provider_rate_limit',
    DND_TIME = 'dnd_time'
}

export interface SettingInterface extends mongoose.Document {
    name: SETTING_NAME;
    value: any;
}

export class SettingsUpdateDTO {
    @ApiProperty({ enum: [...Object.values(SETTING_NAME)] })
    name: SETTING_NAME;

    @ApiProperty({ type: 'object' })
    value: any;
}
