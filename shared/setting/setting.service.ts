import { RateLimitPeriod } from '@entries/ratelimit/enum/ratelimit-period';
import { RateLimit } from '@entries/ratelimit/model/ratelimit';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
    SettingInterface,
    SettingsUpdateDTO,
    SETTING_NAME
} from './interface/setting.interface';

@Injectable()
export class SettingService {
    constructor(
        @InjectModel('setting')
        private readonly settingModel: Model<SettingInterface>
    ) {}

    async getSettings() {
        return await this.settingModel.find(
            {},
            { __v: 0, _id: 0, createdAt: 0, updatedAt: 0 }
        );
    }

    async getDNDTime(): Promise<{
        from_hour: number;
        to_hour: number;
    }> {
        const setting = await this.settingModel.findOne({
            name: SETTING_NAME.DND_TIME
        });
        return {
            from_hour: setting?.value?.from_hour || 24,
            to_hour: setting?.value?.to_hour || 8
        };
    }

    async updateASetting(
        payload: SettingsUpdateDTO
    ): Promise<SettingInterface> {
        const updatedSetting = await this.settingModel.findOneAndUpdate(
            {
                name: payload.name
            },
            payload,
            { upsert: true, new: true }
        );
        if (updatedSetting) {
            return updatedSetting;
        } else {
            throw new BadRequestException('Cannot find setting record');
        }
    }

    async getTimeZoneOffset() {
        const timezoneOffset = await this.settingModel.findOne({
            name: SETTING_NAME.TIME_ZONE_OFFSET
        });
        if (!timezoneOffset) {
            return -3.5 * 60;
        } else {
            return timezoneOffset.value;
        }
    }

    async getGlobalRateLimit(): Promise<RateLimit> {
        const rateLimit = await this.settingModel.findOne({
            name: SETTING_NAME.RATE_LIMIT
        });
        if (rateLimit) {
            return {
                push_notification: rateLimit.value.push_notification,
                email: rateLimit.value.email,
                sms: rateLimit.value.sms,
                per: rateLimit.value.per || RateLimitPeriod.MINUTE
            };
        } else {
            return {
                push_notification: 5,
                email: 5,
                sms: 5,
                per: RateLimitPeriod.MINUTE
            };
        }
    }
    async getUserRateLimit(): Promise<RateLimit> {
        const rateLimit = await this.settingModel.findOne({
            name: SETTING_NAME.USER_RATE_LIMIT
        });
        if (rateLimit) {
            return {
                push_notification: rateLimit.value.push_notification,
                email: rateLimit.value.email,
                sms: rateLimit.value.sms,
                per: rateLimit.value.per || RateLimitPeriod.MINUTE
            };
        } else {
            return {
                push_notification: 5,
                email: 5,
                sms: 5,
                per: RateLimitPeriod.MINUTE
            };
        }
    }
}
