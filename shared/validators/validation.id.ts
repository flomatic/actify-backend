import { Types } from 'mongoose';

export const IdValidation = (id: string): boolean => {
    return Types.ObjectId.isValid(id);
};
