import { Module } from '@nestjs/common';
import { EnumModelModule } from '@shared/models/enum.model';
import { SchemaModule } from '../schema/schema.module';
import { EnumService } from './enum.service';

@Module({
    imports: [EnumModelModule, SchemaModule],
    providers: [EnumService],
    exports: [EnumService]
})
export class EnumModule {}
