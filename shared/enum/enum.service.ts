import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { EnumInterface } from '@shared/models/enum.model';
import { Event } from 'apps/main-processor/src/socket-parser/interfaces/event';
import { Model } from 'mongoose';
import { SchemaInterface } from './../schema/interface/interface.Schema';
import { SchemaService } from './../schema/schema.service';

@Injectable()
export class EnumService {
    constructor(
        @InjectModel('Enum') private readonly enumModel: Model<EnumInterface>,
        private readonly schemaService: SchemaService
    ) {}

    async addEnums(event: Event) {
        const schema: SchemaInterface = await this.schemaService.findSchemaByName(
            event.event_name
        );
        if (schema) {
            const keyValues: Array<{
                key: string;
                value: string;
            }> = this.keyValues(schema, event.body);
            Promise.all(
                keyValues.map(async (keyValue) => {
                    await this.enumModel.updateOne(
                        {
                            name: keyValue.key,
                            source: 'event',
                            value: keyValue.value
                        },
                        {
                            $inc: { weight: 1 },
                            $setOnInsert: {
                                name: keyValue.key,
                                value: keyValue.value,
                                source: 'event'
                            }
                        },
                        { upsert: true }
                    );
                })
            );
        }
    }

    async addUserAttrsEnums(
        values: Array<{ variable: string; value: string }>
    ) {
        const schema: SchemaInterface = await this.schemaService.findSchemaByName(
            'user_attributes'
        );
        if (schema) {
            Promise.all(
                values.map(async (keyValue) => {
                    await this.enumModel.updateOne(
                        {
                            name: keyValue.variable,
                            source: 'user_attributes',
                            value: keyValue.value
                        },
                        {
                            $inc: { weight: 1 },
                            $setOnInsert: {
                                name: keyValue.variable,
                                value: keyValue.value
                            }
                        },
                        { upsert: true }
                    );
                })
            );
        }
    }

    async addUserDeviceEnums(deviceInfo: {
        type: string;
        browser: any;
        browser_version: any;
        os: any;
        os_version: any;
        last_used_date: Date;
        firebase_token: string;
        device_id: string;
        brand: any;
        model: any;
        manufacturer: any;
        app_version: string;
    }) {
        const schema: SchemaInterface = await this.schemaService.findSchemaByName(
            'user_device_attributes'
        );
        if (schema) {
            for (const info in deviceInfo) {
                const key = info;
                const value = deviceInfo[key];
                await this.enumModel.updateOne(
                    {
                        name: key,
                        source: 'user_device_attributes',
                        value: value
                    },
                    {
                        $inc: { weight: 1 },
                        $setOnInsert: {
                            name: key,
                            source: 'user_device_attributes',
                            value: value
                        }
                    },
                    { upsert: true }
                );
            }
        }
    }

    async getUserDeviceValues(variable: string) {
        return await this.enumModel.find({
            name: variable,
            source: 'user_device_attributes'
        });
    }

    async getUserAttributes(variable: string) {
        return await this.enumModel.find({
            name: variable,
            source: 'user_attributes'
        });
    }

    private keyValues(schema, object) {
        const keyValues: any[] = [];
        const ignoreCases = ['date-time', 'date', 'time', 'utc-millisec'];
        circularFunction(schema.event_name, schema.fields, object);
        function circularFunction(cKey, cSchema, cObject) {
            const typeOfObject = cSchema?.type;
            if (typeOfObject === 'object') {
                // tslint:disable-next-line: forin
                for (const key in cSchema.properties) {
                    if (cObject[key]) {
                        circularFunction(
                            cKey + '.' + key,
                            cSchema.properties[key],
                            cObject[key]
                        );
                    }
                }
            } else {
                if (
                    cSchema?.type === 'string' &&
                    !ignoreCases.includes(cSchema?.format)
                ) {
                    keyValues.push({ key: cKey, value: cObject });
                }
            }
        }
        return keyValues;
    }
}
