export class DateRange {
    from: 'now' | 'iso_date_string' | 'year' | 'month' | 'day';
    from_value: any;
    to: 'now' | 'iso_date_string' | 'year' | 'month' | 'day';
    to_value: any;
}
