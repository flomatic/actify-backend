import * as bcrypt from 'bcrypt';
import * as mongoose from 'mongoose';
import { MongooseModule, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ timestamps: true })
export class ClientModel {
    @Prop({ type: String, unique: true })
    email: string;

    @Prop({ type: String })
    password: boolean;

    @Prop({ type: Boolean, default: false })
    admin: string;
}

export type ClientDocument = ClientModel & Document;
export const ClientSchema = SchemaFactory.createForClass(ClientModel);

export const ClientModelModule = MongooseModule.forFeature([
    { schema: ClientSchema, name: 'Client' }
]);

ClientSchema.pre('save', async function (next: mongoose.HookNextFunction) {
    try {
        if (!this.isModified('password')) {
            return next();
        }

        const hashed = await bcrypt.hash(this['password'], 10);
        this['password'] = hashed;

        return next();
    } catch (err) {
        return next(err);
    }
});
