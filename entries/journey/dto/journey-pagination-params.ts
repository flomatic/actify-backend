import { ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';
import { JourneyStatus } from '../enum/journey-status';

export class JourneyPaginationParamsDTO {
    @ApiProperty({ type: 'number', required: false, default: 1 })
    page?: number;

    @ApiProperty({ type: 'number', required: false, default: 10 })
    itemsPerPage?: number;

    @ApiProperty({ type: 'string', required: false })
    name?: string;

    @ApiProperty({
        name: 'status',
        type: 'string',
        required: false,
        enum: [...Object.values(JourneyStatus)]
    })
    status?: JourneyStatus;

    @ApiProperty({ type: 'string', required: false })
    sortBy?: string;

    @ApiProperty({ type: 'number', required: false, enum: [1, -1] })
    sortDesc?: number;

    @ApiProperty({
        type: Types.ObjectId,
        required: false,
        description: 'Tag id'
    })
    tag?: Types.ObjectId;
}
