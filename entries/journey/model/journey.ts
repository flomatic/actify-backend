import { RateLimit } from '@entries/ratelimit/model/ratelimit';
import { MongooseModule, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { TagModel } from '@entries/tag/model/tag.model';
import { Document, Types } from 'mongoose';
import { JourneyStatus } from '../enum/journey-status';
import { JourneyField } from './journey-field';
@Schema()
export class Journey {
    @ApiProperty({
        description: 'Name of the journey',
        type: 'string',
        example: 'Test Journey - Swagger'
    })
    @Prop({ type: String, unique: true })
    name: string;

    @ApiProperty({
        description: 'Description of the journey',
        type: 'string',
        example: 'Description - Swagger'
    })
    @Prop({ type: String })
    description: string;

    @ApiProperty({
        description: 'A list tag ids related to this journey',
        type: Array
    })
    @Prop({ type: Types.ObjectId, ref: 'Tag' })
    tags: TagModel[];

    @ApiProperty({
        description: 'Status of the journey',
        enum: [...Object.values(JourneyStatus)],
        example: JourneyStatus.STOPPED
    })
    @Prop({ type: String })
    status: JourneyStatus;

    @ApiProperty({
        description: 'The required fields',
        isArray: true,
        type: JourneyField
    })
    @Prop({ type: [Object] })
    fields: Array<JourneyField>;

    @ApiProperty({
        description:
            'Meta data for this journey. This is used the preserve graph positions',
        type: 'object'
    })
    @Prop({ type: Object })
    meta: any;

    @ApiPropertyOptional({
        default: {
            email: 100,
            sms: 100,
            push_notification: 100
        },
        type: RateLimit,
        description: 'rate limit'
    })
    @Prop({ type: RateLimit })
    rate_limit?: RateLimit;

    @ApiPropertyOptional({
        default: false,
        type: Boolean,
        description: 'ignore do not disturb'
    })
    @Prop({ type: Boolean, default: false })
    ignore_dnd?: boolean;

    @ApiPropertyOptional({
        default: false,
        type: Boolean,
        description: 'ignore global rate limit'
    })
    @Prop({ type: Boolean, default: false })
    ignore_rate_limit?: boolean;

    @Prop({ type: Boolean, default: false })
    isLocked: boolean;
}
export type JourneyDocument = Journey & Document;

export const JourneySchema = SchemaFactory.createForClass(Journey);

export const JourneyModelModule = MongooseModule.forFeature([
    { schema: JourneySchema, name: 'Journey' }
]);
