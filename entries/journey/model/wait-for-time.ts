import { TimeOut } from '@entries/timeout/model/timeout';
import { ApiProperty } from '@nestjs/swagger';

export class WaitForTime {
    @ApiProperty({ description: 'timeout', type: TimeOut })
    time_out: TimeOut;
}
