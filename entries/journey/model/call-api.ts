import { HttpMethod } from '@entries/httpmethod/enum/httpmethod';
import { KeyValue } from '@entries/keyvalue/model/keyvalue';
import { KeyVariable } from '@entries/keyvariable/model/keyvariable';
import { ApiProperty } from '@nestjs/swagger';

export class CallApi {
    @ApiProperty({ description: 'method to call', type: HttpMethod })
    method: HttpMethod;

    @ApiProperty({ description: 'url', type: String })
    url: string;

    @ApiProperty({ description: 'headers', type: [KeyValue] })
    headers: Array<KeyValue>;

    @ApiProperty({ description: 'headers', type: [KeyValue] })
    query: Array<KeyValue>;

    @ApiProperty({ description: 'data', type: [KeyValue] })
    data: Array<KeyValue>;

    @ApiProperty({
        description: 'mapping responses to variables',
        type: [KeyVariable]
    })
    variables: Array<KeyVariable>;
}
