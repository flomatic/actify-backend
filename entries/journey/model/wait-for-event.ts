import { Filter } from '@entries/filter/model/filter';
import { TimeOut } from '@entries/timeout/model/timeout';
import { ApiProperty } from '@nestjs/swagger';

export class WaitForEvent {
    @ApiProperty({ description: 'name of the event', type: String })
    event_name: string;
    @ApiProperty({ description: 'name of happening to trigger', type: Number })
    number_of_happening: number;
    @ApiProperty({ description: 'event filter', type: [Filter] })
    filters: [Filter];
    @ApiProperty({ description: 'timeout', type: TimeOut })
    time_out: TimeOut;
}
