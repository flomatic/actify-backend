import { JourneyField } from './journey-field';

export class OperationCallback {
    state: 'paused' | 'passed';
    has_next_level: boolean;
    current_id: string;
    destFields: JourneyField[];
    time_out?: Date;
    status?: string;
}
