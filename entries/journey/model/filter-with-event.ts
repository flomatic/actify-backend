import { DateRange } from '@entries/daterange/model/daterange';
import { Filter } from '@entries/filter/model/filter';
import { ApiProperty } from '@nestjs/swagger';

export class FilterWithEvent {
    @ApiProperty({ description: 'name of the event', type: String })
    event_name: string;
    @ApiProperty({ description: 'name of happening to trigger', type: Number })
    number_of_happening: number;
    @ApiProperty({ description: 'event filter', type: [Filter] })
    filters: [Filter];
    @ApiProperty({ description: 'event filter', type: [DateRange] })
    date: DateRange;
}
