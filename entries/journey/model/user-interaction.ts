import { KeyValue } from '@entries/keyvalue/model/keyvalue';
import { NameVariable } from '@entries/namevariable/model/name-variable';
import { ApiProperty } from '@nestjs/swagger';

export class UserInteraction {
    @ApiProperty({ description: 'template id', type: String })
    template_id: string;
    @ApiProperty({ description: 'values', type: [KeyValue] })
    variables: Array<NameVariable>;
}
