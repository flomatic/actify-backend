import { Filter } from '@entries/filter/model/filter';
import { ApiProperty } from '@nestjs/swagger';

export class FilterWithApiVariables {
    @ApiProperty({ description: 'method to call', type: [Filter] })
    filters: [Filter];
}
