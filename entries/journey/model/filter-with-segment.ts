import { ApiProperty } from '@nestjs/swagger';

export class FilterWithSegment {
    @ApiProperty({ description: 'user segmentation id', type: String })
    id: string;
}
