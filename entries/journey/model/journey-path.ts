import { ApiProperty } from '@nestjs/swagger';

export class JourneyPath {
    @ApiProperty({
        description: "name of the current journey field node example: 'success'",
        type: 'string'
    })
    from_node: string;
    @ApiProperty({ description: 'id of the destination journey field' })
    to_id: string;
    @ApiProperty({
        description: "name of the destination journey field node. it's always 'default'",
        type: 'string'
    })
    to_node: string;
}
