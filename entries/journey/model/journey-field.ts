import { ApiProperty } from '@nestjs/swagger';
import { JourneyFieldAction } from '../enum/journey-field-action';
import { CallApi } from './call-api';
import { FilterWithApiVariables } from './filter-with-api-vars';
import { FilterWithEvent } from './filter-with-event';
import { FilterWithSegment } from './filter-with-segment';
import { JourneyPath } from './journey-path';
import { UserInteraction } from './user-interaction';
import { WaitForEvent } from './wait-for-event';
import { WaitForTime } from './wait-for-time';

export class JourneyField {
    id: string;
    @ApiProperty({
        description: 'name of the field',
        enum: { ...Object.values(JourneyFieldAction) },
        default: JourneyFieldAction.GETTING_OFFLINE
    })
    name: JourneyFieldAction;

    @ApiProperty({ description: 'path of journey field', type: JourneyPath })
    path: [JourneyPath];
    @ApiProperty({
        description: 'inputs of current journey field',
        type: [
            FilterWithEvent,
            WaitForEvent,
            WaitForTime,
            UserInteraction,
            CallApi,
            FilterWithApiVariables,
            FilterWithSegment
        ]
    })
    inputs:
        | FilterWithEvent
        | WaitForEvent
        | WaitForTime
        | UserInteraction
        | CallApi
        | FilterWithApiVariables
        | FilterWithSegment;
}
