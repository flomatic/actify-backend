import { KeyValue } from '@entries/keyvalue/model/keyvalue';
import { User } from '@shared/models/user.model';
import { Journey } from './journey';

export class JourneyJobModel {
    user_id: string;
    user: User;
    journey: Journey;
    journey_id: string;
    current_field_ids: Array<string>;
    track_variables: Array<string>;
    variables: Array<KeyValue>;
    event_body: any;
    timeout: Date;
}
