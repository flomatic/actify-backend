export enum JourneyStatus {
    RUNNING = 'running',
    STOPPED = 'stopped',
    TEST = 'test'
}
