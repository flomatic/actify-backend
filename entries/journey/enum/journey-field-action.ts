export enum JourneyFieldAction {
    GETTING_ONLINE = 'getting_online',
    GETTING_OFFLINE = 'getting_offline',
    FILTER_WITH_USER_SEGMENTATION = 'filter_with_user_segmentation',
    FILTER_WITH_EVENT = 'filter_with_event',
    WAIT_FOR_EVENT = 'wait_for_event',
    WAIT_FOR_TIME = 'wait_for_time',
    USER_INTERACTION = 'user_interaction',
    CALL_API = 'call_api',
    FILTER_WITH_API_VARIABLES = 'filter_with_api_variables',
    EXIT = 'exit'
}
