import { DateRange } from '@entries/daterange/model/daterange';
import { Filter } from '@entries/filter/model/filter';
import { LogicalConjunction } from '@entries/operation/enum/logical-conjunction';

export class EventFilter {
    event_name: string;
    number_of_happening: number;
    filters: [Filter];
    operator: LogicalConjunction;
    date?: DateRange;
}
