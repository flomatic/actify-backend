export enum Channel {
    BROWSER = 'browser',
    ANDROID = 'android',
    IOS = 'ios'
}
