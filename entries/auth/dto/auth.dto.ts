import { ApiProperty } from '@nestjs/swagger';

export class LoginDTO {
    @ApiProperty({ description: 'User email address' })
    email: string;
    @ApiProperty({ description: 'User password' })
    password: string;
}

export class RegisterDTO {
    @ApiProperty({ description: 'User email address' })
    email: string;
    @ApiProperty({ description: 'User password' })
    password: string;
}

export class DeleteDTO {
    @ApiProperty({ description: 'The id of the user to delete' })
    id: string;
}

export class ChangePasswordDTO {
    @ApiProperty({ description: 'User email address' })
    email: string;
    @ApiProperty({ description: 'Current password' })
    oldPassword: string;
    @ApiProperty({ description: 'New password' })
    newPassword: string;
}
