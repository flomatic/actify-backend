import { ApiProperty } from '@nestjs/swagger';

export class NameVariable {
    @ApiProperty({ type: String })
    name: string;
    @ApiProperty({ type: Object })
    variable: any;
}
