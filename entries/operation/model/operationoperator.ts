import { ApiProperty } from '@nestjs/swagger';
import { LogicalConjunction } from '../enum/logical-conjunction';

export class OperationOperator {
    @ApiProperty({ description: 'name of the variable', type: String })
    operator: LogicalConjunction;
}
