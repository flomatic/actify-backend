import { ApiProperty } from '@nestjs/swagger';
import { Condition } from '../enum/condition';
import { OperationType } from '../enum/operation-type';

export class Operation {
    @ApiProperty({ description: 'name of the variable', type: String })
    variable: string;
    @ApiProperty({ description: 'condition of the variable', enum: Condition })
    condition: Condition;
    @ApiProperty({ description: 'value of the variable', enum: Condition })
    value: string | number;
    @ApiProperty({ description: 'type of the operation', enum: OperationType })
    type: OperationType;
}
