export enum LogicalConjunction {
    AND = 'and',
    OR = 'or'
}
