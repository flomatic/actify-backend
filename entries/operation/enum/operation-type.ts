export enum OperationType {
    ENUM = 'enum',
    NUMBER = 'number',
    ARRAY_NUMBER = 'array_number',
    ARRAY_ENUM = 'array_enum',
    DATE_TIME = 'date_time',
    BOOLEAN = 'boolean'
}
