export enum Condition {
    EQUAL = 'equal',
    GREATER_OR_EQUAL = 'greater_or_equal',
    LOWER_OR_EQUAL = 'lower_or_equal',
    GREATER = 'greater',
    LOWER = 'lower',
    CONTAINS = 'contains',
    NOT_EQUAL = 'not_equal',
    ONE_OF = 'one_of',
    NONE_OF = 'none_of'
}
