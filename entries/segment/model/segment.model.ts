import { EventFilter } from '@entries/event-filter/model/event-filter';
import { Filter } from '@entries/filter/model/filter';
import { KeyValue } from '@entries/keyvalue/model/keyvalue';
import { VariableValue } from '@entries/variablevalue/model/variablevalue';
import { MongooseModule, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { SegmentCategory } from '../enum/segment-category';
import { SegmentMembersType } from '../enum/segment-member-type';
import { UserCalculateStatus } from '../enum/segment-user-count-status.enum';
import { SegmentCriteria } from '../interface/segment-criteria.interface';

export class SegmentMembers {
    @ApiProperty({ type: String, enum: [...Object.values(SegmentMembersType)] })
    @Prop({ type: String })
    type: SegmentMembersType;

    @ApiProperty({
        type: [KeyValue],
        example: { key: 'email', value: 'alihvs@gmail.com' }
    })
    @Prop({ type: [KeyValue] })
    users: Array<KeyValue>;
}
export class UserCount {
    @Prop({ type: Number, default: -1 })
    value: number;

    @Prop({ type: String, default: UserCalculateStatus.CALCULATING })
    status: UserCalculateStatus;

    @Prop({ type: Number })
    last_calculation_duration?: number;
}
@Schema({ timestamps: true })
export class Segment {
    @Prop({ type: String })
    @ApiProperty({ type: String, example: 'All Users' })
    name: string;

    @Prop({ type: String })
    @ApiProperty({ type: String, example: 'A segment containing all users' })
    description: string;

    @Prop({ type: String })
    @ApiProperty({
        type: SegmentCategory,
        enum: [...Object.values(SegmentCategory)]
    })
    category: SegmentCategory;

    @Prop({ type: SegmentMembers })
    @ApiProperty({ type: SegmentMembers })
    members: SegmentMembers;

    @Prop({ type: [Object] })
    @ApiProperty({ type: [Object] })
    user_attrs: Array<Filter>;

    @Prop({ type: [Object] })
    @ApiProperty({ type: [Object] })
    device_attrs: Array<Filter>;

    @Prop({ type: [Object] })
    @ApiProperty({ type: [Object] })
    criteria: Array<SegmentCriteria>;

    @Prop({ type: [Object] })
    @ApiProperty({ type: [Object] })
    events: Array<EventFilter>;

    @Prop({ type: UserCount, default: {} })
    user_count: UserCount;
}

export type SegmentDocument = Segment & Document;

export const SegmentSchema = SchemaFactory.createForClass(Segment);

export const SegmentModelModule = MongooseModule.forFeature([
    { schema: SegmentSchema, name: 'Segment' }
]);
