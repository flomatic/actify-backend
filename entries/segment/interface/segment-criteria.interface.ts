import { ActivityLevel } from '@entries/activity-level/enum/activity-level';
import { Channel } from '@entries/channel/enum/channel';
import { DateRange } from '@entries/daterange/model/daterange';
import { SegmentOperationType } from '../enum/segment-operation-type';

export interface Location {
    country: string;
    city: string;
    time_zone: string;
}

export class SegmentCriteria {
    key: number;
    operation:
        | {
              type: SegmentOperationType;
              value: ActivityLevel | DateRange | Channel | Location;
          }
        | {
              operator: 'and' | 'or';
          };
}
