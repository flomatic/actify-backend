import { SegmentCategory } from '../enum/segment-category';

export interface SegmentPaginationParamsDTO {
    name?: string;
    description?: string;
    category?: SegmentCategory;
    page: number;
    itemsPerPage: number;
    sortBy?: string;
    sortDesc?: number;
}
