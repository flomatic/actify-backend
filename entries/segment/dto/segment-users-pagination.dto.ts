import { ApiProperty } from '@nestjs/swagger';

export class SegmentUsersPaginationDTO {
    @ApiProperty({ type: 'number', required: false, default: 1 })
    page?: number;

    @ApiProperty({ type: 'number', required: false, default: 10 })
    itemsPerPage?: number;
}
