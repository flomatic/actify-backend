export enum SegmentOperationType {
    ACTIVITY_LEVEL = 'activity_level',
    LAST_VISITED = 'last_visited',
    CHANNEL = 'channel',
    LOCATION = 'location',
    JOINED_DATE = 'joined_date'
}
