export enum SegmentCategory {
    DYNAMIC = 'dynamic',
    STATIC = 'static',
    SYSTEM = 'system'
}
