export enum UserCalculateStatus {
    CALCULATING = 'calculating',
    READY = 'ready'
}
