import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { RateLimitPeriod } from '../enum/ratelimit-period';

export class RateLimit {
    @ApiProperty({ default: 'hour', enum: Object.values(RateLimitPeriod) })
    @Prop({ type: String, default: 'hour' })
    per: RateLimitPeriod;
    @ApiProperty({ default: 100 })
    @Prop({ type: Number })
    email: number;
    @ApiProperty({ default: 100 })
    @Prop({ type: Number })
    push_notification: number;
    @ApiProperty({ default: 100 })
    @Prop({ type: Number })
    sms: number;
}
