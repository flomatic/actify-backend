export enum RateLimitPeriod {
    HOUR = 'hour',
    MINUTE = 'minute',
    DAY = 'day'
}
