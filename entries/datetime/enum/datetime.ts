export enum DateTime {
    ISO_DATE_STRING = 'iso_date_string',
    DAYS = 'days',
    HOURS = 'hours',
    WEEKS = 'weeks',
    YEARS = 'years',
    MONTHS = 'months',
    MINUTES = 'minutes'
}
