import { ApiProperty } from '@nestjs/swagger';

export class KeyValue {
    @ApiProperty({ type: String })
    key: string;
    @ApiProperty({ type: Object })
    value: any;
}
