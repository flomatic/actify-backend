import { JourneyFieldAction } from '@entries/journey/enum/journey-field-action';
import { JourneyBlockCategory } from '../enum/journey-block-category.enum';
import { JourneyPortEnum } from '../enum/journey-block-port.enum';

export const JourneyBlockTemplates = [
    {
        name: JourneyFieldAction.GETTING_ONLINE,
        title: 'Getting Online',
        description: {
            short: 'Select the channel',
            full:
                'This block filters the users who get online from a certain channel.'
        },
        category: JourneyBlockCategory.TRIGGER,
        check_in_streams: {
            connection: true,
            event: false
        },
        path_support: {
            in: [],
            out: [
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.CONDITION,
                JourneyBlockCategory.FLOW_CONTROL
            ]
        },
        ports: {
            in: [],
            out: [JourneyPortEnum.DEFAULT]
        }
    },
    {
        name: JourneyFieldAction.GETTING_OFFLINE,
        title: 'Getting Offline',
        description: {
            short: 'Select the channel',
            full:
                'This block filters the users who go offline after' +
                'registering a session from a certain channel.'
        },
        check_in_streams: {
            connection: true,
            event: false
        },
        path_support: {
            in: [],
            out: [
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.CONDITION,
                JourneyBlockCategory.FLOW_CONTROL
            ]
        },
        ports: {
            in: [],
            out: [JourneyPortEnum.DEFAULT]
        },
        category: JourneyBlockCategory.TRIGGER
    },
    {
        name: JourneyFieldAction.USER_INTERACTION,
        title: 'User Interaction',
        description: {
            short: 'Select a template',
            full:
                'Select a message template and fill in the required variables.'
        },
        path_support: {
            in: [
                JourneyBlockCategory.TRIGGER,
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ],
            out: [
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ]
        },
        check_in_streams: {
            connection: false,
            event: false
        },
        ports: {
            in: [JourneyPortEnum.DEFAULT],
            out: [JourneyPortEnum.SMS_SENT, JourneyPortEnum.SMS_FAILED]
        },
        category: JourneyBlockCategory.ACTION
    },
    {
        name: JourneyFieldAction.CALL_API,
        title: 'Call API',
        description: {
            short: 'Enter data',
            full:
                'This node calls an endpoint and stores the response in a global variable.'
        },
        path_support: {
            in: [
                JourneyBlockCategory.TRIGGER,
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ],
            out: [
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ]
        },
        check_in_streams: {
            connection: false,
            event: false
        },
        ports: {
            in: [JourneyPortEnum.DEFAULT],
            out: [JourneyPortEnum.API_SUCCESS, JourneyPortEnum.API_FAILED]
        },
        category: JourneyBlockCategory.ACTION
    },
    {
        name: JourneyFieldAction.FILTER_WITH_API_VARIABLES,
        title: 'Filter API Variables',
        description: {
            short: 'Select a variable',
            full: 'Check if a previously set variable meets a certain criteria.'
        },
        path_support: {
            in: [
                JourneyBlockCategory.TRIGGER,
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ],
            out: [
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ]
        },
        check_in_streams: {
            connection: false,
            event: false
        },
        ports: {
            in: [JourneyPortEnum.DEFAULT],
            out: [JourneyPortEnum.VARIABLE_TRUE, JourneyPortEnum.VARIABLE_FALSE]
        },
        category: JourneyBlockCategory.CONDITION
    },
    {
        name: JourneyFieldAction.FILTER_WITH_USER_SEGMENTATION,
        title: 'Filter with Segment',
        description: {
            short: 'Filter with user segmentation',
            full: 'Filter with user segmentation.'
        },
        path_support: {
            in: [
                JourneyBlockCategory.TRIGGER,
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ],
            out: [
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ]
        },
        check_in_streams: {
            connection: false,
            event: false
        },
        ports: {
            in: [JourneyPortEnum.DEFAULT],
            out: [JourneyPortEnum.VARIABLE_TRUE, JourneyPortEnum.VARIABLE_FALSE]
        },
        category: JourneyBlockCategory.CONDITION
    },
    {
        name: JourneyFieldAction.FILTER_WITH_EVENT,
        title: 'Filter with Event',
        description: {
            short: 'Select an event',
            full:
                'This node will fire after a series of filters' +
                'and occurrences of a specific event meet the criteria.'
        },
        path_support: {
            in: [
                JourneyBlockCategory.TRIGGER,
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ],
            out: [
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ]
        },
        check_in_streams: {
            connection: false,
            event: false
        },
        ports: {
            in: [JourneyPortEnum.DEFAULT],
            out: [
                JourneyPortEnum.EVENT_HAPPENED,
                JourneyPortEnum.EVENT_NOT_HAPPENED
            ]
        },
        category: JourneyBlockCategory.CONDITION
    },
    {
        name: JourneyFieldAction.WAIT_FOR_EVENT,
        title: 'Wait for Event',
        description: {
            short: 'Select an event',
            full: 'Waits for the occupance of a specific event and its filters'
        },
        path_support: {
            in: [
                JourneyBlockCategory.TRIGGER,
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ],
            out: [
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ]
        },
        check_in_streams: {
            connection: false,
            event: true
        },
        ports: {
            in: [JourneyPortEnum.DEFAULT],
            out: [
                JourneyPortEnum.EVENT_HAPPENED,
                JourneyPortEnum.EVENT_NOT_HAPPENED
            ]
        },
        category: JourneyBlockCategory.FLOW_CONTROL
    },
    {
        name: JourneyFieldAction.WAIT_FOR_TIME,
        title: 'Wait for Time',
        description: {
            short: 'Enter a value',
            full: 'A general purpose timeout.'
        },
        path_support: {
            in: [
                JourneyBlockCategory.TRIGGER,
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ],
            out: [
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL
            ]
        },
        check_in_streams: {
            connection: false,
            event: false
        },
        ports: {
            in: [JourneyPortEnum.DEFAULT],
            out: [JourneyPortEnum.DEFAULT]
        },
        category: JourneyBlockCategory.FLOW_CONTROL
    },
    {
        name: JourneyFieldAction.EXIT,
        title: 'Exit',
        description: {
            short: 'Exit the journey',
            full: 'Exit a journey and finish it for the current user.'
        },
        path_support: {
            in: [
                JourneyBlockCategory.TRIGGER,
                JourneyBlockCategory.ACTION,
                JourneyBlockCategory.FLOW_CONTROL,
                JourneyBlockCategory.CONDITION
            ],
            out: []
        },
        check_in_streams: {
            connection: false,
            event: false
        },
        ports: {
            in: [JourneyPortEnum.DEFAULT],
            out: []
        },
        category: JourneyBlockCategory.FLOW_CONTROL
    }
];
