import { ApiProperty } from '@nestjs/swagger';
import { JourneyBlockTemplateCheckInStreamsDTO } from '../dto/journey-block-checkin-streams.dto';
import { JourneyBlockTemplateDescriptionDTO } from '../dto/journey-block-description.dto';
import { JourneyBlockTemplatePathSupportDTO } from '../dto/journey-block-path-support.dto';
import { JourneyBlockTemplatePortsDTO } from '../dto/journey-block-ports.dto';
import { JourneyBlockCategory } from '../enum/journey-block-category.enum';

export class JourneyBlockTemplateInterface {
    @ApiProperty({ type: String })
    name: string;

    @ApiProperty({ type: String })
    title: string;

    @ApiProperty({ type: JourneyBlockTemplateDescriptionDTO })
    description: JourneyBlockTemplateDescriptionDTO;

    @ApiProperty({ type: String, enum: Object.values(JourneyBlockCategory) })
    category: JourneyBlockCategory;

    @ApiProperty({ type: JourneyBlockTemplateCheckInStreamsDTO })
    check_in_streams: JourneyBlockTemplateCheckInStreamsDTO;

    @ApiProperty({ type: JourneyBlockTemplatePathSupportDTO })
    path_support: JourneyBlockTemplatePathSupportDTO;

    @ApiProperty({ type: JourneyBlockTemplatePortsDTO })
    ports: JourneyBlockTemplatePortsDTO;
}
