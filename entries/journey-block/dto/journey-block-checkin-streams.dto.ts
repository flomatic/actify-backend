import { ApiProperty } from '@nestjs/swagger';

export class JourneyBlockTemplateCheckInStreamsDTO {
    @ApiProperty({ type: Boolean })
    connection: boolean;
    @ApiProperty({ type: Boolean })
    event: boolean;
}
