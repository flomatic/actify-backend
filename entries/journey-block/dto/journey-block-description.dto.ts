import { ApiProperty } from '@nestjs/swagger';

export class JourneyBlockTemplateDescriptionDTO {
    @ApiProperty({ type: String })
    short: string;
    @ApiProperty({ type: String })
    full: string;
}
