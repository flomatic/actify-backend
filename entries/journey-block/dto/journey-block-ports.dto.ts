import { ApiProperty } from '@nestjs/swagger';
import { JourneyPortEnum } from '../enum/journey-block-port.enum';

export class JourneyBlockTemplatePortsDTO {
    @ApiProperty({ example: [], enum: Object.values(JourneyPortEnum) })
    in: JourneyPortEnum[];
    @ApiProperty({ example: [], enum: Object.values(JourneyPortEnum) })
    out: JourneyPortEnum[];
}
