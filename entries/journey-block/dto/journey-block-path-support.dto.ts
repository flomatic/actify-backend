import { ApiProperty } from '@nestjs/swagger';
import { JourneyBlockCategory } from '../enum/journey-block-category.enum';

export class JourneyBlockTemplatePathSupportDTO {
    @ApiProperty({ example: [], enum: Object.values(JourneyBlockCategory) })
    in: JourneyBlockCategory[];
    @ApiProperty({ example: [], enum: Object.values(JourneyBlockCategory) })
    out: JourneyBlockCategory[];
}
