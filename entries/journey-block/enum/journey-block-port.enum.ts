export enum JourneyPortEnum {
    DEFAULT = 'default',
    SMS_SENT = 'sent',
    SMS_FAILED = 'failed',
    API_SUCCESS = 'success',
    API_FAILED = 'failed',
    EVENT_HAPPENED = 'happened',
    EVENT_NOT_HAPPENED = 'not_happened',
    VARIABLE_TRUE = 'true',
    VARIABLE_FALSE = 'false'
}
