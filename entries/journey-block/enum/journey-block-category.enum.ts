export enum JourneyBlockCategory {
    ACTION = 'action',
    CONDITION = 'condition',
    FLOW_CONTROL = 'flow_control',
    TRIGGER = 'trigger'
}
