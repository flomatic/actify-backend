import { MongooseModule, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class SessionDevice {
    @Prop({ type: String, index: true, unique: true })
    device_id: string;
    @Prop({ type: [String] })
    sessions: Array<string>;
}

export type SessionDeviceDocument = SessionDevice & Document;

export const SessionDeviceSchema = SchemaFactory.createForClass(SessionDevice);

export const SessionDeviceModeModule = MongooseModule.forFeature([
    { schema: SessionDeviceSchema, name: 'SessionDevice' }
]);
