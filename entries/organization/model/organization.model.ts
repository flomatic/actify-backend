import { MongooseModule, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class OrganizationModel {
    @Prop({ type: String, unique: true })
    name: string;

    @Prop({ type: Boolean, default: true })
    enable: boolean;

    @Prop({ type: String })
    secret: string;
}

export type OrganizationDocument = OrganizationModel & Document;
export const OrganizationSchema = SchemaFactory.createForClass(
    OrganizationModel
);

export const OrganizationModelModule = MongooseModule.forFeature([
    { schema: OrganizationSchema, name: 'Organization' }
]);

export interface OrganizationInterface extends Document {
    name: string;
    enable: boolean;
    secret: string;
}
