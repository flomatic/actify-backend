import { ApiProperty } from '@nestjs/swagger';

export class KeyVariable {
    @ApiProperty({ type: String })
    key: string;
    @ApiProperty({ type: String })
    variable: string;
}
