import { ApiProperty } from '@nestjs/swagger';

export class RootPingResponseDTO {
    @ApiProperty({ description: 'A dummy boolean value. Always returns true' })
    works: boolean;
}
