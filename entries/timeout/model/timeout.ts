export class TimeOut {
    type: 'iso_date_string' | 'days' | 'hours' | 'minutes' | 'seconds';
    value: number | string;
}
