import { ApiProperty } from '@nestjs/swagger';

export class OperationByIdDTO {
    @ApiProperty({ type: 'string' })
    id: string;
}
