import { MongooseModule, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class TagModel {
    @Prop({ type: String, unique: true, required: true })
    name: string;

    @Prop({ type: String, required: false })
    description?: string;
}

export type TagDocument = TagModel & Document;
export const TagSchema = SchemaFactory.createForClass(TagModel);

export const TagModelModule = MongooseModule.forFeature([
    { schema: TagSchema, name: 'Tag' }
]);
