import { ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';

export class AddTagDTO {
    @ApiProperty({ description: 'Name of the tag', example: 'sample_tag' })
    name: string;
    @ApiProperty({
        description: 'A description to summarize the role of this tag.',
        example: 'sample description'
    })
    description?: string;
}

export class AddTagResponseDTO {
    @ApiProperty({ type: String })
    _id: Types.ObjectId;
    @ApiProperty()
    name: string;
    @ApiProperty()
    description?: string;
}

export class EditTagDTO {
    @ApiProperty({ type: String })
    name?: string;
    @ApiProperty({ type: String })
    description?: string;
}
