import { Operation } from '@entries/operation/model/operation';
import { OperationOperator } from '@entries/operation/model/operationoperator';
import { ApiProperty } from '@nestjs/swagger';

export class Filter {
    @ApiProperty({ description: 'must be incremental', type: Number })
    key: number;
    @ApiProperty({
        description: 'operation or operation operator',
        type: [Operation, OperationOperator]
    })
    operation: Operation | OperationOperator;
}
