export enum ActivityLevel {
    HIGH = 'high',
    MEDIUM = 'medium',
    LOW = 'low'
}
